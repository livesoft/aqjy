package henu.bean;

import java.io.Serializable;

public class Company extends Bean {
	private int comid;
	private int belongid;
	private String companyname;
	private String companytype;
	private String address;
	private String legalperson;
	private String businesslicense;
	private String organizationcode;
	private String contactphone;
	private String isexistcompany;
	private String createtime;
	private String businessrange;
	private String organizecard;
	private String contactaddress;
	private String registmoney;
	private String registeroffice;
	private String businesslimit;
	private String allowbusinesscard;
	private String postcode;
	private String basicaccoutopen;
	private String basicaccoutid;
	private String companyprincipal;
	private String memo1;
	private String memo2;
	private String memo3;
	private String memo4;
	public int getComid() {
		return comid;
	}
	public void setComid(int comid) {
		this.comid = comid;
	}
	public int getBelongid() {
		return belongid;
	}
	public void setBelongid(int belongid) {
		this.belongid = belongid;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	public String getCompanytype() {
		return companytype;
	}
	public void setCompanytype(String companytype) {
		this.companytype = companytype;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLegalperson() {
		return legalperson;
	}
	public void setLegalperson(String legalperson) {
		this.legalperson = legalperson;
	}
	public String getBusinesslicense() {
		return businesslicense;
	}
	public void setBusinesslicense(String businesslicense) {
		this.businesslicense = businesslicense;
	}
	public String getOrganizationcode() {
		return organizationcode;
	}
	public void setOrganizationcode(String organizationcode) {
		this.organizationcode = organizationcode;
	}
	public String getContactphone() {
		return contactphone;
	}
	public void setContactphone(String contactphone) {
		this.contactphone = contactphone;
	}
	public String getIsexistcompany() {
		return isexistcompany;
	}
	public void setIsexistcompany(String isexistcompany) {
		this.isexistcompany = isexistcompany;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getBusinessrange() {
		return businessrange;
	}
	public void setBusinessrange(String businessrange) {
		this.businessrange = businessrange;
	}
	public String getOrganizecard() {
		return organizecard;
	}
	public void setOrganizecard(String organizecard) {
		this.organizecard = organizecard;
	}
	public String getContactaddress() {
		return contactaddress;
	}
	public void setContactaddress(String contactaddress) {
		this.contactaddress = contactaddress;
	}
	public String getRegistmoney() {
		return registmoney;
	}
	public void setRegistmoney(String registmoney) {
		this.registmoney = registmoney;
	}
	public String getRegisteroffice() {
		return registeroffice;
	}
	public void setRegisteroffice(String registeroffice) {
		this.registeroffice = registeroffice;
	}
	public String getBusinesslimit() {
		return businesslimit;
	}
	public void setBusinesslimit(String businesslimit) {
		this.businesslimit = businesslimit;
	}
	public String getAllowbusinesscard() {
		return allowbusinesscard;
	}
	public void setAllowbusinesscard(String allowbusinesscard) {
		this.allowbusinesscard = allowbusinesscard;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getBasicaccoutopen() {
		return basicaccoutopen;
	}
	public void setBasicaccoutopen(String basicaccoutopen) {
		this.basicaccoutopen = basicaccoutopen;
	}
	public String getBasicaccoutid() {
		return basicaccoutid;
	}
	public void setBasicaccoutid(String basicaccoutid) {
		this.basicaccoutid = basicaccoutid;
	}
	public String getCompanyprincipal() {
		return companyprincipal;
	}
	public void setCompanyprincipal(String companyprincipal) {
		this.companyprincipal = companyprincipal;
	}
	public String getMemo1() {
		return memo1;
	}
	public void setMemo1(String memo1) {
		this.memo1 = memo1;
	}
	public String getMemo2() {
		return memo2;
	}
	public void setMemo2(String memo2) {
		this.memo2 = memo2;
	}
	public String getMemo3() {
		return memo3;
	}
	public void setMemo3(String memo3) {
		this.memo3 = memo3;
	}
	public String getMemo4() {
		return memo4;
	}
	public void setMemo4(String memo4) {
		this.memo4 = memo4;
	}
	

}
