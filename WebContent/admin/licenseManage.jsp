
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@taglib prefix="s" uri="/struts-tags" %>
   

<form id="pagerForm" method="post" action="/aqjy/usertotalview/LicenseAction_findAll">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${page.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" /> 
    <input type="hidden" name="orderDirection"  value="${param.orderDirection}" />  
    <input type="hidden" name="property"  value="${property}">
    <input type="hidden" name="key"  value="${key}">
</form>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="/aqjy/usertotalview/LicenseAction_findAll" method="post">
	<div class="searchBar">
		<table class="searchContent">
			<tr>
				<td>
					内容属性：
				</td>
				<td>
              
					<select class="combox" name="property" >
							<option value="" >根据名称查询</option>
							<option value="idcard">身份证号</option>
							<option value="cartype">准驾车型</option>			
					</select>
				</td>
				<td>
					要查询的值：
						<input type="text" value="" name="key">			
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>	
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
			</ul>
			<div style="color:red;margin-top:5px;">注意:点击标题可以进行排序；检索模式下若想查询所有信息，再点击一次检索按钮即可；数据较多时可改变每页多少条以显示页码</div>
		</div>
	</div>	
	</form>
</div>

<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="delete" href="/aqjy/usertotalview/LicenseAction_delete?uid={sid_user}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="/aqjy/usertotalview/LicenseAction_find?uid={sid_user}" target="navTab"><span>修改</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th>身份证号</th>
				<th>准驾车型</th>
				<%-- <th orderField="firsttime" <c:if test='${orderField == ""firsttime"" }'> class="${orderDirection}"  </c:if> >发布时间</th> --%>
				<th>初次领证时间</th>
				<th>有效期</th>
				<th>有效期起始时间</th>
				<th>年审信息</th>
<!-- 				<th>驾驶证复印件</th> -->
				<th>备注</th>
				
				
			</tr>
		</thead>
		<tbody>
			<c:forEach  var="li"  items="${list }">
				<tr target="sid_user" rel="${li.idcard }">
					<%-- <td><a href="/aqjy/usertotalview/LicenseAction_findDetail?uid=${sp.sid}" target="navTab">${sp.title }</a></td> --%>
					<td><a href="/aqjy/usertotalview/LicenseAction_findDetail?uid=${li.idcard}" target="navTab">${li.idcard }</a></td>
					<td>${li.cartype }</td>
					<td>${li.firsttime }</td>
					<td>${li.limittime}</td>
					<td>${li.begintime }</td>
					<td>${li.yearaudit}</td>
				<%-- 	<td>${li.lphoto }</td> --%>
					<td>${li.memo }</td>
					
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10" >10</option>
					<option value="50" selected="selected">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '100' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" selected="selected">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '500' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" >100</option>
					<option value="500" selected="selected">500</option>
				</c:if>
			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>

	</div>
</div>
