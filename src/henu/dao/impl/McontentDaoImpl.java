package henu.dao.impl;
/**
 * 对Mcontent表DAO的实现
 * @author 田星杰
 * 2015-01-22
 */
import henu.bean.Company;
import henu.bean.Mcontent;
import henu.dao.McontentDao;
import henu.dao.factory.DaoFactory;
import henu.util.Dbcp;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class McontentDaoImpl implements McontentDao{

	@Override
	public int save(Mcontent mcontent) {
		// TODO Auto-generated method stub
		int result = 0;
		try {
			String sql="insert into mcontent(content,timePerTime,filePath,videoUrl,publisher,publishTime,memo,basicid) values(?,?,?,?,?,?,?,?)";
			QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
			Object[] params ={mcontent.getContent(),mcontent.getTimePerTime(),mcontent.getFilePath(),mcontent.getVideoUrl(),mcontent.getPublisher(),mcontent.getPublishTime(),mcontent.getMemo(),mcontent.getBasicid()};
			result=qr.update(sql, params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return result;
	}

	@Override
	public int delete(int mcid) {
		// TODO Auto-generated method stub
		int result = 0;
		try {
			QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
			String sql = "delete from mcontent where mcid = ?";
			result = qr.update(sql, mcid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int update(int mcid, Mcontent mcontent) {
		// TODO Auto-generated method stub
		int result = 0;
		try {          
			QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
			String sql = "update mcontent set content=?,timePerTime=?,videoUrl=?,filePath=?,publisher=?,publishTime=?,memo=?,basicid=?where mcid=?";
			Object[] params ={mcontent.getContent(),mcontent.getTimePerTime(),mcontent.getVideoUrl(),mcontent.getFilePath(),mcontent.getPublisher(),mcontent.getPublishTime(),mcontent.getMemo(),mcontent.getBasicid(),mcid};
			result = qr.update(sql, params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}


	@Override
	public Mcontent findAllBymcId(int mcid) {
		// TODO Auto-generated method stub
		Mcontent bf=null;
		String sql="select * from mcontent where mcid=?";
		QueryRunner qr=DaoFactory.getRunner();
		try {
			bf = qr.query(sql,new BeanHandler<Mcontent>(Mcontent.class), mcid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bf;
	}

	@Override
	public List<Mcontent> findByProperty(String property, String key,
			String order, String sort, int start, int end) {
		// TODO Auto-generated method stub
		List <Mcontent> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "SELECT * FROM (SELECT tt.*,ROWNUM ro FROM(select * from MCONTENT where "
				+ property
				+ "='"
				+ key
				+ "'  order by "
				+ order
				+ " "
				+ sort
				+ ") tt WHERE ROWNUM <=" + end + ") WHERE ro > " + start + "";
		try {
			list = runner.query(sql, new BeanListHandler<Mcontent>(Mcontent.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Mcontent> findAll(String order, String sort,String property, String key) {
		// TODO Auto-generated method stub
		List<Mcontent> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "SELECT * FROM MCONTENT where " + property + " = ? "
				+ " ORDER BY  " + order + " " + sort;
		try {
			list = runner.query(sql, new BeanListHandler<Mcontent>(Mcontent.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Mcontent findMcontent(int basicid) {
		// TODO Auto-generated method stub
		Mcontent bf=null;
		QueryRunner qr=DaoFactory.getRunner();
		try {
			String  sql = "SELECT * FROM MCONTENT where basicid= ? ";
			bf = qr.query(sql,new BeanHandler<Mcontent>(Mcontent.class), basicid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bf;
	}
	
}
