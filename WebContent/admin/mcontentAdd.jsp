<!-- 此页面用于后台用户具体会议内容的填写 -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div class="pageFormContent nowrap" layoutH="97">

	<form method="post" action="/aqjy/upFile" enctype="multipart/form-data"	class="pageForm required-validate" onsubmit="return iframeCallback(this);">
		
		<fieldset>
			<input type="hidden" name="uptype" value="onlyup" />
			<legend align="center"><font color="red">会议附件上传</font></legend>
		
				<p>
				<label>添加非视频文件</label>
					<input type="file" name="upload">
				</p>
				
				<!-- <p>
				<label>添加视频文件</label>
					<input type="file" name="upload">
				</p> -->
				
			<dl class="nowrap">
				<dd>
					<input type="submit" value="上传文件" />
				</dd>
			</dl>

		</fieldset>
		</form>
	<form method="post" action="/aqjy/safetymetting/mettingcontent_add" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
	<fieldset>
			<legend align="center"><font color="red">会议内容填写</font></legend>
			<p>
				<label>每页浏览时间：</label>
					<input type="text" name="mc.timePerTime" class="date required" dateFmt="HH:mm:ss" maxlength="20" readonly="true"/>				
			</p>
			<p>
				<label></label>
			</p>
			<p>
				<label>会议内容：</label>
					<div class="unit">
						<textarea class="editor" name="mc.content" rows="8" cols="100"
							upLinkUrl="upload.php" upLinkExt="zip,rar,txt" 
							upImgUrl="upload.php" upImgExt="jpg,jpeg,gif,png" 
							upFlashUrl="upload.php" upFlashExt="swf"
							upMediaUrl="upload.php" upMediaExt:"avi">
					
						</textarea>
					</div>					
			</p>
			<p>
				<label></label>
			</p>
			<p>
				<label>文本附件路径：</label>
				<input type="text" name="mc.filePath" />			
			</p>
			<p>
				<label></label>
			</p>
			<p>
				<label>视频附件路径：</label>
					<input type="text" name="mc.videoUrl" />				
			</p>
			<p>
				<label></label>
			</p>
			<p>
				<label>发布人：</label>
					<input type="text" name="mc.publisher" maxlength="20" class="required" />				
			</p>
			<p>
				<label></label>
			</p>
			<p>
				<label>备注：</label>
					<input type="text" name="mc.memo" maxlength="20" />				
			</p>
			<input type="hidden" name="mc.basicid" value="${mc.basicid }">
			
			</fieldset>
		
		 <div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>

</html>