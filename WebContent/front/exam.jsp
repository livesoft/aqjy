<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.exam_page{width: 900px;font-family: Microsoft YaHei;margin-left: 40px;margin-top:20px;}
.exam_head{width:900px;border-bottom: 2px solid black;}
.exam_main{position: relative;width: 840px;margin-left: auto;margin-right: auto;margin-top: 15px;}
.exam_main1{position: absolute;top:0;width: 840px;background-color:white;z-index:100;text-align: center;}
.exam_main1 h2{font-size:20px;color: red;margin-bottom: 10px;}
.exam_main2 h2{font-size:18px;}
.main1_text{height: 200px;padding-top: 10px;}
#begin{border:1px solid white;padding-left:10px;padding-right:10px;height: 30px;font-family: Microsoft YaHei;color: white;background-color: #0f355c;font-weight: bold;cursor: pointer;margin-bottom: 20px;}
#begin:HOVER{background-color: #18619f;}
.exam_main2{width: 840px;height:300px;position:absolute;top:0;z-index:99;}
.exam_timeUnfinished{float:right;height: 25px;line-height: 25px;}
.exam_topicUnfinished{float:right;margin-right: 20px;height: 25px;line-height: 25px}
.exam_topicUnfinished input{width: 20px;text-align: center;}
#timer{float:right;height: 25px;line-height: 25px;}
.exam_ti{height: 150px;margin-top: 30px;margin-left: 20px;}
.exam_ti li{list-style-type: none;}
.exam_button{text-align: center;margin-top: 20px;}
.exam_btn{border:1px solid white;padding-left:10px;padding-right:10px;height: 30px;font-family: Microsoft YaHei;color: white;background-color: #0f355c;font-weight: bold;cursor: pointer;margin-right: 20px;}
.exam_btn:HOVER {background-color: #18619f;}
.titrue{width:120px;height:30px;line-height:30px;text-align:center;background-color: #2cc21b;font-size:16px;color: white;font-weight: bold;display: none;}
.tifalse{width:120px;height:30px;line-height:30px;text-align:center;background-color: #de1714;font-size:16px;color: white;font-weight: bold;display: none;}
</style>
<script src="exam/exam_djs.js" type="text/javascript"></script>
	<div class="pageContent" layoutH="97">
		<div class="exam_page">
			<div class="exam_head">
				<img alt="" src="exam/exam_head.png">
			</div>
			<div class="exam_main">
			<div id="exam_main1" class="exam_main1">
			<h2>考试须知</h2>
				<div class="main1_text"><p>text</p><p>text</p><p>text</p></div>
				<input type="button" value="开始考试" id="begin" onclick="begin_djs()">
			</div>
			<div class="exam_main2">
				<div id="timer"></div>
				<div class="exam_timeUnfinished">剩余时间：</div>
				<div class="exam_topicUnfinished">剩余题目：<input type="text" id="shengyu" value="${totalexams-1}" disabled="disabled">题</div>
				<div class="exam_ti">
					<h2 id="content"></h2>
					<div id="xuanzeti">
						<ul>
						<li><input  type="radio" class="tiXuanxiang" name="ans" value="A">A、<span id="optiona"></span></li>
						<li><input  type="radio" class="tiXuanxiang" name="ans" value="B">B、<span id="optionb"></span></li>
						<li><input  type="radio" class="tiXuanxiang" name="ans" value="C">C、<span id="optionc"></span></li>
						<li><input  type="radio" class="tiXuanxiang" name="ans" value="D">D、<span id="optiond"></span></li>
						</ul>
					</div>
					<div id="panduanti">
						<ul>
							<li><input  type="radio" class="tiXuanxiang" name="ans" value="0">对</li>
							<li><input  type="radio" class="tiXuanxiang" name="ans" value="1">错</li>
						</ul>
					</div>
				</div>
				<!-- onsubmit="return validateCallback(this,navTabAjaxDone)"action="/yxxt/test/compuTest" -->
				<form method="post"  class="pageForm required-validate" action="/aqjy/exer/compuExer" onsubmit="return validateCallback(this,navTabAjaxDone)">
					<div class="exam_button"><input type="button" class="exam_btn" onclick="preExam()" value="上一题"><input type="button" class="exam_btn" value="下一题"  onclick="nextExam(1)"><input type="button" class="exam_btn" id="jiaojuan" value="交卷" ></div>
				</form>
			</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	var pointer = -1 ;				//指向当前题目
	var totalexam =${totalques};
	var timelong = ${exam.duration}; //考试的时间
	function begin_djs(){
		$(".exam_main1").slideToggle();
		timer(timelong);
		nextExam(0);			//此处用户开始的时候现实第一个题目
	}
	
	//让i指向当前的题目下一题的时候把i +1然后去数据库取出来
	function preExam(){
		//处理上一题
		pointer -- ;
		if(pointer< 0){
			pointer=0;
			alert("已经是第一题！");
			return;
		}
		$("#shengyu").val(totalexam-pointer-1);			//先把题目的数量减少1
		var sheng = $("#shengyu").val();			//获取剩余的题目是否为0 如果小于 0就结束考试
		//alert("当前的索引值："+pointer);
		$.get('/aqjy/json/preExer?args='+pointer,function(data){
			//alert("上一题的内容："+data);
			data=JSON.parse(data);
			$("#content").html(data.content);
			var tx =  data.type ; 
			var answer = data.answer  ;
			var options =  $(".tiXuanxiang");
			//alert("做过的题目的答案："+answer);
			if(tx == '选择题'){
				$("#xuanzeti").show();
				$("#panduanti").hide();
				$("#optiona").html(data.optiona);
				$("#optionb").html(data.optionb);
				$("#optionc").html(data.optionc);
				$("#optiond").html(data.optiond);
				//这是个选择题，看看传过来的答案是ABCD的哪一个 
				//先获取答案
				/*
				*/
				//6个选项 
				if(answer=="A"){
					options[0].checked="checked";
				}else if(answer=="B"){
					options[1].checked="checked";
				}else if(answer=="C"){
					options[2].checked="checked";
				}else if(answer=="D"){
					options[3].checked="checked";
				}else{
					//alert("选择题：发生未知错误");
				}
			}else{
					$("#xuanzeti").hide();
					$("#panduanti").show();
				if(answer=="0"){
					options[4].checked="checked";
				}else if(answer=="1"){
					options[5].checked="checked";
				}else{
					//alert("判断题：发生未知错误");
				}
			}
		},'json');
		$("input").removeAttr("checked");		//此处还是让他重新做
		//这个地方需要显示学生做过的答案 答案就是answer ，
	}
	
	function nextExam(fir){
		//var tixuan = $(".tiXuanxiang").length ; 
		//alert(tixuan);//这果然是一个数组
		//tixuan[0].attr("checked","checked");
		pointer++;
		//alert("当前的索引值："+pointer);
		$("#shengyu").val(totalexam-pointer-1);			//先把题目的数量减少1
		var sheng = $("#shengyu").val();			//获取剩余的题目是否为0 如果小于 0就结束考试
		if(sheng < 0){
				gameover();
		}
			//检查选项是不是为空
			var rr = $(':radio[type=radio]:checked').length ; 
			if((rr == 0)&& (fir==1) ){
				alert("亲，这个题你还没做呢！");
			}else{
			//单击之后触发这个事件 获取一个题目
			//alert("总共的题目数："+totalexam);
			$.get('/aqjy/json/nextExer?args='+pointer,function(data){
				data=JSON.parse(data);
				$("#content").html(data.content);
				var tx =  data.type ; 
				if(tx == '选择题'){
					$("#xuanzeti").show();
					$("#panduanti").hide();
					$("#optiona").html(data.optiona);
					$("#optionb").html(data.optionb);
					$("#optionc").html(data.optionc);
					$("#optiond").html(data.optiond);
					$(".tiXuanxiang");
				}else{
					$("#xuanzeti").hide();
					$("#panduanti").show();
				}
			},'json');
				$(':radio[type=radio]:checked').removeAttr("checked"); ; 
			}
	}
	//----------------------------------选题的时候把答案转给后台-----------------------------------------//
	$(".tiXuanxiang").click(function(){
		//alert("你的答案:"+this.value);
		$.get('/aqjy/json/recExer',{'args':pointer,'args0':this.value});		//只需要把答案发送过去不用管结果的事情
		//选过之后把选中的那个选中移除
	});
	
	$("#jiaojuan").click(function(){
		$("form").submit();
	});
	//----------------------------------考试结束调用此方法-----------------------------------------//
	function gameover(){
		$("form").submit();
	}
	</script>
