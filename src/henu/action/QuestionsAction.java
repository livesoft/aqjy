package henu.action;

import henu.bean.Questions;
import henu.dao.QuestionsDao;
import henu.dao.factory.DaoFactory;
import henu.service.ExportExcel;
import henu.service.ImportExcel;
import henu.service.RandomList;
import henu.util.AJAX;
import henu.util.Page;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class QuestionsAction extends ActionSupport {

	private String args;
	private AJAX ajax = new AJAX();
	private String url;
	private Questions ques;
	private List<Questions> list;

	// 文件下载
	private String res;
	private String resName;
	private String resType;

	// 分页排序
	private Page page = new Page();
	private int pageNum = 1;
	private int numPerPage = 10;
	private String orderField = "qid";
	private String orderDirection = "asc";
	// 检索
	private String property = "-1";
	private String key = "-1";

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getRes() {
		return res;
	}

	public void setRes(String res) {
		this.res = res;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getResType() {
		return resType;
	}

	public void setResType(String resType) {
		this.resType = resType;
	}

	public Questions getQues() {
		return ques;
	}

	public void setQues(Questions ques) {
		this.ques = ques;
	}

	public List<Questions> getList() {
		return list;
	}

	public void setList(List<Questions> list) {
		this.list = list;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getNumPerPage() {
		return numPerPage;
	}

	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	public String getOrderDirection() {
		return orderDirection;
	}

	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}

	public String getArgs() {
		return args;
	}

	public void setArgs(String args) {
		this.args = args;
	}

	public AJAX getAjax() {
		return ajax;
	}

	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	// 显示所有的记录
	public String all() {
		//System.out.println("访问到all方法");
		if (orderField.equals("") || orderDirection.equals("")) {
			orderField = "qid";
			orderDirection = "asc";
		}
		if (property.equals("") || key.equals("")) {
			property = "-1";
			key = "－1";
		}
		//System.out.println("action查询的关键字是:" + key);
		//System.out.println("查询的依据:" + property);
		page.setProperties("Questions", pageNum, numPerPage, property, key);
		// 给page这个class设置属性
		list = DaoFactory.getQuesttionDaoInstance().findByProperty(
				property.trim(), key.trim(), orderField.trim(),
				orderDirection.trim(), page.getStart(), page.getEnd());
		url = "/admin/questionsManage.jsp";
		return SUCCESS;
	}

	/**
	 * 删除一条记录
	 * */
	public String del() {
		//System.out.println("访问到delete方法：" + args);
		// 已经得到要删除的id 调用删除方法
		int result = DaoFactory.getQuesttionDaoInstance().delete(args);
		if (result > 0) {
			HandleMsg("200", "删除成功！", "quesm");
		} else {
			HandleMsg("300", "删除失败！", "quesm");
			//System.out.println("删除失败");
		}
		return SUCCESS;
	}

	public String add() {
		/**
		 * 那边提交的话可能会出现异常数据 根据type的类型重新封装bean 然后调用dao存储
		 * */
		int result = DaoFactory.getQuesttionDaoInstance().save(ques);

		if (result > 0) {
			HandleMsg("200", "添加成功!", "quesm");
		} else {
			HandleMsg("300", "添加失败!", "quesm");
		}
		return SUCCESS;
	}

	public String find() {
		// System.out.println("访问到find方法: "+args);
		// 根据需要修改的传过来一个id 然后这里查询出来 这个试题的所有的信息 ，然后跳转到修改试题的界面
		ques = DaoFactory.getQuesttionDaoInstance().findById(args);
		url = "/admin/questionsUpdate.jsp";
		return SUCCESS;
	}
	public String edit() {
		QuestionsDao qudao = DaoFactory.getQuesttionDaoInstance();
		String questype = ques.getType().trim();
		String[] answer = ques.getAnswer().trim().split(",");
		if (questype.equals("选择题")) {
			 System.out.println("answer:"+answer[0].trim());
			 System.out.println("answer:"+answer[1].trim());
			ques.setAnswer(answer[0].trim());
			// 转换答案存储到数据中
		} else if (questype.equals("判断题")) {
			// 处理判断题 把后面的那个答案放到数据库中
			ques.setOptiona("");
			ques.setOptionb("");
			ques.setOptionc("");
			ques.setOptiond("");
			ques.setAnswer(answer[1].trim());			//每次都会提交过来两个答案，不同类型的题目只要选择一个就可以
		}
		int result = qudao.update(args, ques);
		if (result > 0) {
			HandleMsg("200", "修改成功！", "quesm");
		} else {
			HandleMsg("300", "修改失败！", "quesm");
		}
		return SUCCESS;
	}
	
	/**
	 * 导出
	 * */
	public String export() {
		String basepath = ServletActionContext.getServletContext().getRealPath("/");
		list = DaoFactory.getQuesttionDaoInstance().findAll("qid", "asc", "-1", "-1"); // 获取所有的记录生成list
		System.out.println("查询到所有题目的数量："+list.size());
		res = ExportExcel.ques(list, basepath);
		resType = "application/vnd.ms-excel" ; 
		resName="allquestions.xls" ;
		return "export";
	}
	
	public String goimp() {
		url = "/admin/questionsImp.jsp";
		return SUCCESS;
	}

	public String goadd() {
		url = "/admin/questionsAdd.jsp";
		return SUCCESS;
	}

	public void HandleMsg(String statusCode, String msg, String tabid) {
		ajax.setCallbackType("");
		ajax.setMessage(msg);
		ajax.setStatusCode(statusCode);
		ajax.setNavTabId(tabid);
		url = "/admin/ajax.jsp";
	}

}
