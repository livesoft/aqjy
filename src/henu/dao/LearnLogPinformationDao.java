package henu.dao;


import henu.bean.LearnLogPinformation;

import java.util.List;

/**
 * 
 * @author yuchen
 *
 */


public interface LearnLogPinformationDao {
	
	
	
	/*//删除学习记录
	public int delete(String llid);
	*/
	
	/**
	 * 查询全部学习记录并列举出来
	 * @param order
	 * @param sort
	 * @return
	 */
	public List<LearnLogPinformation> findAll(String order,String sort);
	/**
	 * 根据表字段property和相应关键字key进行模糊查找
	 * @param property
	 * @param key
	 * @param order 排序字段名称
	 * @param 排序方式，降序还是升序
	 * @return 返回符合条件的记录。
	 */
	public List<LearnLogPinformation> findByProperty(String property,String key,String order,String sort,int start,int end);
	/**
	 * 查找前台用户自己的学习记录
	 * @param id
	 * @return
	 */
	
	public LearnLogPinformation findById(String id);
	
	public int save(LearnLogPinformation lp);
	
	
	

}
