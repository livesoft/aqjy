<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="pageFormContent nowrap">
	<form method="post" enctype="multipart/form-data" action="/aqjy/public/pinformation_update" class="pageForm required-validate" onsubmit="return iframeCallback(this,navTabAjaxDone);">
		<div class="pageFormContent" layoutH="56" style="width:700px;height:600px;float:left">
			<dl>
				<dt>姓名：</dt>
				<dd><input type="text" name="p.realName" value="${p.realName }" /></dd>
			</dl>			
			<dl>
				<dt>手机号：</dt>
				<dd><input type="text"  name="p.phone" value="${p.phone }" /></dd>		
			</dl>
			<%-- <dl>
				<dt>公司：</dt>
				<dd><input type="text"  name="company" value="${company}" /></dd>
			</dl> --%>
			<dl>
				<dt>住址：</dt>
				<dd><input  type="text" name="p.address" value="${p.address }" /></dd>
			</dl>
			<dl>
				<dt>用户类型：</dt>
				<dd><input type="text"  name="p.regType" value="${p.regType }" ></dd>
			</dl>
			<dl>
				<dt>是否车主：</dt>
				<dd><select name="p.isMaster">
				<option value="是">是</option>
				<option value="否">否</option>
				</select><%-- <input type="text"  name="p.isMaster" value="${p.isMaster}" > --%></dd>
			</dl>
			<dl>
				<dt>注册时间：</dt>
				<dd><input type="text"  name="p.regTime" value="${p.regTime }"class="date" /><a class="inputDateButton" href="javascript:;">选择</a></dd>
			</dl>
			<dl>
				<dt>邮箱：</dt>
				<dd><input type="text" name="p.email" value="${p.email }"  /></dd>
			</dl>
			<dl>
				<dt>身份证正面照片：</dt>
				<dd><input type="file" name="photo" class="required images"></dd>
			</dl>
			<dl>
				<dd style="color: red">注意：修改个人信息后，需要重新审核，在未通过审核之前将不能进入系统。若放弃修改，请点击关闭！</dd>
			</dl>
			
			</div>
			
			<%-- <div style="float:right"><img alt="用户照片" src="../${ p.photoPath}" width="180px" height="180px" ></div> --%>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li>
					<div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div>
				</li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">关闭</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>
