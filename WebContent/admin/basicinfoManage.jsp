<!-- 此页面用于后台用户管理会议通知 包括增删改查的操作按钮 -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<html>
<head></head>
<body>
<form id="pagerForm" method="post" action="/aqjy/safetymetting/content_findAll">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${page.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" /> 
    <input type="hidden" name="orderDirection"  value="${param.orderDirection}" />  
    <input type="hidden" name="property"  value="${property}">
    <input type="hidden" name="key"  value="${key}">
</form>


<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="/aqjy/safetymetting/content_findAll" method="post">
	<div class="searchBar">
		<!--<ul class="searchContent">
			<li>
				<label>我的客户：</label>
				<input type="text"/>
			</li>
			<li>
			<select class="combox" name="province">
				<option value="">所有省市</option>
				<option value="北京">北京</option>
				<option value="上海">上海</option>
				<option value="天津">天津</option>
				<option value="重庆">重庆</option>
				<option value="广东">广东</option>
			</select>
			</li>
		</ul>
		-->
		<table class="searchContent">
			<tr>
				<td>
					查询:
				</td>
				<td>
					<select class="combox" name="property">
						<option value="">请选择查询方式</option>
						<option value="basicid">按会议编号</option>
						<option value="organizeUnit">按组织单位</option>
						<option value="theme">按主题查询</option>
					</select>
				</td>
				<td>
					要查询的值：<input type="text" name="key" />
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
				<!-- <li><a class="button" href="demo_page6.html" target="dialog" mask="true" title="查询框"><span>高级检索</span></a></li> -->
			</ul>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="basicinfoAdd.jsp" target="navTab"><span>添加</span></a></li>
			<li><a class="delete" href="/aqjy/safetymetting/content_del?uid={sid_user}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="/aqjy/safetymetting/content_find?uid={sid_user}" target="navTab"><span>修改</span></a></li>
			<li class="line">line</li>
			<li><a class="icon" href="demo/common/dwz-team.xls" target="dwzExport" targetType="navTab" title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li>
			<li><a class="add" href="/aqjy/safetymetting/content_findMettingID?uid={sid_user}" target="navTab"><span>设置会议内容</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="50">会议编号</th>
				<th width="120">主题</th>
				<th width="130">参加人员</th>
				<th width="120">开始时间</th>
				<th width="120">地点</th>
				<th width="120">结束时间</th>
				<th width="80">组织人</th>
				<th width="120">组织单位</th>
				<th width="150">列席人员</th>
				<th width="120">发布时间</th>
				<th width="80">主持人</th>
				<th width="80">状态</th>
				<th width="80">备注</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach  var="c"  items="${list }">
				<tr target="sid_user" rel="${c.basicid }">
					<td>${c.basicid }</td>
					<td>${c.theme }</td>
					<td>${c.joinPerson }</td>
					<td>${c.startTime }</td>
					<td>${c.address }</td>
					<td>${c.endTime }</td>
					<td>${c.organizer }</td>
					<td>${c.organizeUnit }</td>
					<td>${c.attendPerson }</td>
					<td>${c.publishTime }</td>
					<td>${c.compere }</td>
					<td>${c.baudit }</td>
					<td>${c.remark }</td>	
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10" >10</option>
					<option value="50" selected="selected">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '100' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" selected="selected">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '500' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" >100</option>
					<option value="500" selected="selected">500</option>
				</c:if>
			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>

	</div>
</div>
</body>
</html>