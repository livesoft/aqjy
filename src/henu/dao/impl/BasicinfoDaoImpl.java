package henu.dao.impl;
/**
 * 对basicinfo表DAO的实现
 * @author 田星杰
 * 2015-01-22
 */
import henu.bean.Basicinfo;
import henu.dao.BasicinfoDao;
import henu.dao.factory.DaoFactory;
import henu.util.Dbcp;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class BasicinfoDaoImpl implements BasicinfoDao{
	/*public static void main(String[] args){
		BasicinfoDaoImpl se = new BasicinfoDaoImpl();
		Basicinfo s = new Basicinfo();
		s.setBasicid(1);
		s.setCompere("田星杰");
		System.out.println(se.save(s));
		
		
	}*/
	@Override
	public int save(Basicinfo basicinfo) {
		// TODO Auto-generated method stub
		int result = 0;
			try {
				String sql="insert into basicinfo(joinPerson,startTime,endTime,compere,organizeUnit,organizer,attendPerson,address,theme,remark,baudit,publishTime) values(?,?,?,?,?,?,?,?,?,?,?,?)";
				QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
				Object[] params ={basicinfo.getJoinPerson(),basicinfo.getStartTime(),basicinfo.getEndTime(),basicinfo.getCompere(),basicinfo.getOrganizeUnit(),basicinfo.getOrganizer(),basicinfo.getAttendPerson(),basicinfo.getAddress(),basicinfo.getTheme(),basicinfo.getRemark(),basicinfo.getBaudit(),basicinfo.getPublishTime()};
				result=qr.update(sql, params);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return result;
	}

	@Override
	public int delete(int basicid) {
		// TODO Auto-generated method stub
		int result = 0;
		try {
			QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
			String sql = "delete from basicinfo where basicid = ?";
			result = qr.update(sql, basicid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int update(int basicid, Basicinfo basicinfo) {
		// TODO Auto-generated method stub
		int result = 0;
		try {          
			QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
			String sql = "UPDATE basicinfo SET joinPerson=?,startTime=?,endTime=?,compere=?,organizer=?,organizeUnit=?,attendPerson=?,address=?,theme=?,baudit=?,remark=?,publishTime=? WHERE basicid=?";
			Object[] params ={basicinfo.getJoinPerson(),basicinfo.getStartTime(),basicinfo.getEndTime(),basicinfo.getCompere(),basicinfo.getOrganizer(),basicinfo.getOrganizeUnit(),basicinfo.getAttendPerson(),basicinfo.getAddress(),basicinfo.getTheme(),basicinfo.getBaudit(),basicinfo.getRemark(),basicinfo.getPublishTime(),basicid};
			result = qr.update(sql, params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Basicinfo findAllBybasicId(int basicid) {
		// TODO Auto-generated method stub
		Basicinfo bf=null;
		String sql="select * from basicinfo where basicid=?";
		QueryRunner qr=DaoFactory.getRunner();
		try {
			bf = qr.query(sql,new BeanHandler<Basicinfo>(Basicinfo.class), basicid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bf;
	}
	
	@Override
	public List<Basicinfo> findAll(String property, String key, String order,
			String sort) {
		// TODO Auto-generated method stub
		List<Basicinfo> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "SELECT * FROM basicinfo where " + property + " = ? "
				+ " ORDER BY  " + order + " " + sort;
		try {
			list = runner.query(sql, new BeanListHandler<Basicinfo>(Basicinfo.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Basicinfo> findByProperty(String property, String key,
			String order, String sort, int start, int end) {
		// TODO Auto-generated method stub
		List <Basicinfo> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "SELECT * FROM (SELECT tt.*, ROWNUM ro FROM (select * from BASICINFO where "+property+"='"+key+"'  order by "+order+" "+sort+") tt WHERE ROWNUM <="+end+") WHERE ro > "+start+"";;
		try {
			list = runner.query(sql, new BeanListHandler<Basicinfo>(Basicinfo.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
}
