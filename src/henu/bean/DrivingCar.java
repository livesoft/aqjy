package henu.bean;

public class DrivingCar extends Bean{
	private int drid;//自增主键
	private int carid;//车辆id
	private String drivingId;
	private String vehicleLicense;//车牌号必填
	private String vehicleType;//车辆类型
	private String owners;//车辆所有人
	private String address;//住址
	private String useCharacter;//使用性质
	private String models;//品牌型号
	private String vin;//车辆识别码
	private String engineid;//发动机编号
	private String registerDate;//注册日期
	private String doclc;//档案编号
	private String menNumber;//核载人数
	private String totalWeight;//总质量
	private String weight;//整备质量
	private String checkWeight;//核定载质量
	private String sizes;//外形尺寸
	private String towWeight;//准牵引质量
	private String validations;//有效期
	private String copyDrivingCar;//行车证复印件
	private String memo;//备用字段
	//carid vehicleLicense vehicleType owner address
	public int getDrid() { //useCharacter model vin  engineid 
		return drid;  //registerDate doclc menNumber totalWeight weight checkWeight    
	}                //size towWeight validation copyDrivingCar memo  memo1 memo2
	public void setDrid(int drid) {
		this.drid = drid;
	}
	public int getCarid() {
		return carid;
	}
	public void setCarid(int carid) {
		this.carid = carid;
	}
	
	public String getDrivingId() {
		return drivingId;
	}
	public void setDrivingId(String drivingId) {
		this.drivingId = drivingId;
	}
	public String getVehicleLicense() {
		return vehicleLicense;
	}
	public void setVehicleLicense(String vehicleLicense) {
		this.vehicleLicense = vehicleLicense;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getOwners() {
		return owners;
	}
	public void setOwners(String owners) {
		this.owners = owners;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getUseCharacter() {
		return useCharacter;
	}
	public void setUseCharacter(String useCharacter) {
		this.useCharacter = useCharacter;
	}
	public String getModels() {
		return models;
	}
	public void setModels(String models) {
		this.models = models;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getEngineid() {
		return engineid;
	}
	public void setEngineid(String engineid) {
		this.engineid = engineid;
	}
	public String getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	public String getDoclc() {
		return doclc;
	}
	public void setDoclc(String doclc) {
		this.doclc = doclc;
	}
	public String getMenNumber() {
		return menNumber;
	}
	public void setMenNumber(String menNumber) {
		this.menNumber = menNumber;
	}
	public String getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getCheckWeight() {
		return checkWeight;
	}
	public void setCheckWeight(String checkWeight) {
		this.checkWeight = checkWeight;
	}
	public String getSizes() {
		return sizes;
	}
	public void setSizes(String sizes) {
		this.sizes = sizes;
	}
	public String getTowWeight() {
		return towWeight;
	}
	public void setTowWeight(String towWeight) {
		this.towWeight = towWeight;
	}
	public String getValidations() {
		return validations;
	}
	public void setValidations(String validations) {
		this.validations = validations;
	}
	public String getCopyDrivingCar() {
		return copyDrivingCar;
	}
	public void setCopyDrivingCar(String copyDrivingCar) {
		this.copyDrivingCar = copyDrivingCar;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
	
}
