<!-- 此页面用于具体的修改会议内容设置 -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<h2 class="contentTitle">会议设置</h2>


<div class="pageContent">
	
	<form method="post" action="/aqjy/safetymetting/mettingcontent_update" class="pageForm required-validate" onsubmit="return validateCallback(this)" enctype="multipart/form-data">
		<div class="pageFormContent nowrap" layoutH="97">

			 <dl>
				<dt></dt><!-- 会议编号： -->
				<dd>
					<input type="hidden" name="mc.mcid" value="${mcontent.mcid }" maxlength="20" class="required" />
					
				</dd>
			</dl> 
			
			<dl>
				<dt>每页浏览时间：</dt>
				<dd>
					<input type="text" name="mc.timePerTime" value="${mcontent.timePerTime }" class="date required" dateFmt="HH:mm:ss" maxlength="20" readonly="true"/>
				    <!-- <a class="inputDateButton" href="javascript:;">选择</a> -->
				</dd>
			</dl>
			
			<dl>
				<dt>会议内容：</dt>
				<dd>	
					<div class="unit">
						<textarea class="editor" name="mc.content"  rows="8" cols="100"
							upLinkUrl="upload.php" upLinkExt="zip,rar,txt" 
							upImgUrl="upload.php" upImgExt="jpg,jpeg,gif,png" 
							upFlashUrl="upload.php" upFlashExt="swf"
							upMediaUrl="upload.php" upMediaExt:"avi">
						${mcontent.content }</textarea>
					</div>	
				</dd>
			</dl>
			
			<dl>
				<dt>添加文本附件：</dt>
				<dd>
					<input type="text" name="mc.filePath" value="${mcontent.filePath }"/>
					
				</dd>
			</dl>
			
			<dl>
				<dt>添加视频附件：</dt>
				<dd>
					<input type="text" name="mc.videoUrl" value="${mcontent.videoUrl }"/>
					
				</dd>
			</dl>
			
			<dl>
				<dt>发布人：</dt>
				<dd>
					<input type="text" name="mc.publisher" value="${mcontent.publisher }" maxlength="20" class="required" />
					
				</dd>
			</dl>
			<%-- 
			<dl>
				<dt>发布时间：</dt>
				<dd>
					<input type="text" name="mc.publishTime" value="${mcontent.publishTime }" maxlength="20" class="required" />
					
				</dd>
			</dl> --%>
			<dl>
				<dt>备注：</dt>
				<dd>
					<input type="text" name="mc.memo" value="${mcontent.memo }" maxlength="20" />
					
				</dd>
			</dl>
			<dl>
				<dt>会议通知编号：</dt>
				<dd>
					<input type="text" name="mc.basicid" value="${mcontent.basicid }" maxlength="20" readonly="readonly" />
					
				</dd>
			</dl>
			<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
			
		</div>
		
	</form>
	
</div>


<script type="text/javascript">
function customvalidXxx(element){
	if ($(element).val() == "xxx") return false;
	return true;
}
</script>
