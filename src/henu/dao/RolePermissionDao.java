package henu.dao;

import henu.bean.RolePermission;

import java.util.List;

/**
 *处理权限分配
 * @author wuhaifeng
 * 2014-12-08
 */
public interface RolePermissionDao {
	/**
	 *给角色增加一条权限记录 
	 *@param rp 权限记录
	 *@return 1 表示添加成功
	 * */
	public int add(RolePermission rp);
	/**
	 *删除角色一条权限记录 
	 *@param  id
	 *@return 1 表示添加成功
	 * */
	public int delete(Integer id);
	/**
	 *列出所有的权限记录 
	 *@return 所有的权限List
	 * */
	public List<RolePermission> findAll();
	/**
	 * 根据属性找到权限记录
	 *@param property 属性
	 *@param 关键字
	 *@return包含权限的List
	 * */
	public List<RolePermission> findByProperty(String property,String key);
	
}
