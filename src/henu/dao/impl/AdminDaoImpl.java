package henu.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import henu.bean.PInformation;
import henu.dao.AdminDao;
import henu.dao.factory.DaoFactory;
import henu.util.Dbcp;
/**
 * 03-22
 * @author wuhaifeng
 * */
public class AdminDaoImpl implements AdminDao{

	@Override
	public List<PInformation> findByProperty(String property, String key,
			String order, String sort, int start, int end) {
		List<PInformation> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "SELECT * FROM PInformation where " + property + " = ? "
				+ " ORDER BY  " + order + " " + sort;
		try {
			list = runner.query(sql, new BeanListHandler<PInformation>(
					PInformation.class), key);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	@Override
	public int delete(int adminid) {
		int result = 0;
		String sql = "delete from MANAGEPERSON where maid=?";
		QueryRunner qr = DaoFactory.getRunner();
		try {
			result = qr.update(sql, adminid);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}
