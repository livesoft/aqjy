package henu.dao;

import henu.bean.Car;
import henu.bean.DataBaseSeq;
import henu.bean.Exam;

import java.util.List;
/**
 * @author 杜山
 * */
public interface CarDao {
	
	public abstract int Save(Car car);

	public abstract int delete(int carid);

	public abstract int Update(Car car);

	public abstract Car findById(int carid);

	public abstract List<Car> findAll(String order, String sort);
	
	public List<Car> findAll(String order, String sort,String property,String key);
	
	public abstract DataBaseSeq getNowSeq();
	
	public abstract List<Car> findByProperty(String property, String key,
			String order, String sort, int start, int end);

}