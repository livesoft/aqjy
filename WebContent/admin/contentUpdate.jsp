<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
     <%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
  <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
	<link rel="stylesheet" href="../ueditor/themes/default/css/ueditor.css">
  	<script> 
  		var ue = UE.getEditor('editor');
  	</script>
</head>
<body>
<div class="pageContent">
	<form method="post" action="/aqjy/safetyeducation/ContentAction_update?uid=${spe.sid }" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent nowrap" layoutH="97">
			<div style="height: 660px;overflow: auto;">
 			<dl>
				<!-- <dt></dt> -->
				<dd>
					<input type="hidden" name="spe.seid"  value="${spe.seid }" readonly="readonly"/>
				</dd>
			</dl> 
			<dl>
				<dt>标题：</dt>
				<dd>
					<input type="text" name="spe.title"  value="${spe.title }"  class="required"/>
				</dd>
			</dl>
			<dl>
				<dt>备注(专题所属)：</dt>
				<dd>
					<input type="text" name="spe.memo" maxlength="50" value="${spe.memo }" class="required" readonly="readonly"/>
				<%-- 	 <s:select  name="spe.seid" list="#session.listmap" listKey="key" listValue="value"	headerKey="" headerValue="请选择"/> --%>
					
				</dd>
			</dl>
			<dl>
				<dt>类别：</dt>
				<dd>
					<%-- <select name="spe.edutype" id="edutype">
					<option>${spe.edutype }</option>			
					
					
					
					
					</select> --%>
					<select name="spe.edutype">
						<c:if test="${spe.edutype=='视频' }">
							<option value="视频" selected="selected">视频</option>
							<option value="文字">文字</option>
						</c:if>
						<c:if test="${spe.edutype=='文字' }">
							<option value="视频" >视频</option>
							<option value="文字" selected="selected">文字</option>
						</c:if>
					</select>
				</dd>
			</dl>
			<dl>
				<dt>发布人：</dt>
				<dd>
					<input type="text" name="spe.publisher" value="${spe.publisher}"  class="required"/>
				</dd>
			</dl>
			<%-- <dl>
				<dt>浏览次数</dt>
				<dd>
					<input type="text" name="spe.viewnumbers" value="${spe.viewnumbers }" class="" />
				</dd>
			</dl>
			 --%>
			<dl>
				<dt>内容</dt>
				<dd>
					<script type="text/plain" id="editor" name="spe.content"  style="width:800px;height: 400px">${spe.content}</script>
				</dd>
			</dl>
			
			
			</div>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button  type="submit">修改</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>

</form>
<%-- <script>
	$(function(){
		$.getJSON("/aqjy/json/getDictionary",{table:"SAFETYEDUCATION",name:"EDUTYPE"},function(data){
			var json=JSON.parse(data);
			for (var i = 0; i < json.length; i++) {
				$("#edutype").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
			}
			
		})
	})
</script> --%>
</body>
</html>