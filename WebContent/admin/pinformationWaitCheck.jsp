<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form id="pagerForm" method="post" action="/aqjy/userManage/pinformation_findWaitCheck">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${page.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" /> 
    <input type="hidden" name="orderDirection"  value="${param.orderDirection}" />  
    <input type="hidden" name="property"  value="${property}">
    <input type="hidden" name="key"  value="${key}">
</form>
<!-- -别人一点连接跳进action action返回json 打开此页面  -->
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="/aqjy/userManage/pinformation_findWaitCheck" method="post">
	<div class="searchBar">
		<table class="searchContent">
			<tr>
				<td>
					查询方式：
				</td>
				<td>
					<select name="property">
					<option value="" >根据名称查询</option>
					 <option value="realName" >姓名</option> 
					 <option value="phone" >手机号</option>
					 <option value="idCard" >身份证号</option>
					</select>	
				</td>
				<td><input type="text" name="key" /></td>
			</tr>
		</table>
		<div class="subBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
			</ul>
				
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="edit" href="/aqjy/userManage/pinformation_findByidCard?args={args}" target="navTab"><span>审核</span></a></li>
			<li><a class="delete" href="/aqjy/userManage/pinformation_deleteByIdCardWithoutCheck?args={args}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li class="line">line</li>
			<li><a class="icon" href="" target="dwzExport"  title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
	<!-- -此处通过 读取数据库加载所有信息 -->
		<thead>
			<tr>
			
                <th>姓名</th>
                <th>身份证号</th>
                <th>性别</th>
				<th>手机号</th>
                <th>审核是否通过</th>
                <th>注册时间</th>
               
			</tr>
		</thead>
		<tbody>
		
			<c:forEach var="cl" items="${list}">
				<tr target="args" rel="${cl.idCard}">
				<td>${cl.realName }</td>
				<td>${cl.idCard}</td>
				<td>${cl.sex }</td>
				<td>${cl.phone }</td>
				<td>${cl.paudit }</td>
				<td>${cl.regTime }</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10" >10</option>
					<option value="50" selected="selected">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '100' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" selected="selected">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '500' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" >100</option>
					<option value="500" selected="selected">500</option>
				</c:if>
			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>
	</div>
</div>
