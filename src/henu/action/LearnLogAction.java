package henu.action;


import henu.bean.Company;
import henu.bean.LearnLog;
import henu.bean.LearnLogPinformation;
import henu.bean.ManagePerson;
import henu.bean.PInformation;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.CONSTANTS;
import henu.util.Page;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class LearnLogAction extends ActionSupport{
	//分页查询
		Page page;
		private int pageNum=1;
		private int numPerPage=10;
		private String orderField="starttime";
		private String orderDirection="asc";
		
		//检索
		private String property="-1";
		private String key="-1";
		
		
		private AJAX ajax;
		private String uid;
		private String url="/admin/ajax.jsp";
		private LearnLog lg;
		private List<LearnLogPinformation> list;
		private LearnLogPinformation lp;
		
		private String title;
		//进行批量删除所传过来id的集合
		private String[] ids;
		public String[] getIds() {
			return ids;
		}
		public void setIds(String[] ids) {
			this.ids = ids;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public LearnLogPinformation getLp() {
			return lp;
		}
		public void setLp(LearnLogPinformation lp) {
			this.lp = lp;
		}
		public Page getPage() {
			return page;
		}
		public void setPage(Page page) {
			this.page = page;
		}
		public int getPageNum() {
			return pageNum;
		}
		public void setPageNum(int pageNum) {
			this.pageNum = pageNum;
		}
		public int getNumPerPage() {
			return numPerPage;
		}
		public void setNumPerPage(int numPerPage) {
			this.numPerPage = numPerPage;
		}
		public String getOrderField() {
			return orderField;
		}
		public void setOrderField(String orderField) {
			this.orderField = orderField;
		}
		public String getOrderDirection() {
			return orderDirection;
		}
		public void setOrderDirection(String orderDirection) {
			this.orderDirection = orderDirection;
		}
		public String getProperty() {
			return property;
		}
		public void setProperty(String property) {
			this.property = property;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public AJAX getAjax() {
			return ajax;
		}
		public void setAjax(AJAX ajax) {
			this.ajax = ajax;
		}
		public String getUid() {
			return uid;
		}
		public void setUid(String uid) {
			this.uid = uid;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public LearnLog getLg() {
			return lg;
		}
		public void setLg(LearnLog lg) {
			this.lg = lg;
		}
		
		public List<LearnLogPinformation> getList() {
			return list;
		}
		public void setList(List<LearnLogPinformation> list) {
			this.list = list;
		}
		
		//每个公司后台查看自己公司司机的学习记录
		public String findAll(){
			ManagePerson m=(ManagePerson) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_ADMIN);
			Company company=DaoFactory.getCompanyDaoInstance().findBy(String.valueOf(m.getComid()));
			//System.out.println(String.valueOf(m.getComid()));
			//System.out.println(company.getCompanyname());
			page = new Page();
			if(orderField.equals("")||orderDirection.equals("")){
				orderField = "starttime";
				orderDirection = "asc";
			}
			if(property.equals("")||key.equals("")){
				//property="comid";
				//key=Integer.toString(m.getComid());
				property="-1";
				key="-1";
			}
			//page.setSql("select count(*) from VIEW_LEARNLOG_PINFO_COMPANY where "+property+"='"+key+"' and comid="+m.getComid()+"",pageNum,numPerPage);
			
			page.setProperties("VIEW_LEARNLOG_PINFO_COMPANY", pageNum, numPerPage, property, key);
			//利用公司的名字来获得公司的COMID
			list= DaoFactory.getLearnLogDaoIntance().findByProperty(property, key, orderField, orderDirection,page.getStart(), page.getEnd(),company.getCompanyname()); 
			
			/*System.out.println(list.size()+"\t\t\t\t:6543");*/
			url="/admin/learnlogManage.jsp";
			return SUCCESS;
		}
		//前台查看自己的学习记录
		public String findById(){
			//后台管理员的权限，只能查看自己公司下的一些东西
			PInformation p =(PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
			/*System.out.println(p.getIdCard());*/
			page = new Page();
			if(orderField.equals("") || orderDirection.equals(""))
			{
				orderDirection = "publish";
				orderDirection = "asc";
			}
			if(property.equals("") || orderDirection.equals(""))
			{
				property="-1";
				key="-1";
			}
			page.setProperties("VIEW_LEARNLOG_PINFO_COMPANY", pageNum, numPerPage, property, key);
			/*list= DaoFactory.getLearnLogDaoIntance().findByProperty(property, key, orderField, orderDirection,page.getStart(), page.getEnd());*/
			lp =  DaoFactory.getLearnLogDaoIntance().findById(p.getIdCard());
			/*System.out.println(lp);*/
			/*System.out.println(list.size()+"\t\t\t\t:6543");*/
			url="/front/learnlogManage.jsp";
			return SUCCESS;
		}
		
		//从视图里面获取值然后更新到表单里面
		public String update(){
			PInformation p =(PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
			
			//获取用户学习的时间
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String curr_date = format.format(date);
			
			//判断用户的学习的次数。。。
			int result = DaoFactory.getLearnLogDaoIntance().findById2(p.getIdCard());
			ajax=new AJAX();
			if (result > 0) {
				//如果里面有个学习记录，则不让
				ajax.setStatusCode("300");
				ajax.setMessage("你已经学习过，无需重复学习");
				
			} else {
				DaoFactory.getLearnLogDaoIntance().add(p.getIdCard(), title, curr_date);
				//里面的记录没有，就添加进去，并体现
				ajax.setStatusCode("200");
				ajax.setMessage("你已完成本季度专题内容的学习！");
				ajax.setCallbackType("forward");
				ajax.setForwardUrl("/aqjy/safetyeducation/LearnLogAction_findById");
			}
			return SUCCESS;
			
		}
		//批量删除这些记录
		public String batchDelete(){
			Object[][] params=new Object[ids.length][];
			
			for (int i = 0; i < params.length; i++) {
				params[i]=new Object[]{ids[i]};
			}
			int[] results = DaoFactory.getLearnLogDaoIntance().batchDelete(params);;
			if(results!=null&&results.length>0)
			{
				HandleMsg("200","删除"+results.length+"条成功","con","");
			}else{
				HandleMsg("300","删这些记录失败","con","");
			}
			return SUCCESS;
		}
		
		
		private void HandleMsg(String statusCode, String msg, String tabid, String callback) {
			ajax =  new AJAX();
			ajax.setCallbackType(callback);
			ajax.setMessage(msg);
			ajax.setStatusCode(statusCode);
			ajax.setNavTabId("log");
		}
		
		
}
