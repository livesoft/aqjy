<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>安全教育培训考核管理系统</title>
<style>
	#accountInfo{
		position: relative;
	}
	#newsMarquee{
		position: absolute;
		top:30px;
		left:0px;
		width:50%;
	}
	#newsMarquee a{
		padding:50px;
	}
	#newsMarquee a:HOVER{
		color:red;
		text-decoration: none;
	}
	
</style>
<link href="themes/default/style.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="themes/css/core.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="themes/css/print.css" rel="stylesheet" type="text/css" media="print"/>
<link href="uploadify/css/uploadify.css" rel="stylesheet" type="text/css" media="screen"/>
<!--[if IE]>
<link href="themes/css/ieHack.css" rel="stylesheet" type="text/css" media="screen"/>
<![endif]-->

<!--[if lte IE 9]>
<script src="js/speedup.js" type="text/javascript"></script>
<![endif]-->
<script src="js/jquery-1.7.2.js" type="text/javascript"></script>
<script src="js/jquery.cookie.js" type="text/javascript"></script>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script src="js/jquery.bgiframe.js" type="text/javascript"></script>
<script src="xheditor/xheditor-1.2.1.min.js" type="text/javascript"></script>
<script src="xheditor/xheditor_lang/zh-cn.js" type="text/javascript"></script>
<script src="uploadify/scripts/jquery.uploadify.js" type="text/javascript"></script>

<!-- svg图表  supports Firefox 3.0+, Safari 3.0+, Chrome 5.0+, Opera 9.5+ and Internet Explorer 6.0+ -->
<script type="text/javascript" src="chart/raphael.js"></script>
<script type="text/javascript" src="chart/g.raphael.js"></script>
<script type="text/javascript" src="chart/g.bar.js"></script>
<script type="text/javascript" src="chart/g.line.js"></script>
<script type="text/javascript" src="chart/g.pie.js"></script>
<script type="text/javascript" src="chart/g.dot.js"></script>

<script src="js/dwz.core.js" type="text/javascript"></script>
<script src="js/dwz.util.date.js" type="text/javascript"></script>
<script src="js/dwz.validate.method.js" type="text/javascript"></script>
<script src="js/dwz.regional.zh.js" type="text/javascript"></script>
<script src="js/dwz.barDrag.js" type="text/javascript"></script>
<script src="js/dwz.drag.js" type="text/javascript"></script>
<script src="js/dwz.tree.js" type="text/javascript"></script>
<script src="js/dwz.accordion.js" type="text/javascript"></script>
<script src="js/dwz.ui.js" type="text/javascript"></script>
<script src="js/dwz.theme.js" type="text/javascript"></script>
<script src="js/dwz.switchEnv.js" type="text/javascript"></script>
<script src="js/dwz.alertMsg.js" type="text/javascript"></script>
<script src="js/dwz.contextmenu.js" type="text/javascript"></script>
<script src="js/dwz.navTab.js" type="text/javascript"></script>
<script src="js/dwz.tab.js" type="text/javascript"></script>
<script src="js/dwz.resize.js" type="text/javascript"></script>
<script src="js/dwz.dialog.js" type="text/javascript"></script>
<script src="js/dwz.dialogDrag.js" type="text/javascript"></script>
<script src="js/dwz.sortDrag.js" type="text/javascript"></script>
<script src="js/dwz.cssTable.js" type="text/javascript"></script>
<script src="js/dwz.stable.js" type="text/javascript"></script>
<script src="js/dwz.taskBar.js" type="text/javascript"></script>
<script src="js/dwz.ajax.js" type="text/javascript"></script>
<script src="js/dwz.pagination.js" type="text/javascript"></script>
<script src="js/dwz.database.js" type="text/javascript"></script>
<script src="js/dwz.datepicker.js" type="text/javascript"></script>
<script src="js/dwz.effects.js" type="text/javascript"></script>
<script src="js/dwz.panel.js" type="text/javascript"></script>
<script src="js/dwz.checkbox.js" type="text/javascript"></script>
<script src="js/dwz.history.js" type="text/javascript"></script>
<script src="js/dwz.combox.js" type="text/javascript"></script>
<script src="js/dwz.print.js" type="text/javascript"></script>
<!--
<script src="bin/dwz.min.js" type="text/javascript"></script>
-->
<script src="js/dwz.regional.zh.js" type="text/javascript"></script>

<script type="text/javascript">

$(function(){
	DWZ.init("dwz.frag.xml", {
		loginUrl:"login_dialog.html", loginTitle:"登录",	// 弹出登录对话框
//		loginUrl:"login.html",	// 跳到登录页面
		statusCode:{ok:200, error:300, timeout:301}, //【可选】
		pageInfo:{pageNum:"pageNum", numPerPage:"numPerPage", orderField:"orderField", orderDirection:"orderDirection"}, //【可选】
		debug:false,	// 调试模式 【true|false】
		callback:function(){
			initEnv();
			$("#themeList").theme({themeBase:"themes"}); // themeBase 相对于index页面的主题base路径
		}
	});
});

</script>
</head>
<body scroll="no">
	<div id="layout">
		<div id="header">
			<div class="headerNav">
				<a class="logo" href="http://j-ui.com">标志</a>
				<ul class="nav">
					<li><a href="changepwd.html" target="dialog" width="600">设置</a></li>
					<li><a href="http://weibo.com/dwzui" target="_blank">微博</a></li>
					<li><a href="/aqjy/public/resetPassword.jsp" target="_blank">修改密码</a></li>
					<li><a href="/aqjy/public/exit">退出</a></li>
				</ul>
				<ul class="themeList" id="themeList">
					<li theme="default"><div class="selected">蓝色</div></li>
					<li theme="green"><div>绿色</div></li>
					<!--<li theme="red"><div>红色</div></li>-->
					<li theme="purple"><div>紫色</div></li>
					<li theme="silver"><div>银色</div></li>
					<li theme="azure"><div>天蓝</div></li>
				</ul>
			</div>

			<!-- navMenu -->
			
		</div>

		<div id="leftside">
			<div id="sidebar_s">
				<div class="collapse">
					<div class="toggleCollapse"><div></div></div>
				</div>
			</div>
			<div id="sidebar">
				<div class="toggleCollapse"><h2>主菜单</h2><div>收缩</div></div>

				<div class="accordion" fillSpace="sidebar">
					<div class="accordionHeader">
						<h2><span>Folder</span>安全教育培训管理平台</h2>
					</div>
					<div class="accordionContent">
						<ul class="tree treeFolder">
							<li><a>我的主页</a>
								<ul>
									<li><a href="main.jsp" target="navTab" >我的主页</a></li>
								</ul>
							</li>
							<li><a>我的信息</a>
								<ul>
							     	<li><a href="/aqjy/public/pinformation_findByUseridCard" target="navTab" rel="pinformationmessage">个人信息</a></li>
									<li><a href="/aqjy/usertotalview/LicenseAction_findLicense" target="navTab" rel="le" fresh="true">驾驶证信息</a></li>
									<li><a href="/aqjy/public/certification_findByUseridCard" target="navTab" rel="certification" >资格证信息</a></li>
									 <li><a href="/aqjy/usertotalview/Car_Ffind" target="navTab" rel="Fallcar" fresh="true">车辆信息</a></li>														
								    <li><a href="/aqjy/public/insurance_InsuranceMassageList" target="navTab" rel="insurance" fresh="true" >保险信息</a></li>
								   <li><a href="/aqjy/usertotalview/businessMessageAction_findAllByCarId" target="navTab" rel="businessMessageInfo" fresh="false">营运证信息</a></li>								   								   
								    <li><a href="/aqjy/usertotalview/drivingCar_findAllByCarId" target="navTab" rel="drivingCarInfos" fresh="false">行车证信息</a></li>
								    <li><a href="/yxxt/video/listVideo" target="navTab" rel="page11" fresh="false">二级维护信息</a></li>
								</ul>
							</li>
							<li><a>安全教育</a>
								<ul>
									<li><a href="/aqjy/safetyeducation/SeminarAction_findAll2" target="navTab" >专题教育</a></li>
									<li><a href="/aqjy/safetyeducation/ContentAction_findDayEdu" target="navTab" rel="cloth">日常教育</a></li>
									<li><a href="/aqjy/safetyeducation/LearnLogAction_findById" target="navTab" rel="page11" >学习记录</a></li>
								</ul>
							</li>
							
							<li><a>安全会议</a>
								<ul>									
									<li><a href="/aqjy/mysafetymetting/mymetting_findAll" target="navTab" rel="StudentH">会议通知</a></li>
									<li><a href="/aqjy/mysafetymetting/mymettingcontent_findAll}" target="navTab" rel="StudentD">我的会议</a></li>
								</ul>
							</li>						
							<li><a>安全考核</a>
								<ul>
									<li><a href="/aqjy/exer/allExer" target="navTab" rel="exerm" fresh="false">考核清单</a></li>
									
								</ul>							
							</li>
							<li><a>公告信息</a>
								<ul>
								<li><a href="/aqjy/interaction/noticeAction_findNoticeListByPropertyIn?key=新闻公告" target="navTab" fresh="false">新闻公告</a></li>
								<li><a href="/aqjy/interaction/noticeAction_findNoticeListByPropertyIn?key=法律法规" target="navTab"  fresh="false">法律法规</a></li>
								<li><a href="/aqjy/interaction/noticeAction_findNoticeListByPropertyIn?key=交通路况" target="navTab"  fresh="false">交通路况</a></li>
								</ul>
							</li>
							<li><a>互动交流</a>
								<ul>
									<li><a href="/aqjy/interaction/onlineChatAction_findListByIdCard" target="navTab" rel="onlineChatInfo" fresh="false">发帖咨询</a></li>
									<li><a href="/yxxt/trafficMode/findByIdCard_trafficMode" target="navTab" rel="trafficMode_info" fresh="false">查看所有已回复的咨询</a></li>
								</ul>
							</li>							
												
						</ul>
					</div>        
				</div>
			</div>
		</div>
		<div id="container">
			<div id="navTab" class="tabsPage">
				<div class="tabsPageHeader">
					<div class="tabsPageHeaderContent"><!-- 显示左右控制时添加 class="tabsPageHeaderMargin" -->
						<ul class="navTab-tab">
							<li tabid="main" class="main"><a href="javascript:;"><span><span class="home_icon">我的主页</span></span></a></li>
						</ul>
					</div>
					<div class="tabsLeft">left</div><!-- 禁用只需要添加一个样式 class="tabsLeft tabsLeftDisabled" -->
					<div class="tabsRight">right</div><!-- 禁用只需要添加一个样式 class="tabsRight tabsRightDisabled" -->
					<div class="tabsMore">more</div>
				</div>
				<ul class="tabsMoreList">
					<li><a href="javascript:;">我的主页</a></li>
				</ul>
				<div class="navTab-panel tabsPageContent layoutBox">
					<div class="page unitBox">
						<div class="accountInfo">
							<div class="alertInfo">
								<p><a href="doc/dwz-user-guide.pdf" target="_blank"><img src="images/LOGO_3.png" alt="" />使用帮助</a></p>
								<p><a href="/aqjy/interaction/onlineChatAction_findListByIdCard" target="navTab"><img src="images/LOGO_4.png" alt="" />你有<span></span>条已回复的咨询</a></p>
							</div>
							<div class="right">
								
							</div>
								<p><span>欢迎您:<span style="color:red">${ PERSON.realName}</span>&nbsp;&nbsp;
									你上次登陆系统的时间为:<s:if test='#session.PERSON.lastTime==null'>你第一次登录该系统,无上次登录时间</s:if><s:else>${PERSON.lastTime }</s:else>
								</span></p>
								<p id="newsMarquee"><span><marquee behavior="scroll" direction="left" scrolldelay="200" onmouseover="this.stop()" onmouseout="this.start()"></marquee></span></p>
							<!-- <p><a href="http://weibo.com/dwzui" target="_blank">我的信息</a>  &nbsp;&nbsp;
								<a href="http://weibo.com/dwzui" target="_blank">修改密码</a>
							</p> -->
						</div>
						<div class="pageFormContent" layoutH="80" >
							
							<div class="index_main"><img src="images/index_main.png" alt="" /></div>
						</div>
						
					<!-- 	<div style="width:230px;position: absolute;top:60px;right:0" layoutH="80">
							<iframe width="100%" height="430" class="share_self"  frameborder="0" scrolling="no" src="http://widget.weibo.com/weiboshow/index.php?width=0&height=430&fansRow=2&ptype=1&skin=1&isTitle=0&noborder=1&isWeibo=1&isFans=0&uid=1739071261&verifier=c683dfe7"></iframe>
						</div> -->
					</div>
					
				</div>
			</div>
		</div>

	</div>

	<div id="footer">Copyright &copy; 2014 <a href="demo_page2.html" target="dialog">技术支持：蒲公英工作室</a></div>


<script>
	$(function(){
		$.getJSON("/aqjy/json/findReplyMessageNumByIdCard",function(data){
			$(".alertInfo span").text(data);
		});
	});

	$.getJSON("/aqjy/json/findNews",function(data){
		var json=JSON.parse(data);
		for(var i=0;i<json.length;i++){
				var title=json[i].title;
				var url="/aqjy/public/findByNidOut?uid="+json[i].nid+"";
				if(json[i].isTop==1){
					$("#newsMarquee marquee").append("<a href='"+url+"' target='_blank'>"+title+"</a>");
				}
			
		}
	});  
</script>
</body>
</html>
