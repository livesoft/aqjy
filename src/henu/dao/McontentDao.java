package henu.dao;

import java.util.List;

import henu.bean.Company;
import henu.bean.Mcontent;

public interface McontentDao {
	/**
	 * 向会议内容表添加信息
	 * @param mcontent
	 */
	public int save(Mcontent mcontent);
	/**
	 * 根据mcid删除基本会议信息表的一条记录
	 * @param mcid
	 */
	public int delete(int mcid);
	/**
	 * 根据mcid、mcontent更新基本会议信息表的一条记录
	 * @param mcid
	 * @param mcontent
	 */
	public int update(int mcid,Mcontent mcontent);

	/**
	 * 根据会议内容ID查找
	 * @param mcid
	 * @return
	 */
	public Mcontent findAllBymcId(int mcid);
	/**
	 * 模糊查找
	 * @param property 字段名
	 * @param key 字段值
	 * @param order  排序字段
	 * @param sort 排序方式
	 * @return
	 */
	public List<Mcontent> findByProperty(String property, String key,
			String order, String sort, int start, int end);
	public List<Mcontent> findAll(String order, String sort,String property,String key);
	/**
	 * 根据会议内容的外码basicid查找
	 * @param basicid
	 * @return
	 */
	public Mcontent findMcontent(int basicid);
}
