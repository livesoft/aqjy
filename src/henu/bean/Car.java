package henu.bean;
/**
 * 车辆信息
 * @author 杜山
 *
 * 2014-11-02
 *
 */
public class Car extends Bean {
	
	/*	车辆号
	车主id
	集团id
	车辆识别码
	发动机号
	车牌号
	车架号
	车长
	车宽
	车高
	载重量
	马力
	车型【数据字典】
	状态
	创建时间
	购置时间
	条形码
	行车证复印件
	车辆照片
	是否挂车
	购车发票复印件
	备注1
	备注2
	备注3*/

	private static final long serialVersionUID = 1L;
	private int carid;
	private String userid;
	private int comid;
	private String vin;
	private String engineno;
	private String vehicleLicense;
	private String vfn;
	private double carLength;
	private double carWidth;
	private double carHeight;
	private double deadWeight;
	private int horsePower;	
	private String vehicleType;
	private String status;
	private String createTime;
	private String buyTime;
	private String qrCode;
	private String copyLicence;
	private String carPhoto;
	private String isTrailer;
	private String invoice;
	private String memo1;
	private String memo2;
	private String memo3;

	public int getCarid() {
		return carid;
	}
	public void setCarid(int carid) {
		this.carid = carid;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public int getComid() {
		return comid;
	}
	public void setComid(int comid) {
		this.comid = comid;
	}
	public String getEngineno() {
		return engineno;
	}
	public void setEngineno(String engineno) {
		this.engineno = engineno;
	}
	public String getVehicleLicense() {
		return vehicleLicense;
	}
	public void setVehicleLicense(String vehicleLicense) {
		this.vehicleLicense = vehicleLicense;
	}
	public String getVfn() {
		return vfn;
	}
	public void setVfn(String vfn) {
		this.vfn = vfn;
	}
	public double getCarLength() {
		return carLength;
	}
	public void setCarLength(double carLength) {
		this.carLength = carLength;
	}
	public double getCarWidth() {
		return carWidth;
	}
	public void setCarWidth(double carWidth) {
		this.carWidth = carWidth;
	}
	public double getCarHeight() {
		return carHeight;
	}
	public void setCarHeight(double carHeight) {
		this.carHeight = carHeight;
	}
	public double getDeadWeight() {
		return deadWeight;
	}
	public void setDeadWeight(double deadWeight) {
		this.deadWeight = deadWeight;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getBuyTime() {
		return buyTime;
	}
	public void setBuyTime(String buyTime) {
		this.buyTime = buyTime;
	}
	public String getQrCode() {
		return qrCode;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public String getCopyLicence() {
		return copyLicence;
	}
	public void setCopyLicence(String copyLicence) {
		this.copyLicence = copyLicence;
	}
	public String getCarPhoto() {
		return carPhoto;
	}
	public void setCarPhoto(String carPhoto) {
		this.carPhoto = carPhoto;
	}
	public String getIsTrailer() {
		return isTrailer;
	}
	public void setIsTrailer(String isTrailer) {
		this.isTrailer = isTrailer;
	}
	public String getInvoice() {
		return invoice;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public String getMemo1() {
		return memo1;
	}
	public void setMemo1(String memo1) {
		this.memo1 = memo1;
	}
	public String getMemo2() {
		return memo2;
	}
	public void setMemo2(String memo2) {
		this.memo2 = memo2;
	}
	public String getMemo3() {
		return memo3;
	}
	public void setMemo3(String memo3) {
		this.memo3 = memo3;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public int getHorsePower() {
		return horsePower;
	}
	public void setHorsePower(int horsePower) {
		this.horsePower = horsePower;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
