<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<body>
<div class="pageFormContent nowrap" layoutH="97">
	<form method="post" action="/aqjy/userManage/manageperson_add" class="pageForm required-validate" enctype="multipart/form-data" onsubmit="return iframeCallback(this,navTabAjaxDone)">
	<fieldset>
			<legend align="center"><font color="red">管理人员信息管理</font></legend>
				<dl>
				<dt>身份证号</dt>
				 <dd><input type="text" name="mp.maid"
					class="required digits" minlength="18" maxlength="18" />
				<dd>
			</dl>

			<dl>
				<dt>手机号</dt> 
				<dd><input type="text" name="mp.phone"
					class="required phone" maxlength="11" /></dd>
			</dl>
			<dl>
				<dt>姓名</dt> 
				<dd><input type="text" name="mp.realName"
					class="required" /></dd>
			</dl>
			<dl>
				<dt>性别</dt> 
				<dd>
					<input name="mp.sex" type="radio" checked value="男"/>男 
					<input name="mp.sex" type="radio" value="女"/>女
				</dd>
			</dl>
			
			<dl>
				<dt>身份证照</dt> 
					<dd><input type="file" name="upload" class="required image"  /></dd>
			</dl>
			<dl>
				<dt>本人照片</dt> 
				<dd><input type="file" name="upload" class="required image" /></dd>
			</dl>
			
			<dl>
				<dt>电子邮箱</dt> 
				<dd><input type="text" name="mp.email" class="email" /></dd>
			</dl>

			<dl>
				<dt>密码</dt> 
				<dd>
				<input type="password" id="password"
					name="mp.password" class="required alphanumeric" minlength="6"
					maxlength="20" alt="" /> <span class="info"></span>
				</dd>
			</dl>
			<dl>
				<dt>确认密码</dt> 
				<dd>
				<input type="password" name="mp.repassword"
					class="required" equalto="#password" /> <span class="info"></span>
				</dd>
			</dl>

			<dl>
				<dt>提示问题</dt> 
				<dd>
				<select name="mp.questions">
					<option value="你喜欢的人">你喜欢的人</option>
					<option value="你的大学名称">你的大学名称</option>
					<option value="你崇拜的偶像">你崇拜的偶像</option>
					<option value="你学的文科还是理科">你学的文科还是理科</option>
					<option value="你喜欢的动物是">你喜欢的动物是</option>
					<option value="你喜欢哪个季节">你喜欢哪个季节</option>
				</select>
				</dd>
			</dl>
			<dl>
				<dt>答案</dt> 
				<dd>
				<input type="text" name="mp.answer" />
				</dd>
			</dl>
			<dl>
				<dt>公司</dt> 
				<dd>
				<select name="mp.comid" id="comid">
				</select>
				</dd>
			</dl>
			<dl>
				<dt>角色</dt> 
				<dd>
				<select name="mp.role" id="role">
                 </select>
                 </dd>
			</dl>
			<dl>
				<dt>备注</dt>
				<dd>
					<textarea class="editor" name="mp.memo" rows="6" cols="100" tools="mini"></textarea>
				</dd>
					
			</dl>
			</fieldset>
		 <div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
</body>
<script>
	$(function() {
		$.getJSON("/aqjy/json/findAllCompany",function(data){
			var json=JSON.parse(data);
			for(var i=0;i<json.length;i++){
				$("#comid").append("<option value='"+json[i].comid+"'>"+json[i].companyname+"</option>");
			}
		})
		
		$.getJSON("/aqjy/json/getDictionary",{table:"MANAGEPERSON",name:"ROLE"},function(data){
			var json=JSON.parse(data);
			for (var i = 0; i < json.length; i++) {
				$("#role").append("<option value='"+json[i].codeid+"'>"+json[i].codeNumber+"</option>");
			}
			
		})
	})
</script>
</html>
