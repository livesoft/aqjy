<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="../style/style.css" />
<link rel="stylesheet" type="text/css" href="../style/online_Service.css" />
<script type="text/javascript" src="../js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="../js/js.js"></script>
   <div id="footer">
                        <div class="copyright">
                        <ul>
                            <li><a href="http://www.henu.edu.cn/">河南大学</a> 版权所有</li>
                            <li>Copyright &copy; 2014</li>
                            <li>地址：中国 河南 开封.明伦街/金明大道</li>
                        </ul>
                    	</div>
                        <div class="copy">
                        <ul>
                            <li>邮编：475001/475004</li>
                            <li>总机号码：0371—22868833</li>
                            <li><a href="http://wz.henu.edu.cn/">河南大学蒲公英工作室</a> 制作维护</li>
                        </ul>
                    	</div>
 </div>