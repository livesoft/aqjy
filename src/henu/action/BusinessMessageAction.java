package henu.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import henu.bean.BusinessMessage;
import henu.bean.Car;
import henu.bean.ManagePerson;
import henu.bean.PInformation;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.CONSTANTS;
import henu.util.Page;

import com.opensymphony.xwork2.ActionSupport;

public class BusinessMessageAction extends ActionSupport{
	private BusinessMessage bm;//一条营运证信息
	private List<BusinessMessage> list;
	
	//上传文件
	private File businessFile; //营运证照片
    private String businessFileFileName; 
    private File businessPermission; //经营许可证照片
    private String businessPermissionFileName;
    
	//分页排序
	Page page;
	private int pageNum=1;
	private int numPerPage=10;
	private String orderField="businessId";  
    private String orderDirection="asc";  
    //检索
    private String property="-1";
    private String key="-1";
	private String url="/admin/ajax.jsp";
	AJAX ajax;
	//根据身份证号找到carid，再车carid找到营运证
	public String findAllByCarId() throws Exception {
		page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="businessId";
			orderDirection="asc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		//直接查询条数,表比较多，所以直接用sql查询降低难度
		
		PInformation p= (PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
		page.setSql("select count(*) from businessmessage bm,carinformation c where "+property+"='"+key+"' and c.userid='"+p.getIdCard()+"' and c.carid=bm.carid",pageNum,numPerPage);
		list=DaoFactory.getBusinessMessageDaoInstance().findAllByCarId(property, 
				key, p.getIdCard(), orderField, orderDirection, page.getStart(), page.getEnd());
		url="/front/businessMessageManage.jsp";
		return SUCCESS;
	}
	//根据管理员所在的公司的comid，根据comid找到从carinformation中carid，根据carid从businessmessage找到营运证
	public String adminFindAllByComid() throws Exception {
		page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="businessId";
			orderDirection="asc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		//直接查询条数,表比较多，所以直接用sql查询降低难度
		ManagePerson m=(ManagePerson) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_ADMIN);
		page.setSql("select count(*) from businessmessage bm,carinformation c where "+property+"='"+key+"' and c.comid='"+m.getComid()+"' and c.carid=bm.carid",pageNum,numPerPage);
		list=DaoFactory.getBusinessMessageDaoInstance().AdminFindAllByComid(property, 
				key, m.getComid(), orderField, orderDirection, page.getStart(), page.getEnd());
		url="/admin/businessMessageManage.jsp";
		return SUCCESS;
	}
	//注意ajax和url不要生成set方法，否则增加和更新方法失效
	//添加一个营运证信息
	public String add() throws Exception {
		/*先根据身份证号和车牌号从车辆信息表中找出该车辆的信息*/
		ajax=new AJAX();
		PInformation p= (PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
		Car c=DaoFactory.getBusinessMessageDaoInstance().findByIdCardAndVehicleLicense(p.getIdCard(),bm.getVehicleLicense());
		//如果c为空说明车辆信息不完全
		if(c==null){
			ajax.setStatusCode("300");
			ajax.setMessage("请完善车辆信息后再来完成营运证信息");
		}else{
			//根据车牌号和营运证号判断营运证信息已经添加
			BusinessMessage bmFromDb=DaoFactory.getBusinessMessageDaoInstance().
					findByBusinessIdOrVehicleLicense(bm.getBusinessId(),bm.getVehicleLicense());
			if(bmFromDb!=null){
				ajax.setStatusCode("300");
				ajax.setMessage("你已经添加过营运证信息");
			}else{
				//存文件
				String realPath=ServletActionContext.getRequest().getRealPath("/upload/businessMessageImages");
				InputStream in1=new FileInputStream(businessFile);
				InputStream in2=new FileInputStream(businessPermission);
				String fileType1=businessFileFileName.substring(businessFileFileName.lastIndexOf("."));//截取后缀
				String fileType2=businessPermissionFileName.substring(businessPermissionFileName.lastIndexOf("."));//截取后缀
				String fileName1=bm.getBusinessId()+"businessFile"+fileType1;//文件名为营运号+字段名+后缀
				String fileName2=bm.getBusinessId()+"businessPermission"+fileType2;//文件名为营运号+字段名+后缀
				FileOutputStream out1=new FileOutputStream(realPath+"\\"+fileName1);
				FileOutputStream out2=new FileOutputStream(realPath+"\\"+fileName2);
				byte[] buffer=new byte[1024];
				int len=0;
				while((len=in1.read(buffer))>0){
					out1.write(buffer, 0, len);
				}
				out1.close();
				in1.close();
				while((len=in2.read(buffer))>0){
					out2.write(buffer, 0, len);
				}
				out2.close();
				in2.close();
				bm.setBusinessFile("upload/businessMessageImages/"+fileName1);
				bm.setBusinessPermission("upload/businessMessageImages/"+fileName2);
				bm.setCarid(c.getCarid());
				/*然后存取信息*/
				int result=DaoFactory.getBusinessMessageDaoInstance().add(bm);
				if(result!=0){
					ajax.setStatusCode("200");
					ajax.setMessage("填写成功");
					ajax.setCallbackType("closeCurrent");
					ajax.setNavTabId("businessMessageInfo");
				}else{
					ajax.setStatusCode("300");
					ajax.setMessage("填写失败");
				
				}
				
			}
			
		}
		url="/front/ajax.jsp";
		return SUCCESS;
	}
	//根据id删除一条信息
	public String delete() throws Exception {
		ajax=new AJAX();
		int result=DaoFactory.getBusinessMessageDaoInstance().delete(bm.getBid());
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("删除成功");
			ajax.setCallbackType("");
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("删除失败");
		}
		url="/front/ajax.jsp";
		return SUCCESS;
	}
	//管理员根据id删除一条信息
	public String adminDelete() throws Exception {
		ajax=new AJAX();
		int result=DaoFactory.getBusinessMessageDaoInstance().delete(bm.getBid());
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("删除成功");
			ajax.setCallbackType("");
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("删除失败");
		}
		url="/admin/ajax.jsp";
		return SUCCESS;
	}
	//根据id找到一条记录
	public String find() throws Exception {
		bm=DaoFactory.getBusinessMessageDaoInstance().find(bm.getBid());
		url="/front/businessMessageUpdate.jsp";
		return SUCCESS;
	}
	//根据id更新一条记录
	public String update() throws Exception {
		ajax=new AJAX();
		//存文件
		String realPath=ServletActionContext.getRequest().getRealPath("/upload/businessMessageImages");
		InputStream in1=new FileInputStream(businessFile);
		InputStream in2=new FileInputStream(businessPermission);
		String fileType1=businessFileFileName.substring(businessFileFileName.lastIndexOf("."));//截取后缀
		String fileType2=businessPermissionFileName.substring(businessPermissionFileName.lastIndexOf("."));//截取后缀
		String fileName1=bm.getBusinessId()+"businessFile"+fileType1;//文件名为营运号+字段名+后缀
		String fileName2=bm.getBusinessId()+"businessPermission"+fileType2;//文件名为营运号+字段名+后缀
		FileOutputStream out1=new FileOutputStream(realPath+"\\"+fileName1);
		FileOutputStream out2=new FileOutputStream(realPath+"\\"+fileName2);
		byte[] buffer=new byte[1024];
		int len=0;
		while((len=in1.read(buffer))>0){
			out1.write(buffer, 0, len);
		}
		out1.close();
		in1.close();
		while((len=in2.read(buffer))>0){
			out2.write(buffer, 0, len);
		}
		out2.close();
		in2.close();
		bm.setBusinessFile("upload/businessMessageImages/"+fileName1);
		bm.setBusinessPermission("upload/businessMessageImages/"+fileName2);
		int result=DaoFactory.getBusinessMessageDaoInstance().update(bm);
		System.out.println(result);
		System.out.println(result);
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("更新成功");
			ajax.setCallbackType("closeCurrent");
			ajax.setNavTabId("businessMessageInfo");
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("更新失败");
		}
		System.out.println(result);
		return SUCCESS;
	}
	//前台用户根据id查看一条信息
	public String see() throws Exception {
		bm=DaoFactory.getBusinessMessageDaoInstance().find(bm.getBid());
		url="/front/businessMessageSee.jsp";
		return SUCCESS;
	}
	//后台用户根据id查看一条信息
	public String adminSee() throws Exception {
		bm=DaoFactory.getBusinessMessageDaoInstance().find(bm.getBid());
		url="/admin/businessMessageAdminSee.jsp";
		return SUCCESS;
	}
	public BusinessMessage getBm() {
		return bm;
	}
	public void setBm(BusinessMessage bm) {
		this.bm = bm;
	}
	public List<BusinessMessage> getList() {
		return list;
	}
	public void setList(List<BusinessMessage> list) {
		this.list = list;
	}
	public File getBusinessFile() {
		return businessFile;
	}
	public void setBusinessFile(File businessFile) {
		this.businessFile = businessFile;
	}
	public String getBusinessFileFileName() {
		return businessFileFileName;
	}
	public void setBusinessFileFileName(String businessFileFileName) {
		this.businessFileFileName = businessFileFileName;
	}
	public File getBusinessPermission() {
		return businessPermission;
	}
	public void setBusinessPermission(File businessPermission) {
		this.businessPermission = businessPermission;
	}
	public String getBusinessPermissionFileName() {
		return businessPermissionFileName;
	}
	public void setBusinessPermissionFileName(String businessPermissionFileName) {
		this.businessPermissionFileName = businessPermissionFileName;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getNumPerPage() {
		return numPerPage;
	}
	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderDirection() {
		return orderDirection;
	}
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getUrl() {
		return url;
	}
	public AJAX getAjax() {
		return ajax;
	}
	
	
}
