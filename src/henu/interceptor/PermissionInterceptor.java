package henu.interceptor;

import henu.bean.Admin;
import henu.bean.PInformation;
import henu.util.CONSTANTS;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class PermissionInterceptor implements Interceptor {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void destroy() {

	}

	@Override
	public void init() {

	}

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		return invocation.invoke();
		/*ActionContext act = invocation.getInvocationContext();		//struts的上下文
	//还要配置一个全局的结果视图gone，这个视图处理所有不被允许的访问的请求
		StringBuilder builder = new StringBuilder(invocation.getProxy().getNamespace());
		String currentaction = builder.append("/").append(act.getName()).toString(); //当前action的名字
		System.out.println("当前的action："+currentaction);
		
		String currentns = invocation.getProxy().getNamespace();	//当前的namespace
		System.out.println("当前的namespace："+currentns);
		//先放掉公共连接
		List<String> common = new ArrayList<String>();
		common.add("/admin/adminAction_login");						//用户登录连接
		if(common.contains(currentaction)){						 	//如果包含当前的action就放行
			return invocation.invoke();								//合法的话就放行了
		}
		HttpSession session =  ServletActionContext.getRequest().getSession();
		
		 * 学生为空  	管理员不空  ----管理员当前在线，学生不在线拦截管理员
		 * 学生为空    	管理员为空  ----非法操作 跳转到首页
		 * 学生不空	管理员不空  ----也是非法操作 跳转到登录界面
		 * 学生不空	管理员为空  ----学生登录，不拦截
		 * 
		
		PInformation stu  = (PInformation) session.getAttribute(CONSTANTS.SESSION_PERSON);
		Admin adm = (Admin) session.getAttribute(CONSTANTS.SESSION_ADMIN);
		List<String> allowlist =  (List<String>) act.getSession().get(CONSTANTS.ALLOWLIST);
		if((stu == null)&&(adm == null)){ //跳转到登录
			return Action.LOGIN;
		}else if((stu == null)&&(adm != null)){
			if(allowlist.contains(currentns)){			//如果包含当前的action就放行
				//System.out.println("admin不是空,我要拦截");
				return invocation.invoke();				//合法的话就放行了
			}return Action.NONE;						//假如管理员在线没有通过检验提示权限不足
		}else if((stu != null)&&(adm != null)){
			return Action.LOGIN;
		}else if((stu != null)&&(adm == null)){
			return invocation.invoke();		
		}else{
			return Action.LOGIN;
		}
*/
	}
}
