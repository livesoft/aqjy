package henu.bean;

public class LearnLogPinformation extends Bean{
	private Integer llid;   //ID
	private String idcard;  //用户身份证号
	private String starttime;   //浏览的当前时间.
	private String studytime;   //学习时长
	private String title;       //学习的标题.
	private String realname;    //姓名
	private String companyname;  //公司名字
	public Integer getLlid() {
		return llid;
	}
	public void setLlid(Integer llid) {
		this.llid = llid;
	}
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getStudytime() {
		return studytime;
	}
	public void setStudytime(String studytime) {
		this.studytime = studytime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	
	

}
