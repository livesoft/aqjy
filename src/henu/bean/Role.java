package henu.bean;
/**
 * 用于前台显示Role
 * 03-20
 * @author wuhaifeng
 * */
public class Role{
	
	private String rolename; //权限名字
	private String desc;//权限名称
	private String remark;//权限备注
	private Integer roleid;//权限备注
	private String status;//权限备注
	//权限名称
	private String xinxi="false";
	private String yonghu="false";
	private String huiyi="false";
	private String tongzhi="false";
	private String jiaoyu="false";
	private String kaohe="false";
	private String hudong="false";
	private String xitong="false";
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getRoleid() {
		return roleid;
	}
	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getXinxi() {
		return xinxi;
	}
	public void setXinxi(String xinxi) {
		this.xinxi = xinxi;
	}
	public String getYonghu() {
		return yonghu;
	}
	public void setYonghu(String yonghu) {
		this.yonghu = yonghu;
	}
	public String getHuiyi() {
		return huiyi;
	}
	public void setHuiyi(String huiyi) {
		this.huiyi = huiyi;
	}
	public String getTongzhi() {
		return tongzhi;
	}
	public void setTongzhi(String tongzhi) {
		this.tongzhi = tongzhi;
	}
	
	public String getJiaoyu() {
		return jiaoyu;
	}
	public void setJiaoyu(String jiaoyu) {
		this.jiaoyu = jiaoyu;
	}
	public String getKaohe() {
		return kaohe;
	}
	public void setKaohe(String kaohe) {
		this.kaohe = kaohe;
	}
	public String getHudong() {
		return hudong;
	}
	public void setHudong(String hudong) {
		this.hudong = hudong;
	}
	public String getXitong() {
		return xitong;
	}
	public void setXitong(String xitong) {
		this.xitong = xitong;
	}
	@Override
	public String toString() {
		return "Role_Front [rolename=" + rolename + ", status = "+status+ ", desc=" + desc
				+", roleid = "+roleid +", remark=" + remark + ", xinxi=" + xinxi + ", yonghu="
				+ yonghu + ", huiyi=" + huiyi + ", tongzhi=" + tongzhi
				+ ", jiaoyu=" + jiaoyu + ", kaohe=" + kaohe + ", hudong="
				+ hudong + ", xitong=" + xitong + "]";
	}
}
