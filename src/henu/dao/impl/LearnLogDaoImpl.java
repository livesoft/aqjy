package henu.dao.impl;

import henu.bean.LearnLog;
import henu.bean.LearnLogPinformation;
import henu.dao.LearnLogDao;
import henu.dao.factory.DaoFactory;
import henu.util.Dbcp;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.omg.PortableInterceptor.SUCCESSFUL;

public class LearnLogDaoImpl implements LearnLogDao{

	
	

	@Override
	/*public List<LearnLog> findAll(String order, String sort) {
		List<LearnLog> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "select * from LEARNLOG order by " + order +" " + sort + "";
		try {
			list = runner.query(sql, new BeanListHandler<LearnLog>(LearnLog.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(list.size() +"\t\t\t\t\t12312");
		return list;
	}
*/
	public List<LearnLogPinformation> findAll(String order, String sort) {
		List<LearnLogPinformation> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "select * from LEARNLOG order by " + order +" " + sort + "";
		try {
			list = runner.query(sql, new BeanListHandler<LearnLogPinformation>(LearnLogPinformation.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	/*	System.out.println(list.size() +"\t\t\t\t\t12312");*/
		return list;
	}
	@Override
	public List<LearnLogPinformation> findByProperty(String property, String key,
		String order, String sort, int start, int end,String companyname) {
		List<LearnLogPinformation> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "SELECT * FROM (SELECT tt.*, ROWNUM ro FROM (select * from VIEW_LEARNLOG_PINFO_COMPANY where "+property+"='"+key+"' and companyname='"+companyname+"'  order by "+order+" "+sort+") tt WHERE ROWNUM <="+end+") WHERE ro > "+start+"";
		try {
			list = runner.query(sql, new BeanListHandler<LearnLogPinformation>(LearnLogPinformation.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		/*System.out.println("");*/
		return list;
	}

	
	public LearnLogPinformation findById(String id){
		LearnLogPinformation lp = null;
		String sql = "select * from VIEW_LEARNLOG_PINFO_COMPANY where idcard=?";
		//
		QueryRunner runner  = DaoFactory.getRunner();
		try {
			lp = runner.query(sql, new BeanHandler<LearnLogPinformation>(LearnLogPinformation.class),id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*输出所有System.out.println("impl"+lp);*/
		return lp;
		
	}
	@Override
	public int update(String id, String title,String starttime) {
		// TODO Auto-generated method stub
		int result=0;//初始化result
		
		//进行更新title starttime 的值，从前台用户调用这些值
		String sql="UPDATE LEARNLOG SET TITLE=?,STARTTIME=? WHERE idcard =?";
		QueryRunner qr =new QueryRunner(Dbcp.getDataSource());
		//获取到的三个数据更新到数组里面
		Object[] params={title,starttime,id};
		try {
			result =qr.update(sql,params);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	
		return result;
	}
	@Override
	public void add(String idcard, String title, String starttime) {
		QueryRunner runner = DaoFactory.getRunner();
		int result = 0;
		String sql = "INSERT INTO LEARNLOG (idcard,starttime,studytime,title) VALUES(?,?,?,?)";
		Object[] params = {idcard,starttime,"",title};
		try {
			result = runner.update(sql,params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	//学习记录的批量删除
	@Override
	public int[] batchDelete(Object[][] params) {
		int[] results=null;
		QueryRunner qr=new QueryRunner();
		Connection conn=null;
		try {
			
			conn = Dbcp.getConnection();
			conn.setAutoCommit(false);//放置提交失败自动提交
			String sql = "DELETE FROM LEARNLOG where llid=?";
			results=qr.batch(conn, sql, params);
			conn.commit();//提交
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				conn.rollback();//进行回滚
				conn.commit();//再次提交
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return results;
	}
	
	//学习记录从idcard中获取
	public int findById2(String id){
		int result = 0;
		String sql = "select * from LEARNLOG where idcard=?";
		//
		QueryRunner runner  = DaoFactory.getRunner();
		try {
			result = runner.update(sql,id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*输出所有System.out.println("impl"+lp);*/
		return result;
	}
	
	

	
	

}
