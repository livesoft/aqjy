<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="pageFormContent nowrap"><!-- onsubmit="return validateCallback(this, navTabAjaxDone);" -->
	<form method="post" action="/aqjy/public/certification_jump"class="pageForm required-validate"  onsubmit="return navTabSearch(this);">
		<div class="pageFormContent" layoutH="56" style="width:700px;height:600px;float:left">
			<dl>
				<dt>姓名：</dt>
				<dd><input type="text"  value="${p.realName}" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>性别：</dt>
				<dd><input type="text"  value="${p.sex }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>身份证号：</dt>
				<dd><input type="text"   value="${p.idCard }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>出生年月：</dt>
				<dd><input type="text"  name="c.birthday" value="${c.birthday }" readonly="readonly" ></dd>
			</dl>
			<dl>
				<dt>住址：</dt>
				<dd><input  type="text" name="c.address" value="${c.address }" readonly="readonly"/></dd>
			</dl>			
			<dl>
				<dt>资格证号：</dt>
				<dd><input type="text"  name="c.cnid" value="${c.cnid }" readonly="readonly"/></dd>		
			</dl>
			<dl>
				<dt>发证时间：</dt>
				<dd><input type="text"  name="c.issuedate" value="${c.issuedate }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>证件有效期：</dt>
				<dd><input type="text" name="c.validation" value="${c.validation }" readonly="readonly" /></dd>
			</dl>
			<dl>
				<dt>初次发证时间：</dt>
				<dd><input type="text"  name="c.firstdate" value="${c.firstdate}" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>从业资格类别：</dt>
				<dd><input type="text"  name="c.type" value="${c.type }" readonly="readonly"></dd>
			</dl>
			
			<dl>
				<dt>发证单位：</dt>
				<dd><input type="text" name="c.issueunit" value="${c.issueunit}" readonly="readonly" /></dd>
			</dl>
			<dl>
				<dt>服务单位：</dt>
				<dd><input type="text" name="c.serviceunit" value="${c.serviceunit }" readonly="readonly" /></dd>
			</dl>
			</div>
			<div style="position: absolute;top:10px;right:20px"><img alt="复印件" src="../${c.copyurl}" width="450px" height="280px"  ></div>
		<div class="formBar">
			<ul>
				<li>
					<div class="button"><div class="buttonContent"><button type="submit">编辑信息</button></div></div>
				</li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">关闭</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>
