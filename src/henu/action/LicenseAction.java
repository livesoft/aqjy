package henu.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import henu.bean.LiPinfomation;
import henu.bean.License;
import henu.bean.ManagePerson;
import henu.bean.PInformation;
import henu.dao.LicenseDao;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.CONSTANTS;
import henu.util.Page;

import com.opensymphony.xwork2.ActionSupport;

public class LicenseAction extends ActionSupport{
			//分页查询
			Page page;
			private int pageNum=1;
			private int numPerPage=10;
			private String orderField="firsttime";
			private String orderDirection="desc";
			
			//检索
			private String property="-1";
			private String key="-1";
			
			private AJAX ajax;
			private License li;
			private String url="/admin/ajax.jsp";
			private String uid;
			private List<LiPinfomation> list;
			
			
			private File lphoto; //上传的文件
		    private String lphotoFileName; //文件名称
			
			public Page getPage() {
				return page;
			}
			public void setPage(Page page) {
				this.page = page;
			}
			public int getPageNum() {
				return pageNum;
			}
			public void setPageNum(int pageNum) {
				this.pageNum = pageNum;
			}
			public int getNumPerPage() {
				return numPerPage;
			}
			public void setNumPerPage(int numPerPage) {
				this.numPerPage = numPerPage;
			}
			
			public String getOrderField() {
				return orderField;
			}
			public void setOrderField(String orderField) {
				this.orderField = orderField;
			}
			public String getOrderDirection() {
				return orderDirection;
			}
			public void setOrderDirection(String orderDirection) {
				this.orderDirection = orderDirection;
			}
			public String getProperty() {
				return property;
			}
			public void setProperty(String property) {
				this.property = property;
			}
			public String getKey() {
				return key;
			}
			public void setKey(String key) {
				this.key = key;
			}
			public AJAX getAjax() {
				return ajax;
			}
			/*public void setAjax(AJAX ajax) {
				this.ajax = ajax;
			}*/
			public License getLi() {
				return li;
			}
			public void setLi(License li) {
				this.li = li;
			}
			public String getUrl() {
				return url;
			}
			/*public void setUrl(String url) {
				this.url = url;
			}*/
			public String getUid() {
				return uid;
			}
			public void setUid(String uid) {
				this.uid = uid;
			}
		public List<LiPinfomation> getList() {
				return list;
			}
			public void setList(List<LiPinfomation> list) {
				this.list = list;
			}
			
			public File getLphoto() {
				return lphoto;
			}
			public void setLphoto(File lphoto) {
				this.lphoto = lphoto;
			}
			public String getLphotoFileName() {
				return lphotoFileName;
			}
			public void setLphotoFileName(String lphotoFileName) {
				this.lphotoFileName = lphotoFileName;
			}
			//功能1：所有的驾驶证信息的列表列出全部
			//功能2：列出属于自己公司的详细信息：方法为建立一个视图（VIEW_LI_PINFO）联合两个表。同一个身份证号。确立一个公司的ID，COMID来把对应的公司的comid查到匹配到相应的用户
			public String findAll(){
				ManagePerson m=(ManagePerson) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_ADMIN);
				page = new Page();
				if(orderField.equals("")||orderDirection.equals("")){
					orderField="firsttime";
					orderDirection="asc";
				}
				if(property.equals("")||key.equals("")){
					property="comid";
					key=Integer.toString(m.getComid()); //获取变值
				}
				page.setSql("select count(*) from VIEW_LI_PINFO where "+property+"='"+key+"' and comid="+m.getComid()+"",pageNum,numPerPage);
				
				list = DaoFactory.getLicenseDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd(),m.getComid());
				url="/admin/licenseManage.jsp";
				return SUCCESS;
			}
			
			
			//个人驾驶证信息的增加及图片的上传，以及路径的读取。命名规则为身份证号及后缀。
			public String add() throws Exception{
				String realPath=ServletActionContext.getRequest().getRealPath("/upload/licenseImages");
				/*System.out.println(realPath);*/ //此注释为测试所用，打印图片的绝对路径。
				InputStream in=new FileInputStream(lphoto);
				String fileType=lphotoFileName.substring(lphotoFileName.lastIndexOf("."));//截取后缀
				String fileName=li.getIdcard()+fileType;//文件名为身份证号+后缀
				//之间的间隔用\\隔开
				FileOutputStream out=new FileOutputStream(realPath+"\\"+fileName);
				byte[] buffer=new byte[1024]; 
				int len=0;
				while((len=in.read(buffer))>0){
					out.write(buffer, 0, len);
				}
				out.close();
				in.close();
				//对图片路径的设定
				li.setLphoto("upload/licenseImages/"+fileName);
				
				int result=DaoFactory.getLicenseDaoInstance().save(li);
				ajax = new AJAX();
				
				/*前台添加的方法，用户可以对自己驾驶证信息进行添加，如果已经添加过了了，将会提示添加失败。不允许用户再次添加
				 * 必须把所有的驾驶证信息删除过后，再次进行添加。防止用户随意操作
				 * */
				if(result!=0){
					ajax.setStatusCode("200");
					ajax.setMessage("添加成功");
					ajax.setCallbackType("closeCurrent");
					ajax.setNavTabId("le");
				}else{
					ajax.setStatusCode("300");
					ajax.setMessage("添加失败");
				}
				url="/front/ajax.jsp";
				return SUCCESS;

			}
			public String update() throws Exception{
				/*
				String realPath=ServletActionContext.getRequest().getRealPath("/upload/licenseImages");
				System.out.println(realPath); //此注释为测试所用，打印图片的绝对路径。
				System.out.println(lphoto);
				InputStream in=new FileInputStream(lphoto);
				String fileType=lphotoFileName.substring(lphotoFileName.lastIndexOf("."));//截取后缀
				String fileName=li.getIdcard()+fileType;//文件名为身份证号+后缀
				//之间的间隔用\\隔开
				FileOutputStream out=new FileOutputStream(realPath+"\\"+fileName);
				byte[] buffer=new byte[1024]; 
				int len=0;
				while((len=in.read(buffer))>0){
					out.write(buffer, 0, len);
				}
				out.close();
				in.close();
				//对图片路径的设定
				li.setLphoto("upload/licenseImages/"+fileName);*/
				
				//对用户的驾驶证信息进行更新操作
				
				int result = DaoFactory.getLicenseDaoInstance().update(uid, li);
				
				//获取前台的UID。来选择一个信息进行更新。修改
				 
				if(result>0)
				{
					HandleMsg("200","更新成功","le","closeCurrent");
				}else{
					HandleMsg("300","更新失败","le","closeCurrent");
				}
				return SUCCESS;
			}
			
			//查询更改。从数据库中读取到这个相应的数据。然后跳到更新界面。进行修改
			public String find(){
				LicenseDao dao = DaoFactory.getLicenseDaoInstance(); 
				li = dao.findById(uid);
				url="/admin/licenseUpdate.jsp";
				return SUCCESS;
				
			}
			//删除内容
			//删除：后台管理，以及用户都可拥有对驾驶证信息的删除操作。防止用户添加的照片不符合要求。而不删除，后台管理员可以进行操作。以达到界面的完整性
			public String delete(){
				int result = DaoFactory.getLicenseDaoInstance().delete(uid);
				if(result>0)
				{
					HandleMsg("200","删除成功","le","");
				}else{
					HandleMsg("300","删除失败","le","");
				}
				return SUCCESS;
			}
			//此方法为对驾驶证信息的详细操作。用于管理员对所有的驾驶证信息进行方便的操作。对信息的管理更加方便
			public String findDetail(){
				LicenseDao dao = DaoFactory.getLicenseDaoInstance();
				li = dao.findById(uid);
				url="/admin/licenseDetail.jsp";
				return SUCCESS;
			}
			//用于判断用户是否第一次登录此系统。如果第一次登陆，提示添加驾驶证信息，第二次登陆后则直接显示已添加驾驶证信息
			public String findLicense(){
				//从session里面获取值
				PInformation P = (PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
				li = DaoFactory.getLicenseDaoInstance().findById(P.getIdCard());
				url="/front/licenseManager.jsp";
				return SUCCESS;
				
			}
			private void HandleMsg(String statusCode, String msg, String tabid,String callback) {
				ajax =  new AJAX();
				ajax.setCallbackType(callback);
				ajax.setMessage(msg);
				ajax.setStatusCode(statusCode);
				ajax.setNavTabId("le");
				
			}
			
			
			

}
