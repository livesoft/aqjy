package henu.dao;

import java.util.List;

import henu.bean.*;

public interface SeminarDao{
	/**
	 * 于晨
	 * @param s
	 * @return
	 */
	public int save(Seminar se);
		/**
		 * 保存专题
		 * @param sId
		 * @return
		 */
	
	public int update(int seid,Seminar se);
	
	/**
	 * 查找所有专题，按order字段排序，排序方式为sort，即ASC和DESC
	 * @param order
	 * @param sort
	 * @param start
	 * @param end
	 * @return
	 */
	public List<Seminar> findAll(String order, String sort,String property,String key);
		/**
		 * 模糊查找
		 * 
		 * @param property 字段名
		 * @param key 字段值
		 * @param order 排序字段
		 * @param sort 排序方式
		 * @param start
		 * @param end
		 * @return
		 */
	public List<Seminar> findByProperty(String property,String key,String order,
			String sort,int start,int end);
	
	public Seminar findById(int id);
	/*
	 * 删除
	 */
	public int delete(int seId);
	
	
/*	public List<Seminar> findByName(String subjectname);*/
	/**
	 * 专题下拉列表
	 * @return
	 */
	public List<Seminar> findSubjectName();
	
}