package henu.action;

import com.opensymphony.xwork2.ActionSupport;
/**
 * Action的基本类
 * @author 梁胜斌
 * @author 
 * 2014-11-02
 *
 */
public class BaseAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	public String execute()
	{
		return SUCCESS;
	}
}
