<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<form id="pagerForm" method="post" action="/aqjy/admin/allAdmin">
	<input type="hidden" name="status" value="${param.status}"> <input
		type="hidden" name="keywords" value="${param.keywords}" /> <input
		type="hidden" name="pageNum" value="1" /> <input type="hidden"
		name="numPerPage" value="${page.numPerPage}" /> <input type="hidden"
		name="orderField" value="${param.orderField}" /> <input type="hidden"
		name="orderDirection" value="${param.orderDirection}" /> <input
		type="hidden" name="property" value="${property}"> <input
		type="hidden" name="key" value="${key}">
</form>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);"
		action="/aqjy/ManagePereson/manageperson_findAll" method="post">
		<div class="searchBar">
			<table class="searchContent">
				<tr>
					<td>分类</td>
					<td><select class="combox" name="property">
							<option value="">根据名称查询</option>
							<option value="maid">管理者ID</option>
							<option value="phone">手机号</option>
							<option value="realName">真实姓名</option>
							<option value="vin">公司ID</option>
					</select></td>
					<td>要查询的值： <input type="text" value="" name="key">
					</td>
				</tr>
			</table>
			<div class="subBar">
				<ul>
					<li><div class="buttonActive">
							<div class="buttonContent">
								<button type="submit">检索</button>
							</div>
						</div></li>
				</ul>
				<div style="color: red; margin-top: 5px;">注意:点击标题可以进行排序，检索模式下若想查询所有信息，再点击一次检索按钮即可</div>
			</div>
		</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="delete"
				href="/aqjy/admin/delAdmin?args={maid}"
				target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit"
				href="/aqjy/admin/findAdmin?args={maid}" target="navTab"><span>修改</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138"
		style="text-align: center;">
		<thead>
			<tr>
				<th>真实姓名</th>
				<th>手机号</th>
				<th orderField="maid"
					<c:if test='${orderField == "maid" }'> class="${orderDirection}"  </c:if>>身份证号</th>
				<th>公司</th>
				<th>角色</th>
				<th>状态</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="mp" items="${list_admin}">
				<tr target="maid" rel="${mp.maid}">
					<td>${mp.realName}</td>
					<td>${mp.phone}</td>
					<td>${mp.maid}</td>
					<td>${mp.comname}</td>
					<td>${mp.rolename}</td>
					<td>${mp.audio}</td>
				</tr>
			</c:forEach>

		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span> <select class="combox" name="numPerPage"
				onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="20">20</option>
					<option value="50">50</option>
				</c:if>
				<c:if test="${page.numPerPage eq '20' }">
					<option value="10">10</option>
					<option value="20" selected="selected">20</option>
					<option value="50">50</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="50" selected="selected">50</option>
				</c:if>
			</select> <span>条，共${page.totalCount}条</span>
		</div>

		<div class="pagination" targetType="navTab"
			totalCount="${page.totalCount}" numPerPage="${page.numPerPage}"
			pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>
	</div>
</div>
