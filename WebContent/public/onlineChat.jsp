<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>咨询</title>
<style type="text/css">
	#page{
		width:1000px;
		text-align: center;
		background-color: #f2f2f2;
		height: 50px;
		line-height: 50px;
	}
	#page a{
		text-decoration:none;
		color:#red;
	}
	#page a:HOVER{
		text-decoration:none;
		color:red;
	}
	#main{width:1000px;margin: auto;color:#2e3231;font-size: 13px;}
	#messageTop{width:1000px;height:29px;background-color:#ebfff9;font-weight: bold;font-size: 14px;line-height: 29px;padding-left:5px;font-family:Microsoft Yahei;color: #000080;margin-bottom: 30px;}
	#messageTop a{font-family:Microsoft Yahei;color: #000080;font-size: 14px;text-decoration: none;}
	#messageTop a:HOVER{color: red;}
	.total{width:800px;margin: auto;border: 2px solid #e9f8fb;margin-bottom: 20px;margin-top: 10px;background-color: white;font-weight: bold;font-family: Microsoft Yahei;}
	.top{width:800px;height: 30px;background-color:#89dfaa;line-height: 30px; }
	.flag1{color:#535252;float: left;}
	.flag2{color:#535252;float: left;}
	.name{float:left;margin-left: 20px;}
	.college{float: left;margin-left: 50px;}
	.isTop{color:#ff3534;float: right;margin-right: 10px;}
	.qTime{float: right;padding-right: 10px;}
	.title{width: 780px;height:27px;line-height:22px;background-color: #d3f9e1;padding-left: 20px;padding-top: 5px;}
	.qContents{width: 700px;margin-top: 20px;margin-left:60px;padding-bottom: 30px;}
	.rContents{width:700px;background-color:#d3f9e1;margin-left: 50px;margin-bottom: 20px;}
	.rContents1{width:690px;margin-top: 20px;margin-left: 10px;padding-top:10px;}
	.rTime{width:685px;height:20px;margin-top: 20px;color:#535252;text-align: right;padding-right: 15px;padding-bottom: 5px;}
</style>
</head>
<body>
<div id="main">
	<jsp:include page="head.jsp"></jsp:include>
	<div id="messageTop">所在位置：<a href="/aqjy/index.jsp">首页</a>>>咨询</div>
	<c:if test="${empty list }">
		<div style="width:1000px;height: 600px;line-height: 600px;text-align: center;color:red;font-weight: bold;">管理人员还没有回复任何信息，请耐心等待</div>
	</c:if>
	<c:if test="${not empty list }">
		<c:forEach var="o" items="${list }">
			<div class="total">
				<a name="${o.oid}" id="${o.oid }"></a>
				<div class="top">
					<div class="name"><div class="flag1">咨询者：</div>${o.askName}</div>
					<div class="college"><div class="flag1">咨询板块：</div>${o.range }</div>
					<div class="qTime"><div class="flag1">${o.askTime }</div></div>
					<c:if test="${o.isTop==1 }"><div class="isTop">置顶</div></c:if>
				</div>
				<div class="title"><div class="flag2">问题：</div>${o.title }</div>
				<div class="qContents">${o.askContent }</div>
				<div class="rContents">
					<div class="rContents1"><div class="flag2">答复：</div>${o.replyContent }</div>
					<div class="rTime">${o.replyTime}</div>
				</div>
			</div>
	</c:forEach>
	</c:if>
	<div id="page">
	 当前第[${page.pageNum }]页&nbsp;&nbsp;&nbsp;
    
    <c:if test="${page.pageNum>1}">
    <a href="/aqjy/public/findOnlineChatWithReply?pageNum=${page.pageNum-1 }">上一页</a>
    </c:if>
    &nbsp;
    <c:forEach var="pageNum" begin="${page.startPage}" end="${page.endPage}">
		[<a href="/aqjy/public/findOnlineChatWithReply?pageNum=${pageNum}">${pageNum }</a>]
	</c:forEach>  
	&nbsp;
	
	
	
	<c:if test="${page.pageNum<page.pageNumShown}">
	<a href="/aqjy/public/findOnlineChatWithReply?pageNum=${page.pageNum+1 }">下一页</a>
	</c:if>
	
	&nbsp;&nbsp;&nbsp;
	共[${page.pageNumShown }]页,共[${page.totalCount}]纪录,
	
	<input type="text" style="width: 30px" id="pageNum">
	<input type="button" value=" GO " onclick="goWhich(document.getElementById('pageNum'))">
	
	<script type="text/javascript">
		function goWhich(input){
			var pageNum = input.value;
			if(pageNum==null || pageNum==""){
				alert("请输入页码！");
				return;
			}
			
			if(!pageNum.match("\\d+")){
				alert("请输入数字！");
				input.value="";
				return;
			}
			
			if(pageNum<1 || pageNum>${page.pageNumShown}){
				alert("无效的页码！");
				input.value="";
				return;
			}
			window.location.href="/aqjy/public/findOnlineChatWithReply?pageNum=" + pageNum;
		}
	</script>
	</div>
	<jsp:include page="foot.jsp"></jsp:include>
</div>
</body>
</html>