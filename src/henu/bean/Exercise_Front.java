package henu.bean;

/**
 * 学生成绩管理 03-13
 * 
 * @author wuhaifeng
 * */
public class Exercise_Front {
	private String username;
	private String idCard;
	private Integer score;
	private String exam_name;
	private String sex ;
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getExam_name() {
		return exam_name;
	}

	public void setExam_name(String exam_name) {
		this.exam_name = exam_name;
	}

	@Override
	public String toString() {
		return "Exercise_Front [username=" + username + ", idCard=" + idCard
				+ ", score=" + score + ", exam_name=" + exam_name + "]";
	}

}
