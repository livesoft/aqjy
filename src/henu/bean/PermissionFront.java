package henu.bean;

public class PermissionFront implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer roleid ;//编号，自增
	private String name;
	private String desc;//权限名称
	private String mark;//权限备注
	private String status ="false";
	/*
	 * 权限名称
	 * */
	
	private String xinxi="false";
	private String yonghu="false";
	private String huiyi="false";
	private String tongzhi="false";
	private String jiaoyu="false";
	private String kaohe="false";
	private String hudong="false";
	private String xitong="false";
	public Integer getRoleid() {
		return roleid;
	}
	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getXinxi() {
		return xinxi;
	}
	public void setXinxi(String xinxi) {
		this.xinxi = xinxi;
	}
	public String getYonghu() {
		return yonghu;
	}
	public void setYonghu(String yonghu) {
		this.yonghu = yonghu;
	}
	public String getHuiyi() {
		return huiyi;
	}
	public void setHuiyi(String huiyi) {
		this.huiyi = huiyi;
	}
	public String getTongzhi() {
		return tongzhi;
	}
	public void setTongzhi(String tongzhi) {
		this.tongzhi = tongzhi;
	}
	public String getJiaoyu() {
		return jiaoyu;
	}
	public void setJiaoyu(String jiaoyu) {
		this.jiaoyu = jiaoyu;
	}
	public String getKaohe() {
		return kaohe;
	}
	public void setKaohe(String kaohe) {
		this.kaohe = kaohe;
	}
	public String getHudong() {
		return hudong;
	}
	public void setHudong(String hudong) {
		this.hudong = hudong;
	}
	public String getXitong() {
		return xitong;
	}
	public void setXitong(String xitong) {
		this.xitong = xitong;
	}
}
