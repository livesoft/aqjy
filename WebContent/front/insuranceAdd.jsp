<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="pageFormContent nowrap">
	<form method="post" enctype="multipart/form-data" action="/aqjy/public/insurance_add" class="pageForm required-validate" onsubmit="return iframeCallback(this,navTabAjaxDone);">
		<div class="pageFormContent" layoutH="56">
			<dl>
				<dt>车牌号：</dt>
				<dd><input name="insurance.vehiclelicense" class="required" type="text" value="${vehicle }" size="30"/></dd>
			</dl>
			<dl>
				<dt>录入时间：</dt>
				<dd><input type="text" size="30" name="insurance.enterdate" class="date"value="${insurance.enterdate }" /><a class="inputDateButton" href="javascript:;">选择</a></dd>	
			</dl>
			<dl>
				<dt>更新时间：</dt>
				<dd><input type="text" size="30" name="insurance.updatedate" class="date" value="${insurance.updatedate }"/><a class="inputDateButton" href="javascript:;">选择</a>	</dd>
			</dl>
			<dl>
				<dt>交强险单号：</dt>
				<dd><input name="insurance.tno" class="digits" type="text" size="30" value="${insurance.tno }"/></dd>
			</dl>	
			<dl>
				<dt>交强险文件：</dt>
				<dd><input type="file" name="trafficfile"/></dd>
			</dl>
			<dl>
				<dt>交强险开始时间：</dt>
				<dd><input type="text" name="insurance.trafficstart" class="date" value="${insurance.trafficstart}" /><a class="inputDateButton" href="javascript:;">选择</a>	</dd>
			</dl>
			<dl>
				<dt>交强险结束时间：</dt>
				<dd><input type="text" name="insurance.trafficend" class="date" value="${insurance.trafficend }" /><a class="inputDateButton" href="javascript:;">选择</a></dd>
			</dl>
			<dl>
				<dt>商业险单号：</dt>
				<dd><input name="insurance.ino" class="digits" type="text" size="30" value="${insurance.ino}"/></dd>
			</dl>
			<dl>
				<dt>商业险文件：</dt>
				<dd><input type="file" name="insurancefile"/></dd>
			</dl>
			<dl>
				<dt>商业险开始时间：</dt>
				<dd><input type="text" name="insurance.insurancestart" class="date" value="${insurance.insurancestart }"/><a class="inputDateButton" href="javascript:;">选择</a></dd>	
			</dl>
			<dl>
				<dt>商业险结束时间：</dt>
				<dd><input type="text" name="insurance.insuranceend" class="date" value="${insurance.insuranceend }"/><a class="inputDateButton" href="javascript:;">选择</a></dd>
			</dl>
			<!-- <dl>
				<dt>备注：</dt>
				<dd><input type="text" rows="4" cols="50" name="insurance.memo" /></dd>
			</dl> -->
			
			
			
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>
