package henu.dao.impl;

import henu.bean.CarMaintancePinfoCompany;
import henu.bean.Maintance;
import henu.dao.MaintanceDao;
import henu.dao.factory.DaoFactory;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class MaintanceDaoImpl implements MaintanceDao {

	@Override
	public int save(Maintance maintance) {
		int result = 0;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "INSERT INTO SEMINAR(carid,maintanceTime,maintanceCycle) VALUES(?,?,?)";
		Object[] params = {maintance.getCarid(),maintance.getMaintanceTime(),maintance.getMaintanceCycle()};
		try {
			result=runner.update(sql,params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		
	}

	@Override
	public int delete(String mid) {
		int result = 0 ;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "DELETE FROM maintance WHERE  mid = ?";
		try {
			result = runner.update(sql, mid);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public int update(String mid, Maintance maintance) {
		int result = 0 ;
		String sql = "UPDATE maintance SET maintanceCycle=?, maintanceTime=? WHERE mid = ?";
		Object[] params = { maintance.getMaintanceCycle(), maintance.getMaintanceTime(),mid};
		QueryRunner runner = DaoFactory.getRunner();
		try {
			result = runner.update(sql, params);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public CarMaintancePinfoCompany findById(String mid) {
		CarMaintancePinfoCompany maintance = new CarMaintancePinfoCompany();
		String sql = "SELECT * FROM view_car_main_pinfo_company WHERE mid = ?";
		QueryRunner runner = DaoFactory.getRunner();
		try {
			maintance = runner.query(sql,new BeanHandler<CarMaintancePinfoCompany>(CarMaintancePinfoCompany.class),mid);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return maintance;
	}

	@Override
	public List<CarMaintancePinfoCompany> findAll(String order, String sort, String property,
			String key) {
		List<CarMaintancePinfoCompany> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "SELECT * FROM view_car_main_pinfo_company WHERE " + property + " = ? "
				+ " ORDER BY  " + order + " " + sort;
		try {
			list = runner.query(sql, new BeanListHandler<CarMaintancePinfoCompany>(CarMaintancePinfoCompany.class));
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<CarMaintancePinfoCompany> findByProperty(String property, String key,
			String order, String sort, int start, int end) {
		List<CarMaintancePinfoCompany> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql="SELECT * FROM (SELECT tt.*, ROWNUM ro FROM (select * from view_car_main_pinfo_company where "+property+"='"+key+"'  order by "+order+" "+sort+") tt WHERE ROWNUM <="+end+") WHERE ro > "+start+"";
		try {
			list = runner.query(sql, new BeanListHandler<CarMaintancePinfoCompany>(CarMaintancePinfoCompany.class));
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		//System.out.println(sql);
		return list;
	}

	
}
