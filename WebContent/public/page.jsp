<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
 当前第[${page.pageNum }]页&nbsp;&nbsp;&nbsp;
    
    <c:if test="${page.pageNum>1}">
    <a href="/aqjy/public/findNoticeListByProperty?key=新闻公告&pageNum=${page.pageNum-1 }">上一页</a>
    </c:if>
    &nbsp;
    <c:forEach var="pageNum" begin="${page.startPage}" end="${page.endPage}">
		[<a href="/aqjy/public/findNoticeListByProperty?key=新闻公告&pageNum=${pageNum}">${pageNum }</a>]
	</c:forEach>  
	&nbsp;
	
	
	
	<c:if test="${page.pageNum<page.pageNumShown}">
	<a href="/aqjy/public/findNoticeListByProperty?key=新闻公告&pageNum=${page.pageNum+1 }">下一页</a>
	</c:if>
	
	&nbsp;&nbsp;&nbsp;
	共[${page.pageNumShown }]页,共[${page.totalCount}]纪录,
	
	<input type="text" style="width: 30px" id="pageNum">
	<input type="button" value=" GO " onclick="goWhich(document.getElementById('pageNum'))">
	
	<script type="text/javascript">
		function goWhich(input){
			var pageNum = input.value;
			if(pageNum==null || pageNum==""){
				alert("请输入页码！");
				return;
			}
			
			if(!pageNum.match("\\d+")){
				alert("请输入数字！");
				input.value="";
				return;
			}
			
			if(pageNum<1 || pageNum>${page.pageNumShown}){
				alert("无效的页码！");
				input.value="";
				return;
			}
			window.location.href="/aqjy/public/findNoticeListByProperty?key=新闻公告&pageNum=" + pageNum;
		}
	</script>
