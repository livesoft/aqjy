package henu.action;

import henu.bean.Dictionary;
import henu.dao.factory.DaoFactory;

import java.util.List;

import net.sf.json.JSONArray;

import com.opensymphony.xwork2.ActionSupport;

//此类事数据字典的表
public class DictionaryAction extends ActionSupport{
	private String result;
	private String table;
	private String name;
	
	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		List<Dictionary> list=DaoFactory.getDictionaryDaoInstance().getDictionary(table, name);
		JSONArray json=JSONArray.fromObject(list);
		result=json.toString();
		return SUCCESS;
	}
	

}
