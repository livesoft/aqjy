<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>注册</title>
<style type="text/css">
	#reTop{width:900px;height:29px;background-color:#dce3ee;font-weight: bold;font-size: 14px;line-height: 29px;padding-left:5px;font-family:Microsoft Yahei;color: #132b55;margin-bottom: 10px;}
	#reTop a{font-family:Microsoft Yahei;color: #132b55;font-size: 14px;text-decoration: none;}
	#reTop a:HOVER{color: red;}
	.reMain{width: 800px;/* background-color: gray; */margin-left: auto;margin-right: auto;}
	.reMP{margin-left: 100px;margin-top: 25px;}
	.reMTable{width: 500px;margin-left: 150px;display: block;text-align: left;font-size: 14px;color: #262626;}
	.reS1{color: #5a5a5a;font-size: 12px;}
	.reMTable td{height: 30px;}
	.reMTable td div{color:red;}
	.reMT{width: 480px;height: 200px;margin-left: 130px;}
	.reMText{width: 480px;height: 180px;margin-top: 20px;color: #767676;font-size: 13px;}
	.reMBtn{text-align: center;width: 600px;height:50px;margin-left: 260px;margin-top: 20px;margin-bottom: 30px;}
	.reBTN1{border:0px;width: 85px;height: 32px;background-image: url("../images/reBTN1.png");cursor: pointer;}
	.reBTN2{border:0px;width: 85px;height: 32px;background-image: url("../images/reBTN2.png");cursor: pointer;}
	.reMBtn a{font-size: 14px; display: block;float: left;height: 50px;line-height:70px;margin-left: 10px;}
</style>
<script type="text/javascript" src="../js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="../js/register.js"></script>
<link href="../style/register.css" rel="stylesheet" type="text/css">
</head>
<body>
<jsp:include page="head.jsp"></jsp:include>
<div id="reTop">所在位置：<a href="/aqjy/index.jsp">首页</a>>>注册</div>
<div class="reMain">
<img class="reMP" alt="" src="../images/reT1.png">
<form action="/aqjy/public/register" enctype="multipart/form-data" method="post">
	<table class="reMTable">
		<colgroup>
			<col width="130px" >
			<col width="200px" >
			<col width="170px">
		</colgroup>
		<tr>
			<td colspan="3" align="left"><span class="reS1">注：标记有红色*为必填项目</span></td>
		</tr>
		<tr>
			<td>真实姓名:</td>
			<td><input type="text" name="p.realName" id="realName" maxlength="30"></td>
			<td><div id="error1">*</div></td>
		</tr>
		<tr>
			<td>身份证号:</td>
			<td><input type="text" name="p.idCard" id="idCard"></td>
			<td><div id="error2">*</div></td>
		</tr>
		<tr>
			<td>密码:</td>
			<td><input type="password" name="p.password" id="password"></td>
			<td><div id="error3">*</div></td>
		</tr>
		<tr>
			<td>确认密码:</td>
			<td><input type="password" id="confirmPassword"></td>
			<td><div id="error4">*</div></td>
		</tr>
		<tr>
			<td>手机号码:</td>
			<td><input type="text" name="p.phone" id="phone"></td>
			<td><div id="error5">*</div></td>
		</tr>
		<tr>
			<td>所属公司:</td>
			<td>
				<select name="p.comid" id="comid">
					
				</select>
			</td>
			<td><div>*</div></td>
		</tr>
		<tr>
			<td>注册类型:</td>
			<td>
				<input type="radio" name="p.regType" value="司机" checked="checked">司机
				<input type="radio" name="p.regType" value="车主">车主
			</td>
			<td><div>*</div></td>
		</tr>
		<tr>
			<td>身份证正面照片:</td>
			<td><input type="file" name="photo"></td>
			<td><div id="error6">*</div></td>
		</tr>
		<tr>
			<td>性别:</td>
			<td>
				<input type="radio" name="p.sex" value="男" checked="checked">男
				<input type="radio" name="p.sex" value="女">女
			</td>
			<td><div>*</div></td>
		</tr>
		<tr>
			<td>密保问题:</td>
			<td>
				<select name="p.question">
					<option value="对您人生影响最大的是谁">对您人生影响最大的是谁</option>
					<option value="您最敬仰的历史名人是谁">您最敬仰的历史名人是谁</option>
					<option value="您最欣赏的电影明星是谁">您最欣赏的电影明星是谁</option>
					<option value="您最想上哪里去旅游">您最想上哪里去旅游</option>
					<option value="您最喜欢的课程名">您最喜欢的课程名</option>
					<option value="您最喜欢的动物">您最喜欢的动物</option>
					<option value="您牢记的座右铭">您牢记的座右铭</option>
				</select>
			</td>
			<td><div>*</div></td>
		</tr>
		<tr>
			<td>密保答案:</td>
			<td><input type="text" name="p.answer" id="answer"></td>
			<td><div id="error7">*</div></td>
		</tr>
		<tr>
			<td>家庭住址</td>
			<td><input type="text" name="p.address"></td>
			<td></td>
		</tr>
		<tr>
			<td>邮箱:</td>
			<td><input type="text" name="p.email" id="email"></td>
			<td><div id="error8"></div></td>
		</tr>
	</table>
	<img class="reMP" alt="" src="../images/reT2.png">
	<div class="reMT">
		<textarea class="reMText" rows="" cols="" readonly="readonly">   在您申请注册安全教育系统用户时，即视为您完全同意本系统使用须知，而且知悉
且同意本注册用户特别须知。
一、注册成功后，即成为本系统的注册用户。对于您所享有的用户名和密码的安全您
应自行负责。
二、注册用户对以其用户名进行的任何活动或事件，应自行负责。
三、对本网站任何栏目的使用，注册用户应对其网上言论或发布内容的真实性、准确
性、合法性自行负责，对此不承担任何保证责任。</textarea>
	</div>
	<div class="reMBtn">
		<div style="float: left;"><input type="button" value="" id="sub" class="reBTN1">
		<input type="button" value="" class="reBTN2"></div>
		<a href="/aqjy/public/resetPassword.jsp">找回密码</a>
	</div>
</form>
</div>
<jsp:include page="foot.jsp"></jsp:include>
</body>
</html>