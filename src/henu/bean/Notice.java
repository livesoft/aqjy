package henu.bean;
/**
 * 基础Bean，其他Bean可继承与Bean
 * @author 曹路
 * @author
 * 2015-1-20
 *
 */
public class Notice extends Bean {
	private int nid;//自增主键
	private String title;//公告标题
	private String type;//公告类别
	private String content;//公告内容
	private int viewNumbers;//浏览次数
	private String publishTime;//发布时间
	private String publisher;//发布人
	private int isTop;//是否置顶0代表不置顶
	private String remark;//备注
	public int getNid() {
		return nid;
	}
	public void setNid(int nid) {
		this.nid = nid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getViewNumbers() {
		return viewNumbers;
	}
	public void setViewNumbers(int viewNumbers) {
		this.viewNumbers = viewNumbers;
	}
	public String getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	public int getIsTop() {
		return isTop;
	}
	public void setIsTop(int isTop) {
		this.isTop = isTop;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
