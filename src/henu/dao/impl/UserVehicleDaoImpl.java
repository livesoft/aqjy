package henu.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import henu.bean.UserVehicle;
import henu.dao.UserVehicleDao;
import henu.dao.factory.DaoFactory;
import henu.util.Dbcp;

public class UserVehicleDaoImpl implements  UserVehicleDao  {

	
/*	public static void main(String[] args) {
		UserVehicleImpl uv=new UserVehicleImpl();
		uv.Save(new UserVehicle());
	}*/
	/* (non-Javadoc)
	 * @see henu.dao.impl.UserVehicleDao#Save(henu.bean.UserVehicle)
	 */
	@Override

	public int Save(UserVehicle UserVehicle) {
		// TODO Auto-generated method stub
		QueryRunner runner = new QueryRunner(Dbcp.getDataSource());
		int result = 0;
		String sql = "INSERT INTO USERVEHICLE VALUES(?,?,?,?)";
		Object[] params ={null,UserVehicle.getIdcard(),UserVehicle.getCarid(),UserVehicle.getMemo()};
		try {
			result = runner.update(sql, params);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see henu.dao.impl.UserVehicleDao#delete(int)
	 */
	
	@Override
	public int delete(int uvid) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM USERVEHICLE WHERE uvid=?";
		int result = 0;
		QueryRunner runner = new QueryRunner(Dbcp.getDataSource());
		try {
			result = runner.update(sql, uvid);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
		
	}

	/* (non-Javadoc)
	 * @see henu.dao.impl.UserVehicleDao#Update(henu.bean.UserVehicle)
	 */
	
	@Override
	public int Update(UserVehicle UserVehicle) {
		// TODO Auto-generated method stub
		
		QueryRunner runner = new QueryRunner(Dbcp.getDataSource());
		int result = 0;
		String sql = "UPDATE USERVEHICLE SET idcard=?,carid=?,memo=?  WHERE uvid=?";
		System.out.println(UserVehicle.getUvid());
		Object[] params = {UserVehicle.getIdcard(),UserVehicle.getCarid(),UserVehicle.getMemo(),UserVehicle.getUvid()};
		try {
			result = runner.update(sql, params);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return result;
	}
	

	/* (non-Javadoc)
	 * @see henu.dao.impl.UserVehicleDao#findById(int)
	 */
	
	@Override
	public UserVehicle findById(int uvid) {
		UserVehicle UserVehicle = null;
		String sql = "select * from USERVEHICLE where uvid=?";
		QueryRunner runner = new QueryRunner(Dbcp.getDataSource());
		try {
			UserVehicle = runner.query(sql,	new BeanHandler<UserVehicle>(UserVehicle.class), uvid);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return UserVehicle;
	}

	/* (non-Javadoc)
	 * @see henu.dao.impl.UserVehicleDao#findAll(java.lang.String, java.lang.String)
	 */
	
	@Override
	public List<UserVehicle> findAll(String order, String sort) {
		// TODO Auto-generated method stub
		List<UserVehicle> list = new ArrayList<UserVehicle>();
		QueryRunner qr = DaoFactory.getRunner();
		String sql = " select * from USERVEHICLE order by "+order+" "+sort+" ";
		try {
			list = qr.query(sql, new BeanListHandler<UserVehicle>(UserVehicle.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;

	}

	/* (non-Javadoc)
	 * @see henu.dao.impl.UserVehicleDao#findByProperty(java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, int)
	 */
	
	@Override
	public List<UserVehicle> findByProperty(String property, String key,
			String order, String sort, int start, int end) {
		// TODO Auto-generated method stub
		List<UserVehicle> list=null;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="SELECT * FROM (SELECT tt.*, ROWNUM ro FROM (select * from USERVEHICLE where "+property+"='"+key+"'  order by "+order+" "+sort+") tt WHERE ROWNUM <="+end+") WHERE ro > "+start+"";
			list = qr.query(sql, new BeanListHandler<UserVehicle>(UserVehicle.class));
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 	
		return list;
	
	}
/*	public static void main(String[] args) {
		UserVehicle uv= new UserVehicle();
		uv=DaoFactory.getUserVehicleDaoInstance().findById(10);
		uv.setMemo("123123213");
		DaoFactory.getUserVehicleDaoInstance().Update(uv);
	}*/
}
