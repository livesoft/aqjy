
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
  <%@taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>  
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
<form id="pagerForm" method="post" action="/aqjy/usertotalview/Car_Ffind">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${page.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" /> 
    <input type="hidden" name="orderDirection"  value="${param.orderDirection}" />  
    <input type="hidden" name="property"  value="${property}">
    <input type="hidden" name="key"  value="${key}">
</form>


<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="/aqjy/usertotalview/Car_Ffind" method="post">
	<div class="searchBar">

		<table class="searchContent">
			<tr>
				<td>
					分类
				</td>
				<td>
              
					<select class="combox" name="property" >
							<option value="" >根据名称查询</option>
							<option value="carid">车辆号</option>
							<option value="vehicleType">车牌号</option>
							<option value="userid">车主身份证号</option>
							<option value="vin">车辆识别码</option>												
					</select>
				</td>
				<td>
					要查询的值：
						<input type="text" value="" name="key">			
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
			</ul>
			<div style="color:red;margin-top:5px;">注意:点击标题可以进行排序，检索模式下若想查询所有信息，再点击一次检索按钮即可</div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="/aqjy/front/carAdd.jsp" target="navTab"><span>添加</span></a></li>
			<li><a class="delete" href="/aqjy/usertotalview/Car_delete?uid={carid}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="/aqjy/usertotalview/Car_Fsfind?uid={carid}" target="navTab"><span>修改</span></a></li>
			<li class="line">line</li>
			<li><a class="icon" href="/aqjy/usertotalview/Car_export" target="dwzExport" targetType="navTab" title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li>
			<li class="line">line</li>		
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" style="text-align: center;">
		<thead>
			<tr>
				<th  orderField="carid" <c:if test='${orderField == "carid" }'> class="${orderDirection}"  </c:if>   >车辆编号</th> 		
				<th>车牌号</th>
				<th>集团编号</th>	
				<th>车辆类型</th>
				<th>车主身份证号</th>
				<th>车辆识别码</th>
				<th>状态</th>
				<th>是否挂车</th>
				<th>车辆购置时间</th>
				<th>信息创建时间</th>
			</tr>
		</thead>
		<tbody>
			
			<s:forEach var="car" items="${list}">
				<tr target="carid" rel="${car.carid}">	
				<td>${car.carid}</td>
				<td>${car.vehicleLicense}</td>
				<td>${car.comid}</td>
				<td>${car.vehicleType}</td>
				<td>${car.userid}</td>
				<td>${car.vin}</td>
				<td>${car.status}</td>	
				<td>${car.isTrailer}</td>	
				<td>${car.buyTime}</td>	
				<td>${car.createTime}</td>						
				</tr>
			</s:forEach>
			
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="20">20</option>
					<option value="50">50</option>
				</c:if>
				<c:if test="${page.numPerPage eq '20' }">
					<option value="10">10</option>
					<option value="20" selected="selected">20</option>
					<option value="50">50</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="50" selected="selected">50</option>
				</c:if>
			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>
	</div>
</div>
