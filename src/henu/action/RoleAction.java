package henu.action;

import java.util.ArrayList;
import java.util.List;

import henu.bean.Role;
import henu.bean.Role;
import henu.dao.RoleDao;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.Page;

import com.opensymphony.xwork2.ActionSupport;
/**
 *处理角色 
 * 15-03-21
 * @author wuhaifeng
 * */
public class RoleAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private String args;
	private String url="/admin/ajax.jsp";
	private Role role ;
	private List<Role> list;
	private AJAX ajax ;
	// 分页排序
		private Page page = new Page();
		private int pageNum = 1;
		private int numPerPage = 10;
		private String orderField = "roleid";
		private String orderDirection = "asc";
		// 检索
		private String property = "-1";
		private String key = "-1";
		// 显示所有的记录
		public String all() {
			System.out.println("访问到all 444 role方法");
			if (orderField.equals("") || orderDirection.equals("")) {
				orderField = "roleid";
				orderDirection = "asc";
			}
			if (property.equals("") || key.equals("")) {
				property = "-1";
				key = "-1";
			}
			System.out.println("action查询的关键字是:" + key);
			 System.out.println("查询的依据1:" + property);
			page.setProperties("Role", pageNum, numPerPage, property, key);
			// 给page这个class设置属性
			list = DaoFactory.getRoleDaoInstance().findByProperty(property.trim(),
					key.trim(), orderField.trim(), orderDirection.trim(),
					page.getStart(), page.getEnd());
			System.out.println("查询出的数据："+list.size());
			//获取到所有role 然后再获取其权限
			url = "/admin/roleManage.jsp";
			return SUCCESS;
		}
		/**
		 *增加一个role,添加完成之后默认没有任何权限的
		 * */
		public String add() {
			RoleDao dao = DaoFactory.getRoleDaoInstance();
			//没有初始化的话就用== 进行判定
			int result = dao.save(role);
			//用户添加成功后，开始添加权限
			ajax = new AJAX();
			if(result > 0 ){//操作成功
				ajax.setStatusCode("200");
				ajax.setMessage("添加成功");
				ajax.setRel("rolem");
				ajax.setCallbackType("closeCurrent");
				ajax.setNavTabId("rolem");
			}else{
				ajax.setStatusCode("300");
				ajax.setCallbackType("");
				ajax.setMessage("添加失败");
			}
			return SUCCESS;
		}
		/**
		 * 删除一个角色
		 * */
		public String del(){
			RoleDao dao = DaoFactory.getRoleDaoInstance();
			//没有初始化的话就用== 进行判定
			int result = dao.delete(Integer.parseInt(args));
			//用户添加成功后，开始添加权限
			ajax = new AJAX();
			if(result > 0 ){//操作成功
				ajax.setStatusCode("200");
				ajax.setMessage("删除成功");
			}else{
				ajax.setStatusCode("300");
				ajax.setMessage("删除失败,请将该角色用户，转成其他角色");
			}
			return SUCCESS;
		}
		public String find(){
			System.out.println("find:"+args);
			RoleDao roleDao =  DaoFactory.getRoleDaoInstance();
			role = roleDao.findById(Integer.parseInt(args));
			// 跳转到role 修改
			url="/admin/roleUpdate.jsp";
			return SUCCESS;
		}
		/**
		 * 除了修改信息之外负责增加删除权限,直接把权限信息写进表里面
		 * */
		public String edit(){
			ajax = new AJAX();
			RoleDao roleDao = DaoFactory.getRoleDaoInstance();
			System.out.println("role"+role.toString());
			int result = roleDao.update(Integer.parseInt(args.trim()), role);
			if(result > 0 ){  //更新成功
				ajax.setStatusCode("200");
				ajax.setCallbackType("closeCurrent");  //防止重复提交，直接关闭当前窗口
				ajax.setMessage("更新角色成功");
			}else{ //更新失败
				ajax.setStatusCode("300");
				ajax.setCallbackType("");  //防止重复提交，直接关闭当前窗口
				ajax.setMessage("更新角色失败");
			}
			return SUCCESS;
		}
		/**
		 * 跳转到role add界面
		 * */
		public String goadd(){
			url="/admin/roleAdd.jsp";
			return SUCCESS;
		}
		public String getArgs() {
			return args;
		}
		public void setArgs(String args) {
			this.args = args;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public Role getRole() {
			return role;
		}
		public void setRole(Role role) {
			this.role = role;
		}
		public List<Role> getList() {
			return list;
		}
		public void setList(List<Role> list) {
			this.list = list;
		}
		public AJAX getAjax() {
			return ajax;
		}
		public void setAjax(AJAX ajax) {
			this.ajax = ajax;
		}
		public Page getPage() {
			return page;
		}
		public void setPage(Page page) {
			this.page = page;
		}
		public int getPageNum() {
			return pageNum;
		}
		public void setPageNum(int pageNum) {
			this.pageNum = pageNum;
		}
		public int getNumPerPage() {
			return numPerPage;
		}
		public void setNumPerPage(int numPerPage) {
			this.numPerPage = numPerPage;
		}
		public String getOrderField() {
			return orderField;
		}
		public void setOrderField(String orderField) {
			this.orderField = orderField;
		}
		public String getOrderDirection() {
			return orderDirection;
		}
		public void setOrderDirection(String orderDirection) {
			this.orderDirection = orderDirection;
		}
		public String getProperty() {
			return property;
		}
		public void setProperty(String property) {
			this.property = property;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
}
