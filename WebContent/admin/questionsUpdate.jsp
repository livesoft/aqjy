<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
	<link rel="stylesheet" href="../ueditor/themes/default/css/ueditor.css">
  	<script>
  	UE.getEditor('editor', {
  		initialFrameWidth:"100%" //初始化选项
  		,toolbars: [[
  		            'fullscreen', 'source', '|', 'undo', 'redo', '|',
  		            'bold', 'italic', 'underline', 'fontborder', 'lineheight', '|',
  		            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
  		            'directionalityltr', 'directionalityrtl', 'indent', '|',
  		            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
  		            'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
  		            'simpleupload', 'insertimage', 'emotion', 'scrawl', 'pagebreak',  '|',
  		            'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
  		            'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
  		            'print', 'preview', 'searchreplace', 'help', 'drafts'
  		        ]]
  		});
  	UE.getEditor('option1', {
  		initialFrameWidth:"100%" //初始化选项
  		,toolbars: [[
  		            'fullscreen', 'source', '|', 'undo', 'redo', '|',
  		            'bold', 'italic', 'underline', 'fontborder', 'lineheight', '|',
  		            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
  		            'directionalityltr', 'directionalityrtl', 'indent', '|',
  		            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
  		            'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
  		            'simpleupload', 'insertimage', 'emotion', 'scrawl', 'pagebreak',  '|',
  		            'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
  		            'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
  		            'print', 'preview', 'searchreplace', 'help', 'drafts'
  		        ]]
  		});
  		UE.getEditor('option2', {
  		initialFrameWidth:"100%" //初始化选项
  		,toolbars: [[
  		            'fullscreen', 'source', '|', 'undo', 'redo', '|',
  		            'bold', 'italic', 'underline', 'fontborder', 'lineheight', '|',
  		            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
  		            'directionalityltr', 'directionalityrtl', 'indent', '|',
  		            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
  		            'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
  		            'simpleupload', 'insertimage', 'emotion', 'scrawl', 'pagebreak',  '|',
  		            'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
  		            'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
  		            'print', 'preview', 'searchreplace', 'help', 'drafts'
  		        ]]
  		});
  		UE.getEditor('option3', {
  		initialFrameWidth:"100%" //初始化选项
  		,toolbars: [[
  		            'fullscreen', 'source', '|', 'undo', 'redo', '|',
  		            'bold', 'italic', 'underline', 'fontborder', 'lineheight', '|',
  		            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
  		            'directionalityltr', 'directionalityrtl', 'indent', '|',
  		            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
  		            'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
  		            'simpleupload', 'insertimage', 'emotion', 'scrawl', 'pagebreak',  '|',
  		            'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
  		            'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
  		            'print', 'preview', 'searchreplace', 'help', 'drafts'
  		        ]]
  		});
  		UE.getEditor('option4', {
  		initialFrameWidth:"100%" //初始化选项
  		,toolbars: [[
  		            'fullscreen', 'source', '|', 'undo', 'redo', '|',
  		            'bold', 'italic', 'underline', 'fontborder', 'lineheight', '|',
  		            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
  		            'directionalityltr', 'directionalityrtl', 'indent', '|',
  		            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
  		            'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
  		            'simpleupload', 'insertimage', 'emotion', 'scrawl', 'pagebreak',  '|',
  		            'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
  		            'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
  		            'print', 'preview', 'searchreplace', 'help', 'drafts'
  		        ]]
  		})
  	
  	
  	</script>

<h2 class="contentTitle">试题修改</h2>
<div class="pageFormContent" layoutH="60">
	<form method="post" action="/aqjy/ques/editQues"
		class="pageForm required-validate"
		onsubmit="return validateCallback(this,navTabAjaxDone)">
			<input type="hidden" value="${ques.qid}"  name="args" />
		<fieldset>
			<legend>试题基本信息</legend>
			<dl>
			<dt>当前题号：</dt>
			<dd><span style="font-size:20px ; color:red" >${ques.qid}</span></dd>
			</dl>
			<dl>
				<dt>试题类型：</dt>
				<dd>
					<input type="radio" name="ques.type" value="选择题" checked="checked" />选择题
 					<input type="radio" name="ques.type" value="判断题" />判读题
				</dd>
			</dl>
			<dl>
				<dt>试题科目：</dt>
				<dd>
					<input name="ques.subject" value="${ques.subject}" type="text" alt="输入科目" />
				</dd>
			</dl>
			<dl>
				<dt>试题答案：</dt>
				<dd id="selda">
					<select name="ques.answer" size=1>
						<option value="A" selected="selected">选项A</option>
						<option value="B">选项B</option>
						<option value="C">选项C</option>
						<option value="D">选项D</option>
					</select>
				</dd>
				<dd id="judgeda">
					<input type="radio" name="ques.answer" value="true"
						checked="checked" />对 <input type="radio" name="ques.answer"
						value="false" />错
				</dd>
			</dl>
		</fieldset>
		<fieldset>
			<legend>试题内容及答案</legend>
			<dl class="nowrap">
				<dt style="font-size: 10px;font-style: bold">试题内容：</dt>
				<dd>
					<script type="text/plain" id="editor" name="ques.content"  style="width:800px;height: 300px">${ques.content}</script>
				</dd>
			</dl>
			<div id="selitem">
				<dl class="nowrap">
				<dt style="font-size: 10px;font-style: bold">选项A：</dt>
				<dd>
				<script type="text/plain" id="option1" name="ques.optiona"  style="width:800px;height: 200px">${ques.optiona}</script>
				</dd>
			</dl>
			<dl class="nowrap">
				<dt style="font-size: 10px;font-style: bold">选项B：</dt>
				<dd>
				<script type="text/plain" id="option2" name="ques.optionb"  style="width:800px;height: 200px">${ques.optionb}</script>
				</dd>
			
			</dl>
			<dl class="nowrap">
				<dt style="font-size: 10px;font-style: bold">选项C：</dt>
				<dd>
				<script type="text/plain" id="option3" name="ques.optionc"  style="width:800px;height: 200px">${ques.optionc}</script>
				</dd>
			
			</dl>
			<dl class="nowrap">
				<dt style="font-size: 10px;font-style: bold">选项D：</dt>
				<dd>
				<script type="text/plain" id="option4" name="ques.optiond"  style="width:800px;height: 200px">${ques.optiond}</script>
				</dd>
			
			</dl>
		</div>
		</fieldset>
		<!-- --此处加上js 用于实现题的类型输入切换 -->
		<script>
			$("#judgeda").hide();
			$(':input[name="ques.type"]').click(
					function() {
						//显示选中的选项的值
						/* var x = $(':input[name="ques.type"]')[1].value;
						var y = $(':input[name="ques.type"]')[0].value;
						alert(x);
						alert(y); 
						遍历两个按钮看那一个被选中 
						 */
						var questype = $(':input[name="ques.type"]:checked')
								.val().trim();
						if (questype == '选择题') {
							//是选择题就隐藏所有判断题的东西
							$("#judgeda").hide();
							$("#selda").show();
							$("#selitem").show();
						} else if (questype == '判断题') {
							$("#judgeda").show();
							$("#selda").hide();
							$("#selitem").hide();
						}
					});
		</script>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li><div class="buttonActive">
						<div class="buttonContent">
							<button type="submit">提交</button>
						</div>
					</div></li>
				<li><div class="button">
						<div class="buttonContent">
							<button type="button" class="close">取消</button>
						</div>
					</div></li>
			</ul>
		</div>
	</form>
</div>

