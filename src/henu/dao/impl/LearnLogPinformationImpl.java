package henu.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import henu.bean.LearnLog;
import henu.bean.LearnLogPinformation;
import henu.dao.LearnLogPinformationDao;
import henu.dao.factory.DaoFactory;




public class LearnLogPinformationImpl implements LearnLogPinformationDao{

	@Override
	public List<LearnLogPinformation> findAll(String order, String sort) {
		List<LearnLogPinformation> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "select * from LEARNLOG order by " + order +" " + sort + "";
		try {
			list = runner.query(sql, new BeanListHandler<LearnLogPinformation>(LearnLogPinformation.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	/*	System.out.println(list.size() +"\t\t\t\t\t12312");*/
		return list;
	}

	@Override
	public List<LearnLogPinformation> findByProperty(String property,
			String key, String order, String sort, int start, int end) {
		List<LearnLogPinformation> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "SELECT * FROM (SELECT tt.*, ROWNUM ro FROM (select * from VIEW_LEARNLOG_PINFO_COMPANY where "+property+"='"+key+"'  order by "+order+" "+sort+") tt WHERE ROWNUM <="+end+") WHERE ro > "+start+"";
		try {
			list = runner.query(sql, new BeanListHandler<LearnLogPinformation>(LearnLogPinformation.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("");
		return list;
	}

	@Override
	public LearnLogPinformation findById(String id) {
		LearnLogPinformation lp = null;
		String sql = "select * from VIEW_LEARNLOG_PINFO_COMPANY where idcard=?";
		//
		QueryRunner runner = DaoFactory.getRunner();
		try {
			lp = runner.query(sql, new BeanHandler<LearnLogPinformation>(LearnLogPinformation.class),id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*输出所有System.out.println("impl"+lp);*/
		return lp;
	}

	
	//获取的值存到视图VIEW_LEARNLOG_PINFO_COMPANY
	public int save(LearnLogPinformation lp) {
		int result = 0;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "INSERT INTO VIEW_LEARNLOG_PINFO_COMPANY VALUES(?,?,1,?,?,?,?)";
		Object[] params = {lp.getRealname(),lp.getCompanyname(),lp.getIdcard(),lp.getStarttime(),lp.getStudytime(),lp.getTitle()};
		System.out.println("lp.getRealname():"+lp.getRealname());
		try {
			System.out.println("hhhhh");
			result = runner.update(sql, params);
			System.out.println("result:"+result);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	

}
