
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<body style="position: relative;">

	<div class="pageContent" >
	<form method="post" action="#" enctype="multipart/form-data" class="pageForm required-validate" onsudcit="return iframeCallback(this,navTabAjaxDone);">
		<div class="pageFormContent nowrap" layoutH="97">
		<input type="hidden" name="">
			<dl>
				<dt>行车证号：</dt>
				<dd>
					<input name="dc.drivingId" value="${dc.drivingId}" readonly="readonly"/><!-- 外键 -->
					
				</dd>
			</dl> 
			
			<dl>
				<dt>车牌号：</dt>
				<dd>
					<input type="text" name="dc.vehicleLicense" value="${dc.vehicleLicense}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>车辆类型：</dt>
				<dd>
					<input type="text" name="dc.vehicleType"  value="${dc.vehicleType}" readonly="readonly" />
				</dd>
			</dl>
			<dl>
				<dt>所有人：</dt>
				<dd>
					<input type="text" name="dc.owners"   value="${dc.owners}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>使用性质：</dt>
				<dd>
					<input type="text" name="dc.useCharacter" value="${dc.useCharacter}" readonly="readonly" />
				</dd>
			</dl>
			<dl>
				<dt>品牌型号：</dt>
				<dd>
					<input type="text" name="dc.models" value="${dc.models}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>车辆类型识别代号：</dt><!--数据字典  -->
				<dd>
					<input type="text" name="dc.vin" value="${dc.vin}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>行车证复印件：</dt>
				<dd>
					<img src="../${dc.copyDrivingCar }" style="width:400px;height: 200px;">	
				</dd>
			</dl>
			<dl>
				<dt>发动机号码：</dt>
				<dd>
					<input type="text" name="dc.engineid"value="${dc.engineid}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>档案编号：</dt>
				<dd>
				<input type="text" name="dc.doclc" value="${dc.doclc}" readonly="readonly"/>
					
				</dd>
			</dl>
			<dl>
				<dt>核载人数：</dt>
				<dd>
					<input type="text" name="dc.menNumber" value="${dc.menNumber}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>总质量：</dt>
				<dd>
					<input type="text" name="dc.totalWeight" value="${dc.totalWeight}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>整备质量：</dt>
				<dd>
					<input type="text" name="dc.weight" value="${dc.weight}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>核定载质量：</dt>
				<dd>
					<input type="text" name="dc.checkWeight" value="${dc.checkWeight}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>外型尺寸：</dt>
				<dd>
					<input type="text" name="dc.sizes" value="${dc.sizes}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>准牵引质量：</dt>
				<dd>
					<input type="text" name="dc.towWeight" value="${dc.towWeight}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>住址：</dt>
				<dd>
					<input type="text" name="dc.address" value="${dc.address}" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>有效期：</dt>
				<dd>
					<input type="text" name="dc.validations" value="${dc.validations}" readonly="readonly"/>
				</dd>
			</dl>
			 
			<dl>
				<dt>备注：</dt>
				<dd>
					<textarea  name="dc.memo" class="editor"  rows="6" cols="100" >${dc.memo}</textarea>
				</dd>
			</dl>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li><div class="button"><div class="buttonContent"><button type="button" value="返回" class="button" onclick="history.go(-1)">返回</button></div></div></li>
			</ul>
		</div>
		</div>
	</form>
	
</div>
</body>