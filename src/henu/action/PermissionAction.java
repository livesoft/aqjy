package henu.action;

import henu.bean.Permission;
import henu.bean.PermissionFront;
import henu.bean.Role;
import henu.bean.RolePermission;
import henu.dao.PermissionDao;
import henu.dao.factory.DaoFactory;
import henu.dao.impl.PermissionDaoImpl;
import henu.util.AJAX;
import henu.util.CONSTANTS;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

/**
 * @author wuhaifeng 权限管理
 * */
public class PermissionAction extends ActionSupport {

	private List<PermissionFront> frontlist;
	private PermissionFront perf ;
	private PermissionFront pf  ;
	private String args;
	private String url="/admin/ajax.jsp";
	private Role role ;
	private AJAX ajax ;
	
	
	public PermissionFront getPf() {
		return pf;
	}

	public void setPf(PermissionFront pf) {
		this.pf = pf;
	}

	public List<PermissionFront> getFrontlist() {
		return frontlist;
	}

	public void setFrontlist(List<PermissionFront> frontlist) {
		this.frontlist = frontlist;
	}

	public PermissionFront getPerf() {
		return perf;
	}

	public void setPerf(PermissionFront perf) {
		this.perf = perf;
	}

	

	public String getArgs() {
		return args;
	}

	public void setArgs(String args) {
		this.args = args;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public AJAX getAjax() {
		return ajax;
	}

	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}

	/**
	 * 浏览权限表格数据 没有分页
	 * */
	public String all() {
		// 找到所有的角色
		PermissionDaoImpl perdao = new PermissionDaoImpl();
		Permission per = null;
		frontlist = new ArrayList<PermissionFront>();
		List<Role> rolelist = perdao.findAllRole();
		// 遍历所有的角色，接着找到角色对应的权限信息,这个用户的所有的权限
		//System.out.println("角色一共有："+rolelist.size());
		for (int i = 0; i < rolelist.size(); i++) {
			Role r = rolelist.get(i);
			List<RolePermission> rplist = perdao.findRPByProperty("ROLEID",
					r.getRoleid() + "", "PID", "ASC"); // 用户的id，在关联表里找到他的权限情况
			//System.out.println("当前用户找到的权限是:"+rplist.size());
			//System.out.println("当前权限的名称："+r.getRolename());			
			pf= new PermissionFront();
			pf.setRoleid(r.getRoleid());  // 设置该部门的编号
			pf.setStatus(r.getStatus());  //设置该角色是否可用
			pf.setDesc(r.getDesc());
			pf.setMark(r.getRemark());
			pf.setName(r.getRolename());
			// 已经拿到所有的权限情况假如到bean
			for (RolePermission p : rplist) {
				//System.out.println("要找的权限的id是："+p.getPid());
				per = perdao.findPermisstionById(p.getPid()); // 角色拥有的权限id
				// 得到这个权限的名字
				// 这个人假如获取一个权限就默认这个角色拥有了这个模块功能的权限
				switch (per.getPname()) {// 权限的名称
				case CONSTANTS.PER_HUDONG:
					pf.setHudong("true");
					break;
				case CONSTANTS.PER_HUIYI:
					pf.setHuiyi("true");
					break;
				case CONSTANTS.PER_JIAOYU:
					pf.setJiaoyu("true");
					break;
				case CONSTANTS.PER_KAOHE:
					pf.setKaohe("true");
					break;
				case CONSTANTS.PER_TONGZHI:
					pf.setTongzhi("true");
					break;
				case CONSTANTS.PER_XINXI:
					pf.setXinxi("true");
					break;
				case CONSTANTS.PER_XITONG:
					pf.setXitong("true");
					break;
				case CONSTANTS.PER_YONGHU:
					pf.setYonghu("true");
					break;
				default:
					break;
				}
			}
			// 这里权限已经封装完毕，可以显示了
			frontlist.add(pf);
		}
		//System.out.println("封装好的数据："+frontlist.size());
		url = "/admin/roleManager.jsp";
		return SUCCESS;
	}

	/**
	 * 添加部门
	 * */
	public String add() {
		PermissionDaoImpl perdao = new PermissionDaoImpl();
		priRole(role);
		//没有初始化的话就用== 进行判定
		int result = perdao.saveRole(role);
		//用户添加成功后，开始添加权限
		System.out.println("添加角色结果："+result);
		if(result > 0 ){//操作成功
			HandleResult("添加部门成功！","200","");
		}else{
			HandleResult("添加部门失败！", "300","");
		}
		return SUCCESS;
	}
	public String del() {
		PermissionDao perdao = DaoFactory.getPermissionDaoInstance();
		
		int result = perdao.delRole(args);
		ajax = new AJAX();
		if(result > 0 ){//操作成功
			HandleResult("操作成功！","200","roleset");
		}else{
			HandleResult("操作失败！", "300","roleset");
		}
		return SUCCESS;
	
	}
	/**
	 * 用户的权限更改
	 * */
	public String edit() {
		/**
		 * 更新权限：首先把所有的权限删除
		 * 
		 * 所有添加和删除权限的操作都在这个里面进行的
		 * */
		PermissionDao perdao = DaoFactory.getPermissionDaoInstance();
		int result  = 0 ;
		//更新role的信息
		result = perdao.updateRole(role);
		//System.out.println("更新角色信息结果："+result);
		//System.out.println("要删除的用户的id是："+role.getRoleid());
		
		result = perdao.delRPByRoleid(role.getRoleid()+"");						//删除用户的所有权限记录
		//priPF(perf);
		if(result > -1 ){			//判断是否删除成功，只有删除成功才能进行下一步的操作
			if(perf.getHudong().equals(CONSTANTS.PER_HUDONG)){		//检测军训服装权限
				result = perdao.addModelPermission(CONSTANTS.PER_HUDONG, role.getRoleid());
				if(result < 1 ){
					HandleResult("添加互动失败！","300","");
					return SUCCESS ;		//任何一个过程发现错误就直接结束程序
				}
			}
			if(perf.getHuiyi().equals(CONSTANTS.PER_HUIYI)){		//检测在线开始权限
				result = perdao.addModelPermission(CONSTANTS.PER_HUIYI, role.getRoleid());
				if(result < 1 ){
					HandleResult("添加会议管理权限失败！","300","");
					return SUCCESS ;		//任何一个过程发现错误就直接结束程序
				}
			}
			if(perf.getJiaoyu().equals(CONSTANTS.PER_JIAOYU)){		//检测收费权限
				result = perdao.addModelPermission(CONSTANTS.PER_JIAOYU, role.getRoleid());
				if(result < 1 ){
					HandleResult("添加安全教育管理权限失败！","300","");
					return SUCCESS ;		//任何一个过程发现错误就直接结束程序
				}
			}
			if(perf.getKaohe().equals(CONSTANTS.PER_KAOHE)){		//检测宿舍权限
				result = perdao.addModelPermission(CONSTANTS.PER_KAOHE, role.getRoleid());
				if(result < 1 ){
					HandleResult("添加安全考核权限失败！","300","");
					return SUCCESS ;		//任何一个过程发现错误就直接结束程序
				}
			}
			if(perf.getTongzhi().equals(CONSTANTS.PER_TONGZHI)){		//检测管系统权限
				result = perdao.addModelPermission(CONSTANTS.PER_TONGZHI, role.getRoleid());
				if(result < 1 ){
					HandleResult("添加通知管理权限失败！","300","");
					return SUCCESS ;		//任何一个过程发现错误就直接结束程序
				}
			}
			if(perf.getXinxi().equals(CONSTANTS.PER_XINXI)){		//检测学生信息管理
				result = perdao.addModelPermission(CONSTANTS.PER_XINXI, role.getRoleid());
				if(result < 1 ){
					HandleResult("添加信息管理失败！","300","");
					return SUCCESS ;		//任何一个过程发现错误就直接结束程序
				}
			}
			if(perf.getXitong().equals(CONSTANTS.PER_XITONG)){		//检测咨询权限
				result = perdao.addModelPermission(CONSTANTS.PER_XITONG, role.getRoleid());
				if(result < 1 ){
					HandleResult("添加系统管理权限失败！","300","");
					return SUCCESS ;		//任何一个过程发现错误就直接结束程序
				}
			}
			if(perf.getYonghu().equals(CONSTANTS.PER_YONGHU)){		//检测志愿者权限
				result = perdao.addModelPermission(CONSTANTS.PER_YONGHU, role.getRoleid());
				if(result < 1 ){
					HandleResult("添加志愿者权限失败失败！","300","");
					return SUCCESS ;		//任何一个过程发现错误就直接结束程序
				}
			}
		}else{		//删除用户的权限失败,直接结束程序
			HandleResult("删除用户权限失败！", "300","");
			return SUCCESS;
		}
		HandleResult("修改成功！", "200","");
		return SUCCESS;
	}
	
	public String find(){
		 //System.out.println("传来的id是："+args);							
		PermissionDao perdao = DaoFactory.getPermissionDaoInstance();
		Permission per = null ;
		role = perdao.findRoleById(args);
		List<RolePermission> rplist = perdao.findRPByProperty("ROLEID",args, "PID", "ASC"); // 用户的id，在关联表里找到他的权限情况
		//System.out.println("这个用户权限的数量："+rplist.size());
			pf = new PermissionFront();
		 pf.setStatus(role.getStatus());
		for (RolePermission p : rplist) {
			//System.out.println("要找的权限的id是："+p.getPid());
			per = perdao.findPermisstionById(p.getPid()); // 角色拥有的权限id
			// 得到这个权限的名字
			// 这个人假如获取一个权限就默认这个角色拥有了这个模块功能的权限
			//System.out.println("当前权限的名字："+per.getPname());
			//System.out.println("cons名字："+CONSTANTS.PER_JUN);
			switch (per.getPname()) {// 权限的名称
			case CONSTANTS.PER_HUDONG:
				pf.setHudong("true");
				break;
			case CONSTANTS.PER_HUIYI:
				pf .setHuiyi("true");
				break;
			case CONSTANTS.PER_JIAOYU:
				pf.setJiaoyu("true");
				break;
			case CONSTANTS.PER_KAOHE:
				pf.setKaohe("true");
				break;
			case CONSTANTS.PER_TONGZHI:
				pf.setTongzhi("true");
				break;
			case CONSTANTS.PER_XINXI:
				pf.setXinxi("true");
				break;
			case CONSTANTS.PER_XITONG:
				pf.setXitong("true");
				break;
			case CONSTANTS.PER_YONGHU:
				pf.setYonghu("true");
				break;
			default:
				break;
			}
		}
		//priPF(pf);
		url="/admin/roleUpdate.jsp";
		return 	SUCCESS ;	
	}
	public static void  priRole(Role rtemp){
		System.out.println("部门描述："+rtemp.getDesc());
		System.out.println("部门备注："+rtemp.getRemark());
		System.out.println("部门名字：:"+rtemp.getRolename());
		System.out.println("部门是否可用:"+rtemp.getStatus());
	}
	
	
	/**
	 * @param msg 提示的消息消息 
	 * @param statuscode http状态码
	 * */
	public void HandleResult(String msg,String statuscode ,String tavid){
		ajax = new AJAX();
		ajax.setNavTabId(tavid);
		ajax.setCallbackType("");
		ajax.setMessage(msg);
		ajax.setStatusCode(statuscode);
		url="/admin/ajax.jsp";
	}
}
