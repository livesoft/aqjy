package henu.dao.factory;
import henu.bean.LearnLogPinformation;
import henu.dao.AdminDao;
import henu.dao.BasicinfoDao;
import henu.dao.BusinessMessageDao;
import henu.dao.CarDao;
import henu.dao.CertificationDao;
import henu.dao.CompanyDao;
import henu.dao.ContentDao;
import henu.dao.DictionaryDao;
import henu.dao.DrivingCarDao;
import henu.dao.ExamDao;
import henu.dao.ExerciseDao;
import henu.dao.Group_companyDao;
import henu.dao.InsuranceDao;
import henu.dao.LearnLogDao;
import henu.dao.LearnLogPinformationDao;
import henu.dao.LicenseDao;
import henu.dao.MaintanceDao;
import henu.dao.ManagePersonDao;
import henu.dao.McontentDao;
import henu.dao.MeetingsignDao;
import henu.dao.NoticeDao;
import henu.dao.OnlineChatDao;
import henu.dao.PInformationDao;
import henu.dao.PermissionDao;
import henu.dao.QuestionsDao;
import henu.dao.RoleDao;
import henu.dao.SeminarDao;
import henu.dao.UserVehicleDao;
import henu.dao.impl.AdminDaoImpl;
import henu.dao.impl.BasicinfoDaoImpl;
import henu.dao.impl.BusinessMessageDaoImpl;
import henu.dao.impl.CarDaoImpl;
import henu.dao.impl.CertificationDaoImpl;
import henu.dao.impl.CompanyDaoImpl;
import henu.dao.impl.ContentDaoImpl;
import henu.dao.impl.DictionaryDaoImpl;
import henu.dao.impl.DrivingCarDaoImpl;
import henu.dao.impl.ExamDaoImpl;
import henu.dao.impl.ExerciseDaoImpl;
import henu.dao.impl.Group_CompanyDaoImpl;
import henu.dao.impl.InsuranceDaoImpl;
import henu.dao.impl.LearnLogDaoImpl;
import henu.dao.impl.LearnLogPinformationImpl;
import henu.dao.impl.LicenseDaoImpl;
import henu.dao.impl.MaintanceDaoImpl;
import henu.dao.impl.ManagePersonDaoImpl;
import henu.dao.impl.McontentDaoImpl;
import henu.dao.impl.MeetingsignDaoImpl;
import henu.dao.impl.NoticeDaoImpl;
import henu.dao.impl.OnlineChatDaoImpl;
import henu.dao.impl.PInformationDaoImpl;
import henu.dao.impl.PermissionDaoImpl;
import henu.dao.impl.QuestionsDaoImpl;
import henu.dao.impl.RoleDaoImpl;
import henu.dao.impl.SeminarDaoImpl;
import henu.dao.impl.UserVehicleDaoImpl;
import henu.util.Dbcp;

import org.apache.commons.dbutils.QueryRunner;


public class DaoFactory {
	public static BasicinfoDao getBasicinfoDaoInstance(){
		return new BasicinfoDaoImpl();
	}	
	public static McontentDao getMcontentDaoInstance(){
		return new McontentDaoImpl();
	}
	public static MeetingsignDao getMeetingsignDaoInstance(){
		return new MeetingsignDaoImpl();
	}
	public static ManagePersonDao getManagePersonDaInstance(){
		return new ManagePersonDaoImpl();
	}
	public static ContentDao getContentDaoInstance(){
		return new ContentDaoImpl();
	}
	public static SeminarDao getSeminarDaoInstance(){
		return new SeminarDaoImpl();
	}
	public static LicenseDao getLicenseDaoInstance(){
		return new LicenseDaoImpl();
	}
	public static LearnLogDao getLearnLogDaoIntance(){
		return new LearnLogDaoImpl();
	}
	public static LearnLogPinformationDao getLearnLogPinformationDao(){
		return new LearnLogPinformationImpl();
	}
	public static QuestionsDao getQuesttionDaoInstance() {
		
		return new QuestionsDaoImpl();
	}
	public static NoticeDao getNoticeDaoInstance() {
		
		return new NoticeDaoImpl();
	}
	public static OnlineChatDao getOnlineChatDaoInstance() {
		
		return new OnlineChatDaoImpl();
	}
	public static DictionaryDao getDictionaryDaoInstance() {
		
		return new DictionaryDaoImpl();
	}
	public static BusinessMessageDao getBusinessMessageDaoInstance() {
		
		return new BusinessMessageDaoImpl();
	}
	public static DrivingCarDao getDrivingCarDaoInstance() {
		
		return  new DrivingCarDaoImpl();
	}
	/**
	 * 返回一个QueryRunner,可以直接使用
	 * */
	public static QueryRunner getRunner() {
		return new QueryRunner(Dbcp.getDataSource());
	}
	
	public static ExamDao getExamDaoInstance() {
		return new ExamDaoImpl();
	}
	public static CompanyDao getCompanyDaoInstance() {
		return new CompanyDaoImpl();
	}
	public static PInformationDao getPInformationDaoInstance(){
		return new PInformationDaoImpl();
	}
	public static InsuranceDao getInsuranceDaoInstance(){
		return new InsuranceDaoImpl();
	}
	public static CarDao getCarDaoInstance(){
		return  new  CarDaoImpl();
	}
	public static Group_companyDao getGroup_companyDaoInstance(){
		return  new  Group_CompanyDaoImpl();
	}

    public static MaintanceDao getMaintanceDaoInstance()
	{
		return new MaintanceDaoImpl();
	}
	public static UserVehicleDao getUserVehicleDaoInstance(){
		return  new  UserVehicleDaoImpl();
	}
	public static CertificationDao getCertificationDaoInstance(){
		return new CertificationDaoImpl();
	}
	public static ExerciseDao getExerciseDaoInstance(){
		return new ExerciseDaoImpl();
	}
	public static PermissionDao getPermissionDaoInstance(){
		return new PermissionDaoImpl();
	}
	public static RoleDao getRoleDaoInstance(){
		return new RoleDaoImpl();
	}
	public static AdminDao getAdminDaoInstance() {
		// TODO Auto-generated method stub
		return new AdminDaoImpl();
	}
}


