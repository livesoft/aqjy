
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
  <%@taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>  
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
<form id="pagerForm" method="post" action="/aqjy/usertotalview/UserV_findAll">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${page.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" /> 
    <input type="hidden" name="orderDirection"  value="${param.orderDirection}" />  
    <input type="hidden" name="property"  value="${property}">
    <input type="hidden" name="key"  value="${key}">
</form>


<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="/aqjy/usertotalview/UserV_findAll" method="post">
	<div class="searchBar">

		<table class="searchContent">
			<tr>
				<td>
					分类
				</td>
				<td>

              
					<select class="combox" name="property" >
							<option value="" >根据名称查询</option>
							<option value="uvid">用户号</option>
							<option value="idcard">用户身份证号</option>
							<option value="carid">车辆编号</option>										
					</select>
				</td>
				<td>
					要查询的值：
						<input type="text" value="" name="key">			
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
			</ul>
			<div style="color:red;margin-top:5px;">注意:点击标题可以进行排序，检索模式下若想查询所有信息，再点击一次检索按钮即可</div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="/aqjy/admin/userVehicleAdd.jsp" target="navTab"><span>添加</span></a></li>
			<li><a class="delete" href="/aqjy/usertotalview/UserV_delete?uid={uvid}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="/aqjy/usertotalview/UserV_find?uid={uvid}" target="navTab"><span>修改</span></a></li>
			<li class="line">line</li>
			<li><a class="icon" href="/aqjy/usertotalview/UserV_export" target="dwzExport" targetType="navTab" title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li>
			<li class="line">line</li>
			
<!-- 			<li>
				<form action="/yxxt/upFile?namespace=dormitory&actionName=DormitoryManagerAction_imp" method="post" enctype="multipart/form-data" class="pageForm required-validate" onsubmit="return iframeCallback(this,navTabAjaxDone);">
				<input type="file" name="files">
				<div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div>
				</form>
			</li> -->
		
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" style="text-align: center;">
		<thead>
			<tr>
				<th  orderField="uvid" <c:if test='${orderField == "uvid" }'> class="${orderDirection}"  </c:if>   >用户编号</th> 		
				<th>用户身份证号</th>	
				<th>车辆编号</th>
				<th>备注</th>
			</tr>
		</thead>
		<tbody>
			
			<s:forEach var="userv" items="${list}">
				<tr target="uvid" rel="${userv.uvid}">	
				<td>${userv.uvid}</td>
				<td>${userv.idcard}</td>
				<td>${userv.carid}</td>
				<td>${userv.memo}</td>

						

					
<%-- 					<td>
						<c:if test="${empty dormitory.deployindoor}">尚未添加</c:if>
						<c:if test="${!empty dormitory.deployindoor}">已经添加</c:if>
					</td>
					<td>
						<c:if test="${empty dormitory.picture}">尚未添加</c:if>
						<c:if test="${!empty dormitory.picture}">已经添加</c:if>
					</td>
					<td>${dormitory.orientation}</td>
					<td>${dormitory.roomNums}</td> --%>
					
				</tr>
			</s:forEach>
			
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="20">20</option>
					<option value="50">50</option>
				</c:if>
				<c:if test="${page.numPerPage eq '20' }">
					<option value="10">10</option>
					<option value="20" selected="selected">20</option>
					<option value="50">50</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="50" selected="selected">50</option>
				</c:if>
			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>
	</div>
</div>
