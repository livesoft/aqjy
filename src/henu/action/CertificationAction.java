package henu.action;

import henu.bean.Certification;
import henu.bean.Certification_PInfomation;
import henu.bean.ManagePerson;
import henu.bean.PInformation;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.CONSTANTS;
import henu.util.Page;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class CertificationAction extends ActionSupport {

	//用户资格证号 用于查看/删除
	private String args;
	private AJAX ajax = new AJAX();
	private String url;
	// 文件下载
	private String res;
	private String resName;
	private String resType;

	// 分页排序
	private Page page = new Page();
	private int pageNum = 1;
	private int numPerPage = 10;
	private String orderField = "regTime";
	
	// 检索
	private String property = "-1";
	private String key = "-1";
	
	//用于前台编辑时 预显示不可编辑的基本信息
	private PInformation p;

	//由于前台用户资格证复印件上传
	private File copyurl;
	//
	private String copyurlFileName;
	
	public String getCopyurlFileName() {
		return copyurlFileName;
	}

	public void setCopyurlFileName(String copyurlFileName) {
		this.copyurlFileName = copyurlFileName;
	}

	public AJAX getAjax() {
		return ajax;
	}

	public String getUrl() {
		return url;
	}

	public File getCopyurl() {
		return copyurl;
	}

	public void setCopyurl(File copyurl) {
		this.copyurl = copyurl;
	}

	public PInformation getP() {
		return p;
	}

	public void setP(PInformation p) {
		this.p = p;
	}

	//用户查看
	Certification c=null;
	public Certification getC() {
		return c;
	}

	public void setC(Certification c) {
		this.c = c;
	}
	
	private List<Certification_PInfomation> list=null;
	private Certification_PInfomation certification_pinfo;
	
	public Certification_PInfomation getCertification_pinfo() {
		return certification_pinfo;
	}

	public void setCertification_pinfo(Certification_PInfomation certification_pinfo) {
		this.certification_pinfo = certification_pinfo;
	}

	public List<Certification_PInfomation> getList() {
		return list;
	}

	public void setList(List<Certification_PInfomation> list) {
		this.list = list;
	}
	public String getArgs() {
		return args;
	}

	public void setArgs(String args) {
		this.args = args;
	}

	public String getRes() {
		return res;
	}

	public void setRes(String res) {
		this.res = res;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getResType() {
		return resType;
	}

	public void setResType(String resType) {
		this.resType = resType;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getNumPerPage() {
		return numPerPage;
	}

	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public String all() {
		
		ManagePerson m=(ManagePerson) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_ADMIN);
		//System.out.println(property+"!"+key+"!"+m.getComid()+"!"+page.getStart()+"!"+page.getEnd());
		if (property.equals("") || key.equals("")) {
			property = "-1";
			key = "-1";
		}
		//System.out.println("action查询的关键字是:" + key);
		//System.out.println("查询的依据:" + property);
		page.setProperties1("VIEW_CERTIFICATION_PINFO",pageNum,numPerPage,property,key,Integer.toString(m.getComid()),Integer.toString(m.getComid()));
		// 给page这个class设置属性
		
		list = DaoFactory.getCertificationDaoInstance().findByProperty(property.trim(), key.trim(),Integer.toString(m.getComid()), page.getStart(), page.getEnd());
		url = "/admin/certificationMessage.jsp";
	
		return SUCCESS;
	}
	public String del(){
		int result=0;
		result=DaoFactory.getCertificationDaoInstance().deleteByCnid(args);
		if (result > 0) {
			HandleMsg("200", "删除成功！", "certification","");
		} else {
			HandleMsg("300", "删除失败！", "certification","");
		}
		return SUCCESS;
	}
	//用于前台用户查询自己的资格证信息
	public String findByUseridCard(){
		p = (PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
		c=DaoFactory.getCertificationDaoInstance().findByUserIdcard(p.getIdCard());
		//System.out.println("用户身份证号"+p.getIdCard());
		url="/front/certificationMessage.jsp";
		return SUCCESS;
	}
	//用于前台用户跳转到编辑页面
	public String jump(){
		/*ajax = new AJAX();
		ajax.setCallbackType("forward");
		//ajax.setStatusCode("200");
		ajax.setMessage("");
		ajax.setForwardUrl("/aqjy/front/certificationMessageEdit.jsp");
		url="/admin/ajax.jsp";	*/
		p = (PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
		c=DaoFactory.getCertificationDaoInstance().findByUserIdcard(p.getIdCard());
		//System.out.println("用户身份证号"+p.getIdCard());
		url="/front/certificationMessageEdit.jsp";
	   return SUCCESS;
	}
	//用于前台用户添加或更新个人资格证信息
	public String update() throws IOException{
		p = (PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
		int result = 0;
		Certification ct = DaoFactory.getCertificationDaoInstance().findByUserIdcard(p.getIdCard());
		if(ct!=null){
			String realPath=ServletActionContext.getRequest().getRealPath("/upload/certification");
			//System.out.println("realpath:"+realPath);
			//System.out.println(copyurl);
			InputStream in = new FileInputStream(copyurl);
			String fileType=copyurlFileName.substring(copyurlFileName.lastIndexOf("."));//截取后缀
			String fileName=c.getCnid()+fileType;//文件名为资格证号+后缀
			FileOutputStream out=new FileOutputStream(realPath+"\\"+fileName);
			byte[] buffer=new byte[1024];
			int len=0;
			while((len=in.read(buffer))>0){
				out.write(buffer, 0, len);
			}
			out.close();
			in.close();
			c.setCopyurl("upload/certification/"+fileName);
			result=DaoFactory.getCertificationDaoInstance().update(c);
		
			if (result > 0) {
				HandleMsg("200", "修改成功!", "certification","");
			} else {
				HandleMsg("300", "修改失败!", "certification","");
			}
		}
		else{
			
			String realPath=ServletActionContext.getRequest().getRealPath("/upload/certification");
			InputStream in = new FileInputStream(copyurl);
			String fileType=copyurlFileName.substring(copyurlFileName.lastIndexOf("."));//截取后缀
			String fileName=c.getCnid()+fileType;//文件名为资格证号+后缀
			FileOutputStream out=new FileOutputStream(realPath+"\\"+fileName);
			byte[] buffer=new byte[1024];
			int len=0;
			while((len=in.read(buffer))>0){
				out.write(buffer, 0, len);
			}
				
			out.close();
			in.close();
			c.setCopyurl("upload/certification/"+fileName);
			result=DaoFactory.getCertificationDaoInstance().add(c);
			if (result > 0) {
				HandleMsg("200", "添加成功!", "certification","");
			} 
			else {
				HandleMsg("300", "添加失败!", "certification","");
			}
		}
		return SUCCESS;
	}
	public String findByCnid(){
		
		certification_pinfo=DaoFactory.getCertificationDaoInstance().findByCnid(args);
		url="/admin/certificationToLook.jsp";
		return SUCCESS;
	}
	public void HandleMsg(String statusCode, String msg, String tabid,String callback) {
		ajax.setCallbackType(callback);
		ajax.setMessage(msg);
		ajax.setStatusCode(statusCode);
		ajax.setNavTabId(tabid);
		url = "/admin/ajax.jsp";
	}
}
