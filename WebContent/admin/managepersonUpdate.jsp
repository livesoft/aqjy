<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
</head>
<body>
<div class="pageFormContent nowrap" layoutH="97">
	<form method="post" action="/aqjy/userManage/manageperson_update?uid=${manageperson.maid }"class="pageForm required-validate" enctype="multipart/form-data" onsubmit="return iframeCallback(this,navTabAjaxDone)">
	<fieldset>
			<legend align="center"><font color="red">管理人员信息管理</font></legend>
				
			<dl>
				<dt>手机号</dt> 
					<dd><input type="text" name="mp.phone" class="required phone" maxlength="11" value="${manageperson.phone}" /></dd>
			</dl>
			<dl>
				<dt>姓名</dt> 
				<dd><input type="text" name="mp.realName" class="required" value="${manageperson.realName}"/></dd>
			</dl>
			<dl>
				<dt>性别</dt> 
				<dd><input name="mp.sex" type="radio" checked />男 <input name="mp.sex" type="radio" />女</dd>
			</dl>
			<dl>
				<dt>身份证照</dt> 
					<dd><input type="file" name="upload" class="required image"  /></dd>
			</dl>
			<dl>
				<dt>本人照片</dt> 
				<dd><input type="file" name="upload" class="required image" /></dd>
			</dl>
			<dl>
				<dt>电子邮箱</dt> <dd><input type="text" name="mp.email" class="email" value="${manageperson.email}"/></dd>
			</dl>
			<dl>
				<dt>提示问题</dt>
                    <dd>
                    <select name="mp.questions">
				    <option value="${manageperson.questions}">${manageperson.questions}</option>
					<option value="你喜欢的人">你喜欢的人</option>
					<option value="你的大学名称">你的大学名称</option>
					<option value="你崇拜的偶像">你崇拜的偶像</option>
					<option value="你学的文科还是理科">你学的文科还是理科</option>
					<option value="你喜欢的动物是">你喜欢的动物是</option>
					<option value="你喜欢哪个季节">你喜欢哪个季节</option>
				</select>
				</dd>
			</dl>
			<dl>
				<dt>答案</dt> 
				<dd><input type="text" name="mp.answer" value="${manageperson.answer}"/></dd>
			</dl>
			<dl>
				<dt>公司</dt> 
				<dd>
				    <select name="mp.comid" id="comid" >
				        <option value="${manageperson.comid}" id="mcomp">${manageperson.comid}</option>
				    </select>
				</dd>
			</dl>
			<dl>
				<dt>角色</dt> 
				    <dd>
				    <select name="mp.role" id="role">
						<option value="${manageperson.role}" id="mrole">${manageperson.role}</option>
				    </select></dd>
			</dl>
			<dl>
				<dt>状态</dt>
				    <dd><select name="mp.audio" id="audio">
					    <option value="${manageperson.audio}">${manageperson.audio}</option>
				    </select></dd>
			</dl>

			<dl>
				<dt>备注</dt>
				<dd><textarea class="editor" name="mp.memo" rows="6" cols="100" tools="mini"></textarea></dd>
					
			</dl>
			</fieldset>
		 <div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
</body>
<script>
	$(function() {
		$.getJSON("/aqjy/json/findAllCompany",function(data){
			var json=JSON.parse(data);
			for(var i=0;i<json.length;i++){
				$("#comid").append("<option value='"+json[i].comid+"'>"+json[i].companyname+"</option>");
			}
		})
	})
	
	$(function(){
		$.getJSON("/aqjy/json/getDictionary",{table:"MANAGEPERSON",name:"ROLE"},function(data){
			var json=JSON.parse(data);
			for (var i = 0; i < json.length; i++) {
				$("#role").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
			}
			
		})
	})
	$(function(){
		$.getJSON("/aqjy/json/getDictionary",{table:"PINFORMATION",name:"STATUS"},function(data){
			var json=JSON.parse(data);
			for (var i = 0; i < json.length; i++) {
				$("#audio").append("<option value='"+json[i].codeid+"'>"+json[i].codeNumber+"</option>");
			}
			
		})
	})
</script>
</html>
