package henu.bean;
/**
 * @author 田星杰
 * 2015-01-15
 */
public class Meetingsign extends Bean{
	private int msid;//会议签到ID
	private int mcid;//会议内容ID
	private String signPerson;//签到人员
	private String signTime;//签到时间
	private int comid;//集团ID
	private int basicid;//会议基本内容ID
	public int getBasicid() {
		return basicid;
	}
	public void setBasicid(int basicid) {
		this.basicid = basicid;
	}
	public int getMsid() {
		return msid;
	}
	public void setMsid(int msid) {
		this.msid = msid;
	}
	
	public String getSignPerson() {
		return signPerson;
	}
	public void setSignPerson(String signPerson) {
		this.signPerson = signPerson;
	}
	public String getSignTime() {
		return signTime;
	}
	public void setSignTime(String signTime) {
		this.signTime = signTime;
	}
	public int getMcid() {
		return mcid;
	}
	public void setMcid(int mcid) {
		this.mcid = mcid;
	}
	public int getComid() {
		return comid;
	}
	public void setComid(int comid) {
		this.comid = comid;
	}
	
	
}
