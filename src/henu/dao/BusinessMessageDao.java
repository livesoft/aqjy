package henu.dao;

import henu.bean.BusinessMessage;
import henu.bean.Car;

import java.util.List;

public interface BusinessMessageDao {

	public List<BusinessMessage> findAllByCarId(String property, String key,
			String idCard, String order, String sort, int start, int end);

	public Car findByIdCardAndVehicleLicense(String idCard,
			String vehicleLicense);


	public BusinessMessage findByBusinessIdOrVehicleLicense(String businessId,
			String vehicleLicense);

	public int add(BusinessMessage bm);

	public int delete(int bid);

	public BusinessMessage find(int  bid);

	public int update(BusinessMessage bm);

	public List<BusinessMessage> AdminFindAllByComid(String property,
			String key, int comid, String orderField, String orderDirection,
			int start, int end);
}