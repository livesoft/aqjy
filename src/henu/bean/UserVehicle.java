package henu.bean;

import henu.bean.Bean;

public class UserVehicle extends Bean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	private  int uvid;
	private  String idcard;
	private int carid;
	private String memo;
	public int getUvid() {
		return uvid;
	}
	public void setUvid(int uvid) {
		this.uvid = uvid;
	}
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	public int getCarid() {
		return carid;
	}
	public void setCarid(int carid) {
		this.carid = carid;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
}
