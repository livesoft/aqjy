<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<div class="pageContent">
	<form method="post" action="/aqjy/safetymetting/mettingsign_update" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent nowrap" layoutH="97">
		<!-- <div style="height: 660px;overflow:auto;"> -->
			 <dl>
				<dt>签到编号：</dt>
				<dd>
					<input type="text" name="meetingsign.msid" maxlength="20" value="${meetingsign.msid }" class="required" />
					
				</dd>
			</dl> 
			
			 <dl>
				<dt>公司编号：</dt>
				<dd>
					<input type="text" name="meetingsign.comid" maxlength="20" value="${meetingsign.comid }" />
				</dd>
			</dl> 
			
			<dl>
				<dt>会议内容编号：</dt>
				<dd>
					<input type="text" name="meetingsign.mcid" value="${meetingsign.mcid }"/>
				   
				</dd>
			</dl>
			
			<dl>
				<dt>签到人：</dt>
				<dd>
					<input type="text" name="meetingsign.signPerson" value="${meetingsign.signPerson }"/>
				</dd>
			</dl>
			<%-- <dl>
				<dt>签到时间：</dt>
				<dd>
					<input type="text" name="meetingsign.signTime" value="${meetingsign.signTime }" class="date required" dateFmt="yyyy-MM-dd HH:mm:ss" maxlength="20" readonly="true"/>
					
				</dd>
			</dl> --%>
			<dl>
				<dt>会议通知编号：</dt>
				<dd>
					<input type="text" name="meetingsign.basicid" value="${meetingsign.basicid }"/>
					
				</dd>
			</dl>
			</div>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="submit">修改</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>

</body>
</html>