package henu.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;

import henu.bean.Content;

import henu.bean.Seminar;
import henu.dao.ContentDao;
import henu.dao.SeminarDao;
import henu.dao.factory.DaoFactory;
import henu.service.ExportExcel;
import henu.util.AJAX;
import henu.util.Page;

import com.opensymphony.xwork2.ActionSupport;

public class SeminarAction extends ActionSupport{
	//分页查询
		Page page;
		private int pageNum=1;
		private int numPerPage=10;
		private String orderField="subjecttime";
		private String orderDirection="desc";
		
		//检索
		private String property="-1";
		private String key="-1";
		
		
		private Map<Integer,String> map ;//定义一个值栈
		public Map<Integer, String> getMap() {
			return map;
		}
		public void setMap(Map<Integer, String> map) {
			this.map = map;
		}
		private AJAX ajax;
		private Seminar se;
		private String url="/admin/ajax.jsp";
		private String uid;
		private List<Seminar> list;
		
		
		private String res;
		private String resName;
		private String resType;
		
		public String getUid() {
			return uid;
		}
		public void setUid(String uid) {
			this.uid = uid;
		}
	
		public Page getPage() {
			return page;
		}
		public void setPage(Page page) {
			this.page = page;
		}
		public int getPageNum() {
			return pageNum;
		}
		public void setPageNum(int pageNum) {
			this.pageNum = pageNum;
		}
		public int getNumPerPage() {
			return numPerPage;
		}
		public void setNumPerPage(int numPerPage) {
			this.numPerPage = numPerPage;
		}
		public String getOrderField() {
			return orderField;
		}
		public void setOrderField(String orderField) {
			this.orderField = orderField;
		}
		public String getOrderDirection() {
			return orderDirection;
		}
		public void setOrderDirection(String orderDirection) {
			this.orderDirection = orderDirection;
		}
		public String getProperty() {
			return property;
		}
		public void setProperty(String property) {
			this.property = property;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public AJAX getAjax() {
			return ajax;
		}
		public void setAjax(AJAX ajax) {
			this.ajax = ajax;
		}
		public Seminar getSe() {
			return se;
		}
		public void setSe(Seminar se) {
			this.se = se;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		
		public List<Seminar> getList() {
			return list;
		}
		public void setList(List<Seminar> list) {
			this.list = list;
		}
		
		//列出所有专题
		public String findAll(){
			page = new Page();
			if(orderField.equals("")||orderDirection.equals("")){
				orderField="subjecttime";
				orderDirection="desc";
			}
			if(property.equals("")||key.equals("")){
				property="-1";
				key="-1";
			}
			System.out.println(property);
			page.setProperties("SEMINAR",pageNum,numPerPage,property, key);
			
			list = DaoFactory.getSeminarDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());
		
			url="/admin/seminar.jsp";
			return SUCCESS;
		}
		//前台的专题方法
		public String findAll2(){
			page = new Page();
			if(orderField.equals("")||orderDirection.equals("")){
				orderField="subjecttime";
				orderDirection="desc";
			}
			if(property.equals("")||key.equals("")){
				property="-1";
				key="-1";
			}
			page.setProperties("SEMINAR",pageNum,numPerPage,property,key);
			
			list = DaoFactory.getSeminarDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());
			url="/front/seminaredu.jsp";
			return SUCCESS;
		}
		
		
	
		
		
		//添加专题
		public String add(){
			ajax=new AJAX();
			se.setSubjecttime(new Date().toLocaleString());
			
		/*	if(se.getRemark()==null||se.getRemark().trim().equals("")){
				ajax.setStatusCode("300");
				ajax.setMessage("内容不能为空，请重新填写");
			}else{*/
				System.out.println(se.getSeid()+"\t\t\tthreui"+se.getSubjectname());
				int result = DaoFactory.getSeminarDaoInstance().save(se);
				
				if(result!=0){
					ajax.setStatusCode("200");
					ajax.setMessage("注册成功");
					ajax.setCallbackType("closeCurrent");
					ajax.setNavTabId("seminarInf");
				}else{
					ajax.setStatusCode("300");
					ajax.setMessage("注册失败，请重新填写");
					ajax.setCallbackType("closeCurrent");
					ajax.setNavTabId("seminarInf");
				}
	
			url="/admin/ajax.jsp";
			return SUCCESS;
		}
		//删除专题
		public String delete(){
			System.out.println(uid);
			int seid = Integer.parseInt(uid);
			int result = DaoFactory.getSeminarDaoInstance().delete(seid);
			ajax=new AJAX();
			if(result!=0){
				ajax.setStatusCode("200");
				ajax.setMessage("删除成功");
				ajax.setNavTabId("seminarInf");
				ajax.setCallbackType("");
				
			}else{
				ajax.setStatusCode("300");
				ajax.setMessage("删除失败");
				ajax.setCallbackType("");
			}
			url="/admin/ajax.jsp";
			return SUCCESS; 
		}
		//更新专题
		public String update(){
			SeminarDao dao = DaoFactory.getSeminarDaoInstance();
			se.setSubjecttime(new Date().toLocaleString());
			ajax=new AJAX();
			int sei = Integer.parseInt(ServletActionContext.getRequest().getParameter("seid"));
			int result = dao.update(sei,se);
			System.out.println(se);
			if(result!=0){
				ajax.setStatusCode("200");
				ajax.setMessage("更新成功");
				ajax.setNavTabId("seminarInf");
				ajax.setCallbackType("closeCurrent");
			}else{
				ajax.setStatusCode("300");
				ajax.setMessage("更新失败");
				ajax.setCallbackType("");
			}
			return SUCCESS;
		}
		//查看相应专题下的所有内容列表
		public String findDetail(){
			page = new Page();
			if(orderField.equals("")||orderDirection.equals("")){
				orderField="publish";
				orderDirection="desc";
			}
			if(property.equals("")||key.equals("")){
				property="-1";
				key="-1";
			}
			page.setProperties("safetyeducation",pageNum,numPerPage,property,key);
			ContentDao dao =DaoFactory.getContentDaoInstance();
			List<Content> list = dao.findByDetial(key);
		/*	System.out.println(list.size());
			System.out.println(id);*/
			ServletActionContext.getRequest().getSession().setAttribute("list",list);
			//把传过来的专题id存起来,为content列表用c
			ServletActionContext.getRequest().getSession().setAttribute("sessionUid", key);
			url="/admin/seminarDetail.jsp";
			return SUCCESS;
		}
		
		//查看相应专题下的所有内容列表
				public String findDetail2(){
					page = new Page();
					if(orderField.equals("")||orderDirection.equals("")){
						orderField="publish";
						orderDirection="desc";
					}
					if(property.equals("")||key.equals("")){
						property="-1";
						key="-1";
					}
					page.setProperties("safetyeducation",pageNum,numPerPage,property,key);
					ContentDao dao =DaoFactory.getContentDaoInstance();
					List<Content> list = dao.findByDetial(key);
				/*	System.out.println(list.size());
					System.out.println(id);*/
					ServletActionContext.getRequest().getSession().setAttribute("list",list);
					//把传过来的专题id存起来,为content列表用c
					ServletActionContext.getRequest().getSession().setAttribute("sessionUid", key);
					url="/front/contentManage.jsp";
					return SUCCESS;
				}
		
		
		
		
		//找到uid更新相应的专题，并进行修改
		public String find(){
			
			SeminarDao dao = DaoFactory.getSeminarDaoInstance();
			int seid = Integer.parseInt(uid);
			Seminar s =dao.findById(seid);
			ServletActionContext.getRequest().getSession().setAttribute("seminar", s);
			url="/admin/seminarUpdate.jsp";
			return SUCCESS;
		}		
		//利用goadd方法从值栈中获取列表
		public String goadd(){
			List<Seminar> list = DaoFactory.getSeminarDaoInstance().findSubjectName();
			map = new HashMap<Integer, String>();
			for(Seminar r:list){
				map.put(r.getSeid(), r.getSubjectname());
			}
			ServletActionContext.getRequest().getSession().setAttribute("listmap", map);
			url="/admin/contentAdd.jsp";
			return  SUCCESS;
		}
	
		
}
