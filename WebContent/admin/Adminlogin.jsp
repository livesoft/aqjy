<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>login</title>
<link href="themes/css/Adminlogin.css" rel="stylesheet" type="text/css" />
</head>
<body
	style="background-color: #04090d; background-image: url(themes/default/images/Adminlogin_bg01.png); background-repeat: no-repeat; background-position: center top; overflow: hidden;">
	<div id="mainBody">
		<div id="cloud1" class="cloud"></div>
		<div id="cloud2" class="cloud"></div>
	</div>
	<div class="Main">
		<div class="login_input">
			<div class="inp_login">
				<form id="form" runat="server" action="/aqjy/public/managerLogin">
					<input required='' type='text' name="value" >
					<label alt='用户名' placeholder='Username'></label>
					<input required='' type='password' name="mp.password">
					<label alt='密码' placeholder='Password'></label>
					<input placeholder=""  type='captcha' name="captcha">
					<label alt='验证码' placeholder='Captcha'></label>
					<img src="${base}/jcaptcha.jct" onClick="this.src='${base}/jcaptcha.jct?d='+new Date()" class="captimg" style="margin-left: 135px;" />
					<!-- <span class="submit" id="submit" type="submit" -->
						<input type="submit" id="submit" class="submitb" value=" "/>
						<!-- <a href="" class="button"><img alt="" src="themes/default/images/Adminbtnlogin.gif" /></a> -->
					<!-- </span> -->
				</form>
			</div>
		</div>
		<div class="foot">
			<span class="footspan">河南大学 版权所有 Copyright 20142 地址：中国 河南 开封.明伦街/金明大道<br /></span>
			<span class="footspan">邮编：475001/475004 总机号码：0371—22868833 河南大学蒲公英工作室 制作维护</span>
		</div>
	</div>

</body>
</html>
