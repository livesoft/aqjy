package henu.action;

import henu.bean.Exam;
import henu.dao.ExamDao;
import henu.dao.QuestionsDao;
import henu.dao.factory.DaoFactory;
import henu.service.ImportExcel;
import henu.service.RandomList;
import henu.util.AJAX;
import henu.util.Page;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 考试管理
 * 
 * @author wuhaifeng
 * */
public class ExamAction extends ActionSupport {

	private String args;
	private AJAX ajax = new AJAX();
	private String url;
	private Exam exam;
	private List<Exam> list;
	private Map<String, String> examsubmap;
	private String result;
	private JSONObject jsonob; // 定义的json对象，用来传输接送相关的信息

	public JSONObject getJsonob() {
		return jsonob;
	}

	public void setJsonob(JSONObject jsonob) {
		this.jsonob = jsonob;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	// 分页排序
	private Page page = new Page();
	private int pageNum = 1;
	private int numPerPage = 10;
	private String orderField = "eid";
	private String orderDirection = "asc";
	// 检索
	private String property = "-1";
	private String key = "-1";

	public String getArgs() {
		return args;
	}

	public void setArgs(String args) {
		this.args = args;
	}

	public AJAX getAjax() {
		return ajax;
	}

	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}

	public List<Exam> getList() {
		return list;
	}

	public void setList(List<Exam> list) {
		this.list = list;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getNumPerPage() {
		return numPerPage;
	}

	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	public String getOrderDirection() {
		return orderDirection;
	}

	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Map<String, String> getExamsubmap() {
		return examsubmap;
	}

	public void setExamsubmap(Map<String, String> examsubmap) {
		this.examsubmap = examsubmap;
	}

	// 显示所有的记录
	public String all() {
		// System.out.println("访问到all方法");
		if (orderField.equals("") || orderDirection.equals("")) {
			orderField = "eid";
			orderDirection = "asc";
		}
		if (property.equals("") || key.equals("")) {
			property = "-1";
			key = "－1";
		}
		// System.out.println("action查询的关键字是:" + key);
		// System.out.println("查询的依据:" + property);
		page.setProperties("Exam", pageNum, numPerPage, property, key);
		// 给page这个class设置属性
		list = DaoFactory.getExamDaoInstance().findByProperty(property.trim(),
				key.trim(), orderField.trim(), orderDirection.trim(),
				page.getStart(), page.getEnd());
		url = "/admin/examManage.jsp";
		return SUCCESS;
	}

	/**
	 * 删除一条记录
	 * */
	public String del() {
		// System.out.println("访问到delete方法：" + args);
		// 已经得到要删除的id 调用删除方法
		int delres = DaoFactory.getExamDaoInstance().delete(args);
		if (delres > 0) {
			HandleMsg("200", "删除成功！", "examm");
		} else {
			HandleMsg("300", "删除失败！", "examm");
			// System.out.println("删除失败");
		}
		return SUCCESS;
	}

	public String goadd() {
		examsubmap = new HashMap<String, String>();
		// 此处需要一次获取所有的专题类型也就是subject,我需要一个查询来获取所有的专题
		List<String> allsub = DaoFactory.getQuesttionDaoInstance().findAllSub();
		for (String str : allsub) {
			examsubmap.put(str, str);
		}
		ServletActionContext.getRequest().getSession()
				.setAttribute("examsubmap", examsubmap);
		url = "/admin/examAdd.jsp";
		return SUCCESS;
	}

	public String add() {
		/**
		 * 那边提交的话可能会出现异常数据 根据type的类型重新封装bean 然后调用dao存储
		 * */
		//String type = exam.getSubject();
		//System.out.println("type"+type);
		int addres = DaoFactory.getExamDaoInstance().save(exam);
		if (addres > 0) {
		//	System.out.println("添加成功了");
			ajax.setCallbackType("closeCurrent");
		//	ajax.setForwardUrl("/aqjy/exam/allExam");		//跳转到管理界面
			ajax.setRel("examm");
			ajax.setMessage("添加成功！");
			ajax.setStatusCode("200");
			url = "/admin/ajax.jsp";
		} else {
			HandleMsg("300", "添加失败!", "examm");
		}
		return SUCCESS;
	}

	public String find() {
		// System.out.println("访问到find方法: "+args);
		// 根据需要修改的传过来一个id 然后这里查询出来 这个试题的所有的信息 ，然后跳转到修改试题的界面
		exam = DaoFactory.getExamDaoInstance().findById(Integer.parseInt(args));
		examsubmap = new HashMap<String,String>();
		List<String> allsub = DaoFactory.getQuesttionDaoInstance().findAllSub();
		for (String str : allsub) {
			examsubmap.put(str, str);
		}
		ServletActionContext.getRequest().getSession().setAttribute("examsubmap", examsubmap);
		url = "/admin/examUpdate.jsp";
		return SUCCESS;
	}

	public String edit() {
		// System.out.println("访问到delete方法：" + args);
		// 已经得到要删除的id 调用删除方法
		int edres = DaoFactory.getExamDaoInstance().update(args, exam);
		if (edres > 0) {
				ajax.setCallbackType("closeCurrent");
			//	ajax.setForwardUrl("/aqjy/exam/allExam");		//跳转到管理界面
				ajax.setRel("examm");
				ajax.setMessage("更新成功！");
				ajax.setStatusCode("200");
				url = "/admin/ajax.jsp";
		} else {
			HandleMsg("300", "更新失败！", "examm");
			// System.out.println("删除失败");
		}
		return SUCCESS;
	}

	/**
	 * ajax，用于返回当前题目的信息
	 * 
	 * @param result
	 *            包含题目的信息的json对象
	 * */
	public String inf() {
		// 获取题目指定科目信息
		QuestionsDao quesdao = DaoFactory.getQuesttionDaoInstance();
		// 假如是综合题目就需要直接获取全部的数量
		System.out.println("args:"+args);
		jsonob = new JSONObject(); // jsonob放两个数据，选择题的数量和判断题的数量
		if (args.equals("-1")) {
			int qsize = quesdao.status("type", "判断题", "-1", "-1").size();
			jsonob.put("pan", qsize);
			qsize = quesdao.status("type", "选择题", "-1", "-1").size();
			jsonob.put("xuan", qsize);
		} else {
			int qsize = quesdao.status("type", "判断题", "subject", args).size();
			jsonob.put("pan", qsize);
			qsize = quesdao.status("type", "选择题", "subject", args).size();
			jsonob.put("xuan", qsize);
		}
		result = jsonob.toString();
		//System.out.println("转换成的json数据为：" + result);
		return SUCCESS;
	}

	public void HandleMsg(String statusCode, String msg, String tabid) {
		ajax.setCallbackType("");
		ajax.setMessage(msg);
		ajax.setStatusCode(statusCode);
		ajax.setNavTabId(tabid);
		url = "/admin/ajax.jsp";
	}

}
