<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core"%>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>安全教育培训考核管理系统</title>
<link rel="stylesheet" type="text/css" href="style/style.css" />
<link rel="stylesheet" type="text/css" href="style/online_Service.css" />
<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="js/js.js"></script>
<script type="text/javascript" src="js/pictype.js"></script>
</head>
<body >

	<!-- ******* header 部分  *******-->
    <div class="header">
    	<div class="h_right">
           <span></span>
        </div>
         
    </div>
    <!-- ******* 主题部分  *******-->
    <div class ="conter">
    	<div class="left">
        	<p><img src="images/fresh.png" /></p>
           	<form action="/aqjy/public/login" method="post">
            <table>
            	<tr>
                    <td><input class="input" type="text" name="p.phone" value="18317893811" id="username"/></td>
                </tr>
                <tr>
                    <td><input class="input" type="text" value="123456" id="password"  name="p.password"/></td>
                </tr>
                <tr> 
                	<td>
                	<div class="LORE"><a class="registerP" href="/aqjy/public/register.jsp"></a><input id="submit" type="image" src="images/login.png"/></div>
                    </td>
                </tr>
            </table>
            </form>
            <div class="left_bottom">
            <p><img src="images/link.png" /></p>
                <ul>
                    <li><img src="images/lis.png" alt="" /><a href="#" target="_blank">中国公安部</a></li>
                    <li><img src="images/lis.png" alt="" /><a href="#" target="_blank">中国交通运输部</a></li>
                    <li><img src="images/lis.png" alt="" /><a href="#" target="_blank">河南省人民政府</a></li>
                    <li><img src="images/lis.png" alt="" /><a href="#" target="_blank">河南省交通运输厅</a></li>
                    <li><img src="images/lis.png" alt="" /><a href="#" target="_blank">河南省公路信息港</a></li>
                </ul>
            </div>
            <div class="left_bottom">
            	<p><img src="images/weather.png" /></p>
 				<div class="left_weather"><iframe width="220" scrolling="no" height="200" frameborder="0" allowtransparency="true" src="http://i.tianqi.com/index.php?c=code&id=19&color=%23&icon=1&temp=0&num=1"></iframe></div>
            </div>
        </div>
          <!--   center-right  -->
        <div class="right">
        <!--********导航部分**********-->
        <div class="nav">
    	<ul>
        	<li style="background-image: none;margin-left: 15px;"><a href="/aqjy/index.jsp">首页</a></li>
            <li><a href="/aqjy/public/findNoticeListByProperty?key=新闻公告" target="_blank">新闻公告</a></li>
            <li><a href="#" target="_blank">重要提醒</a></li>
            <li><a href="#" target="_blank">系统介绍</a></li>
            <li><a href="#" target="_blank">其他</a></li>
            <li><a href="#" target="_blank">...</a></li>
        </ul>
    </div>
    <div class="rightmian">
			<!--   图片轮播  -->
        	<div id="pic">
					<div class="picture">
						<a href="#"><img class="pic1" src="images/pic1.png" /></a>
					</div>
					<div class="picture">
						<a href="#"><img class="pic1" src="images/pic2.png" /></a>
					</div>
					<div class="picture">
						<a href="#"><img class="pic1" src="images/pic3.png" /></a>
					</div>
					<div class="picture">
						<a href="#"><img class="pic1" src="images/pic4.png" /></a>
					</div>
					<div class="picture0">
						<img class="pic1" src="images/pic0.png" />
					</div>
					<ul class="num">
						<li class="on"></li>
						<li></li>
						<li></li>
						<li></li>
					</ul>
				</div>
            <!--   center-right-left咨询信息  -->
            <div style="height: 330px;">
            <div class="consult">
            	<p>新闻公告<a href="/aqjy/public/findNoticeListByProperty?key=新闻公告" target="_blank">更多</a></p>
                <ul>	
                </ul>
            </div>
            
            <!--   center-right-right咨询信息  -->
            <div class="news">
            	<p>热点咨询<a href="/aqjy/public/findOnlineChatWithReply" target="_blank">更多</a></p>
            	<ul></ul>
            </div>
            </div>
        </div>
        </div>
        
    </div>

    <!-- ******* footer部分 ******-->
    <div id="footer">
                        <div class="copyright">
                        <ul>
                            <li><a href="http://www.henu.edu.cn/">河南大学</a> 版权所有</li>
                            <li>Copyright &copy; 2014</li>
                            <li>地址：中国 河南 开封.明伦街/金明大道</li>
                        </ul>
                    	</div>
                        <div class="copy">
                        <ul>
                            <li>邮编：475001/475004</li>
                            <li>总机号码：0371—22868833</li>
                            <li><a href="http://wz.henu.edu.cn/">河南大学蒲公英工作室</a> 制作维护</li>
                        </ul>
                    	</div>
               
            
 </div><!-- footer -->
<!--②二维码-->

<!-- 代码begin -->
<div id="online_qq_layer">
  <div id="online_qq_tab">
    <div class="online_icon"></div>
  </div>
  <div id="onlineService">
    <div class="online_windows overz">
      <div class="online_w_top"> </div>
      <!--online_w_top end-->
      <div class="online_w_c overz">
        <div class="online_bar collapse" id="onlineSort1">
          <h2> <a href="javascript:;">在线客服</a> </h2>
          <div class="online_content overz" id="onlineType1" style="display: block;">
            <ul class="overz">
              <li><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=0000000000&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:12345678:51" /></a></li>
              <li><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=0000000000&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:12345678:51" /></a></li>
              <li><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=0000000000&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:12345678:51" /></a></li>
              <li><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=0000000000&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:12345678:51" /></a></li>
              <li>电话：13600000000</li>
            </ul>
          </div>
          <!--online_content end-->
        </div>

        
        <!--online_bar end-->
        <div class="online_bar collapse" id="onlineSort3">
          <h2> <a href="javascript:;">扫一扫，体验功能</a> </h2>
          <div class="online_content overz" id="onlineType3" style="display: block;"> <img src="images/scan.jpg" style="display: block;margin: 0 auto 5px;width: 90px"/> </div>
        </div>
        <!--online_bar end-->
      </div>
      <!--online_w_c end-->
      <div class="online_w_bottom"> </div>
      <!--online_w_bottom end-->
    </div>
    <!--online_windows end-->
  </div>
</div>
</body>
</html>
