package henu.bean;

public class Dictionary {
	private int ddid;
	private int codeid;
	private String codeNumber;
	private String remark;
	public int getDdid() {
		return ddid;
	}
	public void setDdid(int ddid) {
		this.ddid = ddid;
	}
	public int getCodeid() {
		return codeid;
	}
	public void setCodeid(int codeid) {
		this.codeid = codeid;
	}
	public String getCodeNumber() {
		return codeNumber;
	}
	public void setCodeNumber(String codeNumber) {
		this.codeNumber = codeNumber;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
