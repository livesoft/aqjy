package henu.dao;

import henu.bean.Company;
import henu.bean.ManagePerson;

import java.util.List;

/**
 * @author Administrator
 *
 */
/**
 * @author Administrator
 *
 */
public interface ManagePersonDao {
	
	/**
	 * @param mp
	 * @return
	 * 添加
	 */
	public int add(ManagePerson mp);
	
	/**
	 * @param maid
	 * @return
	 * 根据ID删除
	 */
	public int deleteById(String maid);
	/**
	 * @param mp
	 * @return
	 */
	public int update(String maid,ManagePerson mp);
	public void updateLastTime(String idCard);
	/**
	 * @param phone
	 * @param password
	 * @return
	 * 判断登录
	 */
	public ManagePerson login(String phone,String password);
	public ManagePerson findByMaidOrPhone(String maid,String phone);
	public ManagePerson findById(String maid);
	public List<ManagePerson> findAll();
	public List<ManagePerson> findByProperty(String property, String key,String order, String sort, int start, int end);
	public List<ManagePerson> findAllByComId(String property, String key,int comId,String order, String sort, int start, int end);
}
