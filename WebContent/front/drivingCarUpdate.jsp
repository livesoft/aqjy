
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<body style="position: relative;">

	<div class="pageContent" >
	<form method="post" action="/aqjy/usertotalview/drivingCar_update?uid=${drivingCar.drid }" enctype="multipart/form-data" class="pageForm required-validate" onsubmit="return iframeCallback(this,navTabAjaxDone);">
		<div class="pageFormContent nowrap" layoutH="97">
		<input type="hidden" name="dc.drid" value="${drivingCar.drid}">
			<dl>
				<dt>行车证号：</dt>
				<dd>
					<input type="text" name="dc.drivingId" value="${drivingCar.drivingId}" maxlength="50" class="required" readonly/>
				</dd>
			</dl>
			<dl>
				<dt>车牌号：</dt>
				<dd>
					<input type="text" name="dc.vehicleLicense"  value="${drivingCar.vehicleLicense}" maxlength="20" class="required" readonly/>
				</dd>
			</dl>
			<dl>
				<dt>车辆类型：</dt>
				<dd>
					<select name="dc.vehicleType" id="vehicleType">
						<option value="${drivingCar.vehicleType }">${drivingCar.vehicleType}</option>
					</select>
				</dd>
			</dl>
			<dl>
				<dt>所有人：</dt>
				<dd>
					<input type="text" name="dc.owners"  value="${drivingCar.owners}" class="required"/>
				</dd>
			</dl>
			<dl>
				<dt>使用性质：</dt>
				<dd>
					<input type="text" name="dc.useCharacter"  value="${drivingCar.useCharacter}"/>
				</dd>
			</dl>
			<dl>
				<dt>品牌型号：</dt>
				<dd>
					<input type="text" name="dc.models"  value="${drivingCar.models}"/>
				</dd>
			</dl>
			<dl>
				<dt>车辆类型识别代号：</dt>
				<dd>
					<input type="text" name="dc.vin"  value="${drivingCar.vin}"/>
				</dd>
			</dl>
			<dl>
				<dt>行车证复印件：</dt>
				<dd>
					<input type="file" name="upload"  class="image"/>
				</dd>
			</dl>
			<dl>
				<dt>发动机号码：</dt>
				<dd>
					<input type="text" name="dc.engineid"  value="${drivingCar.engineid}" />
				</dd>
			</dl>
			<dl>
				<dt>档案编号：</dt>
				<dd>
				<input type="text" name="dc.doclc"  value="${drivingCar.doclc}"/>
					
				</dd>
			</dl>
			<dl>
				<dt>核载人数：</dt>
				<dd>
					<input type="text" name="dc.menNumber"  value="${drivingCar.menNumber}"/>
				</dd>
			</dl>
			<dl>
				<dt>总质量：</dt>
				<dd>
					<input type="text" name="dc.totalWeight"  value="${drivingCar.totalWeight}"/>
				</dd>
			</dl>
			<dl>
				<dt>整备质量：</dt>
				<dd>
					<input type="text" name="dc.weight"  value="${drivingCar.weight}"/>
				</dd>
			</dl>
			<dl>
				<dt>核定载质量：</dt>
				<dd>
					<input type="text" name="dc.checkWeight"  value="${drivingCar.checkWeight}"/>
				</dd>
			</dl>
			<dl>
				<dt>外型尺寸：</dt>
				<dd>
					<input type="text" name="dc.sizes"  value="${drivingCar.sizes}"/>
				</dd>
			</dl>
			<dl>
				<dt>准牵引质量：</dt>
				<dd>
					<input type="text" name="dc.towWeight"  value="${drivingCar.towWeight}"/>
				</dd>
			</dl>
			<dl>
				<dt>住址：</dt>
				<dd>
					<input type="text" name="dc.address"  value="${drivingCar.address}"  maxlength="50"/>
				</dd>
			</dl>
			<dl>
				<dt>有效期：</dt>
				<dd>
					<select name="dc.validation">
					    <option value="${drivingCar.validations}">${drivingCar.validations}</option>
						<option value="1年">1年</option>
						<option value="2年">2年</option>
						<option value="3年">3年</option>
					</select>
				</dd>
			</dl>
			
			<dl>
				<dt>备注：</dt>
				<dd>
						<textarea rows="6" cols="100"  class="editor"  name="dc.memo " tools="mini" >${drivingCar.memo }</textarea>
				</dd>
			</dl>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
		</div>
	</form>
	
</div>

</body>
<script>
$(function(){
	$.getJSON("/aqjy/json/getDictionary",{table:"CARINFORMATION，DRIVINGCAR",name:"VEHICLETYPE"},function(data){
		var json=JSON.parse(data);
		for (var i = 0; i < json.length; i++) {
			$("#vehicleType").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
		}
		
	})
	/* $.getJSON("/aqjy/json/getDictionary",{table:"BUSINESSMESSAGE",name:"BUSINESSRAGE"},function(data){
		var json=JSON.parse(data);
		for (var i = 0; i < json.length; i++) {
			$("#businessRage").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
		}
		
	}) */
})
</script>
