package henu.action;

import henu.bean.Company;
import henu.dao.CompanyDao;
import henu.dao.factory.DaoFactory;
import henu.service.ExportExcel;
import henu.service.FileService;
import henu.util.AJAX;
import henu.util.Page;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class CompanyAction extends ActionSupport{
        private Company c;
        private String uid;
        private String result;
		private List<Company> list;
        AJAX ajax;
        private String url="/admin/ajax.jsp";
        Page page;
    	private int pageNum=1;
      	private int numPerPage=10;
      	private String orderField="comid";  
    	private String property="-1";
        private String key="-1";
       //导出文件
      	private String res;
      	private List<String> filepaths;
    	private List notNeedId;
    	private String resName;
    	private String resType;
    	private String belongid;
    	public String getBelongid() {
			return belongid;
		}
		public void setBelongid(String belongid) {
			this.belongid = belongid;
		}
		public String getCompanyname() {
			return companyname;
		}
		public void setCompanyname(String companyname) {
			this.companyname = companyname;
		}
		private String companyname;
    	public String getRes() {
			return res;
		}
		public void setRes(String res) {
			this.res = res;
		}
		public List<String> getFilepaths() {
			return filepaths;
		}
		public void setFilepaths(List<String> filepaths) {
			this.filepaths = filepaths;
		}
		public List getNotNeedId() {
			return notNeedId;
		}
		public void setNotNeedId(List notNeedId) {
			this.notNeedId = notNeedId;
		}
		public String getResName() {
			return resName;
		}
		public void setResName(String resName) {
			this.resName = resName;
		}
		public String getResType() {
			return resType;
		}
		public void setResType(String resType) {
			this.resType = resType;
		}

    	
        public String getResult() {
			return result;
		}
		public void setResult(String result) {
			this.result = result;
		}
		public String getUid() {
			return uid;
		}
		public void setUid(String uid) {
			this.uid = uid;
		}
        
      	public String getProperty() {
			return property;
		}
		public void setProperty(String property) {
			this.property = property;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public Page getPage() {
			return page;
		}
		public void setPage(Page page) {
			this.page = page;
		}
		public int getPageNum() {
			return pageNum;
		}
		public void setPageNum(int pageNum) {
			this.pageNum = pageNum;
		}
		public int getNumPerPage() {
			return numPerPage;
		}
		public void setNumPerPage(int numPerPage) {
			this.numPerPage = numPerPage;
		}
		public String getOrderField() {
			return orderField;
		}
		public void setOrderField(String orderField) {
			this.orderField = orderField;
		}
		public String getOrderDirection() {
			return orderDirection;
		}
		public void setOrderDirection(String orderDirection) {
			this.orderDirection = orderDirection;
		}
		private String orderDirection="asc";  
		public AJAX getAjax() {
			return ajax;
		}
		public void setAjax(AJAX ajax) {
			this.ajax = ajax;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public Company getC() {
			return c;
		}
		public void setC(Company c) {
			this.c = c;
		}
		public List<Company> getList() {
			return list;
		}
		public void setList(List<Company> list) {
			this.list = list;
		}
	public String add() throws Exception{
	    
		String url1 = new String();
    	for(String str : FileService.getFilelist()){
    		str=str.substring(str.indexOf("upload"));
    		url1 += str+";";		    		
    		System.out.println(str);
    	}
		
        CompanyDao dao =DaoFactory.getCompanyDaoInstance();
		int result = dao.save(c);
		ajax=new AJAX();
		if(result!=0){
			
			ajax.setStatusCode("200");
			ajax.setMessage("添加成功");
			ajax.setNavTabId("companyinfo");
			ajax.setCallbackType("closeCurrent");
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("添加失败，请重新填写");
		}
		return "success";
		
	}
	public String delete() throws Exception{
		 CompanyDao dao = DaoFactory.getCompanyDaoInstance();
		 int result=dao.delete(uid);
			ajax=new AJAX();
			if(result!=0){
				ajax.setStatusCode("200");
				ajax.setMessage("删除成功");
				ajax.setNavTabId("companyinfo");
				ajax.setCallbackType("");
				
			}else{
				ajax.setStatusCode("300");
				ajax.setMessage("删除失败");
			}
			
		return SUCCESS;
	}
	 public String find()throws Exception{
			CompanyDao dao=DaoFactory.getCompanyDaoInstance();
			System.out.println("8888888888"+uid);
			c=dao.findBy(uid);
			url="/admin/CompanyUpdate.jsp";
			return SUCCESS;
		}
	 
	 public String update() throws Exception{
		 CompanyDao dao = DaoFactory.getCompanyDaoInstance();
		 int result=dao.upadte(c);
		 System.out.println(result);
		 ajax=new AJAX();
		 if(result != 0){
				ajax.setStatusCode("200");
				ajax.setMessage("更新成功");
				ajax.setNavTabId("companyinfo");
				ajax.setCallbackType("closeCurrent");
				
			}else{
				ajax.setStatusCode("300");
				ajax.setMessage("更新失败");
			}
		return 	SUCCESS;
	}
	public String findAll() throws Exception{
		page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="comid";
			orderDirection="asc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("Company",pageNum,numPerPage,property,key);
		
		list = DaoFactory.getCompanyDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());
		url="/admin/CompanyManage.jsp";
		return SUCCESS;
	}   
	public String findAllCompany() throws Exception{
		result=JSONArray.fromObject(DaoFactory.getCompanyDaoInstance().findAllCompany()).toString();
		return SUCCESS;
	}     
  
	public String export() {
		/*
		 * 这个aciton的目的就是生产一个地址就够了
		 */
		/*String basepath = "D:/workplace/yxxt/WebContent";*/
		String basepath = ServletActionContext.getServletContext().getRealPath("/");
		list = DaoFactory.getCompanyDaoInstance().findAll("comid", "asc"); // 获取所有的记录生成list
		res = ExportExcel.companyExce(list, basepath);
		resName="CompanyMod.xls";
		resType="application/vnd.ms-excel";
		return "export";
	}
	
	public String findBelongid(){
		System.out.println("findBelongId-------------->>>>>>>>>>>>>>");
		JSONArray json = JSONArray.fromObject(DaoFactory.getCompanyDaoInstance().findBelongid());
		belongid  = json.toString();
		return SUCCESS;
	}
	
	public String findCopmpanyName(){
		System.out.println("companyname");
		JSONArray json = JSONArray.fromObject(DaoFactory.getCompanyDaoInstance().findCompany(belongid));
		companyname  = json.toString();
		return SUCCESS;
	}
		
}
