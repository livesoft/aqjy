package henu.dao.impl;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import henu.bean.Notice;
import henu.dao.NoticeDao;
import henu.util.Dbcp;

public class NoticeDaoImpl implements NoticeDao {
	/*
	 * c存储一条新闻
	 */
	@Override
	public int save(Notice n) {
		// TODO Auto-generated method stub
		int result=0;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="insert into notice(title,type,content,viewNumbers,publishTime,publisher,isTop) values(?,?,?,?,?,?,?)";
			Object[] params={n.getTitle(),n.getType(),n.getContent(),n.getViewNumbers(),
					n.getPublishTime(),n.getPublisher(),n.getIsTop()};
			result=qr.update(sql, params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Notice> findByProperty(String property, String key,
			String order, String sort,int start,int end) {
		// TODO Auto-generated method stub
		List<Notice> list=null;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="SELECT * FROM (SELECT tt.*, ROWNUM ro FROM (select * from notice where "+property+"='"+key+"'  order by isTop desc,"+order+" "+sort+") tt WHERE ROWNUM <="+end+") WHERE ro > "+start+"";
			list=qr.query(sql, new BeanListHandler<Notice>(Notice.class));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return list;
	}

	@Override
	public int delete(String uid) {
		// TODO Auto-generated method stub
		int result=0;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="delete from notice where nid=?";
			result=qr.update(sql, uid);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Notice findByNid(String uid) {
		// TODO Auto-generated method stub
		Notice n=null;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="select * from notice where nid=?";
			n=qr.query(sql, new BeanHandler<Notice>(Notice.class),uid);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return n;
	}

	@Override
	public int update(String uid, Notice n) {
		// TODO Auto-generated method stub
		int result=0;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="update notice set title=?,publisher=?,publishTime=?,type=?,content=?,isTop=? where nid=?";
			Object[] params={n.getTitle(),n.getPublisher(),n.getPublishTime(),n.getType(),n.getContent(),n.getIsTop(),uid};
			result=qr.update(sql, params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void noticeViewNumbersAddOne(String uid) {
		// TODO Auto-generated method stub
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="update notice set viewNumbers=viewNumbers+1 where nid=?";
			qr.update(sql, uid);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	
	

}
