package henu.dao;

import java.util.List;

import henu.bean.Notice;

public interface NoticeDao {

	public int save(Notice n);

	public List<Notice> findByProperty(String property, String key, String order,
			String sort, int start, int end);

	public int delete(String uid);

	public Notice findByNid(String uid);

	public int update(String uid, Notice n);

	public void noticeViewNumbersAddOne(String uid);
	
}
