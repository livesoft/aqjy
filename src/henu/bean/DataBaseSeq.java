package henu.bean;

/**
 * 获取数据库某个序列的当前值用
 * @author 杜山
 * 2015-01-24
 *
 */
public class DataBaseSeq extends Bean{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private int nextval;

	public int getNextval() {
		return nextval;
	}

	public void setNextval(int nextval) {
		this.nextval = nextval;
	}

	
}
