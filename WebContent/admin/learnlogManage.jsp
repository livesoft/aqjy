
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@taglib prefix="s" uri="/struts-tags" %>
   

<form id="pagerForm" method="post" action="/aqjy/safetyeducation/LearnLogAction_findAll">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${page.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" /> 
    <input type="hidden" name="orderDirection"  value="${param.orderDirection}" />  
    <input type="hidden" name="property"  value="${property}">
    <input type="hidden" name="key"  value="${key}">
</form>
<div class="pageHeader">
	
	<form onsubmit="return navTabSearch(this);" action="/aqjy/safetyeducation/LearnLogAction_findAll" method="post">
	<div class="searchBar">
	
		<!--<ul class="searchContent">
			<li>
				<label>我的客户：</label>
				<input type="text"/>
			</li>
			<li>
			<select class="combox" name="province">
				<option value="">所有省市</option>
				<option value="北京">北京</option>
				<option value="上海">上海</option>
				<option value="天津">天津</option>
				<option value="重庆">重庆</option>
				<option value="广东">广东</option>
			</select>
			</li>
		</ul>
		-->
		<table class="searchContent">
			<tr>
				<td>
					内容属性：
				</td>
				<td>
              
					<select class="combox" name="property" >
							<option value="" >根据名称查询</option>
							<option value="realname">姓名</option>
							<option value="idcard">身份证号</option>			
							<option value="companyname">所属公司</option>			
					</select>
				</td>
				<td>
					要查询的值：
						<input type="text" value="" name="key">			
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>	
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
				
			</ul>
			<div style="color:red;margin-top:5px;">注意:点击标题可以进行排序；检索模式下若想查询所有信息，再点击一次检索按钮即可；数据较多时可改变每页多少条以显示页码</div>
		</div>
	</div>
	</form>
</div>

<div class="pageContent">
<div class="panelBar">
			<ul class="toolBar">
				<li>
					<a title="确实要删除这些记录吗?" target="selectedTodo" rel="ids" href="/aqjy/safetyeducation/LearnLogAction_batchDelete" class="delete"><span>批量删除默认方式</span></a>	
				<li>
			</ul>
		</div>
	<%-- <div class="panelBar">
		<ul class="toolBar">
			<li class="line">line</li>
			<li><a class="icon" href="/aqjy/safetyeducation/ContentAction_export" target="dwzExport" targetType="navTab" title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li> 
		</ul>
	</div> --%>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th width="22"><input type="checkbox" group="ids" class="checkboxCtrl"></th>
				<th>姓名</th>
				<th>身份证号</th>
				<th>所属公司</th>
				<th>标题</th>
				<th orderField="starttime" <c:if test='${orderField == "starttime" }'> class="${orderDirection}"  </c:if> >浏览时间</th>
				<%-- <th orderField="publishtime" <c:if test='${orderField == "publishtime" }'> class="${orderDirection}"  </c:if> >发布时间</th> --%>
				<th>浏览时长</th>
				
			</tr>
		</thead>
		<tbody>
			<c:forEach  var="lg"  items="${list }">
				<tr target="sid_user" rel="${lg.llid }">
					<td><input name="ids" value="${lg.llid }" type="checkbox"></td>
					<%-- <td><a href="/aqjy/safetyeducation/ContentAction_findDetail?uid=${sp.sid}" target="navTab">${sp.title }</a></td> --%>
					<td>${lg.realname }</td>
					<td>${lg.idcard }</td>
					<td>${lg.companyname }</td>
					<td>${lg.title }</td>
					<td>${lg.starttime}</td>
					<td>${lg.studytime }</td>
					
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10" >10</option>
					<option value="50" selected="selected">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '100' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" selected="selected">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '500' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" >100</option>
					<option value="500" selected="selected">500</option>
				</c:if>
			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>

	</div>
</div>
