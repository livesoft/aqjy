<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<div class="pageFormContent nowrap" layoutH="97">

	<form method="post" action="/aqjy/upFile" enctype="multipart/form-data"	class="pageForm required-validate" onsubmit="return iframeCallback(this);">
		
		<fieldset>
			<input type="hidden" name="uptype" value="onlyup" />
			<input type="hidden" name="mainpath" value="CarInfo" />
			
			<legend align="center"><font color="red">证件图片上传</font></legend>
		
				<p>
				<label>驾驶证复印件</label>
					<input type="file" name="upload">
				</p>
				
				<p>
				<label>车辆照片</label>
					<input type="file" name="upload">
				</p>
				<p>
				<label>购车发票复印件</label>
					<input type="file" name="upload">
				</p>

			<dl class="nowrap">
				<dd>
					<input type="submit" value="上传图片" />
				</dd>
			</dl>

		</fieldset>
		</form>
	<form method="post" action="/aqjy/usertotalview/Car_add" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
	<fieldset>
			<legend align="center"><font color="red">车辆信息填写</font></legend>
			

			<p>
				<label>车主身份证</label>
					<input type="text" name="car.userid"  readonly="readonly" value="${PERSON.idCard}"/>				
			</p>
			<p>
				<label>所属于集团编号</label>
					<input type="text" name="car.comid"  class="required digits"   />				
			</p>
			<p>
				<label>车辆识别码</label>
					<input type="text" name="car.vin"   />				
			</p>
			
			<p>
				<label>发动机号</label>
					<input type="text" name="car.engineno"   />				
			</p>
			
			<p>
				<label>车牌号</label>
					<input type="text" name="car.vehicleLicense"   />				
			</p>
			
			<p>
				<label>车架号</label>
					<input type="text" name="car.vfn"   />				
			</p>
			<p>
				<label>车长</label>
					<input type="text" name="car.carLength"   />				
			</p>
			<p>
				<label>车宽</label>
					<input type="text" name="car.carWidth"   />				
			</p>
			
			<p>
				<label>车高</label>
					<input type="text" name="car.carHeight"   />				
			</p>
			
			<p>
				<label>载重量</label>
					<input type="text" name="car.deadWeight"   />				
			</p>
			<p>
				<label>马力</label>
					<input type="text" name="car.horsePower"   />				
			</p>
			<p>
				<label>车型</label>
					<input type="text" name="car.vehicleType"   />				
			</p>
			<p>
			<label>购车时间</label>
					<input type="text" name="car.buyTime"  class="date textInput readonly"  datefmt="yyyy-MM-dd"  readonly="true" />					
			</p>
			<p>
				<label>状态</label>
					<input type="text" name="car.status"   />				
			</p>
			
			<p>
				<label>是否挂车</label>
					<input type="text" name="car.isTrailer"   />				
			</p>
			
			<p>
				<label>条形码</label>
					<input type="text" name="car.qrCode"   />				
			</p>
			
<!-- 			<p>
				<label>行车证复印件</label>
					<input type="text" name="car.copyLicence"   />				
			</p>
			
			<p>
				<label>车辆照片</label>
					<input type="text" name="car.carPhoto"   />				
			</p>
			

			<p>
				<label>购车发票复印件</label>
					<input type="text" name="car.invoice"   />				
			</p> -->
			
			</fieldset>
			<%-- <dl >
				<dt>宿舍楼编号</dt>
				<dd>
					<input type="text" name="bId" class="required" />				
				</dd>
			</dl>
			<dl >
				<dt>房间号</dt>
				<dd>
					<input type="text" name="sh.roomNum"  class="required" />				
				</dd>
			</dl>
			<dl >
				<dt>床位号</dt>
				<dd>
				<select id="select" name="sh.seatNum" class="required">
					<c:forEach	 var="bed"  items="${bedlist}">
						<option value="${bed.dataValue}">${bed.dataValue}</option>
					</c:forEach>
			</select>		
				</dd>
			</dl>
			<dl >
				<dt>宿舍类型</dt>
				<dd>
					<select name="sh.dormitoryType" id="dtype" class="required" >
						<option value="四人间">四人间</option>
						<option value="六人间">六人间</option>
					</select>		
				</dd>
			</dl> --%>

		 <div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
<!-- <script src="jquery-1.7.2.js"></script>
<script>
$(function(){

	
	checkNum();
	
	$("#dtype").click(
		function()
		{
			checkNum();
		}
	);
	function checkNum()
	{
		$("#select").children().show();
		if($("#dtype").val()=="四人间")
			$("#select").children().eq(3).nextAll().hide();			
		else	if($("#dtype").val()=="六人间")
				$("#select").children().eq(5).nextAll().hide();
	}
	
})
</script> -->
</html>