<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="pageFormContent nowrap">
	<form method="post" action="/aqjy/usertotalview/insurance_update.action?args=${insurance.inid}" class="pageForm required-validate"  onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent" layoutH="56" style="height:2000px">
			<dl>
				<dt>车辆ID：</dt>
				<dd><input name="insurance.carid" type="text" size="30" value="${insurance.carid }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>车牌号：</dt>
				<dd><input name="insurance.vehiclelicense" class="required" type="text" size="30" alt="请输入车牌号" value="${insurance.vehiclelicense }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>交强险开始时间：</dt>
				<dd><input type="text" name="insurance.trafficstart" class="date" value="${insurance.trafficstart }" readonly="readonly" /></dd>	
			</dl>
			<dl>
				<dt>交强险结束时间：</dt>
				<dd><input type="text" name="insurance.trafficend" class="date"  value="${insurance.trafficend }" readonly="readonly" /></dd>	
			</dl>
			<dl>
				<dt>交强险单号：</dt>
				<dd><input name="insurance.tno" class="digits" type="text" size="30" value="${insurance.tno }" readonly="readonly" /></dd>
			</dl>
			<dl>
				<dt>商业险开始时间：</dt>
				<dd><input type="text" name="insurance.insurancestart" class="date"  value="${insurance.insurancestart }" readonly="readonly" /></dd>
			</dl>
			<dl>
				<dt>商业险结束时间：</dt>
				<dd><input type="text" name="insurance.insuranceend" class="date" value="${insurance.insuranceend }" readonly="readonly" /></dd>
			</dl>
			<dl>
				<dt>商业险单号：</dt>
				<dd><input name="insurance.ino" class="digits" type="text" size="30"  value="${insurance.ino }" readonly="readonly" /></dd>
			</dl>
			<div style="position: absolute;top:10px;right:20px"><img alt="交强险照片" src="../${insurance.trafficfile}" width="500px" height="500px"  ></div>
			<div style="position: absolute;top:530px;right:20px"><img alt="商业险照片" src="../${insurance.insurancefile}" width="500px" height="500px"  ></div>
			<%-- <dl>
				<dt>备注：</dt>
				<dd><input type="text" rows="4" cols="50" name="insurance.memo" value="${insurance.memo }" readonly="readonly" /></dd>
			</dl> --%>
			<dl>
				<dt>录入时间：</dt>
				<dd><input type="text" size="30" name="insurance.enterdate" class="date" value="${insurance.enterdate }" readonly="readonly" /></dd>
			</dl>
			<dl>
				<dt>更新时间：</dt>
				<dd><input type="text" size="30" name="insurance.updatedate" class="date" value="${insurance.updatedate }" readonly="readonly" /></dd>
			</dl>
			
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<!-- <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li> -->
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">关闭</button></div></div>
				</li>
				
			</ul>
		</div>
	</form>
</div>