package henu.service;

import henu.util.AJAX;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
public class upfileCompany extends ActionSupport{
	 //文件上传
	 private String url="/admin/ajax.jsp";
	 AJAX ajax;
	
    public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public AJAX getAjax() {
		return ajax;
	}

	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}


	private File files ; 
	private String filesFileName ; 
	private String filesContentType ;
	private String savePath;
	public String getSavePath() {
		return savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	public File getFiles() {
		return files;
	}

	public void setFiles(File files) {
		this.files = files;
	}

	public String getFilesFileName() {
		return filesFileName;
	}

	public void setFilesFileName(String filesFileName) {
		this.filesFileName = filesFileName;
	}

	public String getFilesContentType() {
		return filesContentType;
	}

	public void setFilesContentType(String filesContentType) {
		this.filesContentType = filesContentType;
	}

	public String execute() throws Exception {
        String realpath = ServletActionContext.getServletContext().getRealPath("/upload/images");
        //D:\apache-tomcat-6.0.18\webapps\struts2_upload\images
        System.out.println("realpath: "+realpath);
        if (files != null) {
            File savefile = new File(new File(realpath), filesFileName);
            if (!savefile.getParentFile().exists())
                savefile.getParentFile().mkdirs();
            FileUtils.copyFile(files, savefile);
        }

        /*if(files != null){
	        	ajax.setStatusCode("200");
	  			ajax.setMessage("文件上传成功");
	  			ajax.setNavTabId("companyinfo");
	  			ajax.setCallbackType("closeCurrent");
  		}else{
	  			ajax.setStatusCode("300");
	  			ajax.setMessage("上传失败，请重新上传");
  		}
            */
        return SUCCESS;
    }
   

		/*private void close(FileOutputStream fos, FileInputStream fis) {
		    if (fis != null) {
		        try {
		            fis.close();
		        } catch (IOException e) {
		            System.out.println("FileInputStream关闭失败");
		            e.printStackTrace();
		        }
		    }
		    if (fos != null) {
		        try {
		            fos.close();
		        } catch (IOException e) {
		            System.out.println("FileOutputStream关闭失败");
		            e.printStackTrace();
		        }
		    }
		}*/

}
