package henu.bean;

public class BusinessMessage extends Bean{
	private int bid;//自增
	private String businessId;//营运证号
	private String masterName;//业主名
	private String address;//地址
	private int carid;//车辆的id
	private String vehicleLicense;//车牌号
	private String businessPermission;//经营许可证照片路径
	private String seats;//座位和吨数
	private String carType;//车辆类型
	private String carSize;//车辆尺寸
	private String businessRage;//经营范围
	private String publishOffice;//发证机关
	private String publishDate;//发证日期
	private String validityBegin;//有效期开始时间
	private String validityEnd;//有效期结束时间
	private String businessFile;//营运证复印件
	private String memo;//备注
	public int getBid() {
		return bid;
	}
	public void setBid(int bid) {
		this.bid = bid;
	}
	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	public String getMasterName() {
		return masterName;
	}
	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getCarid() {
		return carid;
	}
	public void setCarid(int carid) {
		this.carid = carid;
	}
	public String getVehicleLicense() {
		return vehicleLicense;
	}
	public void setVehicleLicense(String vehicleLicense) {
		this.vehicleLicense = vehicleLicense;
	}
	public String getBusinessPermission() {
		return businessPermission;
	}
	public void setBusinessPermission(String businessPermission) {
		this.businessPermission = businessPermission;
	}
	public String getSeats() {
		return seats;
	}
	public void setSeats(String seats) {
		this.seats = seats;
	}
	public String getCarType() {
		return carType;
	}
	public void setCarType(String carType) {
		this.carType = carType;
	}
	public String getCarSize() {
		return carSize;
	}
	public void setCarSize(String carSize) {
		this.carSize = carSize;
	}
	public String getBusinessRage() {
		return businessRage;
	}
	public void setBusinessRage(String businessRage) {
		this.businessRage = businessRage;
	}
	public String getPublishOffice() {
		return publishOffice;
	}
	public void setPublishOffice(String publishOffice) {
		this.publishOffice = publishOffice;
	}
	public String getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}
	public String getValidityBegin() {
		return validityBegin;
	}
	public void setValidityBegin(String validityBegin) {
		this.validityBegin = validityBegin;
	}
	public String getValidityEnd() {
		return validityEnd;
	}
	public void setValidityEnd(String validityEnd) {
		this.validityEnd = validityEnd;
	}
	public String getBusinessFile() {
		return businessFile;
	}
	public void setBusinessFile(String businessFile) {
		this.businessFile = businessFile;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
}
