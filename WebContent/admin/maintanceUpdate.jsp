
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

	<div class="pageContent">
	<form method="post" action="/aqjy/usertotalview/maintanceAction_update?uid=${cmpc.mid }" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>车牌号码：</dt>
				<dd>
					<input type="text" name=""  value="${cmpc.vehicleLicense }" readonly="readonly" class="required"/>
				</dd>
			</dl>
			<dl>
				<dt>车主姓名：</dt>
				<dd>
					<input type="text" name="" value="${cmpc.realname }" readonly="readonly" class="required"/>
				</dd>
			</dl>
			
			<dl>
				<dt>所属公司：</dt>
				<dd>
					<input type="text" value="${cmpc.companyName}"  class="required" readonly="readonly" />
				</dd>
			</dl>
			<dl>
				<dt>维护时间：</dt>
				<dd>
					<input type="text" name="maintance.maintanceTime" value="${cmpc.maintanceTime}" class="date required" dateFmt="yyyy-MM-dd" maxlength="20"/>
				</dd>
			</dl>
			<dl>
				<dt>维护周期：</dt>
				<dd><input type="text" name="maintance.maintanceCycle" value="${cmpc.maintanceCycle}"  class="required" />月</dd>
				
				<dd><input type="hidden" name="maintance.carid" value="${cmpc.carid }" /></dd>
			</dl>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="submit">修改</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
		</div>
	</form>
	
</div>
<script>

</script>