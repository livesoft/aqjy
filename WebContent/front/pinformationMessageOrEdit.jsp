<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="pageFormContent nowrap">
	<form method="post" action="/aqjy/public/pinformation_jump"class="pageForm required-validate" target="navTab" onsubmit="return navTabSearch(this);">
		<div class="pageFormContent" layoutH="56" style="width:700px;height:600px;float:left">
			<dl>
				<dt>姓名：</dt>
				<dd><input type="text" name="p.realName" value="${p.realName }" readonly="readonly"/></dd>
			</dl>
			
			<dl>
				<dt>身份证号：</dt>
				<dd><input type="text"  name="p.idCard" value="${p.idCard }" readonly="readonly"/></dd>
			</dl>			
			<dl>
				<dt>手机号：</dt>
				<dd><input type="text"  name="p.phone" value="${p.phone }" readonly="readonly"/></dd>		
			</dl>
			<dl>
				<dt>公司：</dt>
				<dd><input type="text"  name="company" value="${company }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>审核状态：</dt>
				<dd><input type="text"  name="p.paudit" value="${p.paudit }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>住址：</dt>
				<dd><input  type="text" name="p.address" value="${p.address }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>用户类型：</dt>
				<dd><input type="text"  name="p.regType" value="${p.regType }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>注册时间：</dt>
				<dd><input type="text"  name="p.regTime" value="${p.regTime }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>邮箱：</dt>
				<dd><input type="text" name="p.email" value="${p.email }" readonly="readonly" /></dd>
			</dl>
			</div>
			<div style="position: absolute;top:10px;right:20px"><img alt="身份证正面照片" src="../${ p.photoPath}" width="450px" height="280px"  ></div>
			
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">编辑信息</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">关闭</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>
