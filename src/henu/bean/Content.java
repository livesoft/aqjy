package henu.bean;

/**
 * 
 * @author 于晨
 * 2015年
 *
 */

public class Content extends Bean{
	private Integer sid;      //ID
	private Integer seid;     //专题类别，与专题类别表关联
	private String title;   //标题
	private String content;   //内容
	private String edutype;  //类型
	private String publishtime;   //发布时间
	private String publisher;    //发布人
	private Integer viewnumbers;    //浏览次数
	private String memo;     //备注
	

	public Integer getSid() {
		return sid;
	}
	public void setSid(Integer sid) {
		this.sid = sid;
	}
	
	public Integer getSeid() {
		return seid;
	}
	public void setSeid(Integer seid) {
		this.seid = seid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPublishtime() {
		return publishtime;
	}
	public void setPublishtime(String publishtime) {
		this.publishtime = publishtime;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getEdutype() {
		return edutype;
	}
	public void setEdutype(String edutype) {
		this.edutype = edutype;
	}
	
	public Integer getViewnumbers() {
		return viewnumbers;
	}
	public void setViewnumbers(Integer viewnumbers) {
		this.viewnumbers = viewnumbers;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
	
}
