package henu.bean;
/**
 * 视图view_car_main_pinfo_company对应的BEAN
 * @author 梁胜彬
 * 2015-01-24
 */
public class CarMaintancePinfoCompany extends Bean{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int carid;
	private String istrailer;
	private String vehicleLicense;
	private int comid;
	private String companyName;
	private String idcard;
	private String realname;
	private int mid;
	private String maintanceCycle;
	private String maintanceTime;
	public int getCarid() {
		return carid;
	}
	public void setCarid(int carid) {
		this.carid = carid;
	}
	public String getIstrailer() {
		return istrailer;
	}
	public void setIstrailer(String istrailer) {
		this.istrailer = istrailer;
	}
	public String getVehicleLicense() {
		return vehicleLicense;
	}
	public void setVehicleLicense(String vehicleLicense) {
		this.vehicleLicense = vehicleLicense;
	}
	public int getComid() {
		return comid;
	}
	public void setComid(int comid) {
		this.comid = comid;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public String getMaintanceCycle() {
		return maintanceCycle;
	}
	public void setMaintanceCycle(String maintanceCycle) {
		this.maintanceCycle = maintanceCycle;
	}
	public String getMaintanceTime() {
		return maintanceTime;
	}
	public void setMaintanceTime(String maintanceTime) {
		this.maintanceTime = maintanceTime;
	}
	
	
}
