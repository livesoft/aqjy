<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<div class="pageContent">
	<form method="post" action="/aqjy/interaction/onlineChatAction_update" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent nowrap" layoutH="97">
		<input type="hidden" name="o.oid" value="${o.oid }">
			
			<dl>
				<dt>姓名：</dt>
				<dd>
					<input type="text" name="o.askName" value="${o.askName}" readonly="readonly" class="required"/>
				</dd>
			</dl>
				<dl>
				<dt>身份证号：</dt>
				<dd>
					<input type="text" name="o.idCard" value="${o.idCard }" readonly="readonly" class="required"/>
				</dd>
			</dl>
			
			<dl>
				<dt>咨询主题：</dt>
				<dd>
					<input type="text" name="o.title" value="${o.title }" class="required" style="width:300px;" maxlength="25"/>
				</dd>
			</dl>
			<dl>
				<dt>咨询版块：</dt>
				<dd>
					<select name="o.range" id="range">
						<option value="${o.range }">${o.range }</option>
					</select>
				</dd>
			</dl>
			<dl>
				<dt>咨询内容：</dt>
				<dd><textarea name="o.askContent" cols="50" rows="4" maxlength="400">${o.askContent }</textarea></dd>
			</dl>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="submit">修改</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
		</div>
	</form>
	
</div>
<script>
$(function(){
	$.getJSON("/aqjy/json/getDictionary",{table:"ONLINECHAT",name:"RANGE"},function(data){
		var json=JSON.parse(data);
		for (var i = 0; i < json.length; i++) {
			$("#range").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
		}
		
	})
})
</script>
</script>
</body>
</html>