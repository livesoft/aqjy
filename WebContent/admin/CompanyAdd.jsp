
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<html>
<body>

	<form method="post" action="/aqjy/upFile" enctype="multipart/form-data"	class="pageForm required-validate" onsubmit="return iframeCallback(this);">
        <fieldset>
			<input type="hidden" name="uptype" value="onlyup" />
			<legend align="center"><font color="red">证件图片上传</font></legend> 
    		 <p>
    		 	 <label>法人代表身份证</label>
				<input type="file" name="upload">
				<label>营 业 执 照: &nbsp;&nbsp;</label>
				<input type="file" name="upload"/>
				
			</p>
			<BR/>
		
			
		    <p >
				<label>组织机构代证码:</label>
				<input type="file" name="upload" />
				<label>特许经营许可证:</label>
				<input type="file" name="upload" />
			</p>
			<br/>
		<p>
			<label>机构代码证:</label>
				<input type="file" name="upload" />
				&nbsp;&nbsp;
				&nbsp;	&nbsp;	&nbsp;		
					<input type="submit" value="上传图片" />
				
		</p>
		 	</fieldset>
	</form>
	<div class="pageFormContent nowrap" layoutH="120">
	<form method="post" action="/aqjy/userManage/com_add" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<fieldset>
           
			
			<p id="left">
				<label >所属集团编号:</label>
					<select name="c.belongid" id="belongid" class="required">
					  <option>请选择集团</option>
					</select>
			</p>
			<p id="left">
				<label>公司类型:</label>
				<input type="text" name="c.companytype" class="required" id="companytype  "/>
			</p>
			<p id="left">
				<label>公司名称:</label>
				    <select name="c.companyname" class="required " id="companyname">
				       <option>请选择公司</option>
				     </select>
			</p>
			<p id="left">
				<label>注册地址:</label>
					<input type="text" name="c.address"  id="address  "/>
			</p>
			<p id="left">
				<label>法人代表:</label>
					<input type="text" name="c.legalperson"  id="legalperson  "/>
			</p>
			<p id="left">
				<label>联系电话:</label>
					<input type="text" name="c.contactphone"  id="contactphone  "/>
			</p>
			<p id="left">
				<label>是否有子公司:</label>
				   <select name="c.isexistcompany"  id="isexistcompany  ">
				     <option value="1">是</option>
				     <option value="0">否</option>
				   </select>
			</p>
			<p id="left">
				<label>成立日期:</label>
					<input type="text" name="c.createtime"  id="createtime  "/>
			</p>
			<p id="left">
				<label>经营范围:</label>
				  <select name="c.businessrange"  id="businessrange  ">
				    <option value="0">货车</option>
				    <option value="1">私家车</option>
				    <option value="2'">出租车</option>
				  </select>
			</p>
			<p id="left">
				<label>联系地址:</label>
					<input type="text" name="c.contactaddress"  id="contactaddress  "/>
			</p>
			<p id="left">
				<label>注册资金:</label>
					<input type="text" name="c.registmoney"  id="registmoney  "/>
			</p>
			<p id="left">
				<label>登记机关:</label>
					<input type="text" name="c.registeroffice"  id="registeroffice  "/>
			</p>
			
			<p id="left">
				<label>营业期限:</label>
					<input type="text" name="c.businesslimit"  id="businesslimit  "/>
			</p>
			<p id="left">
				<label>邮编:</label>
					<input type="text" name="c.postcode"  id="postcode  "/>
			</p>
			<p id="left">
				<label>基本帐户开户行:</label>
					<input type="text" name="c.basicaccoutopen"  id="basicaccoutopen  "/>
			</p>
			<p id="left">
				<label>基本帐户号:</label>
					<input type="text" name="c.basicaccoutid"  id="basicaccoutid  "/>
			</p>
			<p id="left">
				<label>公司负责人:</label>
					<input type="text" name="c.companyprincipal"  id="companyprincipal  "/>
			</p>
			
			<p id="left">
				<label>备注2:</label>
					<input type="text" name="c.memo2"  id="memo2  "/>
			</p>
			<p id="left">
				<label>备注3:</label>
					<input type="text" name="c.memo3"  id="memo3  "/>
			</p>
			
			<p id="left">
				<label>备注4:</label>
					<input type="text" name="c.memo4"  id="memo4"/>
			</p>
			
		<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	  </fieldset> 
   <!-- </div> -->
     
</form>
</div> 
</body>
<script type="text/javascript">
	$(function(){
		
		$.getJSON("/aqjy/json/findbelong",function(data){
			var json=JSON.parse(data);
			for(var i=0;i<json.length;i++){
				$("#belongid").append("<option value='"+json[i].belongid+"'>"+json[i].belongid+"</option>")
			}
			
		})
	})
	$("#belongid").change(function(){
		$("#companyname").empty();
		$.getJSON("/aqjy/json/findcompanyname",{belongid:$("#belongid").val()},function(data){
			var json=JSON.parse(data);
			for(var i=0;i<json.length;i++){
				$("#companyname").append("<option value='"+json[i].companyname+"'>"+json[i].companyname+"</option>")
			}
			
		})
	})
</script>
</html>