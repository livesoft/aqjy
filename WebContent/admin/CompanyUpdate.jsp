<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
</head>
<body>
<div class="pageContent">
	
	<form method="post" action="/aqjy/userManage/com_update" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent nowrap" layoutH="97">
          
             <dl id="left">
				<dt >编号:</dt>
				<dd>
					<input type="text" name="c.comid" id="comid" class="required" value="${c.comid }"/>
				</dd>
			</dl>
           
			<dl id="left">
				<dt >所属集团编号:</dt>
				<dd>
					<input type="text" name="c.belongid" id="belongid" class="required" value="${c.belongid }"/>
				</dd>
			</dl>
			
			<dl id="left">
				<dt>公司名称:</dt>
				<dd>
					<input type="text" name="c.companyname" class="required " id="companyname" value="${c.companyname }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>公司类型:</dt>
				<dd>
					<input type="text" name="c.companytype" class="required" id="companytype" value="${c.companytype}"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>注册地址:</dt>
				<dd>
					<input type="text" name="c.address" class="required" id="address  " value="${c.address }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>法人代表:</dt>
				<dd>
					<input type="text" name="c.legalperson" class="required" id="legalperson  " value="${c.legalperson }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>营业执照:</dt>
				<dd>
					<input type="text" name="c.businesslicense" class="required" id="businesslicense  " value="${c.businesslicense }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>机构代码证:</dt>
				<dd>
					<input type="text" name="c.organizationcode" class="required" id="organizationcode  " value="${c.organizationcode }"/>
				</dd>
			</dl>
			
			<dl id="left">
				<dt>联系电话:</dt>
				<dd>
					<input type="text" name="c.contactphone"  id="contactphone  " value="${c.contactphone }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>是否有子公司:</dt>
				<dd>
					<input type="text" name="c.isexistcompany"  id="isexistcompany  " value="${c.isexistcompany }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>成立日期:</dt>
				<dd>
					<input type="text" name="c.createtime"  id="createtime  " value="${c.createtime }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>经营范围:</dt>
				<dd>
					<input type="text" name="c.businessrange"  id="businessrange  " value="${c.businessrange }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>组织机构代征码:</dt>
				<dd>
					<input type="text" name="c.organizecard"  id="organizecard  " value="${c.organizecard }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>联系地址:</dt>
				<dd> 
					<input type="text" name="c.contactaddress"  id="contactaddress  " value="${c.contactaddress }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>注册资金:</dt>
				<dd>
					<input type="text" name="c.registmoney"  id="registmoney  " value="${c.registmoney }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>登记机关:</dt>
				<dd>
					<input type="text" name="c.registeroffice"  id="registeroffice  " value="${c.registeroffice }"/>
				</dd>
			</dl>
			
			<dl id="left">
				<dt>营业期限:</dt>
				<dd>
					<input type="text" name="c.businesslimit"  id="businesslimit  " value="${c.businesslimit }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>特许经营许可证号码:</dt>
				<dd>
					<input type="text" name="c.allowbusinesscard"  id="allowbusinesscard  " value="${c.allowbusinesscard }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>邮编:</dt>
				<dd>
					<input type="text" name="c.postcode"  id="postcode  " value="${c.postcode }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>基本帐户开户行:</dt>
				<dd>
					<input type="text" name="c.basicaccoutopen"  id="basicaccoutopen  " value="${c.basicaccoutopen }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>基本帐户号:</dt>
				<dd>
					<input type="text" name="c.basicaccoutid"  id="basicaccoutid  " value="${c.basicaccoutid }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>公司负责人:</dt>
				<dd>
					<input type="text" name="c.companyprincipal"  id="companyprincipal  " value="${c.companyprincipal }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>备注1:</dt>
				<dd>
					<input type="text" name="c.memo1"  id="memo1  " value="${c.memo1 }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>备注2:</dt>
				<dd>
					<input type="text" name="c.memo2"  id="memo2  " value="${c.memo2 }"/>
				</dd>
			</dl>
			<dl id="left">
				<dt>备注3:</dt>
				<dd>
					<input type="text" name="c.memo3"  id="memo3  " value="${c.memo3 }"/>
				</dd>
			</dl>
			
			<dl id="left">
				<dt>备注4:</dt>
				<dd>
					<input type="text" name="c.memo4"  id="memo4" value="${c.memo4 }"/>
				</dd>
			</dl>
		 <div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
	
</div>

</body>

</html>