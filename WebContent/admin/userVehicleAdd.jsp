<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<div class="pageFormContent nowrap" layoutH="97">
	
	<form method="post" action="/aqjy/usertotalview/UserV_add" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">

			<dl>
				<dt>编号</dt>
				<dd>
					<input type="text" name="uv.uvid"  class="required digits"/>
				</dd>
			</dl>
			
			<dl>
				<dt>用户身份证号</dt>
				<dd>
					<input type="text" name="uv.idcard" class="required digits"/>
				</dd>
			</dl>
			<dl>
				<dt>车辆编号</dt>
				<dd>
					<input type="text" name="uv.carid"   class="required digits"/>
				</dd>
			</dl>
			
			<dl>
				<dt>备注</dt>
				<dd>
					<input type="text" name="uv.memo"   />
				</dd>
			</dl>

		 <div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>

</html>