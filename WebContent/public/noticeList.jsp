<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>更多新闻</title>
<style type="text/css">
	*{
		font-size: 14px;
	}
	#main{
		width: 1000px;
		margin: auto;
		border-bottom: 2px #67bded solid;
	}
	#newsTop{
		width:1000px;
		height:28px;
		background-color:#ebfff9;
		font-weight: bold;
		font-size: 15px;
		line-height: 28px;
		padding-left:5px;
		font-family:Microsoft Yahei;
		color: #000080;
		margin-bottom: 10px;
	}
	#newsTop a{
		font-family:Microsoft Yahei;
		color: #000080;
		font-size: 15px;
		text-decoration: none;
	}
	#newsTop a:HOVER{
	color: red;
	}
	#newsTitle{
		margin-top:2px;
		width:796px;
		height:40px;
		background-color:#f2f2f2;
		float: left;
		border: 2px #dbdbdb solid;
		font-size: 20px;
		line-height: 40px;
	}
	#newsTime{
		margin-top:2px;
		width:198px;
		height:40px;
		background-color:#f2f2f2;
		border: 2px #dbdbdb solid;
		border-left:none;
		line-height:40px;
		font-size: 20px;
		text-align:center;
		float: left;	
	}
	table{
		width: 1000px;
		border-collapse: collapse;
	}
	#content td{
		border-bottom: 1px dashed #aabac6;
		height:30px;
		font-size: 15px;
		text-align: left;

		
	}
	#content{
		width:1000px;
		
	}
	#content a{
		
		text-decoration:none;
		color:#333333;
	}
	#content a:HOVER{
		color:#00498d;
	}
	#content span{
		color:#ff0000;
		font-size: 14px;
	}
	#page{
		width:1000px;
		text-align: center;
		background-color: #f2f2f2;
		height: 50px;
		line-height: 50px;
		
	}
	#page a{
		text-decoration:none;
		color:#red;
	}
	#page a:HOVER{
		text-decoration:none;
		color:red;
	}
</style>
<script  src="../js/jquery-1.11.1.js"></script>
<script type="text/javascript">
$(function(){
	$("td").eq(0).css({"width":"800px"});
	$("tr").mouseover(function(){
		$(this).css({"background-color":"#ebfff9"});
	})
	$("tr").mouseleave(function(){
		$(this).css({"background-color":""});
	})
})
</script>
</head>
<body>
	<div id="main">
		<jsp:include page="head.jsp"></jsp:include>
		<div id="newsTop">所在位置：<a href="/aqjy/index.jsp">首页</a>>>新闻公告</div>
		<c:if test="${empty list }">
			<div style="width:1000px;height: 600px;line-height: 600px;text-align: center;color:red;font-weight: bold;">管理人员还没有发布新闻</div>
		</c:if>
		<c:if test="${not empty list }">
			<div id="content">
				<table>
					<c:forEach var="n" items="${list }">
					
						<tr>
							<c:if test="${n.isTop==1 }">
								<td>&nbsp;&nbsp;<img src="../images/icon.jpg">&nbsp;<a href="/aqjy/public/findByNidOut?uid=${n.nid }" target="_blank">${n.title }<span>【置顶】</span></a></td>
								<td>${n.publishTime }</td>
							</c:if>
							<c:if test="${n.isTop==0 }">
								<td>&nbsp;&nbsp;<img src="../images/icon.jpg">&nbsp;<a href="/aqjy/public/findByNidOut?uid=${n.nid }" target="_blank">${n.title }</a></td>
								<td>${n.publishTime }</td>
							</c:if>
						</tr>
					
					</c:forEach>
				</table>
				
			</div>
		</c:if>
		<div id="page"><jsp:include page="page.jsp"></jsp:include></div>
		<jsp:include page="foot.jsp"></jsp:include>
	</div>
	

</body>
</html>