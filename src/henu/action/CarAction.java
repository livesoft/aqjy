package henu.action;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import henu.bean.Car;
import henu.bean.PInformation;
import henu.bean.UserVehicle;
import henu.dao.factory.DaoFactory;
import henu.service.ExportExcel;
import henu.service.FileService;
import henu.util.AJAX;
import henu.util.CONSTANTS;
import henu.util.Page;

import com.opensymphony.xwork2.ActionSupport;


public class CarAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
		private Car car;
		private String url="/admin/ajax.jsp";
		private List<Car> list;
		private String uid;
		AJAX	ajax;
		private List<String> filepaths;


		//
		private String res;
		private String resName;
		private String resType;
		
	public String getRes() {
			return res;
		}
		public void setRes(String res) {
			this.res = res;
		}
		public String getResName() {
			return resName;
		}
		public void setResName(String resName) {
			this.resName = resName;
		}
		public String getResType() {
			return resType;
		}
		public void setResType(String resType) {
			this.resType = resType;
		}


		public List<String> getFilepaths() {
			return filepaths;
		}
		public void setFilepaths(List<String> filepaths) {
			this.filepaths = filepaths;
		}


		// 分页排序 后台
			Page page;
			private int pageNum=1;
	      	private int numPerPage=10;
	      	private String orderField="";  
	    	private String property="";
	        private String key="";
	        private String orderDirection = "asc";
	    

		
		public Page getPage() {
				return page;
			}
			public void setPage(Page page) {
				this.page = page;
			}
			public int getPageNum() {
				return pageNum;
			}
			public void setPageNum(int pageNum) {
				this.pageNum = pageNum;
			}
			public int getNumPerPage() {
				return numPerPage;
			}
			public void setNumPerPage(int numPerPage) {
				this.numPerPage = numPerPage;
			}
			public String getOrderField() {
				return orderField;
			}
			public void setOrderField(String orderField) {
				this.orderField = orderField;
			}
			public String getProperty() {
				return property;
			}
			public void setProperty(String property) {
				this.property = property;
			}
			public String getKey() {
				return key;
			}
			public void setKey(String key) {
				this.key = key;
			}
			public String getOrderDirection() {
				return orderDirection;
			}
			public void setOrderDirection(String orderDirection) {
				this.orderDirection = orderDirection;
			}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		
		
		
		public Car getCar() {
			return car;
		}
		public void setCar(Car car) {
			this.car = car;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public List<Car> getList() {
			return list;
		}
		public void setList(List<Car> list) {
			this.list = list;
		}
		public AJAX getAjax() {
			return ajax;
		}
		public void setAjax(AJAX ajax) {
			this.ajax = ajax;
		}
		
		
		
		public String getUid() {
			return uid;
		}
		public void setUid(String uid) {
			this.uid = uid;
		}
		public String findAll()
		{
			page=new Page();
			if(orderField.equals("")||orderDirection.equals("")){
				orderField="carid";
				orderDirection="asc";
			}
			if(property.equals("")||key.equals("")){
				property="-1";
				key="-1";
			}
			page.setProperties("carinformation",pageNum,numPerPage,property,key);
			list = DaoFactory.getCarDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());

			url="/admin/carManager.jsp";
			return SUCCESS;
		}
		
		public String add()
		{
			ajax = new AJAX();
			
	    	String url1 = new String();
	    	for(String str : FileService.getFilelist()){
        		str=str.substring(str.indexOf("upload"));
        		url1 += str+";";		    		
	    		System.out.println(str);
	    	}
	    	
	    	int carid = DaoFactory.getCarDaoInstance().getNowSeq().getNextval();
	    	car.setCarid(carid);
	    	
			if(DaoFactory.getCarDaoInstance().Save(car)!=0){
				
		    	//向用户车辆对照表中添加信息
		    	UserVehicle UserVehicle = new UserVehicle();
		    	UserVehicle.setIdcard(car.getUserid());
		    	System.out.println("车辆号为"+carid);
		    	UserVehicle.setCarid(carid);
		    	
				if(DaoFactory.getUserVehicleDaoInstance().Save(UserVehicle)!=0){
					ajax.setStatusCode("200");
					ajax.setMessage("保存成功");
					
					ajax.setCallbackType("closeCurrent");
				}
				else{
					ajax.setStatusCode("300");
					ajax.setMessage("保存失败");
					
					ajax.setCallbackType("closeCurrent");
				}

			}
			else{
				ajax.setStatusCode("300");
				ajax.setMessage("保存失败");
				
				ajax.setCallbackType("closeCurrent");
			}
			
			
			if(ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON)!=null){
				ajax.setNavTabId("Fallcar");
			}
			else{
				ajax.setNavTabId("allcar");
			}
			
			return SUCCESS;
		}
		public String delete()
		{
			ajax = new AJAX();
			
			if(DaoFactory.getCarDaoInstance().delete(Integer.parseInt(uid))!=0){
				ajax.setStatusCode("200");
				ajax.setMessage("删除成功");				
				ajax.setCallbackType("");
			}
			else{
				ajax.setStatusCode("300");
				ajax.setMessage("删除失败");
				
				ajax.setCallbackType("");
			}
			
			if(ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON)!=null){
				ajax.setNavTabId("Fallcar");
			}
			else{
				ajax.setNavTabId("allcar");
			}
			return SUCCESS;
		}
		public String update()
		{
			ajax = new AJAX();
			
			if(DaoFactory.getCarDaoInstance().Update(car)!=0){
				ajax.setStatusCode("200");
				ajax.setMessage("修改成功");
				
				ajax.setCallbackType("closeCurrent");
			}
			else{
				ajax.setStatusCode("300");
				ajax.setMessage("修改失败");
				
				ajax.setCallbackType("closeCurrent");
			}
			
			if(ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON)!=null){
				ajax.setNavTabId("Fallcar");
			}
			else{
				ajax.setNavTabId("allcar");
			}
			
			return SUCCESS;
		}
		
		public String find()
		{
			car=DaoFactory.getCarDaoInstance().findById(Integer.parseInt(uid));
			url="/admin/carUpdate.jsp";
			return SUCCESS;
		}
		
		
		//前台查询
		public String Ffind()
		{
			page=new Page();

			
			PInformation p=(PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
			uid=p.getIdCard();
			
			if(orderField.equals("")||orderDirection.equals("")){
				orderField="carid";
				orderDirection="asc";
			}
			if(property.equals("")||key.equals("")){
				property="userid";
				key=uid;
			}
			page.setProperties("carinformation",pageNum,numPerPage,property,key);

			list = DaoFactory.getCarDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());
			url="/front/carManager.jsp";
			return SUCCESS;
		}
		
		public String Fsfind()
		{
			car=DaoFactory.getCarDaoInstance().findById(Integer.parseInt(uid));
			url="/front/carUpdate.jsp";
			return SUCCESS;
		}
	
		
		
		
		public String export() {

			String basepath = ServletActionContext.getServletContext().getRealPath("/");
			list = DaoFactory.getCarDaoInstance().findAll("carid","asc"); // 获取所有的记录生成list
			res = ExportExcel.CarEx(list, basepath);
			resName="CarMod.xls";
			resType="application/vnd.ms-excel";
			return "export";
		}
}
