package henu.action;

import henu.bean.OnlineChat;
import henu.bean.PInformation;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.CONSTANTS;
import henu.util.Page;

import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class OnlineChatAction extends ActionSupport {
	private List<OnlineChat> list;
	private OnlineChat o;
	//分页排序
	Page page;
	private int pageNum=1;
	private int numPerPage=10;
	private String orderField="askTime";  
    private String orderDirection="desc";  
    //检索
    private String property="-1";
    private String key="-1";
    AJAX ajax;
    private String url="/admin/ajax.jsp";
    private String uid;
    private String result;
    //根据身份证号找出每个人咨询的一条或者多条信息
    public String findListByIdCard()throws Exception{
    	PInformation p=(PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
		page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="askTime";
			orderDirection="desc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("onlinechat",pageNum,numPerPage,"idcard",p.getIdCard());
		list=DaoFactory.getOnlineChatDaoInstance().findListByIdCard(p.getIdCard(), property, key, orderField, orderDirection, page.getStart(), page.getEnd());
		url="/front/onlineChatManage.jsp";
		return SUCCESS;
	}
    //根据身份证号找出管理员给用户回复消息的条数
    public String findReplyMessageNumByIdCard()throws Exception{
    	PInformation p=(PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
    	result=Integer.toString(new Page().getTotalData1("onlinechat", "isDisplay", "1", "idCard", p.getIdCard()));
    	return SUCCESS;
    }
  //根据id找出一条信息用于回复
    public String AdminFindByOid()throws Exception{
    	o=DaoFactory.getOnlineChatDaoInstance().findByOid(uid);
    	//回复信息
    	url="/admin/onlineChatReply.jsp";
    	return SUCCESS;
    }
    //根据id找出一条信息用于管理员修改
    public String adminFindByOidUsedUpdate()throws Exception{
    	o=DaoFactory.getOnlineChatDaoInstance().findByOid(uid);
    	//修改回复信息
    	url="/admin/onlineChatAdminUpdate.jsp";
    	return SUCCESS;
    }
    //首页找出9条信息
    public String findOnlineChat()throws Exception{
		list=DaoFactory.getOnlineChatDaoInstance().findAllByProperties("isDisplay","1", "askTime", "desc", 0, 9);
		JSONArray json=JSONArray.fromObject(list);
		result=json.toString();
		return SUCCESS;
	}
    //公共页面显示所有的已回复信息
    public String findOnlineChatWithReply()throws Exception{
    	page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="askTime";
			orderDirection="desc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("onlinechat",pageNum,numPerPage,"isDisplay","1");
		list=DaoFactory.getOnlineChatDaoInstance().findAllByProperties("isDisplay","1",orderField, orderDirection, page.getStart(), page.getEnd());
		url="/public/onlineChat.jsp";
		return SUCCESS;
    }
    //提问一个问题
    public String addQuestion()throws Exception{
    	ajax=new AJAX();
    	if(o.getAskContent()==null||o.getAskContent().equals("")){
    		ajax.setStatusCode("300");
			ajax.setMessage("内容不能为空");
    	}else{
    		int result;
    		o.setAskTime(new Date().toLocaleString());
    		o.setIsTop(0);
    		o.setIsDisplay(0);
    		result=DaoFactory.getOnlineChatDaoInstance().addQuestion(o);
    		if(result!=0){
    			ajax.setStatusCode("200");
    			ajax.setMessage("咨询成功，等待回复");
    			ajax.setCallbackType("closeCurrent");
    			ajax.setNavTabId("onlineChatInfo");
    		}else{
    			ajax.setStatusCode("300");
    			ajax.setMessage("咨询失败，请重新咨询");
    		}
    	}
    	url="/front/ajax.jsp";
		return SUCCESS;
	}
    //管理员回复一条信息
    public String adminReply()throws Exception{
    	
    	ajax=new AJAX();
    	if(o.getReplyContent()==null||o.getReplyContent().equals("")){
    		ajax.setStatusCode("300");
    		ajax.setMessage("内容不能为空");
    	}else{
    		int result;
    		o.setReplyTime(new Date().toLocaleString());
    		o.setIsDisplay(1);
    		result=DaoFactory.getOnlineChatDaoInstance().adminReply(o);
    		if(result!=0){
    			ajax.setStatusCode("200");
    			ajax.setMessage("回复成功");
    			ajax.setCallbackType("closeCurrent");
    			ajax.setNavTabId("onlineChatWithoutReply");
    		}else{
    			ajax.setStatusCode("300");
    			ajax.setMessage("回复失败");
    		}
    	}
    	
    	return SUCCESS;
    }
    //管理员回复一条信息
    public String adminUpdate()throws Exception{
    	
    	ajax=new AJAX();
    	if(o.getReplyContent()==null||o.getReplyContent().equals("")){
    		ajax.setStatusCode("300");
    		ajax.setMessage("内容不能为空");
    	}else{
    		int result;
    		o.setReplyTime(new Date().toLocaleString());
    		result=DaoFactory.getOnlineChatDaoInstance().adminUpdate(o);
    		if(result!=0){
    			ajax.setStatusCode("200");
    			ajax.setMessage("回复成功");
    			ajax.setCallbackType("closeCurrent");
    			ajax.setNavTabId("onlineChatWithReply");
    		}else{
    			ajax.setStatusCode("300");
    			ajax.setMessage("回复失败");
    		}
    	}
    	
    	return SUCCESS;
    }
    //根据id删除一条记录，这个删除方法为管理员没有回复前用户自己所删除的
    public String delete()throws Exception{
		
		int result=DaoFactory.getOnlineChatDaoInstance().delete(uid);
		ajax=new AJAX();
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("删除成功");
			ajax.setNavTabId("onlineChatInfo");
			ajax.setCallbackType("");
			
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("删除失败");
			ajax.setCallbackType("");
		}
		url="/front/ajax.jsp";
		return SUCCESS;
	}
    //管理员根据id删除一条未回复的记录
    public String adminDeleteWithoutReply()throws Exception{
		
		int result=DaoFactory.getOnlineChatDaoInstance().delete(uid);
		ajax=new AJAX();
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("删除成功");
			ajax.setNavTabId("onlineChatWithoutReply");
			ajax.setCallbackType("");
			
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("删除失败");
			ajax.setCallbackType("");
		}
		url="/admin/ajax.jsp";
		return SUCCESS;
	}
    //管理员根据id删除一条已回复的记录
    public String adminDeleteWithReply()throws Exception{
		
		int result=DaoFactory.getOnlineChatDaoInstance().delete(uid);
		ajax=new AJAX();
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("删除成功");
			ajax.setNavTabId("onlineChatWithReply");
			ajax.setCallbackType("");
			
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("删除失败");
			ajax.setCallbackType("");
		}
		url="/admin/ajax.jsp";
		return SUCCESS;
	}
    //用户根据id找到一个问题,用来更新问题
    public String findByOid()throws Exception{
		
		o=DaoFactory.getOnlineChatDaoInstance().findByOid(uid);
		url="/front/onlineChatUpdate.jsp";
		return SUCCESS;
	}
    //此方法是管理员未回复前用户用户自己所更新的
    public String update()throws Exception{
    	ajax=new AJAX();
    	if(o.getAskContent()==null||o.getAskContent().equals("")){
    		ajax.setStatusCode("300");
			ajax.setMessage("内容不能为空");
    	}else{
    		int result;
    		o.setAskTime(new Date().toLocaleString());
    		result=DaoFactory.getOnlineChatDaoInstance().update(o.getOid(),o);
    		if(result!=0){
    			ajax.setStatusCode("200");
    			ajax.setMessage("更新成功，等待回复");
    			ajax.setCallbackType("closeCurrent");
    			ajax.setNavTabId("onlineChatInfo");
    		}else{
    			ajax.setStatusCode("300");
    			ajax.setMessage("更新失败，请重新更新");
    		}
    	}
    	url="/front/ajax.jsp";
    	return SUCCESS;
    }
    //管理员查找未回复的咨询
    public String findAllWithoutReply()throws Exception{
		page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="askTime";
			orderDirection="desc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("onlinechat",pageNum,numPerPage,"isDisplay","0");
		list=DaoFactory.getOnlineChatDaoInstance().findAllByProperties("isDisplay","0",orderField, orderDirection, page.getStart(), page.getEnd());
		url="/admin/onlineChatWithoutReplyManage.jsp";
		return SUCCESS;
	}
  //管理员查找已回复的咨询
    public String findAllWithReply()throws Exception{
		page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="askTime";
			orderDirection="desc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("onlinechat",pageNum,numPerPage,"isDisplay","1");
		list=DaoFactory.getOnlineChatDaoInstance().findAllByProperties("isDisplay","1",orderField, orderDirection, page.getStart(), page.getEnd());
		url="/admin/onlineChatWithReplyManage.jsp";
		return SUCCESS;
	}
	public List<OnlineChat> getList() {
		return list;
	}
	public void setList(List<OnlineChat> list) {
		this.list = list;
	}
	public OnlineChat getO() {
		return o;
	}
	public void setO(OnlineChat o) {
		this.o = o;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getNumPerPage() {
		return numPerPage;
	}
	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderDirection() {
		return orderDirection;
	}
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public AJAX getAjax() {
		return ajax;
	}
	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
    
}
