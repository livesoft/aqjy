package henu.dao;

import henu.bean.Company;

import java.util.List;

public interface CompanyDao {
	
    public int save(Company c);
    
    public int delete(String comid);
    
    public int upadte(Company c);
    
    public Company findBy(String comid);
    
    public List<Company> findById(String comid);
    
    public List<Company> findAll(String order,String sort);
    
    public List<Company> findByProperty(String property, String key, String order,
			String sort,int start,int end);

	public List<Company> findAllCompany();
	
	public List<Company> findBelongid();
	
	public List<Company> findCompany(String belongid);
}
