package henu.dao;

import java.util.List;

import henu.bean.Exam;
import henu.bean.Exercise;

/**
 * @author wuhaifeng
 * */

public interface ExerciseDao   {
	
	/**
	 * 保存Exam对象
	 * @param exam
	 * @return
	 */
	public int save(Exercise exercise) ;
	
	/**
	 * 根据exid进行删除
	 * @param eid
	 * @return
	 */
	public int delete(String exid);
	/**
	 * 查找指定eid的记录
	 * @param eid
	 * @return
	 */
	public Exam findById(String eid);
	
	/**
	 * 查找所有的记录
	 * @param order 排序字段
	 * @param sort 排序方式
	 * @return
	 */
	
	public List<Exam> findAll(String order, String sort,String property,String key);
	/**
	 * 根据指定字段，进行模糊查询
	 * @param property 字段名称
	 * @param key
	 * @param order 排序字段
	 * @param sort 排序方式
	 * @return
	 */
	public List<Exam> findByProperty(String property, String key, String order, String sort,int start ,int end);
	
	/**
	 * 根据指定题目编号的集合/数组获取某个考生的试题
	 * select * from tb_Exam where teid in (13,44,23) 
	 * @param eid
	 * @return
	 */
	public List<Exam> getExam(String[] eid);
	
	/**
	 * 根据编号进行修改
	 * @param eid
	 * @param exam
	 * @return
	 */
	public int update (String eid , Exam exam);
	
	/**
	 * 计算用户分数
	 * @param idcard 用户的身份证
	 * @param 考试的id
	 * @return 用户在该场考试中获取的分数
	 * */
	public int sumScore(String idcard,int eid);
	/**
	 * 统计用户的考试情况
	 * @param eid 考试id
	 * @return 返回各个分数阶段的人数
	 **/
	public int[] tongji(int eid);
	/**
	 * 获取所有学生的id
	 * @return 所有学生的id
	 * */
	public List<String> findAllUser();
	/**
	 * 删除用户某场考试的成绩
	 * @param eid 考试
	 * @param idCard 考生的身份证
	 * @return true 为删除成功
	 * */
	public boolean delScore(int eid,String iCard);
}
