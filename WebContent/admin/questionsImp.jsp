<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<h2 class="contentTitle">题库导入</h2>
<div class="pageContent" style="margin: 0 10px" layoutH="50">
	<form method="post" action="/aqjy/ques/quesimport" enctype="multipart/form-data" class="pageForm required-validate"   onsubmit="return iframeCallback(this,navTabAjaxDone)">
		<fieldset>
			<legend style="color: red; font-size: 25px; font-style: bold">题库导入说明</legend>
			<dl>
				<dd style="font-size: 15px; margin-top: 10px;">请严格按照管理员提供的模版文件编写试题文件然后进行导入操作
				<dd>
				<dd style="font-size: 15px;">请使用xls格式excel文件，如果是ms
					office 2007 以上版本，在保存文件的时候请选择97-03 xls格式
				</dd>
				<dd>
					<br>
					<img src="/aqjy/images/download.png"></img><a style="color:red" href="/aqjy/upload/model/questionsmodel.xls">下载模板</a>
				</dd>
			</dl>
		</fieldset>
		<div class="divider"></div>
		<fieldset>
			<legend>文件上传</legend>
			<dl>
				<dt>
					<span style="font-size: 20px; color: red">选择XLS</span>
				</dt>
				<dd>
					<input class="required" type="file" name="files">
				</dd>
			</dl>
		</fieldset>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive">
						<div class="buttonContent">
							<button type="submit">提交</button>
						</div>
					</div></li>
				<li><div class="button">
						<div class="buttonContent">
							<button type="button" class="close">取消</button>
						</div>
					</div></li>
			</ul>
		</div>
	</form>
</div>