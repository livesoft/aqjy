package henu.dao;
import java.util.List;
import henu.bean.CarMaintancePinfoCompany;

import henu.bean.Maintance;
/**
 * 二级维护的DAO层
 * @author 梁胜彬
 * 2015-01-24
 * 新增加
 */
public interface MaintanceDao {

	public int save(Maintance maintance);
	
	public int delete(String mid);
	
	public int update(String mid, Maintance maintance);
	
	public CarMaintancePinfoCompany findById(String mid);
	
	public List<CarMaintancePinfoCompany> findAll(String order, String sort,String property,String key);
	/**
	 * 模糊查找
	 * 
	 * @param property 字段名
	 * @param key 字段值
	 * @param order 排序字段
	 * @param sort 排序方式
	 * @param start
	 * @param end
	 * @return
	 */
	public List<CarMaintancePinfoCompany> findByProperty(String property,String key,String order,
			String sort,int start,int end);
}
