package henu.dao;

import henu.bean.Certification;
import henu.bean.Certification_PInfomation;

import java.util.List;

public interface CertificationDao {

	public Certification_PInfomation findByCnid(String args);
	public int deleteByCnid(String args);
	public List<Certification_PInfomation> findByProperty(String property, String key,String comid, int start, int end);
	public Certification findByUserIdcard(String idCard);
	public int update(Certification certification);
	//前台首次登录需用户添加资格证
	public int add(Certification c);
}
