package henu.action;


import java.util.List;

import org.apache.struts2.ServletActionContext;

import henu.bean.UserVehicle;
import henu.dao.factory.DaoFactory;
import henu.service.ExportExcel;
import henu.util.AJAX;
import henu.util.Page;

import com.opensymphony.xwork2.ActionSupport;

public class UserVehicleAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UserVehicle uv;
	private String url="/admin/ajax.jsp";
	private List<UserVehicle> list;
	private String uid;
	AJAX	ajax;
	
	
	// 分页排序
	Page page;
	private int pageNum = 1;
	private int numPerPage = 10;
	private String orderField = "uvid";
	private String orderDirection = "asc";
	// 检索
	private String property = "-1";
	private String key = "-1";
	//
	private String res;
	private String resName;
	private String resType;
	
	
	
	public String getRes() {
		return res;
	}
	public void setRes(String res) {
		this.res = res;
	}
	public String getResName() {
		return resName;
	}
	public void setResName(String resName) {
		this.resName = resName;
	}
	public String getResType() {
		return resType;
	}
	public void setResType(String resType) {
		this.resType = resType;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getNumPerPage() {
		return numPerPage;
	}
	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderDirection() {
		return orderDirection;
	}
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	public UserVehicle getUv() {
		return uv;
	}
	public void setUv(UserVehicle uv) {
		this.uv = uv;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public List<UserVehicle> getList() {
		return list;
	}
	public void setList(List<UserVehicle> list) {
		this.list = list;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public AJAX getAjax() {
		return ajax;
	}
	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String add()
	{
		ajax = new AJAX();
		if(DaoFactory.getUserVehicleDaoInstance().Save(uv)!=0)
		{
			ajax.setStatusCode("200");
			ajax.setMessage("添加成功");
			ajax.setNavTabId("alluserv");
			ajax.setCallbackType("closeCurrent");
		}
		else
		{
			ajax.setStatusCode("300");
			ajax.setMessage("添加失败");
			ajax.setNavTabId("alluserv");
			ajax.setCallbackType("closeCurrent");
		}
		return SUCCESS;
	}
	
	public String delete()
	{
		
		ajax = new AJAX();
		
		if(DaoFactory.getUserVehicleDaoInstance().delete(Integer.parseInt(uid))!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("删除成功");
			ajax.setNavTabId("alluserv");
			ajax.setCallbackType("");
		}
		else{
			ajax.setStatusCode("300");
			ajax.setMessage("删除失败");
			ajax.setNavTabId("alluserv");
			ajax.setCallbackType("");
		}
		return SUCCESS;

	}
	
	public String update()
	{
		ajax = new AJAX();

		if(DaoFactory.getUserVehicleDaoInstance().Update(uv)!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("修改成功");
			ajax.setNavTabId("alluserv");
			ajax.setCallbackType("closeCurrent");
		}
		else{
			ajax.setStatusCode("300");
			ajax.setMessage("修改失败");
			ajax.setNavTabId("alluserv");
			ajax.setCallbackType("closeCurrent");
		}
		return SUCCESS;
	}
	
	public String find()
	{
		
		uv=DaoFactory.getUserVehicleDaoInstance().findById(Integer.parseInt(uid));
		url="/admin/userVehicleUpdate.jsp";
		return SUCCESS;

	}
	
	public String findAll()
	{
		page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="uvid";
			orderDirection="asc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("uservehicle",pageNum,numPerPage,property,key);
		list = DaoFactory.getUserVehicleDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());
		url="/admin/userVehicleManager.jsp";
		
		return SUCCESS;
	}
	public String export() {
		/*
		 * 这个aciton的目的就是生产一个地址就够了
		 */
		/*String basepath = "D:/workplace/yxxt/WebContent";*/
		String basepath = ServletActionContext.getServletContext().getRealPath("/");
		list = DaoFactory.getUserVehicleDaoInstance().findAll("uvid","asc"); // 获取所有的记录生成list
		res = ExportExcel.UserVEx(list, basepath);
		resName="UserVehicle.xls";
		resType="application/vnd.ms-excel";
		return "export";
	}
	
	
	
}
