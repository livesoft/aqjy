package henu.action;
/**
 * 主要用于对安全会议内容表部分的处理
 * @author 田星杰
 * 2015-01-25
 */
import henu.bean.Mcontent;
import henu.dao.McontentDao;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.Page;

import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class MettingContentAction extends ActionSupport{
	//分页查询
	Page page;
	private int pageNum=1;
	private int numPerPage=10;
	private String orderField="mcid";
	private String orderDirection="asc";
	
	//检索
	private String property="-1";
	private String key="-1";
	
	
	private AJAX ajax;
	//声明一个安全会议内容类的实例变量用于存放查询到的记录
	private Mcontent mc;
	private String url="/admin/ajax.jsp";
	//存放安全会议内容表的主码ID
	private String uid;
	//声明一个List<Mcontent>类型的变量用于存放查询到的记录的存放
	private List<Mcontent> list;
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getNumPerPage() {
		return numPerPage;
	}
	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderDirection() {
		return orderDirection;
	}
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public AJAX getAjax() {
		return ajax;
	}
	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public Mcontent getMc() {
		return mc;
	}
	public void setMc(Mcontent mc) {
		this.mc = mc;
	}
	public List<Mcontent> getList() {
		return list;
	}
	public void setList(List<Mcontent> list) {
		this.list = list;
	}
	//列出会议内容信息
	public String findAll(){
		page = new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="mcid";
			orderDirection="asc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("MCONTENT",pageNum,numPerPage,property,key);
		
		list = DaoFactory.getMcontentDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());
		
		url="/admin/mcontentManage.jsp";
		
		
		return SUCCESS;
	}
	//设置会议内容
	public String add(){
		//自动获取当前系统时间作为会议内容的发布时间
		mc.setPublishTime(new Date().toLocaleString());
		int result = DaoFactory.getMcontentDaoInstance().save(mc);
		ajax=new AJAX();
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("设置成功");
			ajax.setCallbackType("closeCurrent");
			ajax.setNavTabId("seminarInf");
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("设置失败，请重新填写");
			ajax.setCallbackType("closeCurrent");
			ajax.setNavTabId("seminarInf");
		}
		url="/admin/ajax.jsp";
		return SUCCESS;
	}
	//删除会议基本信息
	public String del(){
		int seid = Integer.parseInt(uid);
		//int seid=bf.getBasicid();
		int result = DaoFactory.getMcontentDaoInstance().delete(seid);
		ajax=new AJAX();
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("删除成功");
			ajax.setNavTabId("seminarInf");
			ajax.setCallbackType("");
			
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("删除失败");
			ajax.setCallbackType("");
		}
		url="/admin/ajax.jsp";
		return SUCCESS; 
	}
	//更新会议内容
	public String update(){
		McontentDao dao = DaoFactory.getMcontentDaoInstance();
		ajax=new AJAX();
		//自动获取当前系统时间作为更新后会议内容的发布时间
		mc.setPublishTime(new Date().toLocaleString());
		int result = dao.update(mc.getMcid(),mc);
		
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("更新成功");
			ajax.setNavTabId("seminarInf");
			ajax.setCallbackType("closeCurrent");
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("更新失败");
			ajax.setCallbackType("");
		}
		return SUCCESS;
	}
	//用于得到用户选中的记录的信息，并存放在session范围内的mcontent中，通过URL转到修改页面
	public String find(){
		McontentDao dao = DaoFactory.getMcontentDaoInstance();
		int seid = Integer.parseInt(uid);
		Mcontent s =dao.findAllBymcId(seid);
		ServletActionContext.getRequest().getSession().setAttribute("mcontent", s);
		url="/admin/mcontentUpdate.jsp";
		return SUCCESS;
	}
	
}
