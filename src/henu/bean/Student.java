package henu.bean;
/**
 * 学生与考试的关联,记录考生的考试成绩
 * @author wuhaifeng
 * */
public class Student implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer stuid ;			//这个表的流水id
	private Integer eid;
	private Integer idcard;			//考生的id
	private Integer score ;			//考生的分数
	private String credit;			//本场考试中所获取的学分
	

	public Integer getStuid() {
		return stuid;
	}
	public void setStuid(Integer stuid) {
		this.stuid = stuid;
	}
	public Integer getEid() {
		return eid;
	}
	public void setEid(Integer eid) {
		this.eid = eid;
	}

	public Integer getIdcard() {
		return idcard;
	}
	public void setIdcard(Integer idcard) {
		this.idcard = idcard;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}

	
	
}
