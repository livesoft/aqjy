<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta charset="UTF-8">
<style type="text/css">
.examEtitle{margin:auto;margin-top: 150px;margin-bottom: 10px;width: 330px;}
.examEtitle span{font-weight: bold;font-size: 25px;margin: 0 5px 0 5px;font-family: Microsoft YaHei}
.jdtbg{width: 330px;height: 30px;background-image: url("exam/jdtbg.png");margin: auto;margin-top:30px;}
.jdtlr{width: 15px;height: 30px;float: left;}
.examEfoot{width: 500px;margin: auto;margin-top: 50px;}
.examEfoot p{color: #5c5c5c;font-size: 14px;font-family: Microsoft YaHei;text-indent: 2em;text-align: left;}
</style>
<script type="text/javascript">
	var mytimer = null;
	var bar = 0;
	var sorce = ${TESTSCORE};/* 传入成绩，百分制 */
	function startbar() {
		mytimer = window.setTimeout("processbar()", 5);
	}
	function processbar() {
		if (bar < sorce * 3) {
			bar=bar+2;
			document.getElementById("processbar").style.width = bar + "px";
			mytimer = window.setTimeout("processbar()", 5);
		}
	}
</script>
<div style="text-align: center;">
<div class="examEtitle"><span style="font-size:14px;font-style:bold">${PERSON.realName}</span>您的成绩为
<c:if test="${TESTSCORE >= 60}"><span style="color:green; font-weight: bold;" >${TESTSCORE}分</span>,恭喜您过关了！</c:if>
<c:if test="${TESTSCORE < 60}"><span style="color:red;font-weight: bold;" >${TESTSCORE}分</span>，再接再厉！</c:if>
</div>
	<div class="jdtbg" onload="startbar()">
		<div class="jdtlr"><img alt="" src="exam/jdtl.png" onload="startbar()"></div>
		<div id="processbar" style=" width: 0px; height: 30px;float: left;background-image: url('exam/jdtpx.png');background-repeat: repeat-x;"></div>
		<div class="jdtlr"><img alt="" src="exam/jdtr.png"></div>
</div>
	</div>