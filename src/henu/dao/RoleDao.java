package henu.dao;

import henu.bean.Exam;
import henu.bean.Role;

import java.util.List;

/**
 * 对角色的管理
 * @author wuhaifeng
 * 2014-12-08
 */
public interface RoleDao {
	/**
	 * 增加一个角色
	 * @param role 
	 * @return 1表示添加成功 
	 * */
	public int save(Role role);
	/**
	 * 根据id删除一个角色
	 * @param id 要删除的角色的id
	 * @return 1 表示删除成功
	 * */
	public int delete(Integer roleId);
	/**
	 * 更新一个角色
	 * @param id 要更新的角色的id 
	 * @param role 新的角色
	 * @return 1 表示更新成功
	 * */
	public int update(int id,Role role);
	/**
	 * 根据指定字段，进行查询
	 * @param property 字段名称
	 * @param key
	 * @param order 排序字段
	 * @param sort 排序方式
	 * @return
	 */
	public List<Role> findByProperty(String property, String key, String order, String sort,int start ,int end);
	/**
	 * 根据id 查询到指定role
	 * @param roleid 角色id
	 * @return 查询结果
	 * */
	public Role findById(int roleid);
	
}
