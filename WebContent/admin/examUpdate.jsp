<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.js"> </script>
<script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
<link rel="stylesheet" href="../ueditor/themes/default/css/ueditor.css">
<script type="text/javascript">
//引入百度编辑器
UE.getEditor('editor', {
		initialFrameWidth:"100%" //初始化选项
		,toolbars: [[
		            'fullscreen', 'source', '|', 'undo', 'redo', '|',
		            'bold', 'italic', 'underline', 'fontborder', 'lineheight', '|',
		            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
		            'directionalityltr', 'directionalityrtl', 'indent', '|',
		            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
		            'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
		            'simpleupload', 'insertimage', 'emotion', 'scrawl', 'pagebreak',  '|',
		            'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
		            'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
		            'print', 'preview', 'searchreplace', 'help', 'drafts'
		        ]]
		});

$('input:radio').change(function(){
	$("#sub").toggle(300);
	//此处判断一下是不是综合考试，是的话设置一下状态
	var cradio = $('input:radio:checked').val();
	if(cradio == 0){
  	//综合考试就把所有的试题获取到 
		$.get('/aqjy/json/infExam?args=-1',function(data){
			data = $.parseJSON(data);
			$("#csnum").val(data.xuan);
			$("#cpnum").val(data.pan);
			//每次设置完毕之后更新校验
			$("#xuanlen").attr("max",data.xuan);
			$("#panlen").attr("max",data.pan);
		},'json');
	}
	
});
	//动态显示当前题库的信息
	$("#subset").change(function(){
		
		$.get('/aqjy/json/infExam?args='+$("#subset").val(),function(data){
			data = $.parseJSON(data);
			$("#csnum").val(data.xuan);
			$("#cpnum").val(data.pan);
			$("#xuanlen").attr("max",data.xuan);
			$("#panlen").attr("max",data.pan);
		},'json');
		
	});
	//要判断分数为100分才能进行表单提交,表单提交之后跳转到管理页面这样就解决了重复提交的问题
	function check(){
		var xuanlen =document.getElementById("xuanlen").value;
		var panlen = document.getElementById("panlen").value;
		var xuanfen =document.getElementById("xuanfen").value;
		var panfen = document.getElementById("panfen").value;
		
		panlennum = parseInt(panlen);
		panfennum = parseInt(panfen);
		
		xuanlennum = parseInt(xuanlen);
		xuanfennum = parseInt(xuanfen);
		//判断是不是数字不是数据不进行加发
		if(!(isNaN(panlennum)||isNaN(panfennum)||isNaN(xuanlennum)||isNaN(xuanfennum))){
			document.getElementById("zongfen").value=(xuanlennum*xuanfennum)+(panlennum*panfennum);
		}
	};
	
	$("#xuanlen").blur(function(){
		check();
	});
	
	$("#panlen").blur(function(){
		check();
	});
	
	$("#xuanfen").blur(function(){
		check();
	});
	
	$("#panfen").blur(function(){
		check();
	});
</script>

<h2 class="contentTitle">考试信息设置</h2>
<div class="pageFormContent" layoutH="60">
	<form method="post" action="/aqjy/exam/editExam" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<fieldset>
			<input type="hidden" name="args" value="${exam.eid}" />
			<legend>当前题库信息</legend>
			<dl class="nowrap">
				<dt>选择题数量:</dt>
				<dd>
					<input value="0" id="csnum" readonly="readonly" type="text" />
				</dd>
			</dl>
			<dl class="nowrap">
				<dt>判断题数量:</dt>
				<dd>
					<input value="0"  id="cpnum" readonly="readonly" type="text" />
				</dd>
			</dl>
			
			<dl class="nowrap">
				<dt>当前设置试卷的总分：</dt>
				<dd>
					<input id="zongfen" class="required number" value="0" min="100"
						max="100" readonly="readonly" type="text" />
				</dd>
			</dl>
			<legend>考试信息设置</legend>
			<dl class="nowrap">
				<dt>考试名称:</dt>
				<dd>
					<textarea  cols="60" rows="5" class="required" name="exam.examname">${exam.examname }</textarea>
				</dd>
			</dl>
			<dl class="nowrap">
				<dt>考试时长(分钟):</dt>
				<dd>
					<input type="text" value ="${exam.duration}" name="exam.duration" class="required number" max="400" min="0" >
				</dd>
			</dl>
			<!-- 设置考试类型 -->
			<dl class="nowrap">
				<dt>考试类型：</dt>
				<dd>
					<input type="radio" name="exam.examtype" checked="checked" value="1" /><span>专题考试</span><input
						type="radio" name="exam.examtype" value="0" /><span>综合考试</span>
				</dd>
			</dl>
			<!-- 设置专题是否显示 -->
			<dl id="sub" class="nowrap">
				<dt>专题设置:</dt>
				<dd>
					<s:select name="exam.subject" id="subset" list="#session.examsubmap" listKey="key" listValue="value" ></s:select>
				</dd>
			</dl>
			<dl class="nowrap">
				<dt>选择题数量：</dt>
				<dd>
					<input value ="${exam.selectcount}" class="required number" id="xuanlen" max="0"
						min="0" name="exam.selectcount" type="text" />
				</dd>
			</dl>
			<dl class="nowrap">
				<dt>选择题分值：</dt>
				<dd>
					<input value ="6" value="${exam.selectscore}" name="exam.selectscore" id="xuanfen" class="required number" min="1" max="100"
						type="text" />
				</dd>
			</dl>
			<dl class="nowrap">
				<dt>判断题数量:</dt>
				<dd>
					<input value ="${exam.judgecount}" class="required number" id="panlen" min="0" max="0"
						name="exam.judgecount" type="text" />
				</dd>
			</dl>
			
			<dl class="nowrap">
				<dt>判断题分值:</dt>
				<dd>
					<input value ="${exam.judgescore}" name="exam.judgescore" id="panfen" class="required number" min="1" max="100" type="text" />
				</dd>
			</dl>
			<dl class="nowrap">
				<dt>学分:</dt>
				<dd>
					<input value ="${exam.credit}" name="exam.credit" class="required number" min="1" max="100" type="text" />
				</dd>
			</dl>
			<dl class="nowrap">
				<dt>开始时间:</dt>
				<dd>
					<input value ="2010-10-11" value="${exam.examtime}" name="exam.examtime" class="required date"  type="date" />
				</dd>
			</dl>
			<dl class="nowrap">
				<dt>考试说明:</dt>
				<dd>
					<script type="text/plain" id="editor" name="exam.content"  style="width:800px;height: 300px">${exam.content}</script>
				</dd>
			</dl>
				<dl class="nowrap">
				<dt>备注:</dt>
				<dd>
					<textarea  cols="60" rows="5"  name="exam.status">${exam.status}</textarea>
				</dd>
			</dl>
		</fieldset>
		<div class="formBar">
			<ul>
				<li><div class="buttonActive">
						<div class="buttonContent">
							<button type="submit">提交</button>
						</div>
					</div></li>
				<li><div class="button">
						<div class="buttonContent">
							<button type="button" class="close">取消</button>
						</div>
					</div></li>
			</ul>
		</div>
	</form>
</div>
