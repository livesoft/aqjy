package henu.dao;


import henu.bean.OnlineChat;

import java.util.List;

public interface OnlineChatDao {

	public List<OnlineChat> findListByIdCard(String idCard, String property, String key,
			String order, String sort, int start, int end);

	public int addQuestion(OnlineChat o);
	public int delete(String uid);
	public int update(int oid, OnlineChat o);
	public OnlineChat findByOid(String uid);

	public List<OnlineChat> findAllByProperties(String property, String key,
			String order, String sort, int start, int end);

	public int adminReply(OnlineChat o);

	public int adminUpdate(OnlineChat o);

}
