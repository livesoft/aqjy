package henu.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import henu.bean.Exam;
import henu.bean.Role;
import henu.dao.RoleDao;
import henu.dao.factory.DaoFactory;

public class RoleDaoImpl implements RoleDao {

	@Override
	public int save(Role role) {
		QueryRunner runner = DaoFactory.getRunner();
		int result = 0;
		// INSERT INTO test VALUES(2,1,1,2);
		String sql = "INSERT into role VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Object[] params = { null, role.getRolename(), role.getStatus(),
				role.getDesc(), role.getRemark() ,role.getXinxi(),role.getYonghu(),role.getHuiyi(),role.getTongzhi()
				,role.getJiaoyu(),role.getKaohe(),role.getHudong(),role.getXitong()};
		try {
			result = runner.update(sql, params);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return result;
	}
	@Override
	public int delete(Integer roleid) {
		QueryRunner runner = DaoFactory.getRunner();
		int result = 0;
		String sql = "delete from role where roleid = ?";
		try {
			result = runner.update(sql, roleid);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return result;
	}

	@Override
	public int update(int id, Role role) {
		QueryRunner runner = DaoFactory.getRunner();
		int result = 0 ;
		String sql ="" ;
		Object[] params ={1,2};
		try {
			result = runner.update(sql,params );
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Role> findByProperty(String property, String key, String order,
			String sort, int start, int end) {
		List<Role> list = null;
		String sql = "SELECT * FROM (SELECT tt.*, ROWNUM ro FROM (select * from role where "
				+ property
				+ "='"
				+ key
				+ "'  order by "
				+ order
				+ " "
				+ sort
				+ ") tt WHERE ROWNUM <=" + end + ") WHERE ro > " + start + "";
		QueryRunner runner = DaoFactory.getRunner();
		try {
			list = runner.query(sql, new BeanListHandler<Role>(Role.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	@Override
	public Role findById(int roleid) {
		String sql ="SELECT * from role where roleid = ?" ;
		Role role  = null ; 
		QueryRunner runner = DaoFactory.getRunner();
		try {
			role = runner.query(sql, new BeanHandler<Role>(Role.class),roleid);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return role;
	}
}
