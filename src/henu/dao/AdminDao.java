package henu.dao;

import henu.bean.PInformation;

import java.util.List;

/**
 * 处理用户权限
 * 03-22
 * @author wuhaifeng
 * */
public interface AdminDao {
	/**
	 * 根据指定字段，进行查询
	 * @param property 字段名称
	 * @param key
	 * @param order 排序字段
	 * @param sort 排序方式
	 * @return 查询 结果
	 */
	public List<PInformation> findByProperty(String property, String key, String order, String sort,int start ,int end);
	/**
	 * 删除一个用户
	 * @param adminid  要删除的用户的id
	 * @return 删除结果
	 * */
	public int delete(int adminid);

}
