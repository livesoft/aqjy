
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

	<div class="pageContent">
	<form method="post" action="/aqjy/interaction/onlineChatAction_addQuestion" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>姓名：</dt>
				<dd>
					<input type="text" name="o.askName"  value="${PERSON.realName }" readonly="readonly" class="required"/>
				</dd>
			</dl>
			<dl>
				<dt>身份证号：</dt>
				<dd>
					<input type="text" name="o.idCard" value="${PERSON.idCard}" readonly="readonly" class="required"/>
				</dd>
			</dl>
			
			<dl>
				<dt>咨询主题：</dt>
				<dd>
					<input type="text" name="o.title"  class="required" style="width:300px;" maxlength="25"/>
				</dd>
			</dl>
			<dl>
				<dt>咨询版块：</dt>
				<dd>
					<select name="o.range" id="range">
						
					</select>
				</dd>
			</dl>
			<dl>
				<dt>咨询内容：</dt>
				<dd><textarea name="o.askContent" cols="50" rows="4" maxlength="400"></textarea></dd>
			</dl>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="submit">咨询</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
		</div>
	</form>
	
</div>
<script>
$(function(){
	$.getJSON("/aqjy/json/getDictionary",{table:"ONLINECHAT",name:"RANGE"},function(data){
		var json=JSON.parse(data);
		for (var i = 0; i < json.length; i++) {
			$("#range").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
		}
		
	})
})
</script>