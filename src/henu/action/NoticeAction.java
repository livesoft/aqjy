package henu.action;

import henu.bean.Notice;
import henu.dao.NoticeDao;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.Page;

import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;

import com.opensymphony.xwork2.ActionSupport;

public class NoticeAction extends ActionSupport {
	private List<Notice> list;
	
	//分页排序
	Page page;
	private int pageNum=1;
	private int numPerPage=10;
	private String orderField="publishTime";  
    private String orderDirection="desc";  
    //检索
    private String property="-1";
    private String key="-1";
    
	private Notice n;
	private String description;
	private String uid;
	private String url="/admin/ajax.jsp";
	AJAX ajax;
	private String result;//json传值
	/*查询所有的新闻*/
	public String findAll()throws Exception{
		page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="publishTime";
			orderDirection="desc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("notice",pageNum,numPerPage,property,key);
		list=DaoFactory.getNoticeDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());
		url="/admin/noticeManage.jsp";
		return SUCCESS;
	}
	//添加新闻
	public String add()throws Exception{
		ajax=new AJAX();
		if(description==null||description.trim().equals("")){
			ajax.setStatusCode("300");
			ajax.setMessage("内容不能为空，请重新填写");
		}else{
			n.setPublishTime(new Date().toLocaleString());
			n.setViewNumbers(0);;
			n.setContent(description);
			int result=DaoFactory.getNoticeDaoInstance().save(n);
			if(result!=0){
				ajax.setStatusCode("200");
				ajax.setMessage("发布成功");
				ajax.setNavTabId("noticeInfo");
				ajax.setCallbackType("closeCurrent");
			}else{
				ajax.setStatusCode("300");
				ajax.setMessage("发布失败请重新发布");
			}
		}
		
		return SUCCESS;
	}
	//删除新闻
	public String delete()throws Exception{
		
		NoticeDao dao=DaoFactory.getNoticeDaoInstance();
		int result=dao.delete(uid);
		ajax=new AJAX();
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("删除成功");
			ajax.setNavTabId("noticeInfo");
			ajax.setCallbackType("");
			
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("删除失败");
			ajax.setCallbackType("");
		}
		return SUCCESS;
	}
	//根据一个id查询一条信息，用户更新
	public String findByNid()throws Exception{

		n=DaoFactory.getNoticeDaoInstance().findByNid(uid);
		url="/admin/noticeUpdate.jsp";
		return SUCCESS;
	}
	//更新一条新闻
	public String update()throws Exception{
		ajax=new AJAX();
		if(description==null||description.trim().equals("")){
			ajax.setStatusCode("300");
			ajax.setMessage("更新失败,内容不能为空");
			ajax.setCallbackType("");
		}else{
			NoticeDao dao=DaoFactory.getNoticeDaoInstance();
			n.setPublishTime(new Date().toLocaleString());
			n.setContent(description);
			int result=dao.update(uid, n);
			if(result!=0){
				ajax.setStatusCode("200");
				ajax.setMessage("更新成功");
				ajax.setNavTabId("noticeInfo");
				ajax.setCallbackType("closeCurrent");
			}else{
				ajax.setStatusCode("300");
				ajax.setMessage("更新失败");
				ajax.setCallbackType("");
			}
		}
		
		return SUCCESS;
	}
	//查询一个list集合，在前台欢迎页滚动播出
	public String findNews()throws Exception{

		list=DaoFactory.getNoticeDaoInstance().findByProperty("-1","-1","publishTime", "desc", 0, 9);
		result=JSONArray.fromObject(list).toString();
		return SUCCESS;
	}
	//根据id查询一条公告详情,然后转发到公共页面上
	public String findByNidOut()throws Exception{

		n=DaoFactory.getNoticeDaoInstance().findByNid(uid);
		//每次访问浏览次数加一
		DaoFactory.getNoticeDaoInstance().noticeViewNumbersAddOne(uid);
		url="/public/showNoticeDetail.jsp";
		return SUCCESS;
	}
	//根据属性type查询所有的公告
	public String findNoticeListByProperty()throws Exception{
		page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="publishTime";
			orderDirection="desc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("notice",pageNum,numPerPage,"type",key);
		list=DaoFactory.getNoticeDaoInstance().findByProperty("type", key,orderField, orderDirection, page.getStart(), page.getEnd());
		url="/public/noticeList.jsp";
		return SUCCESS;
	}
	//根据属性type查询所有的公告,front里边显示
	public String findNoticeListByPropertyIn()throws Exception{
		page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="publishTime";
			orderDirection="desc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("notice",pageNum,numPerPage,"type",key);
		list=DaoFactory.getNoticeDaoInstance().findByProperty("type", key,orderField, orderDirection, page.getStart(), page.getEnd());
		url="/front/noticeList.jsp";
		return SUCCESS;
	}
	/*在front里边分页如果超链接写成/aqjy/interaction/noticeAction_findNoticeListByPropertyIn?key=${key }
	那么key值将无法带过来，所以写为/aqjy/interaction/noticeAction_findNoticeListByPropertyIn?property=${key }
	和上个方法就key和property不一样，如果不带参数，那么index.jsp超链接和分页页面的超链接保持一致就可以了，即只用一个方法
	*/
	public String findNoticeListByPropertyInPage()throws Exception{
		page=new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="publishTime";
			orderDirection="desc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("notice",pageNum,numPerPage,"type",property);
		list=DaoFactory.getNoticeDaoInstance().findByProperty("type", property,orderField, orderDirection, page.getStart(), page.getEnd());
		url="/front/noticeList.jsp";
		return SUCCESS;
	}
	public List<Notice> getList() {
		return list;
	}
	public void setList(List<Notice> list) {
		this.list = list;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getNumPerPage() {
		return numPerPage;
	}
	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderDirection() {
		return orderDirection;
	}
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Notice getN() {
		return n;
	}
	public void setN(Notice n) {
		this.n = n;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public AJAX getAjax() {
		return ajax;
	}
	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}
	
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
}
