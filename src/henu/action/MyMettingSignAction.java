package henu.action;
import henu.bean.Mcontent;
import henu.bean.Meetingsign;
import henu.bean.PInformation;
import henu.dao.McontentDao;
import henu.dao.factory.DaoFactory;
import henu.util.CONSTANTS;

import java.util.Date;




import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class MyMettingSignAction extends ActionSupport{
	
	
	
	//声明一个会议基本信息类的实例变量用于存放查询到的记录
	//private Meetingsign meetingsign=null;
	private String url="/admin/ajax.jsp";
	//存放会议基本信息表的主码ID
	private String uid;
	//声明一个List<Meetingsign>类型的变量用于存放查询到的记录的存放
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	//用户向会议签到表中添加记录
	public String add(){
		//得到会议内容表中的主键ID和basicid
		McontentDao dao = DaoFactory.getMcontentDaoInstance();
		int seid = Integer.parseInt(uid);
		Mcontent s =dao.findAllBymcId(seid);
		ServletActionContext.getRequest().getSession().setAttribute("mcon", s);
		//向会议签到表中添加一条记录
		int bs=s.getBasicid();
		int mc=s.getMcid();
		//System.out.println(bs);
		//System.out.println(mc);
		//获取用户的信息，主要为了得到公司ID和姓名
		PInformation p1=(PInformation) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_PERSON);
		
		Meetingsign meetingsign=new Meetingsign();
		meetingsign.setBasicid(bs);
		meetingsign.setMcid(mc);
		meetingsign.setSignTime(new Date().toLocaleString());
		meetingsign.setComid(p1.getComid());
		meetingsign.setSignPerson(p1.getRealName());
		int result = DaoFactory.getMeetingsignDaoInstance().save(meetingsign);
		/*ajax=new AJAX();
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("添加成功");
			ajax.setCallbackType("closeCurrent");
			//此处还有修改
			ajax.setNavTabId("meetingsignInf");
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("添加失败，请重新填写");
			ajax.setCallbackType("closeCurrent");
			//此处还有修改
			ajax.setNavTabId("meetingsignInf");
		}
		url="/admin/ajax.jsp";*/
		if(s.getVideoUrl()==null){
		url="/front/joinMetting.jsp";
		}else{
			url="/front/videoMetting.jsp";
		}
		return SUCCESS;
	}
}
