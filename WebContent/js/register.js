$(function(){
	$.getJSON("/aqjy/json/findAllCompany",function(data){
		var json=JSON.parse(data);
		for(var i=0;i<json.length;i++){
			$("#comid").append("<option value='"+json[i].comid+"'>"+json[i].companyname+"</option>");
		}
	})

	var idCard=/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/; 
	var phone=/1[3|5|7|8|][0-9]{9}/;
	var email=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   $("#realName").change (function(){
		if($.trim($(this).val())==""){
			$("#error1").text("请你输入用户名");
		}else{
			$("#error1").text("*");
		}
	})
	$("#idCard").change (function(){
		
		if(!idCard.test($(this).val())){
			$("#error2").text("请输入有效的身份证");
		}else{
			$("#error2").text("*");
		}
	})
	$("#password").change (function(){

		if($.trim($(this).val()).length<=5||$.trim($(this).val()).length>=18){
			$("#error3").text("密码长度为6~18位");
		}else{
			$("#error3").text("*");
		}
	})
	$("#confirmPassword").change (function(){

		if($(this).val()!=$("#password").val()){
			$("#error4").text("和上次密码不一致");
		}else{
			$("#error4").text("*");
		}
	})
	$("#phone").change (function(){
		
		if(!phone.test($(this).val())){
			$("#error5").text("手机格式不正确");
		}else{
			$("#error5").text("*");
		}
	})
	
	$("#answer").change (function(){
		if($.trim($(this).val())==""){
			$("#error7").text("请输入密保答案");
		}else{
			$("#error7").text("*");
		}
	})
	$("#email").change (function(){
		
		if(!email.test($(this).val())&&$.trim($(this).val())!=""){
			$("#error8").text("请输入正确的邮箱");
		}else{
			$("#error8").text("");
		}
	})
	$("#sub").click(function(){
		var filepath=$("input[name='photo']").val(); 
		   var extStart=filepath.lastIndexOf(".");
		   var ext=filepath.substring(extStart,filepath.length).toUpperCase();
		if($.trim($("#realName").val())==""){
			$("#error1").text("请你输入用户名");
		}else if(!idCard.test($("#idCard").val())){
			$("#error2").text("请输入有效的身份证");
		}else if($.trim($("#password").val()).length<=5||$.trim($("#password").val()).length>=18){
			$("#error3").text("密码长度为6~18位");
		}else if($("#confirmPassword").val()!=$("#password").val()){
			$("#error4").text("和上次密码不一致");
		}else if(!phone.test($("#phone").val())){
			$("#error5").text("手机格式不正确");
		}else if($.trim($("#answer").val())==""){
			$("#error7").text("请输入密保答案");
		}else if(!email.test($("#email").val())&&$.trim($("#email").val())!=""){
			$("#error8").text("请输入正确的邮箱");
		}else  if(ext!=".BMP"&&ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
			$("#error6").text("图片以bmp,png,gif,jpeg,jpg结尾");
		}else{
			$("form").submit();
		}
	})
})