<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="pageFormContent nowrap">
	<form method="post" action="/aqjy/usertotalview/insurance_add.action" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
		<div class="pageFormContent" layoutH="56">
			<dl>
				<dt>车辆ID：</dt>
				<dd><input name="insurance.carid" type="text" size="30" /></dd>
			</dl>
			<dl>
				<dt>车牌号：</dt>
				<dd><input name="insurance.vehiclelicense" class="required" type="text" size="30" alt="请输入车牌号"/></dd>
			</dl>
			<dl>
				<dt>交强险开始时间：</dt>
				<dd><input type="text" name="insurance.trafficstart" class="date"  /><a class="inputDateButton" href="javascript:;">选择</a>	</dd>
			</dl>
			<dl>
				<dt>交强险结束时间：</dt>
				<dd><input type="text" name="insurance.trafficend" class="date"  /><a class="inputDateButton" href="javascript:;">选择</a></dd>
			</dl>
			<dl>
				<dt>交强险单号：</dt>
				<dd><input name="insurance.tno" class="digits" type="text" size="30"/></dd>
			</dl>
			<dl>
				<dt>交强险文件：</dt>
				<dd><input type="file" size="30" name="trafficfile"/></dd>
			</dl>
			<dl>
				<dt>商业险开始时间：</dt>
				<dd><input type="text" name="insurance.insurancestart" class="date" /><a class="inputDateButton" href="javascript:;">选择</a></dd>	
			</dl>
			<dl>
				<dt>商业险结束时间：</dt>
				<dd><input type="text" name="insurance.insuranceend" class="date" /><a class="inputDateButton" href="javascript:;">选择</a></dd>
			</dl>
			<dl>
				<dt>商业险单号：</dt>
				<dd><input name="insurance.ino" class="digits" type="text" size="30" /></dd>
			</dl>
			<dl>
				<dt>商业险文件：</dt>
				<dd><input type="file" size="30" name="insurancefile"/></dd>
			</dl>
			<dl>
				<dt>备注：</dt>
				<dd><input type="text" rows="4" cols="50" name="insurance.memo" /></dd>
			</dl>
			<dl>
				<dt>录入时间：</dt>
				<dd><input type="text" size="30" name="insurance.enterdate" class="date" /><a class="inputDateButton" href="javascript:;">选择</a></dd>	
			</dl>
			<dl>
				<dt>更新时间：</dt>
				<dd><input type="text" size="30" name="insurance.updatedate" class="date" /><a class="inputDateButton" href="javascript:;">选择</a>	</dd>
			</dl>
			
		</div>
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>
