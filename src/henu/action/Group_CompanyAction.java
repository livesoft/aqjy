package henu.action;

import henu.dao.factory.DaoFactory;
import net.sf.json.JSONArray;

import com.opensymphony.xwork2.ActionSupport;

public class Group_CompanyAction extends ActionSupport {
    private String group;
    private String company;
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String findGroup()throws Exception{
		System.out.println("89899898989898989");
		JSONArray json=JSONArray.fromObject(DaoFactory.getGroup_companyDaoInstance().findGroup());
		System.out.println(json);
		group=json.toString();
		return SUCCESS;
	}
	public String findCompany()throws Exception{
		JSONArray json=JSONArray.fromObject(DaoFactory.getGroup_companyDaoInstance().findCompany(group));
		company=json.toString();
		return SUCCESS;
	}
}
