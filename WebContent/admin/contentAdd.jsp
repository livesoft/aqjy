<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib uri="/struts-tags" prefix="s"%>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
	<link rel="stylesheet" href="../ueditor/themes/default/css/ueditor.css">
  	<script>
  		var ue = UE.getEditor('editor');
  	</script>
 
  	
	<div class="pageContent">
	<form method="post" action="/aqjy/safetyeducation/ContentAction_add" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent nowrap" layoutH="97">
		<div style="height: 660px;overflow: auto;">
			<dl>
				<dt>所属类别：</dt>
				<dd>
					 <s:select  name="spe.seid" list="#session.listmap" listKey="key" listValue="value"	headerKey="" headerValue="请选择"/>
				</dd>
			</dl>
			<dl>
				<dt>标题：</dt>
				<dd>
					<input type="text" name="spe.title"  class=""/>
				</dd>
			</dl>
		<!-- 	<dl>
				<dt>备注：</dt>
				<dd>
					<input type="text" name="spe.memo" maxlength="50" class="" style="width:128px;"/>
				</dd>
			</dl> -->
			
			
			
			<dl>
				<dt>类别：</dt>
				<dd>
						<select name="spe.edutype" id="edutype">
						</select>
				</dd>
			</dl>
			<dl>
				<dt>发布人：</dt>
				<dd>
					<input type="text" name="spe.publisher"  class="" align="center"/>
				</dd>
			</dl>
			<!-- <dl>
				<dt>浏览次数：</dt>
				<dd>
					<input type="text" name="spe.viewnumbers"    class=""/>
				</dd>
			</dl> -->
			<dl>
				<dt>内容：</dt>
				<dd>

					  <script type="text/plain" id="editor" name="spe.content"  style="width:800px;height: 400px"></script>
				</dd>
			</dl>
			
			
			
			</div>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="submit">发布</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
<script>
	$(function(){
		$.getJSON("/aqjy/json/getDictionary",{table:"SAFETYEDUCATION",name:"EDUTYPE"},function(data){
			var json=JSON.parse(data);
			for (var i = 0; i < json.length; i++) {
				$("#edutype").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
			}
			
		})
	})
</script>
