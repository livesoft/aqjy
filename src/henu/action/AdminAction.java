package henu.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;
import henu.bean.Admin;
import henu.bean.Exam;
import henu.bean.ManagePerson;
import henu.bean.PInformation;
import henu.bean.Role;
import henu.dao.CompanyDao;
import henu.dao.RoleDao;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.Page;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 处理角色管理 03-20
 * @author wuhaifeng
 * */
public class AdminAction extends ActionSupport {

	private List<Admin> list_admin ;
	private String args;
	private AJAX ajax = new AJAX();
	private String url = "/admin/ajax.jsp";
	private Exam exam;
	private List<ManagePerson> list;
	private String result;
	private ManagePerson person; 		//管理人员
	private JSONObject jsonob; // 定义的json对象，用来传输接送相关的信息
	// 分页排序
	private Page page = new Page();
	private int pageNum = 1;
	private int numPerPage = 10;
	private String orderField = "maid";
	private String orderDirection = "asc";
	// 检索
	private String property = "-1";
	private String key = "-1";

	/**
	 * 所有的用户
	 * */
	public String all() {
		//System.out.println("访问到all方法");
		page = new Page();
		if (orderField.equals("") || orderDirection.equals("")) {
			orderField = "maid";
			orderDirection = "asc";
		}
		if (property.equals("") || key.equals("")) {
			property = "-1";
			key = "-1";
		}
		page.setProperties("ManagePerson", pageNum, numPerPage, property, key);
		list = DaoFactory.getManagePersonDaInstance().findByProperty(property, key, orderField, orderDirection,page.getStart(), page.getEnd());
	//	System.out.println("查询到的数量:"+list.size());
		CompanyDao companyDao  = DaoFactory.getCompanyDaoInstance();
		RoleDao roleDao =  DaoFactory.getRoleDaoInstance();
		list_admin = new ArrayList<Admin>();
		Admin admin = null ; 
		//把memo1变成公司
		for(ManagePerson p:list){
			admin = new Admin();
			admin.setAnswer(p.getAnswer());
			admin.setAudio(p.getAudio());
			admin.setCopyIdCard(p.getCopyIdCard());
			admin.setEmail(p.getEmail());
			admin.setLastTime(p.getLastTime());
			admin.setMaid(p.getMaid());
			admin.setMemo(p.getMemo());
			admin.setPassword(p.getPassword());
			admin.setPhone(p.getPhone());
			admin.setPhoto(p.getPhoto());
			admin.setQuestions(p.getQuestions());
			admin.setRealName(p.getRealName());
			admin.setRegistTime(p.getRegistTime());
			admin.setSex(p.getSex());
			admin.setRolename(roleDao.findById(p.getRole()).getRolename());
			admin.setComname(companyDao.findBy(String.valueOf(p.getComid())).getCompanyname());
			list_admin.add(admin);
		}
		url = "/admin/adminManage.jsp";
		return SUCCESS;
	}
	/**
	 * 删除一个后台用户
	 * */
	public String del(){
		ajax = new AJAX();
		int result = DaoFactory.getAdminDaoInstance().delete(Integer.parseInt(args));
		if(result > 0 ){
			ajax.setStatusCode("200");
			ajax.setMessage("删除成功");
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("删除失败");
		}
		return SUCCESS;
	}
	/**
	 * 用户只能管理自己的一部分信息，但是管理员可以管理一个用户的所有的信息
	 * 所以 要能改变用户的所有信息
	 * */
	public String edit(){
		ajax = new AJAX();
		//修改个人信息
		int result = DaoFactory.getManagePersonDaInstance().update(args, person);
		if(result > 0 ){		//更新成功
			ajax.setStatusCode("200");
			ajax.setMessage("更新成功！");
		}else{ //更新失败
			ajax.setStatusCode("300");
			ajax.setMessage("更新失败！");
		}
		url="/admin/ajax.jsp" ;
		return SUCCESS;
	}
	/*
	 * 用于修改一个用户的信息
	 * */
	public String find(){
	 person = DaoFactory.getManagePersonDaInstance().findById(args.trim());
	 url="/admin/adminUpdate.jsp";
	 return SUCCESS;
	}
	public String getArgs() {
		return args;
	}

	public void setArgs(String args) {
		this.args = args;
	}

	public AJAX getAjax() {
		return ajax;
	}

	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}
	public List<ManagePerson> getList() {
		return list;
	}
	public void setList(List<ManagePerson> list) {
		this.list = list;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public JSONObject getJsonob() {
		return jsonob;
	}
	public void setJsonob(JSONObject jsonob) {
		this.jsonob = jsonob;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getNumPerPage() {
		return numPerPage;
	}

	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	public String getOrderDirection() {
		return orderDirection;
	}

	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public List<Admin> getList_admin() {
		return list_admin;
	}
	public void setList_admin(List<Admin> list_admin) {
		this.list_admin = list_admin;
	}
	public ManagePerson getPerson() {
		return person;
	}
	public void setPerson(ManagePerson person) {
		this.person = person;
	}
}