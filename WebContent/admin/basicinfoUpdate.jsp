<!-- 此页面用于修改会议通知 -->
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 
</head>
<body>
	<div class="pageContent">
	<form method="post" action="/aqjy/safetymetting/content_update" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent nowrap" layoutH="97">
		<!-- <div style="height: 660px;overflow:auto;"> -->
			 <dl>
				<dt></dt><!-- 会议编号： -->
				<dd>
					<input type="hidden" name="bf.basicid" maxlength="20" value="${basicinfo.basicid }" class="required" />
					
				</dd>
			</dl> 
			
			 <dl>
				<dt>参加人员：</dt>
				<dd>
					<label><input type="checkbox" name="bf.joinPerson" value="车主司机" ${fn:contains(basicinfo.joinPerson,"车主司机")?'checked':'' }/>车主司机</label>
					<label><input type="checkbox" name="bf.joinPerson" value="集团" ${fn:contains(basicinfo.joinPerson,"集团")?'checked':'' }/>集团</label>
					<label><input type="checkbox" name="bf.joinPerson" value="公司" ${fn:contains(basicinfo.joinPerson,"公司")?'checked':'' }/>公司</label>
					<label><input type="checkbox" name="bf.joinPerson" value="交通运输协会" ${fn:contains(basicinfo.joinPerson,"交通运输协会")?'checked':'' }/>交通运输协会</label>
				</dd>
			</dl> 
			
			<dl>
				<dt>会议开始时间：</dt>
				<dd>
					<input type="text" name="bf.startTime" value="${basicinfo.startTime }" class="date required" dateFmt="yyyy-MM-dd HH:mm:ss" maxlength="20" readonly="true"/>
				    <!-- <a class="inputDateButton" href="javascript:;">选择</a> -->
				</dd>
			</dl>
			
			<dl>
				<dt>会议结束时间：</dt>
				<dd>
					<input type="text" name="bf.endTime" value="${basicinfo.endTime }" class="date required" dateFmt="yyyy-MM-dd HH:mm:ss" maxlength="20" readonly="true"/>
				    <!-- <a class="inputDateButton" href="javascript:;">选择</a> -->
				</dd>
			</dl>
			<dl>
				<dt>主持人：</dt>
				<dd>
					<input type="text" name="bf.compere" value="${basicinfo.compere }" maxlength="20" />
					
				</dd>
			</dl>
			
			<dl>
				<dt>组织人：</dt>
				<dd>
					<input type="text" name="bf.organizer" value="${basicinfo.organizer }" maxlength="20" class="required" />
					
				</dd>
			</dl>
			<dl>
				<dt>组织单位：</dt>
				<dd>
					<input type="text" name="bf.organizeUnit" value="${basicinfo.organizeUnit }" maxlength="20" class="required" />
					
				</dd>
			</dl>
			
			<dl>
				<dt>列席人员：</dt>
				<dd>
					<input type="text" name="bf.attendPerson" value="${basicinfo.attendPerson }" maxlength="20" />
					
				</dd>
			</dl>
			
			<dl>
				<dt>地点：</dt>
				<dd>
					<input type="text" name="bf.address" value="${basicinfo.address }" maxlength="20" />
					
				</dd>
			</dl>
			
			<dl>
				<dt>主题：</dt>
				<dd>
					<input type="text" name="bf.theme" value="${basicinfo.theme }" maxlength="20" class="required" />
					
				</dd>
			</dl>
			
			<dl>
				<dt>状态：</dt>
				<dd>
					<select name="bf.baudit" id="baudit">
					
						<option value="${basicinfo.baudit }" selected="selected">${basicinfo.baudit }</option>
		
					</select>
				</dd>
			</dl>
			<%-- <dl>
				<dt>发布时间：</dt>
				<dd>
					<input type="text" name="bf.publishTime" value="${basicinfo.publishTime }" class="date required" dateFmt="yyyy-MM-dd HH:mm:ss" maxlength="20" readonly="true"/>
				    <!-- <a class="inputDateButton" href="javascript:;">选择</a> -->
				</dd>
			</dl> --%>
			<dl>
				<dt>备注：</dt>
				<dd>
					<input type="text" name="bf.remark" value="${basicinfo.remark }" maxlength="20" />
					
				</dd>
			</dl>
			</div>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="submit">修改</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
<script>
	$(function(){
		$.getJSON("/aqjy/json/getDictionary",{table:"BASICINFO",name:"BAUDIT"},function(data){
			var json=JSON.parse(data);
			for (var i = 0; i < json.length; i++) {
				$("#baudit").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
			}
			
		})
	})
</script>
</body>
</html>