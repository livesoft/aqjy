package henu.dao;

import java.util.List;

import henu.bean.Exam;

/**
 * @author wuhaifeng
 * */

public interface ExamDao   {
	
	/**
	 * 保存Exam对象
	 * @param exam
	 * @return
	 */
	public int save(Exam exam);
	
	/**
	 * 根据eid进行删除
	 * @param eid
	 * @return
	 */
	public int delete(String eid);
	/**
	 * 查找指定eid的记录
	 * @param eid
	 * @return
	 */
	public Exam findById(int eid);
	
	/**
	 * 查找所有的记录
	 * @param order 排序字段
	 * @param sort 排序方式
	 * @return
	 */
	
	public List<Exam> findAll(String order, String sort,String property,String key);
	/**
	 * 根据指定字段，进行查询
	 * @param property 字段名称
	 * @param key
	 * @param order 排序字段
	 * @param sort 排序方式
	 * @return
	 */
	public List<Exam> findByProperty(String property, String key, String order, String sort,int start ,int end);
	
	/**
	 * 根据指定题目编号的集合/数组获取某个考生的试题
	 * select * from tb_Exam where teid in (13,44,23) 
	 * @param eid
	 * @return
	 */
	public List<Exam> getExam(int[] eid);
	
	/**
	 * 根据编号进行修改
	 * @param eid
	 * @param exam
	 * @return
	 */
	public int update (String eid , Exam exam);
}
