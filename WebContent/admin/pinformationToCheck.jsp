<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="pageFormContent nowrap">

	<form method="post" action="/aqjy/userManage/pinformation_pass?args=${p.idCard }&{memo1} "class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
		
		<div style="width:700px;height:450px;float:left">
		<div class="pageFormContent" layoutH="56">
			<dl>
				<dt>姓名：</dt>
				<dd><input type="text" value="${p.realName }" readonly="readonly"/></dd>
			</dl>
			
			<dl>
				<dt>身份证号：</dt>
				<dd><input type="text" value="${p.idCard }" readonly="readonly"/></dd>
			</dl>			
			<dl>
				<dt>手机号：</dt>
				<dd><input type="text" value="${p.phone }" readonly="readonly"/></dd>		
			</dl>
			<dl>
				<dt>公司：</dt>
				<dd><input type="text" value="${p.comid }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>审核状态：</dt>
				<dd><input type="text" name="paudit" id="paudit" value="${p.paudit }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>住址：</dt>
				<dd><input  type="text" <c:if test='${p.address==null}'>该用户未填写住址</c:if> value="${p.address }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>用户类型：</dt>
				<dd><input type="text" value="${p.regType }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>注册时间：</dt>
				<dd><input type="text" value="${p.regTime }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>邮箱：</dt>
				<dd><input type="text" value="${p.email }" readonly="readonly" /></dd>
			</dl>
			<div class="divider"></div>
			<dl>
				<dt>备注：</dt>
				<dd><textarea name="memo1" rows="3" cols="30" <c:if test='${p.paudit== "是" }'>readonly="readonly" </c:if> >${p.memo1 }</textarea></dd>
			</dl>
			
				<c:if test='${p.paudit== "禁用" }'><dl><dd style="color: red">注意：若取消用户禁用，用户将默认为通过审核！</dd></dl></c:if>	
		</div>
		
		</div>
		
		<div style="position: absolute;top:10px;right:20px"><img alt="用户照片" src="../${ p.photoPath}" width="450px" height="280px"  ></div>
	<div class="formBar" >
			<ul>
					<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
					<c:if test='${p.paudit== "否" }'><li><div class="buttonActive"><div class="buttonContent"><button onclick="$('#paudit').val('是')" type="submit">审核通过</button></div></div></li></c:if>
					<c:if test='${p.paudit== "是" }'><li><div class="buttonActive"><div class="buttonContent"><button onclick="$('#paudit').val('否')" type="submit">不予通过</button></div></div></li></c:if>
					<c:if test='${p.paudit!= "禁用" }'><li><div class="buttonActive"><div class="buttonContent"><button onclick="$('#paudit').val('禁用')" type="submit">禁用用户</button></div></div></li></c:if>
					<c:if test='${p.paudit== "禁用" }'><li><div class="buttonActive"><div class="buttonContent"><button onclick="$('#paudit').val('是')" type="submit">取消禁用</button></div></div></li></c:if>
				<li>
						<div class="button"><div class="buttonContent"><button type="button" class="close">关闭</button></div></div>
				</li>
			</ul>
	</div>
		
	</form>
</div>
