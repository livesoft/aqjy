package henu.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import henu.bean.Exercise_Front;
import henu.bean.ManagePerson;
import henu.bean.PInformation;
import henu.dao.ExamDao;
import henu.dao.ExerciseDao;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.CONSTANTS;
import henu.util.Page;

import com.opensymphony.xwork2.ActionSupport;

/**
 * 成绩管理
 * 03-15
 * @author wuhaifeng
 * */
public class ExerManager extends ActionSupport{
		private static final long serialVersionUID = 1L;
		private List<Exercise_Front> exer_list;
		private AJAX ajax;
		private String args;
		private String idcard;
		private String url;
		//分页排序
		private Page page;
		private int pageNum = 1;
		private int numPerPage = 10;
		private String orderField = "eid";
		private String orderDirection = "asc";
		// 检索
		private String property = "-1";
		private String key = "-1";
		
		/**
		 * 成绩管理，显示出所有人的成绩，可以对其成绩进行查看 支持分页 查询 ,这个是根据用户表进行分页查询多一个bean 需要的数据：
		 * 用户姓名，考试分数，考试编号，考试名称，身份证号 ，
		 * */
		public String mark() {
			HttpSession session = ServletActionContext.getRequest().getSession();
			//想办法记录当前是哪个考试正在被访问
			if(args!=null){
				session.setAttribute("EID", args);
			}else{
				args = (String) session.getAttribute("EID");
			}
			page = new Page();
			//ManagePerson m=(ManagePerson) ServletActionContext.getRequest().getSession().getAttribute(CONSTANTS.SESSION_ADMIN);
			ManagePerson m = new ManagePerson();
			m.setComid(11);
			exer_list = new ArrayList<Exercise_Front>();// 显示成绩管理
			List<PInformation> user_list = null ;
			ExerciseDao exerdao = DaoFactory.getExerciseDaoInstance();
			int eid = Integer.parseInt(args);
			if (orderField.equals("") || orderDirection.equals("")) {
				orderField = "qid";
				orderDirection = "asc";
			}
			if (property.equals("") || key.equals("")) {
				property = "-1";
				key = "-1";
			}
			// System.out.println("action查询的关键字是:" + key);
			// System.out.println("查询的依据:" + property);
			/**
			 * 这个page设置的属性应该是两个字段的 检索的数据是 除了包括检索的数据之外还需要检测一个  eid 
			 * */
			 page.setProperties2("pinformation",pageNum,numPerPage,property.trim(), key.trim(),"paudit","是","comid",Integer.toString(m.getComid()));
			// 给page这个class设置属性
			user_list = DaoFactory.getPInformationDaoInstance().findByProperty(
					property.trim(), key.trim(),Integer.toString(m.getComid()), orderDirection.trim(),
					page.getStart(), page.getEnd());
			// user_list是所有的用户，对每个用户取其成绩
			// 再次封装数据即可，把所需的数据放到bean里面
			String idCard = null;
			ExamDao examdao = DaoFactory.getExamDaoInstance();
			String exam_name = examdao.findById(eid).getExamname(); // 当前考试的名字
			for (PInformation user : user_list) {
				Exercise_Front ef = new Exercise_Front();
				idCard = user.getIdCard();
				ef.setIdCard(idCard);
				ef.setUsername(user.getRealName());
				ef.setExam_name(exam_name);
				ef.setScore(exerdao.sumScore(idCard, eid));
				ef.setSex(user.getSex());
				exer_list.add(ef); // 放到exer_list拿到前台输出
			}
			//System.out.println("查询出的数量：" + exer_list.size());
			url = "/admin/exerciseManager.jsp";
			return SUCCESS;
		}
		/**
		 * 删除一个人某次考试的成绩,也就是删除其在所做的题目
		 * @param idcard 这个人的身份证
		 * @param eid 考试
		 * */
		public String del(){
			HttpSession session = ServletActionContext.getRequest().getSession();
			//想办法记录当前是哪个考试正在被访问
			args = (String) session.getAttribute("EID");
			//删除用户考试
			boolean result = false;
			ajax = new AJAX();
			ExerciseDao exerciseDao = DaoFactory.getExerciseDaoInstance();
			result = exerciseDao.delScore(Integer.parseInt(args),idcard);
			if(result == true){
				ajax.setCallbackType("");
				ajax.setMessage("删除成功");
				ajax.setStatusCode("200");
			}else{
				ajax.setCallbackType("");
				ajax.setMessage("删除失败,请重试");
				ajax.setStatusCode("300");
			}
			url="/admin/ajax.jsp";
			return SUCCESS;
		}
		public Page getPage() {
			return page;
		}
		public void setPage(Page page) {
			this.page = page;
		}
		public int getPageNum() {
			return pageNum;
		}
		public void setPageNum(int pageNum) {
			this.pageNum = pageNum;
		}
		public int getNumPerPage() {
			return numPerPage;
		}
		public void setNumPerPage(int numPerPage) {
			this.numPerPage = numPerPage;
		}
		public String getOrderField() {
			return orderField;
		}

		public void setOrderField(String orderField) {
			this.orderField = orderField;
		}
		public String getOrderDirection() {
			return orderDirection;
		}
		public void setOrderDirection(String orderDirection) {
			this.orderDirection = orderDirection;
		}
		public String getProperty() {
			return property;
		}
		public void setProperty(String property) {
			this.property = property;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public List<Exercise_Front> getExer_list() {
			return exer_list;
		}
		public void setExer_list(List<Exercise_Front> exer_list) {
			this.exer_list = exer_list;
		}
		public String getArgs() {
			return args;
		}
		public void setArgs(String args) {
			this.args = args;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public String getIdcard() {
			return idcard;
		}
		public void setIdcard(String idcard) {
			this.idcard = idcard;
		}
		public AJAX getAjax() {
			return ajax;
		}
		public void setAjax(AJAX ajax) {
			this.ajax = ajax;
		}
}
