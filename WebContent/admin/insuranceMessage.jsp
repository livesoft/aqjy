<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form id="pagerForm" method="post" action="/aqjy/usertotalview/insurance_all">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${page.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" /> 
    <input type="hidden" name="orderDirection"  value="${param.orderDirection}" />  
    <input type="hidden" name="property"  value="${property}">
    <input type="hidden" name="key"  value="${key}">
</form>
 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- -别人一点连接跳进action action返回json 打开此页面  -->
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="/aqjy/usertotalview/insurance_all" method="post">
	<div class="searchBar">
		<table class="searchContent">
			<tr>
				<td>
					查询方式：
				</td>
				<td>
					<select name="property">
					 <option value="" >查询方式</option>
					 <option value="VEHICLELICENSE" >车牌号</option>
					  <option value="REALNAME" >车主姓名</option>
					 <option value="IDCARD" >车主身份证号</option>
					</select>	
				</td>
				<td><input type="text" name="key" /></td>
			</tr>
		</table>
		<div class="subBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
			</ul>
				
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<!-- <li><a class="add" href="insuranceAdd.jsp" target="navTab" ><span>添加</span></a></li> -->
			<li><a class="edit" href="/aqjy/usertotalview/insurance_findById.action?args={args}" target="navTab"><span>查看</span></a></li>
			<li><a class="delete" href="/aqjy/usertotalview/insurance_del.action?args={args}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<!-- <li><a class="edit" href="/aqjy/usertotalview/insurance_findById.action?args={args}" target="navTab"><span>修改</span></a></li> -->
			<li class="line">line</li>
			<li><a class="icon" href="" target="dwzExport"  title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
	<!-- -此处通过 读取数据库加载所有信息 -->
		<thead>
			<tr>
				<th>车主姓名</th>
                <th>车牌号</th>
				<th>车辆ID</th>
				<th>交强险单号</th>
                <th>商业险单号</th>
               
			</tr>
		</thead>
		<tbody>
		
			<c:forEach var="cl" items="${list}">
				<tr target="args" rel="${cl.inid}">				
				<td>${cl.realName}</td>
				<td>${cl.vehiclelicense}</td>
				<td>${cl.carid }</td>
				<td>${cl.tno }</td>
				<td>${cl.ino }</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10" >10</option>
					<option value="50" selected="selected">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '100' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" selected="selected">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '500' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" >100</option>
					<option value="500" selected="selected">500</option>
				</c:if>
			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>
	</div>
</div>
