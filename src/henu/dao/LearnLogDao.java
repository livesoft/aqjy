package henu.dao;


import henu.bean.LearnLog;
import henu.bean.LearnLogPinformation;

import java.util.List;




/**
 * 对学习记录进行操作
 * @author 于晨
 *
 */
public interface LearnLogDao {
	
	/*//删除学习记录
	public int delete(String llid);
	
	*//**
	 * 查询全部学习记录并列举出来
	 * @param order
	 * @param sort
	 * @return
	 */
	//public List<LearnLog> findAll(String order,String sort);
	public List<LearnLogPinformation> findAll(String order,String sort);
	
	/**
	 * 根据表字段property和相应关键字key进行模糊查找
	 * @param property
	 * @param key
	 * @param order 排序字段名称
	 * @param 排序方式，降序还是升序
	 * @return 返回符合条件的记录。
	 */
	public List<LearnLogPinformation> findByProperty(String property,String key,String order,String sort,int start,int end,String companyname);
	
	
	/**
	 * 查找前台用户自己的学习记录
	 * @param id
	 * @return
	 */
	public LearnLogPinformation findById(String id);
	
	
	
	/**
	 * 从点击时传递标题，和开始学习的时间
	 * @param id 
	 * @param title
	 * @param curr_date
	 * @return
	 */
	public int update(String id,String title,String starttime);
	
	
	/**
	 * 存取所取到的值
	 * @param idcard
	 * @param title
	 * @param starttime
	 */
	public void add(String idcard,String title,String starttime);
	
	//判断是否存在学习记录
	public int findById2(String id);
	
	//批量删除的方法
	public int[] batchDelete(Object[][] params);
	
	
	

}
