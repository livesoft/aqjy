package henu.action;

import henu.service.ImportExcel;
import henu.service.RandomList;
import henu.util.AJAX;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;
import oracle.net.aso.a;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class QuestionsImp extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private File files ; 
	private String url ; 
	private String filesFileName ; 
	private String filesContentType ;
	

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public File getFiles() {
		return files;
	}
	public void setFiles(File files) {
		this.files = files;
	}
	public String getFilesFileName() {
		return filesFileName;
	}
	public void setFilesFileName(String filesFileName) {
		this.filesFileName = filesFileName;
	}
	public String getFilesContentType() {
		return filesContentType;
	}
	public void setFilesContentType(String filesContentType) {
		this.filesContentType = filesContentType;
	}
	@Override
	public String execute() throws Exception {
//		System.out.println("单独到处action访问到imp");
		FileInputStream bis = null;
		FileOutputStream bos = null;
		String savepath = ServletActionContext.getServletContext().getRealPath("/");
		try {
			savepath = savepath + "/" +RandomList.getNum(9999)+filesFileName;
			//System.out.println("上传文件的地址是："+savepath);
			bis = new FileInputStream(files);
			bos = new FileOutputStream(savepath);
			int temp = 0;
			byte[] buffer = new byte[1024];
			while ((temp = bis.read(buffer)) > 0) {
				bos.write(buffer, 0, temp);
			}
			bos.flush();
			int count = ImportExcel.questionts(savepath);
			//System.out.println("成功了:" + count);
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				bis.close();
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		url="/ajaxDone.html";
		return SUCCESS;
	}
}
