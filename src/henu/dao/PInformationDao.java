package henu.dao;
import henu.bean.PInformation;

import java.util.List;
public interface PInformationDao {
	
	public int add(PInformation pinfo);
	public int deleteByIdCard(String idcard);
	public PInformation findByIdCardOrPhone(String idCard, String phone);
	public PInformation findByIdCardAndQuestionAndAnswer(String idCard,String question, String answer);
	public int resetPassword(String idCard, String newPassword);
	public PInformation login(String phone, String password);
	public void updateLastTime(String idCard);
	
	public int pass(String idCard,String memo1,String paudit);//审核通过，不通过，禁用，不禁用。。。
	public PInformation findByidCard(String idCard);//按身份证查找 用于列出待审核信息
	//为审核司机
	public List<PInformation> findWaitChkeck(String property, String key,String comid, String sort, int start, int end);
	//已审核司机
	public List<PInformation> findByProperty(String property, String key,String comid, String sort, int start, int end) ;
	//用于前台用户更新
	public int update(PInformation p,String idcard);
	
	//前台用户查看公司名称
	public String findByComid(int comid);
	//前台用户编辑信息时 需要查询对应公司的comid 
	public int findByCompanyName(String companyname);
	//返回ismaster是否是车主
	public String findbyid(String idcard);
	//后台查找所有的禁用用户
	public List<PInformation> ban(String property, String key,String comid, String sort, int start, int end);
	
}
