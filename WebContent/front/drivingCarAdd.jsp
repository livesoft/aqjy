
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<body style="position: relative;">

	<div class="pageContent" >
	<form method="post" action="/aqjy/usertotalview/drivingCar_add" enctype="multipart/form-data" class="pageForm required-validate" onsubmit="return iframeCallback(this,navTabAjaxDone);">
		<div class="pageFormContent nowrap" layoutH="97">
		<input type="hidden" name="">
			<dl>
				<dt>行车证号：</dt>
				<dd>
					<input type="text" name="dc.drivingId" value="${dc.drivingId}" maxlength="50" class="required"/>
				</dd>
			</dl>
			<dl>
				<dt>车牌号：</dt>
				<dd>
					<input type="text" name="dc.vehicleLicense"   maxlength="20" class="required"/>
				</dd>
			</dl>
			<dl>
				<dt>车辆类型：</dt>
				<dd>
				    <select name="dc.vehicleType" id="vehicleType">
					</select>
				</dd>
			</dl>
			<dl>
				<dt>所有者：</dt>
				<dd>
					<input type="text" name="dc.owners"   class="required"/>
				</dd>
			</dl>
			<dl>
				<dt>使用性质：</dt>
				<dd>
					<input type="text" name="dc.useCharacter" />
				</dd>
			</dl>
			<dl>
				<dt>品牌型号：</dt>
				<dd>
					<input type="text" name="dc.models" />
				</dd>
			</dl>
			<dl>
				<dt>车辆类型识别代号：</dt><!--数据字典  -->
				<dd>
					<input type="text" name="dc.vin" />
				</dd>
			</dl>
			<dl>
				<dt>行车证复印件：</dt>
				<dd>
					<input type="file" name="upload" class="required image"/>
				</dd>
			</dl>
			<dl>
				<dt>发动机号码：</dt>
				<dd>
					<input type="text" name="dc.engineid" maxlength="50"/>
				</dd>
			</dl>
			<dl>
				<dt>档案编号：</dt>
				<dd>
				<input type="text" name="dc.doclc" maxlength="50"/>
					
				</dd>
			</dl>
			<dl>
				<dt>核载人数：</dt>
				<dd>
					<input type="text" name="dc.menNumber" max="10"/>
				</dd>
			</dl>
			<dl>
				<dt>总质量：</dt>
				<dd>
					<input type="text" name="dc.totalWeight"/>
				</dd>
			</dl>
			<dl>
				<dt>整备质量：</dt>
				<dd>
					<input type="text" name="dc.weight"/>
				</dd>
			</dl>
			<dl>
				<dt>核定载质量：</dt>
				<dd>
					<input type="text" name="dc.checkWeight"/>
				</dd>
			</dl>
			<dl>
				<dt>外型尺寸：</dt>
				<dd>
					<input type="text" name="dc.sizes"/>
				</dd>
			</dl>
			<dl>
				<dt>准牵引质量：</dt>
				<dd>
					<input type="text" name="dc.towWeight"/>
				</dd>
			</dl>
			<dl>
				<dt>住址：</dt>
				<dd>
					<input type="text" name="dc.address" />
				</dd>
			</dl>
			<dl>
				<dt>有效期：</dt>
				<dd>
					<select name="dc.validations">
					   	<option value="1年">1年</option>
						<option value="2年">2年</option>
						<option value="3年">3年</option>
					</select>
				</dd>
			</dl>
			
			<dl>
				<dt>备注：</dt>
				<dd>
					<textarea  name="dc.memo" class="editor"  rows="6" cols="100" tools="mini"></textarea>
				</dd>
			</dl>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="sudcit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
		</div>
	</form>
	
</div>
<script>
/* $(function(){
	$.getJSON("/aqjy/json/getDictionary",{table:"DRIVINGCAR",name:"VEHICLETYPE"},function(data){
		var json=JSON.parse(data);
		for (var i = 0; i < json.length; i++) {
			$("#vehicleType").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
		}
		
	}) */
$(function(){
$.getJSON("/aqjy/json/getDictionary",{table:"CARINFORMATION，DRIVINGCAR",name:"VEHICLETYPE"},function(data){
	var json=JSON.parse(data);
	for (var i = 0; i < json.length; i++) {
		$("#vehicleType").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
	}
	
})
})
</script> 
</body>