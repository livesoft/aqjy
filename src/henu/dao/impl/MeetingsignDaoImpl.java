package henu.dao.impl;
/**
 * 对Meetingsign表DAO的实现
 * @author 田星杰
 * 2015-01-22
 */
import henu.bean.Meetingsign;
import henu.dao.MeetingsignDao;
import henu.dao.factory.DaoFactory;
import henu.util.Dbcp;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class MeetingsignDaoImpl implements MeetingsignDao{

	@Override
	public int save(Meetingsign meetingsign) {
		// TODO Auto-generated method stub
		int result = 0;
		try {
			String sql="insert into meetingsign(comid,mcid,signPerson,signTime,basicid) values(?,?,?,?,?)";
			QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
			Object[] params ={meetingsign.getComid(),meetingsign.getMcid(),meetingsign.getSignPerson(),meetingsign.getSignTime(),meetingsign.getBasicid()};
			result=qr.update(sql, params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return result;
	}

	@Override
	public int delete(int msid) {
		// TODO Auto-generated method stub
		int result = 0;
		try {
			QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
			String sql = "delete from meetingsign where msid = ?";
			result = qr.update(sql, msid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int update(int msid, Meetingsign meetingsign) {
		// TODO Auto-generated method stub
		int result = 0;
		try {          
			QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
			String sql = "update  meetingsign  set comid=?,mcid=?,signPerson=?,signTime=?,basicid=? where msid=?";
			Object[] params ={meetingsign.getComid(),meetingsign.getMcid(),meetingsign.getSignPerson(),meetingsign.getSignTime(),meetingsign.getBasicid(),msid};
			result = qr.update(sql, params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Meetingsign findAllBymcId(int msid) {
		// TODO Auto-generated method stub
		Meetingsign bf=null;
		String sql="select * from meetingsign where msid=?";
		QueryRunner qr=DaoFactory.getRunner();
		try {
			bf = qr.query(sql,new BeanHandler<Meetingsign>(Meetingsign.class), msid);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bf;
	}

	@Override
	public List<Meetingsign> findByProperty(String property, String key,
			String order, String sort, int start, int end) {
		// TODO Auto-generated method stub
		List <Meetingsign> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "SELECT * FROM (SELECT tt.*,ROWNUM ro FROM(select * from meetingsign where "
				+ property
				+ "='"
				+ key
				+ "'  order by "
				+ order
				+ " "
				+ sort
				+ ") tt WHERE ROWNUM <=" + end + ") WHERE ro > " + start + "";
		try {
			list = runner.query(sql, new BeanListHandler<Meetingsign>(Meetingsign.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Meetingsign> findAll(String order, String sort,String property,String key) {
		// TODO Auto-generated method stub
		List<Meetingsign> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "SELECT * FROM meetingsign where " + property + " = ? "
				+ " ORDER BY  " + order + " " + sort;
		try {
			list = runner.query(sql, new BeanListHandler<Meetingsign>(Meetingsign.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

}
