package henu.action;
/**
 * 主要用于前台用户对安全会议内容部分的处理
 * @author 田星杰
 * 2015-01-25
 */

import henu.bean.Mcontent;

import henu.dao.McontentDao;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.Page;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

public class MyMettingContentAction extends ActionSupport{
	//分页查询
		Page page;
		private int pageNum=1;
		private int numPerPage=10;
		private String orderField="mcid";
		private String orderDirection="desc";
		
		//检索
		private String property="-1";
		private String key="-1";
		
		
		private AJAX ajax;
		private Mcontent mc;
		private String url="/admin/ajax.jsp";
		private String uid;
		private List<Mcontent> list;
		public Page getPage() {
			return page;
		}
		public void setPage(Page page) {
			this.page = page;
		}
		public int getPageNum() {
			return pageNum;
		}
		public void setPageNum(int pageNum) {
			this.pageNum = pageNum;
		}
		public int getNumPerPage() {
			return numPerPage;
		}
		public void setNumPerPage(int numPerPage) {
			this.numPerPage = numPerPage;
		}
		public String getOrderField() {
			return orderField;
		}
		public void setOrderField(String orderField) {
			this.orderField = orderField;
		}
		public String getOrderDirection() {
			return orderDirection;
		}
		public void setOrderDirection(String orderDirection) {
			this.orderDirection = orderDirection;
		}
		public String getProperty() {
			return property;
		}
		public void setProperty(String property) {
			this.property = property;
		}
		public String getKey() {
			return key;
		}
		public void setKey(String key) {
			this.key = key;
		}
		public AJAX getAjax() {
			return ajax;
		}
		public void setAjax(AJAX ajax) {
			this.ajax = ajax;
		}
		
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public String getUid() {
			return uid;
		}
		public void setUid(String uid) {
			this.uid = uid;
		}
		public Mcontent getMc() {
			return mc;
		}
		public void setMc(Mcontent mc) {
			this.mc = mc;
		}
		public List<Mcontent> getList() {
			return list;
		}
		public void setList(List<Mcontent> list) {
			this.list = list;
		}
		//列出会议内容信息
		public String findAll(){
			page = new Page();
			if(orderField.equals("")||orderDirection.equals("")){
				orderField="mcid";
				orderDirection="desc";
			}
			if(property.equals("")||key.equals("")){
				property="-1";
				key="-1";
			}
			page.setProperties("MCONTENT",pageNum,numPerPage,property,key);
			
			list = DaoFactory.getMcontentDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());
			//后台用户页面的URL meetingMgt.jsp
			//url="/admin/mContentCfmt.jsp";
			//前台用户页面的URL mettingNotify.jsp
			url="/front/myMetting.jsp";
			
			return SUCCESS;
		}
		
}
