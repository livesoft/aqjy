package henu.bean;
/**
 * 记录考生所做题目
 * @author wuhaifeng
 * */
public class Exercise  implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private Integer exid ; 			//流水
	private String idcard ; 	    //考生的id
	private Integer qid;			//题目的id
	private Integer score ;			//这个题目的得分
	private Integer eid ; 			//考试的id，也就是哪一场考试

	public Integer getExid() {
		return exid;
	}
	public void setExid(Integer exid) {
		this.exid = exid;
	}

	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public Integer getQid() {
		return qid;
	}
	public void setQid(Integer qid) {
		this.qid = qid;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Integer getEid() {
		return eid;
	}
	public void setEid(Integer eid) {
		this.eid = eid;
	}
	
}
