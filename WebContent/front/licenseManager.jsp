
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="pagerForm" method="post" action="">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${model.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" />
</form>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="demo_page1.html" method="post"></form></div>
	<div class="searchBar"></div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		    <c:if test="${empty li }">
			 	<li><a class="add" href="licenseAdd.jsp" target="navTab"><span>添加</span></a></li>
			</c:if>
			<c:if test="${not empty li }">
				<li><a class="delete" href="/aqjy/usertotalview/LicenseAction_delete?uid={sid_user}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="/aqjy/usertotalview/LicenseAction_find?uid={sid_user}" target="navTab"><span>修改</span></a></li>
			</c:if>
			<li class="line">line</li>
			<!-- <li><a class="icon" href="demo/common/dwz-team.xls" target="dwzExport" targetType="navTab" title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li> -->
		</ul>
	</div>
</div>
	<table class="table" width="100%" layoutH="138">
	<c:if test="${empty li }">
	    <div>
	              <br/>
		          <h1>初次登陆，请完善驾驶证信息</h1>
		          <br/>
		          <h1>注意：驾驶证信息只需添加一次，若有变动，须删除原有驾驶证信息。</h1>
		          
	    </div>
	</c:if>
	<c:if test="${not empty li }">
		<thead>
			<tr>
				<th>身份证号</th>
				<th>准驾车型</th>
				<%-- <th orderField="firsttime" <c:if test='${orderField == ""firsttime"" }'> class="${orderDirection}"  </c:if> >发布时间</th> --%>
				<th>初次领证时间</th>
				<th>有效期</th>
				<th>有效期起始时间</th>
				<th>年审信息</th>
<!-- 				<th>驾驶证复印件</th> -->
				<th>备注</th>
			</tr>
		</thead>
		     <tbody>
		     <tr target="sid_user" rel="${li.idcard }">
					<td><a href="/aqjy/usertotalview/LicenseAction_findDetail?uid=${li.idcard}" target="navTab">${li.idcard}</a></td> 
					<td>${li.cartype}</td>
					<td>${li.firsttime}</td>
					<td>${li.limittime}</td>
					<td>${li.begintime}</td>
					<td>${li.yearaudit}</td>
					<%-- <td>${li.lphoto }</td> --%>
					<td>${li.memo}</td>
				</tr>
		</tbody>
	</c:if>
	</table>