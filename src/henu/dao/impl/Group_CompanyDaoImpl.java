package henu.dao.impl;

import java.sql.SQLException;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import henu.bean.Group_Company;
import henu.dao.Group_companyDao;
import henu.util.Dbcp;
public class Group_CompanyDaoImpl implements Group_companyDao {
  public static void main(String[] args){
	  
	  Group_CompanyDaoImpl gd = new Group_CompanyDaoImpl();
	  List<Group_Company> list = gd.findGroup();
	  JSONArray json = JSONArray.fromObject(list);
	  System.out.println(json);
	  
  }
	@Override
	public List<Group_Company> findGroup() {
		// TODO Auto-generated method stub
		List<Group_Company> list = null;
		try {
			QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
			String  sql = "select GROUP from GROUP_COMPANY";
			list = qr.query(sql,new BeanListHandler<Group_Company>(Group_Company.class));
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		return list;
	}

	@Override
	public List<Group_Company> findCompany(String group) {
		// TODO Auto-generated method stub
		List<Group_Company> list = null;
		try {
			QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
			String  sql = "select   company from Group_Company where group=?";
			list = qr.query(sql,new BeanListHandler<Group_Company>(Group_Company.class),group);
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
		return list;
	}

}
