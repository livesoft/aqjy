<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>专题详细信息</title>
</head>
<body>
<div class="pageContent">
	<form method="post" action="" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
		<div class="pageFormContent" layoutH="56" style="width:700px;height:600px;float:left">
			<dl>
				<dt>标题1：</dt>
				<dd><input name="spe.title" type="text" value="${spe.title }" readonly="readonly"/></dd>
			</dl>
			<%-- <div style="position: absolute;top:10px;right: 20px;">
				<img src="..${s.photoPath}" style="width:150px;height:150px;"/>
			</div> --%>
			<dl>
				<dt>内容：</dt>
				<dd><a href="${spe.content }" target="blank">点击查看 </a></dd>
			</dl>
			     
			<dl>
				<dt>类别：</dt>
				<dd><input type="text" name="spe.edutype" value="${spe.edutype }" readonly="readonly"/></dd>
			</dl>
			
			<dl>
				<dt>发布时间：</dt>
				<dd><input type="text" name="spe.publishtime" value="${spe.publishtime}" readonly="readonly"/></dd>
			</dl>
			       
			<dl>
				<dt>发布人：</dt>
				<dd><input type="text" name="spe.publisher" class="" value="${spe.publisher}" readonly="readonly"/></dd>
			</dl>
			   <dl>
				<dt>浏览次数：</dt>
				<dd><input type="text" name="spe.viewnumbers" class="" value="${spe.viewnumbers}" readonly="readonly"/></dd>
			</dl>     
			<dl>
				<dt>备注：</dt>
				<dd><input type="text" name="spe.memo" value="${spe.memo }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>所属专题ID</dt>
				<dd><input type="text" name="spe.seid" value="${spe.seid }"  readonly="readonly"/></dd>
			</dl>
		</div>
		<div class="formBar">
		</div>
	</form>
</div>
</body>
</html>