package henu.service;

import java.util.*;

/**
 * 本类用于生成不重复随机数的集合 可用于从试题库中抽题，指定题目数量，生成指定数量的随机数集合
 * 
 * @author 梁胜彬 2014-11-08
 */
public class RandomList {

	/**
	 * 在一个[0, range-1]范围内生成一个指定数量number的集合
	 * 
	 * @param range
	 *            随机数范围，在[1,range]范围内
	 * @param number
	 *            集合的数量
	 * @return Object数组形式，生成随机数的集合
	 */
	public Object[] randomData(int range, int number) {
		Set<Integer> set = new HashSet<Integer>();
		while (true) {
			// 生成一个随机数
			int data = new Random().nextInt(range) + 1;
			// 把该随机数放到set中，如有重复自动过滤
			set.add(data);

			if (set.size() >= number) {
				break;
			}
		}
		return set.toArray();
	}

	/**
	 * 在一个[0, range-1]范围内生成一个指定数量number的集合，以字符串的形式返回
	 * 字符串的格式如下(235,63,23,3,6)，便于在数据库查询时以 select * from table where id in
	 * (235,63,23,3,6)方式查询
	 * 
	 * @param range
	 *            随机数范围，在[1,range]范围内
	 * @param number
	 *            集合的数量
	 * @param range
	 * @param number
	 * @return
	 */
	public String randomDataString(int range, int number) {
		Object[] obj = randomData(range, number);
		StringBuffer sb = new StringBuffer("(");

		for (int i = 0; i < obj.length; i++) {
			sb.append(obj[i]);
			sb.append(",");
		}
		// 去掉sb最后的一个逗号
		String s = sb.substring(0, sb.length() - 1);
		// 最后加在)
		s = s + ")";
		return s;
	}

	public static void main(String[] args) {

		RandomList rl = new RandomList();
		//第一个参数是范围 第二个参数是要生成数字的个数,第二个数一定比第一个数大 
		Set<Integer> s = rl.randomNum(10, 5);
		System.out.println(s.toString());
	}

	/**
	 * 用于文件名防止重复
	 * @param num 范围
	 * */
	public static String getNum(int num) {
		Random random = new Random();
		int result = random.nextInt(num);
		return String.valueOf(result);
	}
	/**
	 * 生成不重复的数字集合
	 * @param range 范围
	 * @param amount 数量
	 * */
	public Set<Integer> randomNum(int range, int amount) {
		Set<Integer> set = new HashSet<Integer>();
		Random random = new Random();
		int data = 0 ;
		while(set.size()<amount){
			data = random.nextInt(range);	//不包括range 也就是说最大 是range-1 
			set.add(data);
		}
		return set;
	}

}