
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@taglib prefix="s" uri="/struts-tags" %>
   

<form id="pagerForm" method="post" action="/aqjy/safetyeducation/ContentAction_findAll2">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${page.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" /> 
    <input type="hidden" name="orderDirection"  value="${param.orderDirection}" />  
    <input type="hidden" name="property"  value="${property}">
    <input type="hidden" name="key"  value="${key}">
</form>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="/aqjy/safetyeducation/ContentAction_findAll2" method="post">
	<div class="searchBar">
		<!--<ul class="searchContent">
			<li>
				<label>我的客户：</label>
				<input type="text"/>
			</li>
			<li>
			<select class="combox" name="province">
				<option value="">所有省市</option>
				<option value="北京">北京</option>
				<option value="上海">上海</option>
				<option value="天津">天津</option>
				<option value="重庆">重庆</option>
				<option value="广东">广东</option>
			</select>
			</li>
		</ul>
		-->
		<table class="searchContent">
			<tr>
				<td>
					内容属性：
				</td>
				<td>
              
					<select class="combox" name="property" >
							<option value="" >根据名称查询</option>
							<option value="edutype">所属专题类别</option>
							<option value="title">标题</option>			
					</select>
				</td>
				<td>
					要查询的值：
						<input type="text" value="" name="key">			
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>	
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索1</button></div></div></li>
				
			</ul>
			<div style="color:red;margin-top:5px;">注意:点击标题可以进行排序；检索模式下若想查询所有信息，再点击一次检索按钮即可；数据较多时可改变每页多少条以显示页码</div>
		</div>
	</div>
	</form>
</div>

<div class="pageContent">
	<%-- <div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="./contentAdd.jsp" target="navTab"><span>添加</span></a></li>
			<li><a class="delete" href="/aqjy/safetyeducation/ContentAction_delete?uid={sid_user}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="/aqjy/safetyeducation/ContentAction_find?uid={sid_user}" target="navTab"><span>修改</span></a></li>
			<li class="line">line</li>
			<li><a class="icon" href="/aqjy/safetyeducation/ContentAction_export" target="dwzExport" targetType="navTab" title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li> 
		</ul>
	</div> --%>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th>标题</th>
				<th>内容</th>
				<th>类别</th>
				<th orderField="publishtime" <c:if test='${orderField == "publishtime" }'> class="${orderDirection}"  </c:if> >发布时间</th>
				<th>发布人</th>
				<th>浏览次数</th>
				<th>备注</th>
				<th>所属ID</th>
				
				
				
			</tr>
		</thead>
		<tbody>
			<c:forEach  var="sp"  items="${list }">
				<tr target="sid_user" rel="${sp.sid }">
					<td><a href="/aqjy/safetyeducation/ContentAction_findDetail?uid=${sp.sid}" target="navTab" rel="${sp.sid }">${sp.title }</a></td>
					<td>${sp.content }</td>
					<td>${sp.edutype }</td>
					<td>${sp.publishtime}</td>
					<td>${sp.publisher }</td>
					<td>${sp.viewnumbers}</td>
					<td>${sp.memo }</td>
					<td>${sp.seid }</td>
					
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10" >10</option>
					<option value="50" selected="selected">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '100' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" selected="selected">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '500' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" >100</option>
					<option value="500" selected="selected">500</option>
				</c:if>
			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>

	</div>
</div>
