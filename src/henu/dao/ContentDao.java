package henu.dao;

import henu.bean.*;

import java.util.List;


/**
 * 对seminar表的Dao操作
 * @author 于晨
 *
 */



public interface ContentDao{
	/**
	 * 添加专题内容
	 * @param s
	 * @return
	 */
	public int save(Content spe);
		/**
		 * 删除新闻内容，根据sId
		 * @param sId
		 * @return
		 */
	public int delete(String sId);
	/**
	 * 更新内容，根据sId
	 * @param sId
	 * @param s
	 * @return
	 */
	
	public int update(String sId,Content spe);
	
	/**
	 * 查找所有内容，按order字段排序，排序方式为sort，即ASC和DESC
	 * @param order
	 * @param sort
	 * @param start
	 * @param end
	 * @return
	 */
	public List<Content> findAll(String order, String sort);
		/**
		 * 模糊查找
		 * 
		 * @param property 字段名
		 * @param key 字段值
		 * @param order 排序字段
		 * @param sort 排序方式
		 * @param start
		 * @param end
		 * @return
		 */
	public List<Content> findByProperty(String property,String key,String order,String sort,int start,int end);

	public Content findById(String id);
		
	public List<Content> findByDetial(String sId);
	public void updateViewNumberAddOne(String uid);
	public List<Content> findByProperty(String property, String key,
			String string, String uid, String order,
			String sort, int start, int end);
}
		
