package henu.dao;

import henu.bean.UserVehicle;

import java.util.List;

public interface UserVehicleDao {

	/*	public static void main(String[] args) {
		UserVehicleImpl uv=new UserVehicleImpl();
		uv.Save(new UserVehicle());
	}*/
	public abstract int Save(UserVehicle UserVehicle);

	public abstract int delete(int uvid);

	public abstract int Update(UserVehicle UserVehicle);

	public abstract UserVehicle findById(int uvid);

	public abstract List<UserVehicle> findAll(String order, String sort);

	public abstract List<UserVehicle> findByProperty(String property,
			String key, String order, String sort, int start, int end);

}