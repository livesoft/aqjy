package henu.dao;

import henu.bean.LiPinfomation;
import henu.bean.License;

import java.util.List;


/**
 * 驾驶证信息
 * @author Chen
 *
 */
public interface LicenseDao{
	/**
	 * 添加专题内容
	 * @param s
	 * @return
	 */
	public int save(License li);
		/**
		 * 删除新闻内容，根据sId
		 * @param sId
		 * @return
		 */
	public int delete(String leid);
	/**
	 * 更新内容，根据sId
	 * @param sId
	 * @param s
	 * @return
	 */
	
	public int update(String leid,License li);
	
	/**
	 * 查找所有内容，按order字段排序，排序方式为sort，即ASC和DESC
	 * @param order
	 * @param sort
	 * @param start
	 * @param end
	 * @return
	 */
	public List<License> findAll(String order, String sort);
		/**
		 * 模糊查找
		 * 
		 * @param property 字段名
		 * @param key 字段值
		 * @param order 排序字段
		 * @param sort 排序方式
		 * @param start
		 * @param end
		 * @return
		 */
	public List<LiPinfomation> findByProperty(String property,String key,String order,String sort,int start,int end,int comid);

		
	public List<License> findByDetial(String leid);
	
	
	public License findById(String leid);
}