
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@taglib prefix="s" uri="/struts-tags" %>
   

<form id="pagerForm" method="post" action="/aqjy/safetyeducation/SeminarAction_findAll2">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${page.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" /> 
    <input type="hidden" name="orderDirection"  value="${param.orderDirection}" />  
    <input type="hidden" name="property"  value="${property}">
    <input type="hidden" name="key"  value="${key}">
</form>
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="/aqjy/safetyeducation/SeminarAction_findAll2" method="post">
	<div class="searchBar">
	
		<table class="searchContent">
			<tr>
				<td>
					专题属性：
				</td>
				<td>
 
					<select class="combox" name="property" >
						<!-- 	<option value="seid">专题id</option> -->
							<option value="subjectname">专题名称</option>
							<!-- <option value="subjectname">发布人</option> -->
					</select>
				</td>
				<td>
					要查询的值：
						<input type="text" value="" name="key">			
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>	
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
				
			</ul>
			<div style="color:red;margin-top:5px;">注意:点击标题可以进行排序；检索模式下若想查询所有信息，再点击一次检索按钮即可；数据较多时可改变每页多少条以显示页码</div>
		</div>
	</div>
	</form>
</div>

<div class="pageContent">
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<!-- <th>专题ID</th> -->
				<!-- <th>专题类别</th> -->
				<th width="400">专题名称（点击专题查看所属专题列表）</th>
				<th orderField="subjecttime" <c:if test='${orderField == "subjecttime" }'> class="${orderDirection}"  </c:if> >创建时间</th>
				<th width="400">备注</th>
		</thead>
		<tbody>
			<c:forEach  var="s"  items="${list }">
				<tr target="sid_user" rel="${s.seid }">
				<%-- 	<td>${s.seid }</td> --%>
					<%-- <td>${s.sort }</td> --%>
					<td><a href="/aqjy/safetyeducation/SeminarAction_findDetail2?property=seid&key=${s.seid }" target="navTab" style="text-decoration:none;">${s.subjectname }</a></td>
					<td>${s.subjecttime }</td>
					<td>${s.remark }</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10" >10</option>
					<option value="50" selected="selected">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '100' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" selected="selected">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '500' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" >100</option>
					<option value="500" selected="selected">500</option>
				</c:if>
			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>

	</div>
</div>
