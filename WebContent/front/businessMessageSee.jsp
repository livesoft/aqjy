
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<body style="position: relative;">

	<div class="pageContent" >
	<form method="post" action="/aqjy/usertotalview/businessMessageAction_update" enctype="multipart/form-data" class="pageForm required-validate" onsubmit="return iframeCallback(this,navTabAjaxDone);">
		<div class="pageFormContent nowrap" layoutH="97">
		<input type="hidden" name="bm.bid" value="${ bm.bid}">
			<dl>
				<dt>营运证号：</dt>
				<dd>
					<input type="text" name="bm.businessId"  value="${ bm.businessId}" maxlength="50" class="required" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>车牌号：</dt>
				<dd>
					<input type="text" name="bm.vehicleLicense"  value="${ bm.vehicleLicense}" maxlength="20" class="required" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>车辆类型：</dt>
				<dd>
					<input type="text" name="bm.carType"  value="${ bm.carType}" class="required" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>经营范围：</dt>
				<dd>
					<input type="text" name="bm.businessRage"  value="${bm.businessRage}" class="required" readonly="readonly"/>
					
				</dd>
			</dl>
			<dl>
				<dt>发证机关：</dt>
				<dd>
					<input type="text" name="bm.publishOffice"  value="${bm.publishOffice }"  maxlength="60" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>发证日期：</dt>
				<dd>
					<input type="text" value="${ bm.publishDate}" class="required" readonly="readonly"/>
				</dd>
			</dl>
			<dl>
				<dt>有效期开始日期：</dt>
				<dd>
					<input type="text" value="${bm.validityBegin }" class="required" readonly="readonly" dateFmt="yyyy-MM-dd" />
				</dd>
			</dl>
			<dl>
				<dt>有效期结束日期：</dt>
				<dd>
					<input type="text"  value="${ bm.validityEnd}" class="required" readonly="readonly" dateFmt="yyyy-MM-dd" />
				</dd>
				
			</dl>	
			<dl>
				<dt>营运证复印件(照片)：</dt>
				<dd>
					<img src="../${bm.businessFile }" style="width:400px;height: 200px;">	
				</dd>
			</dl>
			<dl>
				<dt>经营许可证(照片)：</dt>
				<dd>
					<img src="../${bm.businessPermission }" style="width:400px;height: 200px;">	
				</dd>
			</dl>
			<dl>
				<dt>业主名：</dt>
				<dd>
						<input type="text" name="bm.masterName" readonly="readonly" value="${bm.masterName }"  maxlength="25"/>
				</dd>
			</dl>
				<dl>
				<dt>地址：</dt>
				<dd>
					<input type="text" name="bm.address" readonly="readonly"  value="${bm.address }"  maxlength="50"/>
				</dd>
			</dl>
			<dl>
				<dt>吨位、座位：</dt>
				<dd>
					<input type="text" name="bm.seats" readonly="readonly" value="${bm.seats}"   maxlength="25"/>
				</dd>
			</dl>
			
			<dl>
				<dt>车辆尺寸：</dt>
				<dd>
					<input type="text" name="bm.carSize" readonly="readonly" value="${bm.carSize}"  maxlength="25"/>	
				</dd>
			</dl>
			
			<dl>
				<dt>备注：</dt>
				<dd>
						<textarea rows="3" cols="30" name="bm.memo " readonly="readonly">${bm.memo }</textarea>
				</dd>
			</dl>
		<!-- <div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div> -->
		</div>
	</form>
	
</div>

</body>