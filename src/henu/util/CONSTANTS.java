package henu.util;
/**
 * 此类用于存放系统用到的各种常量值
 * <font color="red">开发人员可追加，注意加上注释</font>
 * 如放到session中的对象名称
 * @author 梁胜斌
 * @version 1.0
 * 
 * 2014-11-02
 *
 */
public class CONSTANTS {

	
	//================================================================================================================
    //==                                         session名称                                                                                                                                                                                                        ==
	//================================================================================================================
	//校验码
	public static final String SESSION_VALIDATE_CODE = "SESSION_VALIDATE_CODE";
	
	

	//================================================================================================================
    //==                                         常用页面                                                                                                                                                                                                                        ==
	//================================================================================================================
	//错误处理页面
	public static final String PAGE_ERROR = "inc/error.jsp";
	

	//================================================================================================================
    //==                                         数据库信息                                                                                                                                                                                                                   ==
	//================================================================================================================
	public static final String DB = "Oracle";
	public static final String DB_USER = "aqjy";
	public static final String DB_PASS = "123";
	public static final String DB_DRIVER = "oracle.jdbc.OracleDriver";
	public static final String DB_URL = "jdbc:oracle:thin:@125.219.44.33:1521:orcl";
	public static final String DB_REMOVEABANDONEDTIMEOUT = "120";
	public static final String DB_MAXACTIVE = "100";
	public static final String DB_MAXIDLE= "50";
	public static final String DB_MAXWAIT= "1000";
	
	//================================================================================================================
    //==                                      用户session名称                                                                                                                                                                    ==
	//================================================================================================================
	public static String SESSION_PERSON = "PERSON";
	public static String SESSION_ADMIN = "ADMIN";
	public static String SESSION_STATUS = "STATUS";
	
	//================================================================================================================
    //==                                     权限相关                                                                                                                                                                ==
	//================================================================================================================
	public static final String PER_XINXI = "PER_XINXI";
	public static final String PER_YONGHU = "PER_YONGHU";
	public static final String PER_HUIYI = "PER_HUIYI";
	public static final String PER_TONGZHI = "PER_TONGZHI";
	public static final String PER_JIAOYU = "PER_JIAOYU";
	public static final String PER_KAOHE = "PER_KAOHE";
	public static final String PER_HUDONG = "PER_HUDONG";
	public static final String PER_XITONG = "PER_XITONG";
	
	public static final String ALLOWLIST = "ALLOWLIST"; //用户允许访问namespace 列表
	
}
