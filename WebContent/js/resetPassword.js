$(function(){
	var idCard=/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{4}$/;
	$("#idCard").change (function(){
		
		if(!idCard.test($(this).val())){
			$("#error1").text("请输入有效的身份证");
		}else{
			$("#error1").text("*");
		}
	})
	$("#answer").change (function(){
		if($.trim($(this).val())==""){
			$("#error2").text("请输入密保答案");
		}else{
			$("#error2").text("*");
		}
	})
	$("#password").change (function(){

		if($.trim($(this).val()).length<=5||$.trim($(this).val()).length>=18){
			$("#error3").text("密码长度为6~18位");
		}else{
			$("#error3").text("*");
		}
	})
	$("#confirmPassword").change (function(){

		if($(this).val()!=$("#password").val()){
			$("#error4").text("和上次密码不一致");
		}else{
			$("#error4").text("*");
		}
	})
	$("#sub").click(function(){
		
		if(!idCard.test($("#idCard").val())){
			$("#error1").text("请输入有效的身份证");
		}else if($.trim($("#answer").val())==""){
			$("#error2").text("请输入密保答案");
		}else if($.trim($("#password").val()).length<=5||$.trim($("#password").val()).length>=18){
			$("#error3").text("密码长度为6~18位");
		}else if($("#confirmPassword").val()!=$("#password").val()){
			$("#error4").text("和上次密码不一致");
		}else{
			$("form").submit();
		}
	})
})