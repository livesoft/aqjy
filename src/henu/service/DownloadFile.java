package henu.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;


import com.opensymphony.xwork2.ActionSupport;

/**
 * 下模板类，通过本类为用户提供下载导入数据库的模板
 * 通用下载action 
 * @author wuhaifeng
 * 2014-01-17
 */
public class DownloadFile extends ActionSupport{

	
	
	private static final long serialVersionUID = 1L;
	private String resName;
	private String res;
	private String resType;

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getRes() throws UnsupportedEncodingException {
		return res;
	}

	public void setRes(String res) throws UnsupportedEncodingException {
		//乱码是出现在文件名从前台传到后台也就是前台给action赋值的过程中  ，所以要把前台iso-8859-1 转换成utf-8 编码 
		this.res = new String(res.getBytes("iso-8859-1"),"utf-8");
	}

	public String getResType() {
		return resType;
	}

	public void setResType(String resType) {
		this.resType = resType;
	}
	public InputStream getTarget() throws IOException{
		//此处使用输入流打开一个文件  然后使用此方法把这个输入流返回出去 
		return  new FileInputStream(res);
		
	}
}
