package henu.dao;

import java.util.List;

import henu.bean.Questions;

/**
 * @author wuhaifeng
 * */

public interface QuestionsDao   {
	
	/**
	 * 保存Questions对象
	 * @param ques
	 * @return
	 */
	public int save(Questions ques);
	
	/**
	 * 根据id进行删除
	 * @param id
	 * @return
	 */
	public int delete(String id);
	/**
	 * 查找指定ID的记录
	 * @param id
	 * @return
	 */
	public Questions findById(String id);
	
	/**
	 * 查找所有的记录
	 * @param order 排序字段
	 * @param sort 排序方式
	 * @return
	 */
	
	public List<Questions> findAll(String order, String sort,String property,String key);
	/**
	 * 根据指定字段，进行模糊查询
	 * @param property 字段名称
	 * @param key
	 * @param order 排序字段
	 * @param sort 排序方式
	 * @return
	 */
	public List<Questions> findByProperty(String property, String key, String order, String sort,int start ,int end);
	
	/**
	 * 根据指定题目编号的集合/数组获取某个考生的试题
	 * select * from tb_Questions where tid in (13,44,23) 
	 * @param id
	 * @return
	 */
	public List<Questions> getQuestions(String[] id);
	
	/**
	 * 根据编号进行修改
	 * @param id
	 * @param ques
	 * @return
	 */
	public int update (String id , Questions ques);
	
	
	/**
	 * 查找所有的科目
	 * @return 所有的科目名称
	 * */
	public List<String> findAllSub();

	/**
	 * 根据属性查找试题
	 * @param property1  属性1
	 * @param key1  关键字1
	 * @param property2  属性2
	 * @param key2  关键字2
	 * @return 符合查询要求的试题
	 * */
	public List<Questions> status(String property1, String key1,String property2, String key2);
	
}
