
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
  <%@taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>  
  <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<form id="pagerForm" method="post" action="/aqjy/userManage/com_findAll">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${page.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" /> 
    <input type="hidden" name="orderDirection"  value="${param.orderDirection}" />  
    <input type="hidden" name="property"  value="${property}">
    <input type="hidden" name="key"  value="${key}">
</form>


<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="/aqjy/userManage/com_findAll" method="post">
	<div class="searchBar">
		<!--<ul class="searchContent">
			<li>
				<label>我的客户：</label>
				<input type="text"/>
			</li>
			<li>
			<select class="combox" name="province">
				<option value="">所有省市</option>
				<option value="北京">北京</option>
				<option value="上海">上海</option>
				<option value="天津">天津</option>
				<option value="重庆">重庆</option>
				<option value="广东">广东</option>
			</select>
			</li>
		</ul>
		-->
		<table class="searchContent">
			<tr>
				<td>
					新闻属性：
				</td>
				<td>
              
					<select class="combox" name="property">
							<option value="comid" >根据编号</option>
							<option value="belongid">根据公司编号</option>
					</select>
				</td>
				<td>
					要查询的值：
						<input type="text" value="" name="key">			
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>	
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
				
			</ul>
			<div style="color:red;margin-top:5px;">注意:点击标题可以进行排序，检索模式下若想查询所有信息，再点击一次检索按钮即可</div>
		</div>
	</div>
	</form>
	

</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="CompanyAdd.jsp" target="navTab"><span>添加</span></a></li>
			<li><a class="delete" href="/aqjy/userManage/com_delete?uid={sid_user}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="/aqjy/userManage/com_find?uid={sid_user}" target="navTab"><span>修改</span></a></li>
			<li><a class="icon" href="/aqjy/userManage/com_export" target="dwzExport" targetType="navTab" title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th  orderField=holdoverMode <c:if test='${orderField == "holdoverMode" }'> class="${orderDirection}"  </c:if>   >公司编号</th>
				<th>所属集团编号</th>
				<th >公司名称</th>
				<th>公司类型</th>
				<th>注册地址</th>
				<th>法人代表</th>
				<th>法人代表身份证</th>
				<th>营业执照</th>
				<th >机构代码证</th>
				<th>联系电话</th>
				<th>是否有子公司</th>
			    <th>成立日期</th>
				<th>经营范围</th>
				<th >组织机构代码证</th>
				<th>联系地址</th>
			    <th>注册资金</th>
				<th>登记机关</th>
			    <th>营业期限</th>
				<th>特许经营许可证号码</th>
				<th>邮编</th>
				<th>基本账户开户行</th>
				<th>基本账户号</th>
				<th>公司负责人</th>
				<th>备注2</th>
				<th>备注3</th>
				<th>备注4</th>
			</tr>
		</thead>
		<tbody>
			
			<s:forEach var="c" items="${list }">
				<tr target="sid_user" rel="${c.comid }">
				    <td>${c.comid}</td> 
					<td>${c.belongid}</td>
					<td>${c.companyname}</td>
					<td>${c.companytype}</td>
					<td>${c.address}</td>
					<td>${c.legalperson}</td>
					<td>${c.memo1}</td>
					<td>${c.businesslicense}</td>
					<td>${c.organizationcode}</td>
					<td>${c.contactphone}</td>
					<td>${c.isexistcompany}</td>
				    <td>${c.createtime}</td>
					<td>${c.businessrange}</td>
					<td>${c.organizecard}</td>
					<td>${c.contactaddress}</td>
				    <td>${c.registmoney}</td>
					<td>${c.registeroffice}</td> 
					<td>${c.businesslimit}</td>
					<td>${c.allowbusinesscard}</td>
					<td>${c.postcode}</td>
					<td>${c.basicaccoutopen}</td>
					<td>${c.basicaccoutid}</td>	
					<td>${c.companyprincipal}</td>
					<td>${c.memo2}</td>
					<td>${c.memo3}</td>
					<td>${c.memo4}</td> 
				</tr>
			</s:forEach>
			
		</tbody>
	</table>
	<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10" >10</option>
					<option value="50" selected="selected">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '100' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" selected="selected">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '500' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" >100</option>
					<option value="500" selected="selected">500</option>
				</c:if>

			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>
	</div>
</div>
