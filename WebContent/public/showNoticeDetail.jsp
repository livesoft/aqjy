<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新闻详情</title>

<style type="text/css">
	#main{
		width: 1000px;
		margin: auto;
		border-bottom:2px solid #67bded;
	}
	#newsDTop{
		width:1000px;
		height:29px;
		background-color:#ebfff9;
		font-weight: bold;
		font-size: 14px;
		line-height: 29px;
		padding-left:5px;
		font-family:Microsoft Yahei;
		color: #000080;
		margin-bottom: 10px;
	}
	#newsDTop a{
		font-family:Microsoft Yahei;
		color: #000080;
		font-size: 14px;
		text-decoration: none;
	}
	#newsDTop a:HOVER{
	color: red;
	}
	#content{
		width: 900px;
		margin:auto;
		font-family:Microsoft Yahei;
	}
	
	#title{
		color:#1558E8;
		text-align: center;
		padding-top: 30px;
		font-weight: bold;
		font-size: 20px;
		margin-bottom: 10px;
	}
	#time{
		color:#666;
		text-align: right;
		font-size: 14px;
	}
	#bottom{
		overflow:auto;
		width: 900px;
		height:700px;
		margin: auto;
		border-top: 1px solid #cccccc;
		margin-top: 5px;
		font-family:Microsoft Yahei;
	}
	#bottom p{width: 850px;margin-left: auto;margin-right: auto;margin-top: 15px;}

</style>
</head>
<body>
<div id="main">
<jsp:include page="head.jsp"></jsp:include>
<div id="newsDTop">所在位置：<a href="/aqjy/index.jsp">首页</a>><a href="/aqjy/public/findNoticeListByProperty?key=新闻公告" >新闻公告</a>>>详情</div>
	<div id="content">
		<div id="top">
			<p id="title">${n.title }</p>
			<p id="time">发布时间:${n.publishTime }&nbsp;&nbsp;浏览次数:${n.viewNumbers }</p>
		</div>
		<div id="bottom">
			${n.content}
		</div>
	</div>
	<jsp:include page="foot.jsp"></jsp:include>
</div>
</body>
</html>