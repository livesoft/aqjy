package henu.action;
import henu.bean.Content;
import henu.bean.LearnLogPinformation;
import henu.bean.PInformation;
import henu.bean.Seminar;
import henu.dao.ContentDao;
import henu.dao.LearnLogPinformationDao;
import henu.dao.factory.DaoFactory;
import henu.service.ExportExcel;
import henu.util.AJAX;
import henu.util.CONSTANTS;
import henu.util.Page;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;



public class ContentAction extends ActionSupport{
	//分页查询
	Page page;
	private int pageNum=1;
	private int numPerPage=10;
	private String orderField="publishtime";
	private String orderDirection="asc";
	
	//检索
	private String property="-1";
	private String key="-1";
	
	
	private AJAX ajax;
	private String uid;
	private String url="/admin/ajax.jsp";
	private Content spe;
	private List<Content> list;
	
	
	private String res;
	private String resName;
	private String resType;
	
	
	public String getRes() {
		return res;
	}
	public void setRes(String res) {
		this.res = res;
	}
	public String getResName() {
		return resName;
	}
	public void setResName(String resName) {
		this.resName = resName;
	}
	public String getResType() {
		return resType;
	}
	public void setResType(String resType) {
		this.resType = resType;
	}
	public AJAX getAjax() {
		return ajax;
	}
	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	
	public Content getSpe() {
		return spe;
	}
	public void setSpe(Content spe) {
		this.spe = spe;
	}
	public List<Content> getList() {
		return list;
	}
	public void setList(List<Content> list) {
		this.list = list;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getNumPerPage() {
		return numPerPage;
	}
	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderDirection() {
		return orderDirection;
	}
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	//列出所有专题内容
	public String findAll()
	{
	
		page = new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="publishtime";
			orderDirection="asc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("safetyeducation", pageNum, numPerPage, property, key);
		list=DaoFactory.getContentDaoInstance().findByProperty(property, key ,orderField, orderDirection, page.getStart(), page.getEnd());
		/*JSONArray js = JSONArray.fromObject(list);
		System.out.println(js);*/
		
		url="/admin/contentManage.jsp";
		return SUCCESS;
		//修改成uid，seid
	}
	//列出所属专题下的所有内容,第一次获得key空，赋值成uid。用这个。c
	public String findAll2(){
		//拿到uid
		String uid = (String) ServletActionContext.getRequest().getSession().getAttribute("sessionUid");
		page = new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField = "Publishtime";
			orderDirection="asc";
		}
		if(property.equals("")||key.equals("")){
			property="seid";
			key=uid;
		}
		page.setProperties1("safetyeducation", pageNum, numPerPage, property, key,"seid",uid);
		list = DaoFactory.getContentDaoInstance().findByProperty(property, key,"seid",uid, orderField, orderDirection, page.getStart(), page.getEnd());
		url="/admin/seminarDetail.jsp";
		return SUCCESS;
	}
	//前台检索
	public String findAll3(){
		//拿到uid
		String uid = (String) ServletActionContext.getRequest().getSession().getAttribute("sessionUid");
		page = new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField = "Publishtime";
			orderDirection="asc";
		}
		if(property.equals("")||key.equals("")){
			property="seid";
			key=uid;
		}
		page.setProperties1("safetyeducation", pageNum, numPerPage, property, key,"seid",uid);
		list = DaoFactory.getContentDaoInstance().findByProperty(property, key,"seid",uid, orderField, orderDirection, page.getStart(), page.getEnd());
		url="/front/contentManage.jsp";
		return SUCCESS;
	}
	
	
	
	//列出日常教育管理的列表
	public String findDayEdu(){
		page = new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField = "Publishtime";
			orderDirection="asc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("safetyeducation", pageNum, numPerPage, property, key);
		list = DaoFactory.getContentDaoInstance().findByProperty(property, key, orderField, orderDirection, page.getStart(), page.getEnd());
		url="/front/dayeduManager.jsp";
		return SUCCESS;
	}
	//添加内容，利用值栈的存储获取列表值
	public String add()
	{
		ajax = new AJAX();
		Map<Integer,String> map =(Map<Integer, String>) ServletActionContext.getRequest().getSession().getAttribute("listmap");
		for (Integer key : map.keySet()) {
			   if(key.equals(spe.getSeid()))
			   {
				   spe.setMemo(map.get(key));
			   }
		}
	/*	System.out.println(spe);
		System.out.println(spe.getContent());*/
		
		if(spe.getContent()==null||spe.getContent().trim().equals("")){
			ajax.setStatusCode("300");
			ajax.setMessage("内容不能为空，请重新填写");
		}else{
			spe.setPublishtime(new Date().toLocaleString());
			spe.setViewnumbers(0);
			int result = DaoFactory.getContentDaoInstance().save(spe);
			System.out.println(result);
			if(result>0)
			{
				HandleMsg("200","添加成功!","con","closeCurrent");
			}else{
				HandleMsg("300","添加失败!","con","closeCurrent");
			}
		}
		return SUCCESS;
	}
	//删除内容
	public String delete(){
		int result = DaoFactory.getContentDaoInstance().delete(uid);
		if(result>0)
		{
			HandleMsg("200","删除成功","con","");
		}else{
			HandleMsg("300","删除失败","con","");
		}
		return SUCCESS;
	}
	//更新内容
	public String update(){
		ContentDao dao = DaoFactory.getContentDaoInstance();
		spe.setPublishtime(new Date().toLocaleString());
		int result = dao.update(uid, spe);
		if(result>0)
		{
			HandleMsg("200","更新成功","con","closeCurrent");
		}else{
			HandleMsg("300","更新失败","con","closeCurrent");
		}
		return SUCCESS;
	}
	//查询更改
	public String find(){
		
		ContentDao dao=DaoFactory.getContentDaoInstance();
	/*	spe=dao.findBysid(uid);*/
		spe=dao.findById(uid);
		url="/admin/contentUpdate.jsp";
		return SUCCESS;
	}
	//后台查看详细信息
	public String findDetail(){
		
		
		ContentDao dao 	= DaoFactory.getContentDaoInstance();
		spe=dao.findById(uid);
		dao.updateViewNumberAddOne(uid);
	
		url="/admin/contentDetail.jsp";
		return SUCCESS;
	}
	
	//前台查看详细内容
	public String findDetail2(){
		HttpSession session = ServletActionContext.getRequest().getSession();
		LearnLogPinformation view = new LearnLogPinformation();
		ContentDao dao  = DaoFactory.getContentDaoInstance();
		
		/*Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String curr_date = format.format(date);
		System.out.println("当前时间："+curr_date);*/
		
		spe = dao.findById(uid);
		/*dao.updateViewNumberAddOne(uid);
		Content context = DaoFactory.getContentDaoInstance().findById(uid);
		PInformation information = (PInformation) session.getAttribute(CONSTANTS.SESSION_PERSON); 
		String companyname = DaoFactory.getCompanyDaoInstance().findBy(String.valueOf(information.getComid())).getCompanyname();
		view.setIdcard(information.getIdCard());//获得用户的身份证号
		System.out.println(view.getIdcard()); 
		view.setRealname(information.getRealName()); //获得用户的真实名字
		System.out.println(view.getRealname()); 
		view.setCompanyname(companyname);//获得用户的公司名字
		System.out.println(view.getCompanyname()); 
		view.setTitle(context.getTitle()); //获得用户学习的标题名
		System.out.println(view.getTitle()); 
		view.setStarttime(curr_date); //获得用户的开始、点击时间
		LearnLogPinformationDao da  = DaoFactory.getLearnLogPinformationDao();
		da.save(view);  // 存到试图里面
		*/
		url="/front/contentDetail.jsp";
		return SUCCESS;
	}
	
	
	//导出学习记录
	public String export(){
		String basepath = ServletActionContext.getServletContext().getRealPath("/");
		list = DaoFactory.getContentDaoInstance().findAll("sid","asc");
		res = ExportExcel.ContentEx(list,basepath);
		resName="ContentMod.xls" ;
		resType="application/vnd.ms-excel" ;
		return "export";
		
	}
	private void HandleMsg(String statusCode, String msg, String tabid, String callback) {
		ajax =  new AJAX();
		ajax.setCallbackType(callback);
		ajax.setMessage(msg);
		ajax.setStatusCode(statusCode);
		ajax.setNavTabId("con");
	}
	
	
}
