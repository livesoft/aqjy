package henu.action;

import henu.bean.CarMaintancePinfoCompany;
import henu.bean.Maintance;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.Page;

import java.util.List;

public class MaintanceAction extends BaseAction {
	//CarMaintancePinfoCompany
	//分页排序
	Page page;
	private int pageNum=1;
	private int numPerPage=10;
	private String orderField="maintanceTime";
	private String orderDirection="desc";
	
	//检索
	private String property="-1";
	private String key="-1";
	
	
	private AJAX ajax;
	private Maintance maintance;
	private String url="/admin/ajax.jsp";
	private String uid;
	private List<CarMaintancePinfoCompany> list;
	private CarMaintancePinfoCompany cmpc;
	
	//文件下载	
	private String res;
	private String resName;
	private String resType;
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getNumPerPage() {
		return numPerPage;
	}
	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderDirection() {
		return orderDirection;
	}
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public AJAX getAjax() {
		return ajax;
	}
	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}
	public Maintance getMaintance() {
		return maintance;
	}
	public void setMaintance(Maintance maintance) {
		this.maintance = maintance;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String mid) {
		this.uid = mid;
	}
	public CarMaintancePinfoCompany getCmpc() {
		return cmpc;
	}
	public void setCmpc(CarMaintancePinfoCompany cmpc) {
		this.cmpc = cmpc;
	}
	public List<CarMaintancePinfoCompany> getList() {
		return list;
	}
	public void setList(List<CarMaintancePinfoCompany> list) {
		this.list = list;
	}
	public String getRes() {
		return res;
	}
	public void setRes(String res) {
		this.res = res;
	}
	public String getResName() {
		return resName;
	}
	public void setResName(String resName) {
		this.resName = resName;
	}
	public String getResType() {
		return resType;
	}
	public void setResType(String resType) {
		this.resType = resType;
	}
	
	public String findAll()
	{
		
		page = new Page();
		if(this.orderField .equals("") || this.orderDirection.equals(""))
		{
			orderField = "MAINTANCETIME";
			orderDirection = "desc";
		}
		
		if(this.property.equals("") || this.key.equals(""))
		{
			this.property = "-1";
			this.key = "-1";
		}
		
		page.setProperties("VIEW_CAR_MAIN_PINFO_COMPANY", pageNum, numPerPage, property, key);
		
		list = DaoFactory.getMaintanceDaoInstance().findByProperty(property, key,orderField, orderDirection,  page.getStart() ,page.getEnd());
		//System.out.println("list size:" + list.size());
		url = "/admin/maintance.jsp";
		return SUCCESS;
	}
	
	public String delete() throws Exception
	{
		//System.out.println("uid=" + uid);
		int result = DaoFactory.getMaintanceDaoInstance().delete(uid);
		ajax=new AJAX();
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("删除成功");
			ajax.setNavTabId("maintance_info");
			ajax.setCallbackType("");
			
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("删除失败");
			ajax.setCallbackType("");
		}
		url="/admin/ajax.jsp";
		return SUCCESS; 
		
	}
	
	public String find() throws Exception
	{
		this.cmpc = DaoFactory.getMaintanceDaoInstance().findById(uid);
		url="/admin/maintanceUpdate.jsp";
		return SUCCESS;
		
	}
	
	public String add() throws Exception
	{
		
		url="/admin/ajax.jsp";
		return SUCCESS;
	}
	
	public String update() throws Exception
	{
		ajax=new AJAX();
		int result = DaoFactory.getMaintanceDaoInstance().update(uid, maintance);
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("更新成功！");
			ajax.setNavTabId("maintance_info");
			ajax.setCallbackType("closeCurrent");
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("更新失败！");
			ajax.setCallbackType("");
		}
		
		url="/admin/ajax.jsp";
		return SUCCESS;
	
	}
}
