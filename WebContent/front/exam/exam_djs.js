	var nowts;
	function timer(timelong) {
		nowts = (new Date(0, 0, 0, 0, timelong, 0)) - (new Date(0, 0, 0, 0, 0, 0)); //计算剩余的毫秒数
		showTime(nowts);
		setTimeout("nowTimeOver()", 1000);
	}
	function nowTimeOver() {
		if (nowts >= 1) {
			nowts = nowts - 1000;
			showTime(nowts);
			setTimeout("nowTimeOver()", 1000);
		} else {
			document.getElementById("timer").innerHTML = '考试结束。';
			//调用考试结束方法
			gameover();
			alert("考试结束。")
		}
	}
	function showTime(ts) {
		document.getElementById("timer").innerHTML = checkTime(parseInt(
				ts / 1000 / 60 % 60, 10))
				+ "分" + checkTime(parseInt(ts / 1000 % 60, 10)) + "秒";
	}
	function checkTime(i) {
		if (i < 10) {
			i = "0" + i;
		}
		return i;
	}