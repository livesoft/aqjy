package henu.dao.impl;

import henu.bean.Exam;
import henu.dao.ExamDao;
import henu.dao.factory.DaoFactory;
import henu.util.Dbcp;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class ExamDaoImpl implements ExamDao {

	public static void main(String[] args) {
		ExamDaoImpl dap = new ExamDaoImpl();
		//dap.findAllSub();
	
	}

	/**
	 * @param sql
	 *            查询语句
	 * @param param
	 *            查询的参数
	 * @param result
	 *            返回操作成功的个数
	 * */
	@Override
	public int save(Exam exam) {
		// Exam exam = new Exam();
		QueryRunner runner = new QueryRunner(Dbcp.getDataSource());
		int result = 0;
		String sql = "INSERT INTO Exam(examname,content,duration,judgecount,selectcount,judgescore,selectscore,credit,examtime,status,examtype,subject) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		Object[] params = {exam.getExamname(),exam.getContent(),exam.getDuration(),exam.getJudgecount(),exam.getSelectcount(),exam.getJudgescore(),exam.getSelectscore(),exam.getCredit(),exam.getExamtime(),exam.getStatus(),exam.getExamtype(),exam.getSubject()};
		try {
			result = runner.update(sql, params);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return result;
	}
	@Override
	public int delete(String id) {
		// DELETE FROM Exam WHERE qid=1 ;
		String sql = "DELETE FROM Exam WHERE eid=?";
		int result = 0;
		QueryRunner runner = new QueryRunner(Dbcp.getDataSource());
		try {
			result = runner.update(sql, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Exam> findAll(String order, String sort, String property,
			String key) {
		List<Exam> list = null;
		QueryRunner runner = DaoFactory.getRunner();
		String sql = "SELECT * FROM Exam where " + property + " = ? "
				+ " ORDER BY  " + order + " " + sort;
		// System.out.println("final-sql:"+sql);
		try {
			list = runner.query(sql, new BeanListHandler<Exam>(
					Exam.class), key);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * 
	 * @param property
	 *            要查询的字段名称
	 * @param key
	 * @param order
	 *            排序字段
	 * @param sort
	 *            排序方式
	 * @return
	 */
	@Override
	public List<Exam> findByProperty(String property, String key,
			String order, String sort, int start, int end) {
		List<Exam> list = null;
		String sql = "SELECT * FROM (SELECT tt.*, ROWNUM ro FROM (select * from Exam where "
				+ property
				+ "='"
				+ key
				+ "'  order by "
				+ order
				+ " "
				+ sort
				+ ") tt WHERE ROWNUM <=" + end + ") WHERE ro > " + start + "";
		QueryRunner runner = DaoFactory.getRunner();
		try {
			list = runner.query(sql, new BeanListHandler<Exam>(
					Exam.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;

	}

	/*
	 * 这个方法要求的是根据id 这个可以理解为随机获取一张卷子
	 */
	@Override
	public List<Exam> getExam(int[] id) {
		// findById();
		List<Exam> list = new ArrayList<Exam>();
		for (int i = 0; i < id.length; i++) {
			list.add(findById(id[i]));
		}
		return list;
	}

	@Override
	public int update(String id, Exam exam) {
		// UPDATE Exam SET content ='河大建校时间' ,answer ='1912'
		// ,SUBJECT='语文',TYPE='常识' WHERE qid=1 ;
		String sql = "UPDATE  Exam SET examname = ?,content =?,duration= ?,judgecount=? ,Selectcount=?,Judgescore=?,Selectscore=?,Credit=?,Examtime =? , Status =? , Examtype =? , Subject =?  WHERE eid= "
				+ id;
		// System.out.println("sql:"+sql);
		int result = 0;
		Object[] params = {exam.getExamname(),exam.getContent(),exam.getDuration(),exam.getJudgecount(),exam.getSelectcount(),exam.getJudgescore(),exam.getSelectscore(),exam.getCredit(),exam.getExamtime(),exam.getStatus(),exam.getExamtype(),exam.getSubject()};
		QueryRunner runner =DaoFactory.getRunner();
		try {
			result = runner.update(sql, params);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Exam findById(int eid) {
			// 此处返回是bean ,查出来的所有值赋值给bean然后把bean返回
			Exam exam = null;
			String sql = "select * from Exam where eid=?";
			QueryRunner runner =DaoFactory.getRunner();
			try {
				exam = runner.query(sql,new BeanHandler<Exam>(Exam.class), eid);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return exam;
	}
}
