package henu.dao;

import henu.bean.Permission;
import henu.bean.Role;
import henu.bean.RolePermission;

import java.util.List;


/**
 * @author 吴海峰 1月7号 权限管理
 * */
public interface PermissionDao {

	// 找到所有权限
	public List<Permission> findAllPermission();

	// 找到所有角色
	public List<Role> findAllRole();

	// 找到参照表里的所有数据
	public List<RolePermission> findAllRP();

	public List<RolePermission> findRPByProperty(String property, String key,
			String order, String sort) ;

	/**
	 * 根据权限的id找到这个权限
	 * 
	 * @param pid
	 *            权限的id
	 * */
	public Permission findPermisstionById(int pid);

	/**
	 * 根据指定的属性找到对应的权限记录
	 * 
	 * @param pid
	 *            权限的id
	 * */
	public List<Permission> findPermissionByProperty(String property,
			String key, String order, String sort);
	/**
	 * 插入角色的一条记录
	 * 
	 * @param role
	 *            一个新的角色
	 * */
	public int saveRole(Role role) ;

	/**
	 * 删除一个角色
	 * 
	 * @param id
	 *            要删除的角色的id
	 * */
	public int delRole(String id) ;

	/**
	 * @param pid
	 *            权限的id
	 * @param rid
	 *            角色的id
	 * @return 返回操作是否成功
	 * */
	public int addPermission(RolePermission rp) ;

	/**
	 * 添加整个模块的权限的方法
	 * 
	 * @param modelname
	 *            模块名称
	 * @param roleid
	 *            角色的名称
	 * */

	public int addModelPermission(String modelname, int roleid);
		

	public Role findRoleById(String roleid);


	public int delRPByRoleid(String roleid);
		

	public int updateRole(Role role);
	
}