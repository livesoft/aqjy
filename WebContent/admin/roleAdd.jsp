<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
  <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
	<link rel="stylesheet" href="../ueditor/themes/default/css/ueditor.css">
  	<script>
  	UE.getEditor('editor', {
  		initialFrameWidth:"100%" //初始化选项
  		,toolbars: [
  		          ['fullscreen', 'source', 'undo', 'redo', 'bold']
  		        ]
  		})
  		  	UE.getEditor('editor1', {
  		initialFrameWidth:"100%" //初始化选项
  		,toolbars: [
  		          ['fullscreen', 'source', 'undo', 'redo', 'bold']
  		        ]
  		})
  	</script>
<h2 class="contentTitle">权限设置</h2>
<div class="pageFormContent" layoutH="60">
<form method="post" action="/aqjy/role/addRole" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
	
	<!-- 
		private String roleName;//角色名称
	private String description;//角色描述
	private String status;//角色状态
	private String remarks;//角色备注
	 -->
	<fieldset>
		<legend>角色信息设置</legend>
		<dl class="nowrap">
			<dt>角色名称：</dt>
			<dd><input class="required" style="font-style: bold" name="role.rolename" type="text" /></dd>
		</dl>
		<dl class="nowrap">
			<dt>是否可用：</dt>
			<label><input type="checkbox" name="role.status" value="true" />勾选启用</label>
		</dl>
		<dl class="nowrap">
			<dt>角色描述：</dt>
			<dd>
				<script type="text/plain" id="editor" name="role.description"  style="width:800px;height: 200px"></script>
			</dd>	
		</dl>
		<dl class="nowrap">
			<dt>角色备注：</dt>
			<dd>
				<script type="text/plain" id="editor1" name="role.remarks"  style="width:800px;height: 200px"></script>
			</dd>
		</dl>
	</fieldset>
<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
