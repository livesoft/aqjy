<!-- 此页面用于发布会议通知 -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<h2 class="contentTitle">发布会议</h2>


<div class="pageContent">
	
	<form method="post" action="/aqjy/safetymetting/content_add" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent nowrap" layoutH="97">

			<!-- <dl>
				<dt>会议编号：</dt>
				<dd>
					<input type="text" name="bf.basicid" maxlength="20" class="required" />
					
				</dd>
			</dl> -->
			
			<dl>
				<dt>参加人员：</dt>
				<dd>
					<label><input type="checkbox" checked="checked" name="bf.joinPerson" value="车主司机" />车主司机</label>
					<label><input type="checkbox" name="bf.joinPerson" value="集团" />集团</label>
					<label><input type="checkbox" name="bf.joinPerson" value="公司" />公司</label>
					<label><input type="checkbox" name="bf.joinPerson" value="交通运输协会" />交通运输协会</label>
				</dd>
			</dl>
			
			<dl>
				<dt>会议开始时间：</dt>
				<dd>
					<input type="text" name="bf.startTime" class="date required" dateFmt="yyyy-MM-dd HH:mm:ss" maxlength="20" readonly="true"/>
				    <!-- <a class="inputDateButton" href="javascript:;">选择</a> -->
				</dd>
			</dl>
			
			<dl>
				<dt>会议结束时间：</dt>
				<dd>
					<input type="text" name="bf.endTime" class="date required" dateFmt="yyyy-MM-dd HH:mm:ss" maxlength="20" readonly="true"/>
				    <!-- <a class="inputDateButton" href="javascript:;">选择</a> -->
				</dd>
			</dl>
			<dl>
				<dt>主持人：</dt>
				<dd>
					<input type="text" name="bf.compere" maxlength="20" />
					
				</dd>
			</dl>
			
			<dl>
				<dt>组织人：</dt>
				<dd>
					<input type="text" name="bf.organizer" maxlength="20" class="required" />
					
				</dd>
			</dl>
			<dl>
				<dt>组织单位：</dt>
				<dd>
					<input type="text" name="bf.organizeUnit" maxlength="20" class="required" />
					
				</dd>
			</dl>
			
			<dl>
				<dt>列席人员：</dt>
				<dd>
					<input type="text" name="bf.attendPerson" maxlength="20" />
					
				</dd>
			</dl>
			
			<dl>
				<dt>地点：</dt>
				<dd>
					<input type="text" name="bf.address" maxlength="20" />
					
				</dd>
			</dl>
			
			<dl>
				<dt>主题：</dt>
				<dd>
					<input type="text" name="bf.theme" maxlength="20" class="required" />
					
				</dd>
			</dl>
			
			<dl>
				<dt>状态：</dt>
				<dd>
					<select name="bf.baudit" id="baudit">
						
					</select>
				</dd>
			</dl>
			<!-- <dl>
				<dt>发布时间：</dt>
				<dd>
					<input type="text" name="bf.publishTime" class="date required" dateFmt="yyyy-MM-dd HH:mm:ss" maxlength="20" readonly="true"/>
				    <a class="inputDateButton" href="javascript:;">选择</a>
				</dd>
			</dl> -->
			<dl>
				<dt>备注：</dt>
				<dd>
					<input type="text" name="bf.remark" maxlength="20" />
					
				</dd>
			</dl>
			
			<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">发布</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
			
		</div>
		
	</form>
	
</div>
<script>
	$(function(){
		$.getJSON("/aqjy/json/getDictionary",{table:"BASICINFO",name:"BAUDIT"},function(data){
			var json=JSON.parse(data);
			for (var i = 0; i < json.length; i++) {
				$("#baudit").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
			}
			
		})
	})
</script>