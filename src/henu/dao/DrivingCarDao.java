package henu.dao;

import henu.bean.Car;
import henu.bean.DrivingCar;

import java.util.List;

public interface DrivingCarDao {
public int add(DrivingCar dc);
	
	/**
	 * 根据ID删除
	 */
	public int deleteById(int drid);
	/**
	 * @param dc
	 * @return
	 */
	public int update(int uid,DrivingCar dc);
	public DrivingCar findById(int drid);
	public List<DrivingCar> findAllByComid(String property,	String key, int comid, String orderField, String orderDirection,int start, int end);
	public List<DrivingCar> findAll();
	public DrivingCar findByDrivingIdOrVehicleLicense(String drivingId,String vehicleLicense);
	public List<DrivingCar> findByProperty(String property, String key,String order, String sort, int start, int end);
	public Car findByIdCardAndVehicleLicense(String idCard,	String vehicleLicense) ;
	public List<DrivingCar> findAllByCarId(String property, String key,String idCard,
			String order, String sort,int start,int end);
}
