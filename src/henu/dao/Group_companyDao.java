package henu.dao;
import henu.bean.Group_Company;
import java.util.List;

public interface Group_companyDao {
	public List<Group_Company> findGroup();
	public List<Group_Company> findCompany(String group);
}
