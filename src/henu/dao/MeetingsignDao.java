package henu.dao;

import henu.bean.Meetingsign;

import java.util.List;

public interface MeetingsignDao {
	/**
	 * 向会议签到表添加信息
	 * @param meetingsign
	 */
	public int save(Meetingsign meetingsign);
	/**
	 * 根据msid删除会议签到表的一条记录
	 * @param msid
	 */
	public int delete(int msid);
	/**
	 * 根据msid、meetingsign更新会议签到表的一条记录
	 * @param msid
	 * @param meetingsign
	 */
	public int update(int msid,Meetingsign meetingsign);

	/**
	 * 根据会议签到表ID查找
	 * @param msid
	 * @return
	 */
	public Meetingsign findAllBymcId(int msid);
	/**
	 * 模糊查找
	 * @param property 字段名
	 * @param key 字段值
	 * @param order  排序字段
	 * @param sort 排序方式
	 * @return
	 */
	public List<Meetingsign> findByProperty(String property, String key,
			String order, String sort, int start, int end);
	public List<Meetingsign> findAll(String order, String sort,String property,String key); 
}
