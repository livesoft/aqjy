package henu.bean;
/**
 * 二级维护表
 * @author 梁胜彬
 * 2015-01-24
 */
public class Maintance extends Bean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int mid;
	private int carid;
	private String maintanceTime;
	private String maintanceCycle;
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getCarid() {
		return carid;
	}
	public void setCarid(int carid) {
		this.carid = carid;
	}
	public String getMaintanceTime() {
		return maintanceTime;
	}
	public void setMaintanceTime(String maintanceTime) {
		this.maintanceTime = maintanceTime;
	}
	public String getMaintanceCycle() {
		return maintanceCycle;
	}
	public void setMaintanceCycle(String maintanceCycle) {
		this.maintanceCycle = maintanceCycle;
	}
}
