<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="pageContent">
	<form method="post" action="/aqjy/public/insurance_edit" class="pageForm required-validate" onsubmit="return navTabSearch(this);">
		<div class="pageFormContent" layoutH="56" style="width:700px;height:600px;float:left">
			<dl>
				<dt>车牌号：</dt>
				<dd><input type="text" name="i.vehiclelicense" value="${i.vehiclelicense}" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>录入时间：</dt>
				<dd><input type="text" name="i.enterdate" value="${i.enterdate }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>更新时间：</dt>
				<dd><input type="text"  name="i.updatedate" value="${i.updatedate }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>交强险单号：</dt>
				<dd><input type="text" name="i.tno" value="${i.tno}" readonly="readonly" /></dd>
			</dl>
			<dl>
				<dt>交强险文件：</dt>
				<dd><input type="text" name="i.trafficfile" value="${i.trafficfile }" readonly="readonly" /></dd>
			</dl>
			<dl>
				<dt>交强险开始时间：</dt>
				<dd><input type="text"  name="i.trafficstart" value="${i.trafficstart}" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>交强险结束时间：</dt>
				<dd><input type="text"  name="i.trafficend" value="${i.trafficend }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>商业险单号：</dt>
				<dd><input type="text" name="i.ino" value="${i.ino }" readonly="readonly" /></dd>
			</dl>
			<dl>
				<dt>商业险文件：</dt>
				<dd><input  type="text" name="i.insurancefile" value="${i.insurancefile }" readonly="readonly"/></dd>
			</dl>			
			<dl>
				<dt>商业险开始时间：</dt>
				<dd><input type="text"  name="i.insurancestart" value="${i.insurancestart }" readonly="readonly"/></dd>		
			</dl>
			<dl>
				<dt>商业险结束时间：</dt>
				<dd><input type="text"  name="i.insuranceend" value="${i.insuranceend }" readonly="readonly"></dd>
			</dl>
			</div>
			
			<%-- <div style="float:right"><img alt="复印件" src="../${c.copyurl}" width="180px" height="180px" ></div> --%>
		<div class="formBar">
			<ul>
				<li>
					<div class="button"><div class="buttonContent"><button type="submit" >编辑信息</button></div></div>
				</li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">关闭</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>
