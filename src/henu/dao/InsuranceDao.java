package henu.dao;

import henu.bean.Car;
import henu.bean.Insurance;
import henu.bean.InsuranceCarPInformation;

import java.util.List;

public interface InsuranceDao {
	public int add(Insurance insurance,String userid);
	public int deleteById(int inid);
	public int update(int inid,Insurance information);
	public List<Insurance> findAll();
	public Insurance findById(int inid);
	public List<InsuranceCarPInformation> findByProperty(String property, String key,String comid, String sort, int start, int end);
	public InsuranceCarPInformation findByIdCard(String idCard);
	//通过idcard从车辆表中查到车牌号，显示到前台用户界面add 或edit
	public Car findvehiclebyidcard(String idcard);
	//前台查找保险要先通过身份证找到carid所对应的所有车辆
	public List<Car> findAllCar(String idcard);
	////通过Carid查找单个用户每个车的保险
	public Insurance findByCarid(int carid);
}
