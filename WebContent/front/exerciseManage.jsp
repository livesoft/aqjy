<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="/aqjy/exer/beginExer?args={sid_user}" target="navTab" ><span>开始考试</span></a></li>
			<li><a class="add" href="/aqjy/exer/scoreExer?args={sid_user}" target="navTab"><span>查看成绩</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="78">
	<!-- -此处通过 读取数据库加载所有信息 -->
		<thead>
			<tr>
                <th>考试名称</th>
				<th orderField="examtype" <c:if test='${orderField == "examtype" }'> class="${orderDirection}"  </c:if>>类型</th>
				<th>考试时长(分钟)</th>
				<th>开始时间</th>
                <th>专题名称</th>
                <th>考试说明</th>
				<th orderField="credit" <c:if test='${orderField == "credit" }'> class="${orderDirection}"  </c:if>>学分</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="cl" items="${list}">
			<tr target="sid_user" rel="${cl.eid}">
				<td>${cl.examname }</td>
				<td><c:if test="${cl.examtype eq '0' }">专题考试</c:if><c:if test="${cl.examtype eq '1' }">综合考试</c:if></td>
				<td>${cl.duration }</td>
				<td>${cl.examtime }</td>
				<td>${cl.subject }</td>
				<td>${cl.content }</td>
				<td>${cl.credit }</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10" >10</option>
					<option value="50" selected="selected">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '100' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" selected="selected">100</option>
					<option value="500">500</option>
				</c:if>
			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>
	</div>
</div>
