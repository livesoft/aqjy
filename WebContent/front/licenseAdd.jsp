<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@taglib uri="/struts-tags" prefix="s"%>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
	<link rel="stylesheet" href="../ueditor/themes/default/css/ueditor.css">
  	<%-- <script>
  		var ue = UE.getEditor('editor');
  	</script> 
  	编辑器
  	--%>
  	
	<div class="pageContent">
	<form method="post" action="/aqjy/usertotalview/LicenseAction_add" class="pageForm required-validate" onsubmit="return iframeCallback(this,navTabAjaxDone)" enctype="multipart/form-data">
		<div class="pageFormContent nowrap" layoutH="97">
		<div style="height: 660px;overflow: auto;">
			<dl>
				<dt>身份证号：</dt>
				<dd>
					<input type="text" name="li.idcard" value="${PERSON.idCard }" class="" />
				</dd>
			</dl>
			<dl>
				<dt>准驾车型：</dt>
				<dd>
					<input type="text" name="li.cartype"  class=""/>
				</dd>
			</dl>
			<dl>
				<dt>初次领证时间：</dt>
				<dd>
					<input type="text" name="li.firsttime"  class=""/>
				</dd>
			</dl>
			<dl>
				<dt>有效期：</dt>
				<dd>
					<input type="text" name="li.limittime"  class="" />
				</dd>
			</dl>
			<dl>
				<dt>有效期起始时间：</dt>
				<dd>
					<input type="text" name="li.begintime"  class="" />
				</dd>
			</dl>
			<dl>
				<dt>年审信息：</dt>
				<dd>
					<input type="text" name="li.yearaudit"    class=""/>
				</dd>
			</dl>
			<dl>
				<dt>驾驶证复印件：</dt>
				<dd>
					<input type="file" name="lphoto"    class="required images"/>
				</dd>
			</dl>
			<dl>
				<dt>备注：</dt>
				<dd>
						<input type="text" name="li.memo"  class="" />
				</dd>
			</dl>
			
			
			
			</div>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="submit">发布</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
