package henu.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import henu.bean.Dictionary;
import henu.bean.Insurance;
import henu.dao.DictionaryDao;
import henu.util.Dbcp;

public class DictionaryDaoImpl implements DictionaryDao {
	/*table,name分别对应dictionarytype的need和remark*/
	@Override
	public List<Dictionary> getDictionary(String table,String name) {
		// TODO Auto-generated method stub
		
		
		List<Dictionary> list = null;
		try {
			QueryRunner qr = new QueryRunner(Dbcp.getDataSource());
			String sql="select * from dictionarydata where codeid=(select codeid from dictionarytype where need=? and remark=?)";
			Object[] params={table,name};
			list = qr.query(sql, new BeanListHandler<Dictionary>(Dictionary.class),params);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

}
