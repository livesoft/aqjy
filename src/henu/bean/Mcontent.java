package henu.bean;
/**
 * @author 田星杰
 * 2015-01-15
 */
public class Mcontent extends Bean{
	private int mcid;//会议内容ID
	private String content;//会议内容
	private String timePerTime;//每页浏览时间
	private String filePath;//附件路径
	private String videoUrl;//视频附件路径
	private String publisher;//发布人
	private String publishTime;//发布时间
	private String memo;//备注
	private int basicid;//会议基本内容ID 
	public int getBasicid() {
		return basicid;
	}
	public void setBasicid(int basicid) {
		this.basicid = basicid;
	}
	public int getMcid() {
		return mcid;
	}
	public void setMcid(int mcid) {
		this.mcid = mcid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTimePerTime() {
		return timePerTime;
	}
	public void setTimePerTime(String timePerTime) {
		this.timePerTime = timePerTime;
	}
	
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}
	public String getVideoUrl() {
		return videoUrl;
	}
	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
}
