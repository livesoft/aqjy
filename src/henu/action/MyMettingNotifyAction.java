package henu.action;
/**
 * 主要用于前台用户对安全会议通知部分的处理
 * @author 田星杰
 * 2015-01-25
 */
import henu.bean.Basicinfo;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.Page;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

public class MyMettingNotifyAction extends ActionSupport{
	//分页查询
	Page page;
	private int pageNum=1;
	private int numPerPage=10;
	private String orderField="basicid";
	private String orderDirection="desc";
	
	//检索
	private String property="-1";
	private String key="-1";
	
	
	private AJAX ajax;
	//声明一个会议基本信息类的实例变量用于存放查询到的记录
	private Basicinfo bf;
	private String url="/admin/ajax.jsp";
	//存放会议基本信息表的主码ID
	private String uid;
	//声明一个List<Basicinfo>类型的变量用于存放查询到的记录的存放
	private List<Basicinfo> list;
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getNumPerPage() {
		return numPerPage;
	}
	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderDirection() {
		return orderDirection;
	}
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public AJAX getAjax() {
		return ajax;
	}
	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}
	
	public Basicinfo getBf() {
		return bf;
	}
	public void setBf(Basicinfo bf) {
		this.bf = bf;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public List<Basicinfo> getList() {
		return list;
	}
	public void setList(List<Basicinfo> list) {
		this.list = list;
	}
	//列出所有基本会议信息
	public String findAll(){
		page = new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="basicid";
			orderDirection="desc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("BASICINFO",pageNum,numPerPage,property,key);
		
		list = DaoFactory.getBasicinfoDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());
		
		//前台用户页面的URL mettingNotify.jsp
		url="/front/mettingNotify.jsp";
		
		return SUCCESS;
	}
}
