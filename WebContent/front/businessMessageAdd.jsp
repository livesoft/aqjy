
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <script>

</script>
	<div class="pageContent">
	<form method="post" action="/aqjy/usertotalview/businessMessageAction_add" enctype="multipart/form-data" class="pageForm required-validate" onsubmit="return iframeCallback(this,navTabAjaxDone);">
		<div class="pageFormContent nowrap" layoutH="97">
			<dl>
				<dt>营运证号：</dt>
				<dd>
					<input type="text" name="bm.businessId"  value="" maxlength="50" class="required"/>
				</dd>
			</dl>
			<dl>
				<dt>车牌号：</dt>
				<dd>
					<input type="text" name="bm.vehicleLicense"  value="" maxlength="20" class="required"/>
				</dd>
			</dl>
			<dl>
				<dt>车辆类型：</dt>
				<dd>
					<select name="bm.carType" id="carType"></select>
				</dd>
			</dl>
			<dl>
				<dt>经营范围：</dt>
				<dd>
					<select name="bm.businessRage" id="businessRage"></select>
				</dd>
			</dl>
			<dl>
				<dt>发证机关：</dt>
				<dd>
					<input type="text" name="bm.publishOffice"    maxlength="60"/>
				</dd>
			</dl>
			<dl>
				<dt>发证日期：</dt>
				<dd>
					<input type="text" name="bm.publishDate" class="required date" dateFmt="yyyy-MM-dd" />
					<a class="inputDateButton" href="javascript:;">选择</a>
				</dd>
			</dl>
			<dl>
				<dt>有效期开始日期：</dt>
				<dd>
					<input type="text" name="bm.validityBegin" class="required date" dateFmt="yyyy-MM-dd" />
					<a class="inputDateButton" href="javascript:;">选择</a>
				</dd>
			</dl>
			<dl>
				<dt>有效期结束日期：</dt>
				<dd>
					<input type="text" name="bm.validityEnd" class="required date" dateFmt="yyyy-MM-dd" />
					<a class="inputDateButton" href="javascript:;">选择</a>
				</dd>
				
			</dl>	
			<dl>
				<dt>营运证复印件(照片)：</dt>
				<dd>
					<input type="file" name="businessFile" class="required image">	
				</dd>
			</dl>
			<dl>
				<dt>经营许可证(照片)：</dt>
				<dd>
					<input type="file" name="businessPermission" class="required image">	
				</dd>
			</dl>
			<dl>
				<dt>业主名：</dt>
				<dd>
						<input type="text" name="bm.masterName"    maxlength="25"/>
				</dd>
			</dl>
				<dl>
				<dt>地址：</dt>
				<dd>
					<input type="text" name="bm.address"    maxlength="50"/>
				</dd>
			</dl>
			<dl>
				<dt>吨位、座位：</dt>
				<dd>
					<input type="text" name="bm.seats"    maxlength="25"/>
				</dd>
			</dl>
			
			<dl>
				<dt>车辆尺寸：</dt>
				<dd>
					<input type="text" name="bm.carSize"    maxlength="25"/>	
				</dd>
			</dl>
			
			<dl>
				<dt>备注：</dt>
				<dd>
						<textarea rows="3" cols="30" name="${bm.memo }"></textarea>
				</dd>
			</dl>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
		</div>
	</form>
	
</div>
<script>
$(function(){
	$.getJSON("/aqjy/json/getDictionary",{table:"BUSINESSMESSAGE",name:"CARTYPE"},function(data){
		var json=JSON.parse(data);
		for (var i = 0; i < json.length; i++) {
			$("#carType").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
		}
		
	})
	$.getJSON("/aqjy/json/getDictionary",{table:"BUSINESSMESSAGE",name:"BUSINESSRAGE"},function(data){
		var json=JSON.parse(data);
		for (var i = 0; i < json.length; i++) {
			$("#businessRage").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
		}
		
	})
})
</script>