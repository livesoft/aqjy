package henu.bean;
/**
 * 管理人员信息
 * @author 王竞赛
 * 2014-1-20
 */
public class Admin extends Bean {
	
	/*	车辆号
	用户ID
	手机号
	真实姓名
	性别
	电子邮箱
	密码
	提示问题
	答案
	身份证号
	照片
	公司ID
	角色【数据字典】
	状态
	注册日期
	上次访问时间
	备注1
	备注2*/
	private static final long serialVersionUID = 1L;
	private String maid;
	private int comid;
	private String phone;
	private String realName;
	private String sex;
	private String email;
	private String password;
	private String questions;
	private String answer;
	private String photo;
	private String role;
	private String audio;
	private String registTime;
	private String lastTime;
	private String copyIdCard;
	private String memo;
	private String comname;
	private String rolename;
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public String getComname() {
		return comname;
	}
	public void setComname(String comname) {
		this.comname = comname;
	}
	public String getMaid() {
		return maid;
	}
	public void setMaid(String maid) {
		this.maid = maid;
	}
	public int getComid() {
		return comid;
	}
	public void setComid(int comid) {
		this.comid = comid;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getQuestions() {
		return questions;
	}
	public void setQuestions(String questions) {
		this.questions = questions;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getAudio() {
		return audio;
	}
	public void setAudio(String audio) {
		this.audio = audio;
	}
	public String getRegistTime() {
		return registTime;
	}
	public void setRegistTime(String registTime) {
		this.registTime = registTime;
	}
	public String getLastTime() {
		return lastTime;
	}
	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}
	public String getCopyIdCard() {
		return copyIdCard;
	}
	public void setCopyIdCard(String copyIdCard) {
		this.copyIdCard = copyIdCard;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "Admin [maid=" + maid + ", comid=" + comid + ", phone=" + phone
				+ ", realName=" + realName + ", sex=" + sex + ", email="
				+ email + ", password=" + password + ", questions=" + questions
				+ ", answer=" + answer + ", photo=" + photo + ", role=" + role
				+ ", audio=" + audio + ", registTime=" + registTime
				+ ", lastTime=" + lastTime + ", copyIdCard=" + copyIdCard
				+ ", memo=" + memo + ", comname=" + comname + ", rolename="
				+ rolename + "]";
	}
}
