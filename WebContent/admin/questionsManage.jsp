<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<form id="pagerForm" method="post" action="/aqjy/ques/allQues">
	<input type="hidden" name="status" value="${param.status}">
	<input type="hidden" name="keywords" value="${param.keywords}" />
	<input type="hidden" name="pageNum" value="1" />
	<input type="hidden" name="numPerPage" value="${page.numPerPage}" />
	<input type="hidden" name="orderField" value="${param.orderField}" /> 
    <input type="hidden" name="orderDirection"  value="${param.orderDirection}" />  
    <input type="hidden" name="property"  value="${property}">
    <input type="hidden" name="key"  value="${key}">
</form>
 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- -别人一点连接跳进action action返回json 打开此页面  -->
<div class="pageHeader">
	<form onsubmit="return navTabSearch(this);" action="/aqjy/ques/allQues" method="post">
	<div class="searchBar">
		<table class="searchContent">
			<tr>
				<td>
					查询：
				</td>
				<td>
					<select class="combox" name="property">
						<option value="">请选择查询方式</option>
						<option value="qid">按编号查询</option>
						<option value="subject">按科目查询</option>
						<option value="type">按类型查询</option>
					</select>
				</td>
				<td>
					关键词：
						<input type="text" value="" name="key">			
				</td>
			</tr>
		</table>
		<div class="subBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></li>
			</ul>
				<div style="color:red;margin-top:5px;">注意:点击标题可以进行排序；检索模式下若想查询所有信息，再点击一次检索按钮即可；数据较多时可改变每页多少条以显示页码</div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="/aqjy/ques/goaddQues" target="navTab"><span>添加</span></a></li>
			<li><a class="delete" href="/aqjy/ques/delQues?args={sid_user}" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="/aqjy/ques/findQues?args={sid_user}" target="navTab"><span>修改</span></a></li>
			<li class="line">line</li>
			<li><a class="icon" href="/aqjy/ques/exportQues" target="dwzExport"  title="实要导出这些记录吗?"><span>导出EXCEL</span></a></li>
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138">
	<!-- -此处通过 读取数据库加载所有信息 -->
		<thead>
			<tr>
                <th orderField="subject" <c:if test='${orderField == "subject" }'> class="${orderDirection}"  </c:if>>科目</th>
				<th orderField="type" <c:if test='${orderField == "type" }'> class="${orderDirection}"  </c:if>>类型</th>
				<th>内容</th>
                <th>选项A</th>
                <th>选项B</th>
                <th>选项C</th>
                <th>选项D</th>
				<th>答案</th>
				<th>备注</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="cl" items="${list}">
				<tr target="sid_user" rel="${cl.qid}">
				<td>${cl.subject }</td>
				<td>${cl.type }</td>
				<td>${cl.content }</td>
				<td>${cl.optiona }</td>
				<td>${cl.optionb }</td>
				<td>${cl.optionc }</td>
				<td>${cl.optiond }</td>
				<td>${cl.answer }</td>
				<td>${cl.remark }</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
<div class="panelBar">
		<div class="pages">
			<span>显示</span>
			<select class="combox" name="numPerPage" onchange="navTabPageBreak({numPerPage:this.value})">
				<c:if test="${page.numPerPage eq '10' }">
					<option value="10" selected="selected">10</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '50' }">
					<option value="10" >10</option>
					<option value="50" selected="selected">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '100' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" selected="selected">100</option>
					<option value="500">500</option>
				</c:if>
				<c:if test="${page.numPerPage eq '500' }">
					<option value="10" >10</option>
					<option value="50" >50</option>
					<option value="100" >100</option>
					<option value="500" selected="selected">500</option>
				</c:if>
			</select>
			<span>条，共${page.totalCount}条</span>
		</div>
		<div class="pagination" targetType="navTab" totalCount="${page.totalCount}" numPerPage="${page.numPerPage}" pageNumShown="${page.pageNumShown}" currentPage="${page.pageNum }"></div>
	</div>
</div>
