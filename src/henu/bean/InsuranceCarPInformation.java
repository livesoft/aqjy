package henu.bean;

public class InsuranceCarPInformation extends Bean{
	
	private Integer inid; 			//id
	private String carid;			//车辆ID
	private String vehiclelicense;	//车牌号 
	private String enterdate;		//录入时间
	private String updatedate;		//更新时间
	private String insurancefile;	//商业险文件
	private String insurancestart;	//商业险开始时间
	private String insuranceend;	//商业险结束时间
	private String trafficstart;	//交强险开始时间
	private String trafficend;		//交强险结束时间
	private String ino;				//商业险单号
	private String tno;				//交强险单号
	private String trafficfile;		//交强险文件
	private String status;			//状态
	private String memo;			//备注       共15个
	private String realName;
	private int comid;
	private String regTime;
	private String idCard;
	
	
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getRegTime() {
		return regTime;
	}
	public void setRegTime(String regTime) {
		this.regTime = regTime;
	}
	public Integer getInid() {
		return inid;
	}
	public void setInid(Integer inid) {
		this.inid = inid;
	}
	public String getCarid() {
		return carid;
	}
	public void setCarid(String carid) {
		this.carid = carid;
	}
	public String getVehiclelicense() {
		return vehiclelicense;
	}
	public void setVehiclelicense(String vehiclelicense) {
		this.vehiclelicense = vehiclelicense;
	}
	public String getEnterdate() {
		return enterdate;
	}
	public void setEnterdate(String enterdate) {
		this.enterdate = enterdate;
	}
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
	public String getInsurancefile() {
		return insurancefile;
	}
	public void setInsurancefile(String insurancefile) {
		this.insurancefile = insurancefile;
	}
	public String getInsurancestart() {
		return insurancestart;
	}
	public void setInsurancestart(String insurancestart) {
		this.insurancestart = insurancestart;
	}
	public String getInsuranceend() {
		return insuranceend;
	}
	public void setInsuranceend(String insuranceend) {
		this.insuranceend = insuranceend;
	}
	public String getTrafficstart() {
		return trafficstart;
	}
	public void setTrafficstart(String trafficstart) {
		this.trafficstart = trafficstart;
	}
	public String getTrafficend() {
		return trafficend;
	}
	public void setTrafficend(String trafficend) {
		this.trafficend = trafficend;
	}
	public String getIno() {
		return ino;
	}
	public void setIno(String ino) {
		this.ino = ino;
	}
	public String getTno() {
		return tno;
	}
	public void setTno(String tno) {
		this.tno = tno;
	}
	public String getTrafficfile() {
		return trafficfile;
	}
	public void setTrafficfile(String trafficfile) {
		this.trafficfile = trafficfile;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public int getComid() {
		return comid;
	}
	public void setComid(int comid) {
		this.comid = comid;
	}
	
}
