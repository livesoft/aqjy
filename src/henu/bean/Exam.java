package henu.bean;

public class Exam implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer eid ; 
	private String examname ; 
	private String content;
	private String duration;				//考试时常
	private String score;
	private String judgecount;
	private String selectcount;
	private Integer judgescore;
	private Integer selectscore;
	private String credit;					//学分
	private String examtime;				//考试开始的时间
	private String status;					//状态备注
	private String examtype;	
	private String subject;	
	
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getExamtype() {
		return examtype;
	}
	public void setExamtype(String examtype) {
		this.examtype = examtype;
	}
	public Integer getEid() {
		return eid;
	}
	public void setEid(Integer eid) {
		this.eid = eid;
	}
	public String getExamname() {
		return examname;
	}
	public void setExamname(String examname) {
		this.examname = examname;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getJudgecount() {
		return judgecount;
	}
	public void setJudgecount(String judgecount) {
		this.judgecount = judgecount;
	}
	public String getSelectcount() {
		return selectcount;
	}
	public void setSelectcount(String selectcount) {
		this.selectcount = selectcount;
	}
	public Integer getJudgescore() {
		return judgescore;
	}
	public void setJudgescore(Integer judgescore) {
		this.judgescore = judgescore;
	}
	public Integer getSelectscore() {
		return selectscore;
	}
	public void setSelectscore(Integer selectscore) {
		this.selectscore = selectscore;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	public String getExamtime() {
		return examtime;
	}
	public void setExamtime(String examtime) {
		this.examtime = examtime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
