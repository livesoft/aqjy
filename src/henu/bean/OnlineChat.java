package henu.bean;

public class OnlineChat extends Bean {
	private int oid;//自增主键
	private String title;//咨询标题
	private String askContent;//咨询内容
	private String range;//咨询所属范围
	private String askTime;//咨询时间
	private String replyTime;//回复时间
	private String replyContent;//回复内容
	private String replyPerson;//回复人
	private String askName;//询问人姓名
	private String idCard;//查询该用户咨询了多少条
	private int isDisplay;//是否显示，不回复不显示,0代表不回复，1代表回复
	private int isTop;//是否置顶,0代表不置顶，1代表置顶
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAskContent() {
		return askContent;
	}
	public void setAskContent(String askContent) {
		this.askContent = askContent;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	public String getAskTime() {
		return askTime;
	}
	public void setAskTime(String askTime) {
		this.askTime = askTime;
	}
	public String getReplyTime() {
		return replyTime;
	}
	public void setReplyTime(String replyTime) {
		this.replyTime = replyTime;
	}
	public String getReplyContent() {
		return replyContent;
	}
	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}
	public String getReplyPerson() {
		return replyPerson;
	}
	public void setReplyPerson(String replyPerson) {
		this.replyPerson = replyPerson;
	}
	public String getAskName() {
		return askName;
	}
	public void setAskName(String askName) {
		this.askName = askName;
	}
	public int getIsDisplay() {
		return isDisplay;
	}
	public void setIsDisplay(int isDisplay) {
		this.isDisplay = isDisplay;
	}
	public int getIsTop() {
		return isTop;
	}
	public void setIsTop(int isTop) {
		this.isTop = isTop;
	}
}
