<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="../js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="../js/resetPassword.js"></script>
<link href="../style/resetPassword.css" rel="stylesheet" type="text/css">
<title>找回或者重置密码</title>
</head>
<body>
<body>
<form action="/aqjy/public/resetPassword"  method="post">
	<table style="margin: auto">
		<tr>
			<td colspan="3" align="center">标记有红色*为必填项目</td>
		</tr>
		<tr>
			<td>身份证号:</td>
			<td><input type="text" name="p.idCard" id="idCard"></td>
			<td><div id="error1">*</div></td>
		</tr>
		<tr>
			<td>密保问题:</td>
			<td>
				<select name="p.question">
					<option value="对您人生影响最大的是谁">对您人生影响最大的是谁</option>
					<option value="您最敬仰的历史名人是谁">您最敬仰的历史名人是谁</option>
					<option value="您最欣赏的电影明星是谁">您最欣赏的电影明星是谁</option>
					<option value="您最想上哪里去旅游">您最想上哪里去旅游</option>
					<option value="您最喜欢的课程名">您最喜欢的课程名</option>
					<option value="您最喜欢的动物">您最喜欢的动物</option>
					<option value="您牢记的座右铭">您牢记的座右铭</option>
				</select>
			</td>
			<td><div>*</div></td>
		</tr>
		<tr>
			<td>密保答案:</td>
			<td><input type="text" name="p.answer" id="answer"></td>
			<td><div id="error2">*</div></td>
		</tr>
		<tr>
			<td>新密码:</td>
			<td><input type="password" name="p.password" id="password"></td>
			<td><div id="error3">*</div></td>
		</tr>
		<tr>
			<td>确认新密码:</td>
			<td><input type="password" id="confirmPassword"></td>
			<td><div id="error4">*</div></td>
		</tr>
		<tr>
			<td colspan="3" align="center">
				<input type="button" value="找回" id="sub">
			</td>
		</tr>
	</table>
</form>
</body>
</html>