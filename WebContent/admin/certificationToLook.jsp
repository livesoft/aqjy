<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="pageContent" >
	
		<div class="pageFormContent nowrap" layoutH="56" style="width:690px;height:600px;float:left" >
			<dl >
				<dt>姓名：</dt>
				<dd><input type="text" value="${certification_pinfo.name }" readonly="readonly"/></dd>
			</dl>
			
			<dl>
				<dt>性别：</dt>
				<dd><input type="text" value="${certification_pinfo.sex }" readonly="readonly"/></dd>
			</dl>
			
			<dl>
				<dt>出生年月：</dt>
				<dd><input type="text" value="${certification_pinfo.birthday }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>身份证号：</dt>
				<dd><input type="text" value="${certification_pinfo.idCard }" readonly="readonly"/></dd>
			</dl>			
			<dl>
				<dt>住址：</dt>
				<dd><input type="text" value="${certification_pinfo.address }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>从业资格号：</dt>
				<dd><input  type="text" value="${certification_pinfo.cnid }" readonly="readonly"/></dd>
			</dl>
			
			<dl>
				<dt>初次发证时间：</dt>
				<dd><input type="text" value="${certification_pinfo.firstdate }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>发证时间：</dt>
				<dd><input type="text" value="${certification_pinfo.issuedate }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>从业资格类别：</dt>
				<dd><input  type="text" value="${certification_pinfo.type }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>有效期：</dt>
				<dd><input type="text" value="${certification_pinfo.validation }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>发证单位：</dt>
				<dd><input  type="text" value="${certification_pinfo.issueunit }" readonly="readonly"/></dd>
			</dl>
			
			<dl>
				<dt>服务单位：</dt>
				<dd><input type="text" value="${certification_pinfo.serviceunit }" readonly="readonly"></dd>
			</dl>
			<dl>
				<dt>注册时间：</dt>
				<dd><input type="text" value="${certification_pinfo.regTime }" readonly="readonly"></dd>
			</dl>
			</div>
			
			<div style="float:right">
				<div >
					<div>用户照片：</div>
					<div><img alt="用户照片" src="../${certification_pinfo.photo}" width="180px" height="180px" ></div>
				</div>
				<div>
					<div>复印件：</div>
					<div><img alt="复印件" src="../${certification_pinfo.copyurl}" width="280px" height="280px" ></div>
				</div>
			</div>
			
			
		
		<div class="formBar">
			<ul>
				<!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">关闭</button></div></div>
				</li>
			</ul>
		</div>
</div>
