package henu.action;


import henu.bean.Meetingsign;

import henu.dao.MeetingsignDao;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;

import henu.util.Page;

import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class MettingSignAction extends ActionSupport{
	//分页查询
	Page page;
	private int pageNum=1;
	private int numPerPage=10;
	private String orderField="msid";
	private String orderDirection="asc";
	
	//检索
	private String property="-1";
	private String key="-1";
	
	
	private AJAX ajax;
	//声明一个会议基本信息类的实例变量用于存放查询到的记录
	private Meetingsign mcs;
	private String url="/admin/ajax.jsp";
	//存放会议基本信息表的主码ID
	private String uid;
	//声明一个List<Meetingsign>类型的变量用于存放查询到的记录的存放
	private List<Meetingsign> list;
	//用于存放新的list，把编号的信息转换为具体的直观信息
	
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getNumPerPage() {
		return numPerPage;
	}
	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}
	public String getOrderField() {
		return orderField;
	}
	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}
	public String getOrderDirection() {
		return orderDirection;
	}
	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public AJAX getAjax() {
		return ajax;
	}
	public void setAjax(AJAX ajax) {
		this.ajax = ajax;
	}
	
	public Meetingsign getMcs() {
		return mcs;
	}
	public void setMcs(Meetingsign mcs) {
		this.mcs = mcs;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public List<Meetingsign> getList() {
		return list;
	}
	public void setList(List<Meetingsign> list) {
		this.list = list;
	}
	//列出所有会议签到信息
	public String findAll(){
		String meetingtheme=null;
		String meetingcompany=null;
		page = new Page();
		if(orderField.equals("")||orderDirection.equals("")){
			orderField="msid";
			orderDirection="asc";
		}
		if(property.equals("")||key.equals("")){
			property="-1";
			key="-1";
		}
		page.setProperties("MEETINGSIGN",pageNum,numPerPage,property,key);
		
		list = DaoFactory.getMeetingsignDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());
		//将编号信息转换为具体的直观的信息
		for(Meetingsign li:list){
			int s1=li.getBasicid();
			meetingtheme=DaoFactory.getBasicinfoDaoInstance().findAllBybasicId(s1).getTheme();
		    String s = Integer.toString(li.getComid());
		    meetingcompany=DaoFactory.getCompanyDaoInstance().findBy(s).getCompanyname();
			
		}
		//查出所签到的会议的主题，放入session范围内，命名为meetingtheme
		ServletActionContext.getRequest().getSession().setAttribute("meetingtheme", meetingtheme);
		//签到人所属的公司名称，放入session范围内，命名为meetingcompany
		ServletActionContext.getRequest().getSession().setAttribute("meetingcompany", meetingcompany);
		url="/admin/meetingSignManage.jsp";
		return SUCCESS;
	}
	//删除会议基本信息
	public String del(){
		int seid = Integer.parseInt(uid);
		//int seid=bf.getBasicid();
		int result = DaoFactory.getMeetingsignDaoInstance().delete(seid);
		ajax=new AJAX();
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("删除成功");
			ajax.setNavTabId("meetingsignInf");
			ajax.setCallbackType("");
			
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("删除失败");
			ajax.setCallbackType("");
		}
		url="/admin/ajax.jsp";
		return SUCCESS; 
	}
	//更新会议基本信息
	public String update(){
		MeetingsignDao dao = DaoFactory.getMeetingsignDaoInstance();
		ajax=new AJAX();
		//自动获取当前系统时间作为更新后会议通知的发布时间
		mcs.setSignTime(new Date().toLocaleString());
		int result = dao.update(mcs.getMsid(),mcs);
		//System.out.println(result);
		if(result!=0){
			ajax.setStatusCode("200");
			ajax.setMessage("更新成功");
			ajax.setNavTabId("meetingsignInf");
			ajax.setCallbackType("closeCurrent");
		}else{
			ajax.setStatusCode("300");
			ajax.setMessage("更新失败");
			ajax.setCallbackType("");
		}
		return SUCCESS;
	}
	//用于得到用户选中的记录的信息，并存放在session范围内的basicinfo中，通过URL转到修改页面
	public String find(){
		MeetingsignDao dao = DaoFactory.getMeetingsignDaoInstance();
		int seid = Integer.parseInt(uid);
		Meetingsign s =dao.findAllBymcId(seid);
		ServletActionContext.getRequest().getSession().setAttribute("meetingsign", s);
		url="/admin/meetingSignUpdate.jsp";
		return SUCCESS;
	}
}
