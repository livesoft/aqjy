<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
  <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
	<link rel="stylesheet" href="../ueditor/themes/default/css/ueditor.css">
  	<script> 
  		var ue = UE.getEditor('editor');
  	</script>
</head>
<body>
<div class="pageContent">
	<form method="post" action="/aqjy/interaction/noticeAction_update?uid=${n.nid }" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent nowrap" layoutH="97">
			<div style="height: 660px;overflow: auto;">
			<dl>
				<dt>新闻标题：</dt>
				<dd>
					<input type="text" name="n.title" maxlength="50" value="${n.title }" class="required" style="width:500px;"/>
				</dd>
			</dl>
			<dl>
				<dt>作者：</dt>
				<dd>
					<input type="text" name="n.publisher" value="${ADMIN.realName }"  class="required"/>
				</dd>
			</dl>
			<dl>
				<dt>类别：</dt>
				<dd>
					<select name="n.type" id="type">
					
						<option value="${n.type }" selected="selected">${n.type }</option>			
					</select>
				</dd>
			</dl>
			<dl>
				<dt>是否置顶：</dt>
				<dd>
					<c:if test="${n.isTop==0 }">
						<input type="radio" value="0" name="n.isTop" checked="checked">否
						<input type="radio" value="1" name="n.isTop" >是
					</c:if>
					<c:if test="${n.isTop==1 }">
						<input type="radio" value="0" name="n.isTop">否
						<input type="radio" value="1" name="n.isTop"  checked="checked">是
					</c:if>
				</dd>
			</dl>
			<dl>
				<dt>内容：</dt>
				<dd>
			
					<script type="text/plain" id="editor" name="description"  style="width:800px;height: 400px">${n.content}</script>
				
				</dd>
			</dl>
			</div>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button  type="submit">修改</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
	<script>
	$(function(){
		$.getJSON("/aqjy/json/getDictionary",{table:"NOTICE",name:"TYPE"},function(data){
			var json=JSON.parse(data);
			for (var i = 0; i < json.length; i++) {
				$("#type").append("<option value='"+json[i].codeNumber+"'>"+json[i].codeNumber+"</option>");
			}
			
		})
	})
</script>
</body>
</html>