package henu.bean;

public class Certification extends Bean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;				//id
	private String cnid;		//从业资格证编号
	private String idCard;		//身份证号
	private String name;		//姓名
	private String sex;			//性别
	private String birthday;	//出生年月
	private String address;		//住址
	private String photo;		//照片
	private String type;		//从业资格类别
	private String firstdate;	//初次发证时间
	private String validation;	//有效期
	private String issuedate;	//发证时间
	private String issueunit;	//发证单位
	private String serviceunit;	//服务单位
	private String copyurl;		//复印件	
	private String memo;		//备注  共16个
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCnid() {
		return cnid;
	}
	public void setCnid(String cnid) {
		this.cnid = cnid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFirstdate() {
		return firstdate;
	}
	public void setFirstdate(String firstdate) {
		this.firstdate = firstdate;
	}
	public String getValidation() {
		return validation;
	}
	public void setValidation(String validation) {
		this.validation = validation;
	}
	public String getIssuedate() {
		return issuedate;
	}
	public void setIssuedate(String issuedate) {
		this.issuedate = issuedate;
	}
	public String getIssueunit() {
		return issueunit;
	}
	public void setIssueunit(String issueunit) {
		this.issueunit = issueunit;
	}
	public String getServiceunit() {
		return serviceunit;
	}
	public void setServiceunit(String serviceunit) {
		this.serviceunit = serviceunit;
	}
	public String getCopyurl() {
		return copyurl;
	}
	public void setCopyurl(String copyurl) {
		this.copyurl = copyurl;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}

}
