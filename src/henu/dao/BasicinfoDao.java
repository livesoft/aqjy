package henu.dao;
import henu.bean.Basicinfo;

/**
 * 对basicinfo表进行DAO操作
 * @author 田星杰
 * 2015-01-19
 */
import java.util.List;


public interface BasicinfoDao {
	/**
	 * 向基本会议信息表添加信息
	 * @param basicinfo
	 */
	public int save(Basicinfo basicinfo);
	/**
	 * 根据basicid删除基本会议信息表的一条记录
	 * @param basicid
	 */
	public int delete(int basicid);
	/**
	 * 根据basicid、basicinfo更新基本会议信息表的一条记录
	 * @param basicid
	 * @param basicinfo
	 */
	public int update(int basicid , Basicinfo basicinfo);
	
	/**
	 * 根据基本会议信息表ID查找
	 * @param basicid
	 * @return
	 */
	public Basicinfo findAllBybasicId(int basicid);
	/**
	 * 模糊查找
	 * @param property 字段名
	 * @param key 字段值
	 * @param order  排序字段
	 * @param sort 排序方式
	 * @return
	 */
	public List<Basicinfo> findByProperty(String property, String key,
			String order, String sort, int start, int end);
	/**
	 * 查找所有
	 * @param property 字段名
	 * @param key 字段值
	 * @param order  排序字段
	 * @param sort 排序方式
	 * @return
	 */
	public List<Basicinfo> findAll(String property, String key,String order, String sort);
	
	
}
