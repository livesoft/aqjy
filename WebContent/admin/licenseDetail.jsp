<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<script type="text/javascript" charset="utf-8"
	src="../ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8"
	src="../ueditor/ueditor.all.js">
	
</script>
<script type="text/javascript" charset="utf-8"
	src="../ueditor/lang/zh-cn/zh-cn.js"></script>
<link rel="stylesheet" href="../ueditor/themes/default/css/ueditor.css">

<%-- <script>
  		var ue = UE.getEditor('editor');
  	</script> --%>


<div class="pageContent">
	<div class="pageFormContent nowrap" layoutH="97">
		<div style="height: 660px; overflow: auto;">
			<div style="position: absolute; top: 10px; right: 20px;">
				<h1 style="color: red; font-size: 14px;">驾驶证复印件：</h1>
				<img src="../${li.lphoto}"
					style="width: 600px; height: 450px; padding-top: 10px; padding-right: 10px;" />
			</div>
			<dl>
				<dt>身份证号：</dt>
				<dd>
					<input type="text" name="li.idcard" value="${li.idcard }" class=""
						readonly="readonly" />
				</dd>
			</dl>
			<dl>
				<dt>准驾车型：</dt>
				<dd>
					<input type="text" name="li.cartype" value="${li.cartype }"
						class="" readonly="readonly" />
				</dd>
			</dl>
			<dl>
				<dt>初次领证时间：</dt>
				<dd>
					<input type="text" name="li.firsttime" value="${li.firsttime }"
						class="" readonly="readonly" />
				</dd>
			</dl>
			<dl>
				<dt>有效期：</dt>
				<dd>
					<input type="text" name="li.limittime" value="${li.limittime }"
						class="" readonly="readonly" />
				</dd>
			</dl>
			<dl>
				<dt>有效期起始时间：</dt>
				<dd>
					<input type="text" name="li.begintime" value="${li.begintime }"
						class="" readonly="readonly" />
				</dd>
			</dl>
			<dl>
				<dt>年审信息：</dt>
				<dd>
					<input type="text" name="li.yearaudit" value="${li.yearaudit }"
						class="" readonly="readonly" />
				</dd>
			</dl>
			<dl>
				<dt>备注：</dt>
				<dd>
					<input type="text" name="li.memo" value="${li.memo }" class=""
						readonly="readonly" />
				</dd>
			</dl>
			<%-- <dl>
				<dt>备注</dt>
				<dd>
					<input type="text" name="li.memo" value="${li.memo }" class="" readonly="readonly" />
				</dd>
			</dl> --%>
		</div>
	</div>
</div>


