package henu.bean;

public class Seminar extends Bean{
	private int seid;
	private String sort;
	private String subjectname;
	private String subjecttime;
	private String remark;
	public int getSeid() {
		return seid;
	}
	public void setSeid(int seid) {
		this.seid = seid;
	}
	
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getSubjectname() {
		return subjectname;
	}
	public void setSubjectname(String subjectname) {
		this.subjectname = subjectname;
	}
	public String getSubjecttime() {
		return subjecttime;
	}
	public void setSubjecttime(String subjecttime) {
		this.subjecttime = subjecttime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	

}
