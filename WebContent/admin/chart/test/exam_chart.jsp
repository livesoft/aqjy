<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script type="text/javascript">
    var options = {
    	stacked: false,
    	gutter:20,
		axis: "0 0 1 1", // Where to put the labels (trbl)
		axisystep: 5 // How many x interval labels to render (axisystep does the same for the y axis)
	};
	
    
    
    /* Title settings */		
    title = "新生考试成绩统计图";
    titleXpos = 390;
    titleYpos = 85;

    /* Pie Data */
    pieRadius = 90;
    pieXpos = 150;
    pieYpos = 180;
  var passstu = "${passstu}";
  var nopass = 	"${stage[0]}" ; 
  //alert("及格的人数:"+passstu);
 // alert("不及格的人数:"+nopass);
    pieData = [parseInt(passstu),parseInt(nopass)];
    pieLegend = ["%%.%% – 及格","%%.%% – 不及格"];

    pieLegendPos = "east";
    
    
	$(function() {
		var r = Raphael("chartHolder");
		r.text(titleXpos, titleYpos, title).attr({"font-size":20});
		var pie = r.piechart(pieXpos, pieYpos, pieRadius, pieData, {legend: pieLegend, legendpos: pieLegendPos});
		// 设置这个圆形划过的时候的动作
		pie.hover(function () {
			this.sector.stop();
			this.sector.scale(1.1, 1.1, this.cx, this.cy);
			if (this.label) {
				this.label[0].stop();
				this.label[0].attr({ r: 7.5 });
				this.label[1].attr({"font-weight": 800});
			}
		}, function () {
			this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");
			if (this.label) {
				this.label[0].animate({ r: 5 }, 500, "bounce");
				this.label[1].attr({"font-weight": 400});
			}
		});
		
		//这个事柱状图
		var m6 = "${stage[1]}" ; 
		var m7 = "${stage[2]}" ; 
		var m8 = "${stage[3]}" ; 
		var m9 = "${stage[4]}" ; 
		var data3 = [[nopass,m6,m7,m8,m9]];
		var chart3 = r.barchart(40, 320, 620, 120, data3, options).hover(function() {
            this.flag = r.popup(this.bar.x, this.bar.y, this.bar.value).insertBefore(this);
        }, function() {
            this.flag.animate({opacity: 0}, 500, ">", function () {this.remove();});
        });
		chart3.label([["0分-59分", "60分－69分", "70分－79分", "80分－89分", "90分－100分"]],true);
	
	});
</script>
<div style="border: 1 solid red ">

<div id="chartHolder" style="height:850px"></div>

</div>