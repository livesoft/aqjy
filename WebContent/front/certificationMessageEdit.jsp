<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="pageFormContent nowrap">
	<form method="post" enctype="multipart/form-data" action="/aqjy/public/certification_update" class="pageForm required-validate" onsubmit="return iframeCallback(this,navTabAjaxDone);">
		<div class="pageFormContent" layoutH="56" style="width:700px;height:600px;float:left">
			 <dl>
				<dt>姓名：</dt>
				<dd><input type="text" name="c.name" value="${p.realName}" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>性别：</dt>
				<dd><input type="text" name="c.sex" value="${p.sex }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>身份证号：</dt>
				<dd><input type="text"  name="c.idCard" value="${p.idCard }" readonly="readonly"/></dd>
			</dl>
			<dl>
				<dt>出生年月：</dt>
				<dd><input type="text"  name="c.birthday" value="${c.birthday }" class="date" ><a class="inputDateButton" href="javascript:;">选择</a></dd>
			</dl>
			<dl>
				<dt>住址：</dt>
				<dd><input  type="text" name="c.address" value="${c.address }" /></dd>
			</dl>			
			<dl>
				<dt>资格证号：</dt>
				<dd><input type="text"  name="c.cnid" value="${c.cnid }" /></dd>		
			</dl>
			<dl>
				<dt>发证时间：</dt>
				<dd><input type="text"  name="c.issuedate" value="${c.issuedate }" class="date" ><a class="inputDateButton" href="javascript:;">选择</a></dd>
			</dl>
			<dl>
				<dt>证件有效期：</dt>
				<dd><input type="text" name="c.validation" value="${c.validation }"  /></dd>
			</dl>
			<dl>
				<dt>初次发证时间：</dt>
				<dd><input type="text"  name="c.firstdate" value="${c.firstdate}" class="date"/><a class="inputDateButton" href="javascript:;">选择</a></dd>
			</dl>
			<dl>
				<dt>从业资格类别：</dt>
				<dd><input type="text"  name="c.type" value="${c.type }" ></dd>
			</dl>
			
			<dl>
				<dt>发证单位：</dt>
				<dd><input type="text" name="c.issueunit" value="${c.issueunit}"  /></dd>
			</dl>
			<dl>
				<dt>服务单位：</dt>
				<dd><input type="text" name="c.serviceunit" value="${c.serviceunit }"  /></dd>
			</dl>
			<dl>
				<dt>资格证复印件：</dt>
				<dd><input type="file" name="copyurl" class="required images"/></dd>
			</dl>
			</div>
			
			
		<div class="formBar">
			<ul>
				<li>
					<div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div>
				</li>
				<li>
					<div class="button"><div class="buttonContent"><button type="button" class="close">关闭</button></div></div>
				</li>
			</ul>
		</div>
	</form>
</div>
