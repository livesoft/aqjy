
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
	<link rel="stylesheet" href="../ueditor/themes/default/css/ueditor.css">
  	<script>
  		var ue = UE.getEditor('editor');
  	</script>
 
  	
	<div class="pageContent">
	<form method="post" action="/aqjy/safetyeducation/SeminarAction_update?seid=${seminar.seid }" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
		<div class="pageFormContent nowrap" layoutH="97">
		<div style="height: 660px;overflow:auto;">
		
			<dl>
				
				<dt>专题类别：</dt>
				<dd>
					<input type="text" name="se.sort" maxlength="50" value="${seminar.sort }" class="required" />
				</dd>
			</dl>
			<%-- <dl>
				<dt>类别ID：</dt>
				<dd>
					<input type="text" name="se.seid" maxlength="50" value="${seminar.seid }"  class="required" />
				</dd>
			</dl> --%>
			<dl>
				<dt>专题名称：</dt>
				<dd>
					<input type="text" name="se.subjectname"  size="30" value="${seminar.subjectname }" class="required"/></a>
				</dd>
			</dl>
			<%-- <dl>
				<dt>发布时间：</dt>
				<dd>
					<input type="text" name="se.subjecttime" class="date" value="${seminar.subjecttime }" size="30" /><a class="inputDateButton" href="javascript:;"></a>
				</dd>
			</dl> --%>
			
			<dl>
				<dt>备注：</dt>
				<dd>

					  <script type="text/plain" id="editor" name="se.remark" value="${seminar.remark }"  style="width:800px;height: 400px"></script>
				</dd>
			</dl>
			</div>
		<div class="formBar" style="margin-bottom: 0">
			<ul>
				<li ><div class="buttonActive"><div class="buttonContent" ><button type="submit">发布</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
