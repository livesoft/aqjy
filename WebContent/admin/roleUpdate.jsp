<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>
	<link rel="stylesheet" href="../ueditor/themes/default/css/ueditor.css">
  	<script>
  	UE.getEditor('editor', {
  		initialFrameWidth:"100%" //初始化选项
  		,toolbars: [
  		          ['fullscreen', 'source', 'undo', 'redo', 'bold']
  		        ]
  		})
  		  	UE.getEditor('editor1', {
  		initialFrameWidth:"100%" //初始化选项
  		,toolbars: [
  		          ['fullscreen', 'source', 'undo', 'redo', 'bold']
  		        ]
  		})
  	</script>
<h2 class="contentTitle">权限设置</h2>
<div class="pageFormContent" layoutH="60">
<form method="post" action="/aqjy/role/editRole" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone)">
	<!-- 
		private String roleName;//角色名称
	private String description;//角色描述
	private String status;//角色状态
	private String remarks;//角色备注
	 -->
	<fieldset>
		<input type="hidden" name="role.roleid" value="${role.roleid}">
		<legend>部门信息设置</legend>
		<dl class="nowrap">
			<dt>部门名称：</dt>
			<dd><input class="required" style="font-style: bold" value=${role.rolename} name="role.rolename" type="text" /></dd>
		</dl>
		<dl class="nowrap">
			<!-- 为了防止role报错  -->
			<dt>是否可用：</dt>
			<dd><input type="hidden" value="name" name="role.name"/></dd>
			<label><input type="checkbox" name="role.status" <c:if test="${role.status eq \"true\"}"> checked="checked" </c:if> value="true" />勾选启用</label>
		</dl>
		<dl class="nowrap">
			<dt>权限设置：</dt>
			<label><input type="checkbox" name="role.xinxi"  <c:if test="${role.xinxi eq \"true\"}"> checked="checked" </c:if> value="junxun" />信息统计</label>
			<label><input type="checkbox" name="role.yonghu" <c:if test="${role.yonghu eq \"true\"}"> checked="checked" </c:if> value="yonghu" />用户管理</label>
			<label><input type="checkbox" name="role.huiyi"  <c:if test="${role.huiyi eq \"true\"}"> checked="checked" </c:if> value="kaoshi" />会议管理</label>
			<label><input type="checkbox" name="role.tongzhi"  <c:if test="${role.tongzhi eq \"true\"}"> checked="checked" </c:if>  value="zhixun" />通知管理</label>
			<label><input type="checkbox" name="role.jiaoyu" <c:if test="${role.jiaoyu eq \"true\"}"> checked="checked" </c:if> value="zhiyuan" />安全教育</label>
			<label><input type="checkbox" name="role.kaohe"   <c:if test="${role.kaohe eq \"true\"}"> checked="checked" </c:if> value="sushe" />安全考核</label>
			<label><input type="checkbox" name="role.hudong" <c:if test="${role.hudong eq \"true\"}"> checked="checked" </c:if>  value="shoufei" />互动管理</label>
			<label><input type="checkbox" name="role.xitong"  <c:if test="${role.xitong eq \"true\"}"> checked="checked" </c:if>  value="xitong" />系统管理</label>
		</dl>
		<dl class="nowrap">
			<dt>部门描述：</dt>
			<dd>
				<script type="text/plain" id="editor" name="role.desc"  style="width:800px;height: 200px">${role.desc}</script>
			</dd>	
		</dl>
		<dl class="nowrap">
			<dt>部门备注：</dt>
			<dd>
				<script type="text/plain" id="editor1" name="role.remark"  style="width:800px;height: 200px">${role.remark}</script>
			</dd>
		</dl>
	</fieldset>
<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
</div>
