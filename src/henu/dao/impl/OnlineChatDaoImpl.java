package henu.dao.impl;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import henu.bean.OnlineChat;
import henu.dao.OnlineChatDao;
import henu.util.Dbcp;


public class OnlineChatDaoImpl implements OnlineChatDao {
	@Override
	public List<OnlineChat> findListByIdCard(String idCard,String property, String key,
			String order, String sort,int start,int end) {
		// TODO Auto-generated method stub
		List<OnlineChat> list=null;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="SELECT * FROM (SELECT tt.*, ROWNUM ro FROM (select * from onlinechat where idCard='"+idCard+"'  order by isTop desc,replyTime desc,"+order+" "+sort+") tt WHERE ROWNUM <="+end+") WHERE ro > "+start+"";
			list=qr.query(sql, new BeanListHandler<OnlineChat>(OnlineChat.class));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return list;
	}

	@Override
	public int addQuestion(OnlineChat o) {
		// TODO Auto-generated method stub
		int result=0;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="insert into onlinechat(title,askContent,range,askTime,askName,idCard,isDisplay,isTop) values(?,?,?,?,?,?,?,?)";
			Object[] params={o.getTitle(),o.getAskContent(),o.getRange(),o.getAskTime(),o.getAskName(),o.getIdCard(),o.getIsDisplay(),o.getIsTop()};
			result=qr.update(sql, params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int delete(String uid) {
		
		int result=0;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="delete from onlinechat where oid=?";

			result=qr.update(sql, uid);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public int update(int oid, OnlineChat o) {
		// TODO Auto-generated method stub
		int result=0;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="update onlinechat set title=?,askContent=?,range=?,askTime=? where oid=?";
			Object[] params={o.getTitle(),o.getAskContent(),o.getRange(),o.getAskTime(),oid};
			result=qr.update(sql, params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public OnlineChat findByOid(String uid) {
		// TODO Auto-generated method stub
		OnlineChat o=null;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="select * from onlinechat where oid=?";
			o=qr.query(sql, new BeanHandler<OnlineChat>(OnlineChat.class),uid);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return o;
	}

	@Override
	public List<OnlineChat> findAllByProperties(String property, String key,
			String order, String sort,int start,int end) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		List<OnlineChat> list=null;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="SELECT * FROM (SELECT tt.*, ROWNUM ro FROM (select * from onlinechat where  "+property+"='"+key+"'  order by isTop desc,replyTime desc, "+order+" "+sort+") tt WHERE ROWNUM <="+end+") WHERE ro > "+start+"";
			list=qr.query(sql, new BeanListHandler<OnlineChat>(OnlineChat.class));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return list;
	}

	@Override
	public int adminReply(OnlineChat o) {
		int result=0;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="update onlinechat set replyTime=?,replyContent=?,replyPerson=?,isDisplay=?,isTop=? where oid=?";
			Object[] params={o.getReplyTime(),o.getReplyContent(),o.getReplyPerson(),o.getIsDisplay(),o.getIsTop(),o.getOid()};
			result=qr.update(sql, params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int adminUpdate(OnlineChat o) {
		// TODO Auto-generated method stub
		int result=0;
		try {
			QueryRunner qr=new QueryRunner(Dbcp.getDataSource());
			String sql="update onlinechat set replyTime=?,replyContent=?,replyPerson=?,isTop=? where oid=?";
			Object[] params={o.getReplyTime(),o.getReplyContent(),o.getReplyPerson(),o.getIsTop(),o.getOid()};
			result=qr.update(sql, params);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}
}
