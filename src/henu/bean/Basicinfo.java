package henu.bean;
/**
 * @author 田星杰
 * 2015-01-15
 */
public class Basicinfo extends Bean{
	private int basicid;//会议基本内容ID
	private String joinPerson;//参加人员
	private String startTime;//开始时间
	private String endTime;//结束时间
	private String compere;//主持人
	private String attendPerson;//列席人员
	private String address;//地点
	private String theme;//主题
	private String baudit;//状态
	private String remark;//备注
	private String organizeUnit;//组织单位
	private String organizer;//组织人
	
	private String publishTime;//发布时间
	
	public String getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}
	public int getBasicid() {
		return basicid;
	}
	public void setBasicid(int basicid) {
		this.basicid = basicid;
	}
	public String getJoinPerson() {
		return joinPerson;
	}
	public void setJoinPerson(String joinPerson) {
		this.joinPerson = joinPerson;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getCompere() {
		return compere;
	}
	public void setCompere(String compere) {
		this.compere = compere;
	}
	public String getAttendPerson() {
		return attendPerson;
	}
	public void setAttendPerson(String attendPerson) {
		this.attendPerson = attendPerson;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	
	public String getBaudit() {
		return baudit;
	}
	public void setBaudit(String baudit) {
		this.baudit = baudit;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getOrganizeUnit() {
		return organizeUnit;
	}
	public void setOrganizeUnit(String organizeUnit) {
		this.organizeUnit = organizeUnit;
	}
	public String getOrganizer() {
		return organizer;
	}
	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}
	
}
