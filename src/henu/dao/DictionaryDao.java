package henu.dao;

import henu.bean.Dictionary;

import java.util.List;

public interface DictionaryDao {
	public List<Dictionary> getDictionary(String table,String name);
}
