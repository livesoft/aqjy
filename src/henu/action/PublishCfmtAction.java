package henu.action;
/**
 * 主要用于对安全会议通知部分的处理
 * @author 田星杰
 * 2015-01-25
 */
import henu.bean.Basicinfo;
import henu.bean.Mcontent;
import henu.dao.BasicinfoDao;
import henu.dao.McontentDao;
import henu.dao.factory.DaoFactory;
import henu.util.AJAX;
import henu.util.Page;

import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class PublishCfmtAction extends ActionSupport{
	//分页查询
			Page page;
			private int pageNum=1;
			private int numPerPage=10;
			private String orderField="basicid";
			private String orderDirection="asc";
			
			//检索
			private String property="-1";
			private String key="-1";
			
			
			private AJAX ajax;
			//声明一个会议基本信息类的实例变量用于存放查询到的记录
			private Basicinfo bf;
			private String url="/admin/ajax.jsp";
			//存放会议基本信息表的主码ID
			private String uid;
			//声明一个List<Basicinfo>类型的变量用于存放查询到的记录的存放
			private List<Basicinfo> list;
			public Page getPage() {
				return page;
			}
			public void setPage(Page page) {
				this.page = page;
			}
			public int getPageNum() {
				return pageNum;
			}
			public void setPageNum(int pageNum) {
				this.pageNum = pageNum;
			}
			public int getNumPerPage() {
				return numPerPage;
			}
			public void setNumPerPage(int numPerPage) {
				this.numPerPage = numPerPage;
			}
			public String getOrderField() {
				return orderField;
			}
			public void setOrderField(String orderField) {
				this.orderField = orderField;
			}
			public String getOrderDirection() {
				return orderDirection;
			}
			public void setOrderDirection(String orderDirection) {
				this.orderDirection = orderDirection;
			}
			public String getProperty() {
				return property;
			}
			public void setProperty(String property) {
				this.property = property;
			}
			public String getKey() {
				return key;
			}
			public void setKey(String key) {
				this.key = key;
			}
			public AJAX getAjax() {
				return ajax;
			}
			public void setAjax(AJAX ajax) {
				this.ajax = ajax;
			}
			
			public Basicinfo getBf() {
				return bf;
			}
			public void setBf(Basicinfo bf) {
				this.bf = bf;
			}
			public String getUrl() {
				return url;
			}
			public void setUrl(String url) {
				this.url = url;
			}
			public String getUid() {
				return uid;
			}
			public void setUid(String uid) {
				this.uid = uid;
			}
			
			public List<Basicinfo> getList() {
				return list;
			}
			public void setList(List<Basicinfo> list) {
				this.list = list;
			}
			//列出所有基本会议信息
			public String findAll(){
				page = new Page();
				if(orderField.equals("")||orderDirection.equals("")){
					orderField="basicid";
					orderDirection="asc";
				}
				if(property.equals("")||key.equals("")){
					property="-1";
					key="-1";
				}
				page.setProperties("BASICINFO",pageNum,numPerPage,property,key);
				
				list = DaoFactory.getBasicinfoDaoInstance().findByProperty(property, key,orderField, orderDirection, page.getStart(), page.getEnd());
				
				url="/admin/basicinfoManage.jsp";
				
				
				return SUCCESS;
			}
			//添加会议通知
			public String add(){
				//自动获取当前系统时间作为会议通知的发布时间
				bf.setPublishTime(new Date().toLocaleString());
				int result = DaoFactory.getBasicinfoDaoInstance().save(bf);
				ajax=new AJAX();
				if(result!=0){
					ajax.setStatusCode("200");
					ajax.setMessage("添加成功");
					ajax.setCallbackType("closeCurrent");
					ajax.setNavTabId("mcontentInf");
				}else{
					ajax.setStatusCode("300");
					ajax.setMessage("添加失败，请重新填写");
					ajax.setCallbackType("closeCurrent");
					ajax.setNavTabId("mcontentInf");
				}
				url="/admin/ajax.jsp";
				return SUCCESS;
			}
			//删除会议基本信息
			public String del(){
				int seid = Integer.parseInt(uid);
				//int seid=bf.getBasicid();
				int result = DaoFactory.getBasicinfoDaoInstance().delete(seid);
				ajax=new AJAX();
				if(result!=0){
					ajax.setStatusCode("200");
					ajax.setMessage("删除成功");
					ajax.setNavTabId("mcontentInf");
					ajax.setCallbackType("");
					
				}else{
					ajax.setStatusCode("300");
					ajax.setMessage("删除失败");
					ajax.setCallbackType("");
				}
				url="/admin/ajax.jsp";
				return SUCCESS; 
			}
			//更新会议基本信息
			public String update(){
				BasicinfoDao dao = DaoFactory.getBasicinfoDaoInstance();
				ajax=new AJAX();
				//自动获取当前系统时间作为更新后会议通知的发布时间
				bf.setPublishTime(new Date().toLocaleString());
				int result = dao.update(bf.getBasicid(),bf);
				//System.out.println(result);
				if(result!=0){
					ajax.setStatusCode("200");
					ajax.setMessage("更新成功");
					ajax.setNavTabId("mcontentInf");
					ajax.setCallbackType("closeCurrent");
				}else{
					ajax.setStatusCode("300");
					ajax.setMessage("更新失败");
					ajax.setCallbackType("");
				}
				return SUCCESS;
			}
			//用于得到用户选中的记录的信息，并存放在session范围内的basicinfo中，通过URL转到修改页面
			public String find(){
				BasicinfoDao dao = DaoFactory.getBasicinfoDaoInstance();
				int seid = Integer.parseInt(uid);
				Basicinfo s =dao.findAllBybasicId(seid);
				ServletActionContext.getRequest().getSession().setAttribute("basicinfo", s);
				url="/admin/basicinfoUpdate.jsp";
				return SUCCESS;
			}
			//把会议通知和会议内容联系起来，完成用户转向会议内容填写的页面，并且判断用户是否已经编写过此会议通知的具体内容
			public String findMettingID(){
				BasicinfoDao dao = DaoFactory.getBasicinfoDaoInstance();
				int seid = Integer.parseInt(uid);
				Basicinfo s =dao.findAllBybasicId(seid);
				ServletActionContext.getRequest().getSession().setAttribute("mc", s);
				//获取基本会议信息表的主键，赋值给会议内容的外键，用于查询判断用户是否已经编写过此会议通知的具体内容
				int m=s.getBasicid();
				McontentDao da = DaoFactory.getMcontentDaoInstance();
				Mcontent li=da.findMcontent(m);
				//判断用户是否已经编写过此会议通知的具体内容
				if(li!=null){
					url="/admin/mcontentAddError.html";
				}else{
					url="/admin/mcontentAdd.jsp";
				}
				return SUCCESS;
			}	
}
